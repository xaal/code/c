/* Minimal code to handle messages from the SensFloor® SE3-P transciever
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SENSFLOOR
#define SENSFLOOR

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <sys/queue.h>


#define SF_MESSAGE_START	0xFD	/* Start frame delimiter */

/* Message data structure
   Note: Two bytes values are stored in big endian
 */
typedef struct {
  uint16_t gpid;		/* Group IDentification (check endianness) */
  union {
    uint16_t modidr;		/* Module Receive Id (check endianness) */
    struct {
      uint8_t x;		/*   X coordinate */
      uint8_t y;		/*   Y coordinate */
    } modids;			/* Module Send Id */
  };
  uint8_t gp1;			/* _special use_ */
  struct {
    uint8_t ft:4;		/*   Function Type */
    uint8_t id:4;		/*   mat ID */
  } ftid;			/* _special use_ */
  union {
    uint8_t byte;		/*   in raw form */
    struct {
      uint8_t sc:1;		/*   sensor State Changed */
      uint8_t mm:1;		/*   Master Message */
      uint8_t sr:1;		/*   Status Request */
      uint8_t sa:1;		/*   Status Answer */
      uint8_t sd:1;		/*   Signed Data values */
      uint8_t cc:1;		/*   Change device Configuration */
      uint8_t rc:1;		/*   Re-Calibration */
      uint8_t tr:1;		/*   message from the TRansceiver */
    };
  } def;			/* message DEFinition */
  uint8_t gp2;			/* _special use_ */
  uint8_t data[8];		/* Data - Sensor Capacity for each mat */
} sf_message_t;


/* Functions Types definitions */
#define SF_FT_OFF	0
#define SF_FT_PRESENCE	1
#define SF_FT_FALL	2


/* Typical values for messages DEFinitions */

/* messages send by one module */
#define SF_DEF_AU	0x00	/* Automatic messages for Unsigned data */
#define SF_DEF_AS	0x10	/* Automatic messages for Signed data */
#define SF_DEF_EU	0x01	/* Event message for Unsigned data */
#define SF_DEF_ES	0x11	/* Event message for Signed data */
#define SF_DEF_SU	0x08	/* Status message for Unsigned data */
#define SF_DEF_SS	0x18	/* Status message for Signed data */

/* messages send towards a module */
#define SF_DEF_MA	0x02	/* MAster message */
#define SF_DEF_RQ	0x04	/* ReQuest message */
#define SF_DEF_CC	0x20	/* module's Configuration Change */
#define SF_DEF_RT	0x40	/* Re-calibration Trigger */

/* Information blocks definition for status-request messages */
#define SF_PARA_RC	0x00	/* Relative Capacity of the sensor plates */
#define SF_PARA_RCL	0x01	/* Relative Capacity Level */
#define SF_PARA_ST	0x02	/* sensors STates */
#define SF_PARA_CT	0x03	/* CounTers */
#define SF_PARA_ACF	0x0C	/* Absolute Capacity of the First 4 sensors */
#define SF_PARA_ACL	0x0D	/* Absolute Capacity of the Last 4 sensors */


/* Open a communication channel on an USB/serial port
   Input: serial device to open (e.g., /dev/ttyUSB0
   Output: file descriptor
 */
int sf_open_serial(const char *device);


/* Open a communication channel on an IP TCP server
   Input: server hostname and TCP port (e.g., sensfloor.homnet 5000)
   Output: file descriptor
 */
int sf_open_tcp(const char *host, const char *port);


/* Open a communication channel on a file
   Input: file name
   Output: file descriptor
 */
int sf_open_file(const char *name);


/* Wait for next message
   Input: file descriptor
   Output: pointer to internal-allocated buffer with the message
      or NULL pointer in case of invalid/trunkated message
 */
sf_message_t *sf_get_message(int fd);


/* Send a message
   Input: file descriptor, pointer to the message
 */
void sf_put_message(int fd, const sf_message_t * msg);


/* Dump message in hexadecimal
   Input: FILE stream and message
 */
void sf_dump_message(FILE * stream, sf_message_t * msg);


/* Pretty print a message
   Input: FILE stream and message
 */
void sf_print_message(FILE * stream, sf_message_t * msg);


/* List of floors
 */
typedef LIST_HEAD(sf_floor_head, sf_floor_entry) sf_floors_t;

/* A "floor" data structure
 */
typedef struct sf_floor_entry {
  unsigned gpid;
  unsigned x_max, y_max;
  short *mats;     /* capacitance; expected to be of size [x_max*y_max*8] */
  LIST_ENTRY(sf_floor_entry) entries;
} sf_floor_t;


/* Init a floor data structure
   Input: GPID, width, height
   Output: pointer to the created data structure
 */
sf_floor_t *sf_init_floor(const unsigned gpid, unsigned x_max, unsigned y_max);


/* Resize a floor
   Input: pointer to the floor structure, new width, new height
 */
void sf_resize_floor(sf_floor_t *floor, unsigned x_max, unsigned y_max);


/* Free a floor
   Input: pointer to a floor
 */
void sf_free_floor(sf_floor_t *floor);


/* Retreive a floor in the list
   Input: floors list, gpid
   Output: pointer to the finded floor, or NULL
 */
sf_floor_t *sf_get_floor(const sf_floors_t *floors, unsigned gpid);


/* Wait for a SensFloor message and update the "floors" data structure with received data
   Input: file descriptor, pointer to a list of floors
   Output: updated floors
 */
void sf_update_floors(int fd, sf_floors_t *floors);



/***
 * Chapter: Fall Detector
 ***/
 
/* List of falls
 */
typedef LIST_HEAD(sf_fall_head, sf_fall_entry) sf_falls_t;

/* Data structure for a fall
 */
typedef struct sf_fall_entry {
  unsigned gpid;
  double x, y;
  time_t date;
  bool confirmed;
  LIST_ENTRY(sf_fall_entry) entries;
} sf_fall_t;


/* List of neighbors
 */
typedef LIST_HEAD(sf_neigh_head, sf_neigh_entry) sf_neighs_t;
typedef struct sf_neigh_entry {
  unsigned x, y, m;
  short c;
  LIST_ENTRY(sf_neigh_entry) entries;
} sf_neigh_t;


/* Add a mat in the list of neighbors
   Input: list of neighbors, a neighbor (x,y,m,c)
   Output: tell if this is a 'new' one or not
 */
bool sf_add_neigh(sf_neighs_t *neighs, unsigned x, unsigned y, unsigned m, short c);


/* Build the cluster of contiguous mats with a threshold capacitance
   Input: a floor, a starting mat (x,y,m), the threshold
   Output: the list of neighors
 */
sf_neighs_t *sf_cluster(const sf_floor_t *floor, unsigned x, unsigned y, unsigned m, short threshold);


/* Count neighbors with a capacitance higher than the threshold
   Input: list of neighbors, threshold
   Output: the number
 */
unsigned sf_cluster_size(sf_neighs_t *neighs, short threshold);


/* Free a list of neighbors
 * Note: free the list data-structure itself
 */
void sf_free_neighs(sf_neighs_t *neighs);


/* Retreive a fall in the list given a position and a threshold delta
   Input: a list of falls, a position (x,y), 
      and the minimal squared delta between positions to claim that it is the same or not
   Output: pointer to the fall in the list
 */
sf_fall_t *sf_find_fall(const sf_falls_t *falls, unsigned gpid, double x, double y, double delta2);


/* Compute the center of a cluster of neighbors and update the list of falls with it
   Input: list of falls to update, a cluster of neighors
   Output: tell if this is a 'new' fall or not
 */
bool sf_update_fall(sf_falls_t *falls, const sf_neighs_t *neighs, unsigned gpid);


/* Empty a list of falls
 * Note: do not free the list data-structure itself
 */
void sf_empty_falls(sf_falls_t *falls);


/* Remove the 'confirmed' mark on falls
   Input: list of falls
 */
void sf_refute_falls(sf_falls_t *falls);


/* Withdraw unconfirmed falls
   Input: list of falls
   Output: tell if some falls have been removed from the list
 */
bool sf_withdraw_falls(sf_falls_t *falls);



/* Look at floors state to detect falls
   Input: list of floors, list of falls to update
   Output: tell if there are new falls or not
 */
bool sf_detect_fall(const sf_floors_t *floors, sf_falls_t *falls);

#endif
