#ifndef ASCIIART
#define ASCIIART


/* VT100 tricks */
#define RESET_SCREEN	"\033c"		/* reset terminal settings */
#define ERASE_SCREEN	"\033[2J"	/* erases screen, go to (1,1) */
#define ERASE_REST	"\033[0J"	/* erases rest of the screen */
#define MOVE_CURSOR	"\033[%d;%dH"	/* move cursor to .. .. */
#define HIDE_CURSOR	"\033[?25l"
#define SHOW_CURSOR	"\033[?25h"	


/* utf8 graphics */
#define BB	"█"	/* black block */
#define WB	" "	/* white block */
#define BS	"■"	/* black square */
#define WS	"□"	/* white square */
#define BLR	"◢"	/* black lower right triangle */
#define WLR	"◿"	/* white lower right triangle */
#define BLL	"◣"	/* black lower left triangle */
#define WLL	"◺"	/* white lower left triangle */
#define BUL	"◤"	/* black upper left triangle */
#define WUL	"◸"	/* white upper left triangle */
#define BUR	"◥"	/* black upper right triangle */
#define WUR	"◹"	/* white upper right triangle */
#define FE	"↧"	/* symbol for a fall detection event */
#define PE	"☺"	/* symbol for a presence detection event */
#define HB	"─"	/* horizontal border */
#define VB	"│"	/* vertical border */
#define CDR	"╭"	/* rounder corner down and right */
#define CDL	"╮"	/* rounder corner down and left */
#define CUR	"╰"	/* rounder corner up and right */
#define CUL	"╯"	/* rounder corner up and left */


/* Example of floor
    ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ 
    ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ 
    ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ↧↧ ↧↧ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ 
    ◺◹ ◸◿ ◺◹ ◸◿ ◣◥ ◸◿ ◺◹ ◸↧ ↧↧ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◸◿ ◺◹
    ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◤◿ ◺◹ ◸◿ ◺◹ ◸◿ ◣◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ 
    ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ☺☺ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◺◹ ◸◿ ◸◿ ◺◹

  Reordored tiles 
   ╭───┬───╮
   │6╲7│0╱1│  6 7 0 1 ⎫
   ├───╋───┤          ⎬ ⇒ 01234567: "◸" "◿" "◹" "◺" "◿" "◸" "◺" "◹"
   │5╱4│3╲2│  5 4 3 2 ⎭
   ╰───┴───╯
*/
   
const char *white_tiles[] = { WUL, WLR, WUR, WLL, WLR, WUL, WLL, WUR };
const char *black_tiles[] = { BUL, BLR, BUR, BLL, BLR, BUL, BLL, BUR };

#endif
