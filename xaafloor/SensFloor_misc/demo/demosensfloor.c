/* test libsensfloor
 * A minimal library to handle messages from the SensFloor® SE3-P tranciever
 *
 * Note: This library is dedicated to messages from floor to PC
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libsensfloor.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>

FILE *output;

void bye() {
  sf_restore_terminal(output);
}

void bye_handler(int s) {
  exit(EXIT_SUCCESS);
}


int main(int argc, char *argv[]) {
  int fd, opt;
  bool arg_error = false, log_only = false;
  sf_message_t *msg;
  char *host=NULL, *port=NULL, *device=NULL, *name=NULL;
  unsigned gpid = 0, x_max = 2, y_max = 2;
  sf_floor_t *floor;
  short threshold = 10;

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "d:s:p:f:g:x:y:t:hl")) != -1) {
    switch (opt) {
      case 'd':
	device = optarg;
	break;
      case 's':
	host = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'f':
	name = optarg;
	break;
      case 'g':
	gpid = strtol(optarg, NULL, 0);
	break;
      case 'x':
	x_max = strtol(optarg, NULL, 0);
	break;
      case 'y':
	y_max = strtol(optarg, NULL, 0);
	break;
      case 't':
	threshold = atoi(optarg);
	break;
      case 'l':
	log_only = true;
	break;
      default:
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }

  if ( !( name || device || (host && port) ) || arg_error) {
    fprintf(stderr, "Usage: %s [-d <device> | -s <host> -p <port> |-f <file>]\n"
		    "\t\t\t[-g <gpid>] [-x <x_max>] [-y <y_max>] [-t <threshold>] [-l]\n"
	     "  -d <device>\tSerial device of the SE3-P tranciever (e.g., /dev/ttyUSB0)\n"
	     "  -s <host>\tIP address of the SE10-Raspbery Pi server\n"
	     "  -p <port>\tTCP port of the SE10-Raspbery Pi (e.g., 5000)\n"
	     "  -f <file>\tFile name with raw data, '-' for stdin\n"
	     "  -g <gpid>\tGroup ID of the floor to filter on (or 0 for any, default)\n"
	     "  -x <x_max>\tWith of the floor (2 by default, resized if needed)\n"
	     "  -y <y_max>\tHeight of the floor (2 by default, resized if needed)\n"
	     "  -t <threshold>\tActivation threshold (10 by default)\n"
	     "  -l\t\tLog only, no graphics\n"
	     "  -h\t\tThis help\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  if (device)
    fd = sf_open_serial(device);
  else if (host && port)
    fd = sf_open_tcp(host, port);
  else if (name)
    fd = sf_open_file(name);
  else {
    fprintf(stderr, "No data channel!\n");
    exit(EXIT_FAILURE);
  }

  output = stdout;

  if (!log_only) {
    floor = sf_init_floor(gpid, x_max, y_max);

    atexit(bye);
    signal(SIGHUP,  bye_handler);
    signal(SIGINT,  bye_handler);
    signal(SIGQUIT, bye_handler);
    signal(SIGTERM, bye_handler);

    sf_draw_floor(output, floor, threshold);

    if (!name) sf_put_message(fd, &sf_discovermsg);
  }

  do {
    msg = sf_get_message(fd);
    if (!log_only) {
      sf_refresh_floor(output, floor, msg, threshold);
      fprintf(output, "\033[%d;1H", floor->y_max*2+3);
    }
    sf_dump_message(output, msg);
    sf_print_message(output, msg);
    if (log_only)
      fprintf(output, "\n");
  } while (1);
}
