/* libsensfloor
 * A minimal library to handle messages from the SensFloor® SE3-P tranciever
 *
 * Note: This library is dedicated to messages from floor to PC
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libsensfloor.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netdb.h>
#include <time.h>
#include <fcntl.h>
#include <endian.h>

#include <asciiart.h>

/* Macros for min/max.  */
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))



int sf_open_serial(const char *device) {
   const speed_t speed = B115200;
   struct termios tty;
   int fd;

   fd = open(device, O_RDWR | O_NOCTTY | O_SYNC);
   if (fd < 0) {
      fprintf(stderr, "open(%s): %s\n", device, strerror(errno));
      exit(EXIT_FAILURE);
   }

   /*baudrate 115200, 8 bits, no parity, 1 stop bit */
   if (tcgetattr(fd, &tty) < 0) {
      fprintf(stderr, "tcgetattr(%s): %s\n", device, strerror(errno));
      exit(EXIT_FAILURE);
   }

   cfsetospeed(&tty, speed);
   cfsetispeed(&tty, speed);

   tty.c_cflag |= (CLOCAL | CREAD);	/* ignore modem controls */
   tty.c_cflag &= ~CSIZE;
   tty.c_cflag |= CS8;			/* 8-bit characters */
   tty.c_cflag &= ~PARENB;		/* no parity bit */
   tty.c_cflag &= ~CSTOPB;		/* only need 1 stop bit */
   tty.c_cflag &= ~CRTSCTS;		/* no hardware flowcontrol */

   /* setup for non-canonical mode */
   tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
   tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
   tty.c_oflag &= ~OPOST;

   /* fetch bytes as they become available */
   tty.c_cc[VMIN] = 1;
   tty.c_cc[VTIME] = 1;

   if (tcsetattr(fd, TCSANOW, &tty) != 0) {
      fprintf(stderr, "tcsetattr(%s): %s\n", device, strerror(errno));
      exit(EXIT_FAILURE);
   }
   return fd;
}


int sf_open_tcp(const char *host, const char *port) {
  int s, fd;
  struct addrinfo hints;
  struct addrinfo *result, *rp;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;
  hints.ai_protocol = 0;

  s = getaddrinfo(host, port, &hints, &result);
  if (s != 0) {
    fprintf(stderr, "getaddrinfo(%s,%s): %s\n", host, port, gai_strerror(s));
    exit(EXIT_FAILURE);
  }

  for (rp = result; rp != NULL; rp = rp->ai_next) {
    fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (fd == -1)
      continue;
    if (connect(fd, rp->ai_addr, rp->ai_addrlen) != -1)
      break;
    close(fd);
  }
  if (rp == NULL) {
    perror("connect");
    exit(EXIT_FAILURE);
  }
  freeaddrinfo(result);
  return fd;
}


int sf_open_file(const char *name) {
  int fd;

  if ( strcmp("-", name) == 0 )
    return STDIN_FILENO;

  fd = open(name, O_RDONLY);
  if (fd == -1) {
    fprintf(stderr, "open(%s): %s\n", name, strerror(errno));
    exit(EXIT_FAILURE);
  }
  return fd;
}


sf_message_t *sf_get_message(int fd) {
  static sf_message_t msg;
  uint8_t c;
  int n, i;

  do {
    n = read(fd, &c, 1);
    if (n == -1) {
      perror("read()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "connexion closed\n");
      exit(EXIT_SUCCESS);
    }
  } while ( c != SF_MESSAGE_START );

  i = 0;
  do {
    n = read(fd, (uint8_t*)(&msg)+i, sizeof(msg)-i);
    if (n == -1) {
      perror("read()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "connexion closed\n");
      exit(EXIT_SUCCESS);
    }
    i += n;
  } while ( i < sizeof(msg) );
  return &msg;
}


void sf_put_message(int fd, const sf_message_t *msg) {
  uint8_t c = SF_MESSAGE_START;
  unsigned n, i;

  n = write(fd, &c, 1);
  if (n == -1) {
    perror("write()");
    exit(EXIT_FAILURE);
  } else if (n == 0) {
    fprintf(stderr, "connexion closed\n");
    exit(EXIT_SUCCESS);
  }

  i = 0;
  do {
    n = write(fd, (uint8_t*)(&msg)+i, sizeof(msg)-i);
    if (n == -1) {
      perror("write()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "connexion closed\n");
      exit(EXIT_SUCCESS);
    }
    i += n;
  } while ( i < sizeof(msg) );
}


void sf_dump_message(FILE *stream, sf_message_t *msg) {
  time_t now = time(NULL);
  fprintf(stream, "%.19s\t%04x %04x %02x %02x "
		  "%1d%1d%1d%1d%1d%1d%1d%1d %02x\t"
		  "%02x %02x %02x %02x %02x %02x %02x %02x\n",
	  ctime(&now),
	  be16toh(msg->gpid), be16toh(msg->modidr), msg->gp1, *(char *)(&msg->ftid),
	  msg->def.tr, msg->def.rc, msg->def.cc, msg->def.sd,
	  msg->def.sa, msg->def.sr, msg->def.mm,msg->def.sc, msg->gp2,
	  msg->data[0], msg->data[1], msg->data[2], msg->data[3],
	  msg->data[4], msg->data[5], msg->data[6], msg->data[7]);
}




void sf_print_message(FILE *stream, sf_message_t *msg) {
  fprintf(stream, "\tFloor:%04x  X:%u  Y:%u\n",
	  be16toh(msg->gpid), msg->modids.x, msg->modids.y);
  switch (msg->ftid.ft) {
    case 1:
      fprintf(stream, "\tPresence detected on mat %d", msg->ftid.id);
      break;
    case 2:
      fprintf(stream, "\tFall detected on mat %d", msg->ftid.id);
      break;
    default: ;
  }
  fprintf(stream, "\t");
  if (msg->def.sc) fprintf(stream, "Sensor state changed, ");
  if (msg->def.mm) fprintf(stream, "Master message, ");
  if (msg->def.sr) fprintf(stream, "Status request, ");
  if (msg->def.sa) fprintf(stream, "Status answer, ");
  if (msg->def.sd) fprintf(stream, "Signed data values, ");
  if (msg->def.cc) fprintf(stream, "Change device configuration, ");
  if (msg->def.rc) fprintf(stream, "Recalibration, ");
  if (msg->def.tr) fprintf(stream, "Message from the tranceiver, ");
  fprintf(stream, "\b\b   \n");
  if (msg->def.sc) {
    if (msg->def.sd)
      fprintf(stream, "\tMats: %+04d %+04d %+04d %+04d %+04d %+04d %+04d %+04d\n",
	  msg->data[0]-128, msg->data[1]-128, msg->data[2]-128, msg->data[3]-128,
	  msg->data[4]-128, msg->data[5]-128, msg->data[6]-128, msg->data[7]-128);
    else
      fprintf(stream, "\tMats: %04u %04u %04u %04u %04u %04u %04u %04u\n",
	  msg->data[0], msg->data[1], msg->data[2], msg->data[3],
	  msg->data[4], msg->data[5], msg->data[6], msg->data[7]);
  }
}



sf_floor_t *sf_init_floor(const unsigned gpid, unsigned x_max, unsigned y_max) {
  unsigned x, y;

  sf_floor_t *floor = (sf_floor_t *)malloc(sizeof(sf_floor_t));
  floor->gpid = gpid;
  floor->x_max = x_max;
  floor->y_max = y_max;

  floor->mats = (sf_mat_t ***) malloc(x_max * sizeof(sf_mat_t**));
  for (x = 0; x < x_max; x++) {
    floor->mats[x] = (sf_mat_t **) malloc(y_max * sizeof(sf_mat_t*));
    for (y = 0; y < y_max; y++)
      floor->mats[x][y] = (sf_mat_t *) calloc(8, sizeof(sf_mat_t));
  }

  return floor;
}


static void free_mats(sf_mat_t ***mats, unsigned x_max, unsigned y_max) {
  unsigned x, y;

  for (x = 0; x < x_max; x++) {
    for (y = 0; y < y_max; y++)
      free(mats[x][y]);
    free(mats[x]);
  }
  free(mats);
}


void sf_resize_floor(sf_floor_t *floor, unsigned x_max, unsigned y_max) {
  sf_mat_t ***old_mats, ***new_mats;
  unsigned x, y, w, h, m;

  old_mats = floor->mats;
  new_mats = (sf_mat_t ***) malloc(x_max * sizeof(sf_mat_t**));
  for (x = 0; x < x_max; x++) {
    new_mats[x] = (sf_mat_t **) malloc(y_max * sizeof(sf_mat_t*));
    for (y = 0; y < y_max; y++)
      new_mats[x][y] = (sf_mat_t *) calloc(8, sizeof(sf_mat_t));
  }

  w = MIN( floor->x_max, x_max);
  h = MIN( floor->y_max, y_max);
  for (x = 0; x < w; x++)
    for (y = 0; y < h; y++)
      for (m = 0; m < 8; m++)
	new_mats[x][y][m] = old_mats[x][y][m];
  floor->mats = new_mats;
  free_mats(old_mats, floor->x_max, floor->y_max);
  floor->x_max = x_max;
  floor->y_max = y_max;
}


void sf_free_floor(sf_floor_t *floor) {
  free_mats(floor->mats, floor->x_max, floor->y_max);
  free(floor);
}


void sf_draw_mat(FILE *stream, sf_mat_t mat, unsigned idx, short threshold) {
  switch( mat.ft ) {
    case SF_FT_OFF:
      if (mat.cp > threshold)
	fprintf(stream, black_tiles[idx]);
      else
	fprintf(stream, white_tiles[idx]);
      break;
    case SF_FT_PRESENCE:
      fprintf(stream, PE);
      break;
    case SF_FT_FALL:
      fprintf(stream, FE);
      break;
    default:
      fprintf(stream, "%u", mat.ft);
  }
}


void sf_draw_device(FILE *stream, sf_floor_t *floor, unsigned x, unsigned y, short threshold) {
  /* note: Y-axis is inverted */
  fprintf(stream, MOVE_CURSOR, (floor->y_max-1-y)*2+2, x*6+3);

  /* up line: mats 6 7 0 1 */
  sf_draw_mat(stream, floor->mats[x][y][6], 6, threshold);
  sf_draw_mat(stream, floor->mats[x][y][7], 7, threshold);
  fprintf(stream, " ");
  sf_draw_mat(stream, floor->mats[x][y][0], 0, threshold);
  sf_draw_mat(stream, floor->mats[x][y][1], 1, threshold);
  fprintf(stream, " ");

  /* down line: mats 5 4 3 2 */
  fprintf(stream, MOVE_CURSOR, (floor->y_max-1-y)*2+3, x*6+3);
  sf_draw_mat(stream, floor->mats[x][y][5], 5, threshold);
  sf_draw_mat(stream, floor->mats[x][y][4], 4, threshold);
  fprintf(stream, " ");
  sf_draw_mat(stream, floor->mats[x][y][3], 3, threshold);
  sf_draw_mat(stream, floor->mats[x][y][2], 2, threshold);
  fprintf(stream, " ");
}


void sf_draw_floor(FILE *stream, sf_floor_t *floor, short threshold) {
  unsigned x, y;

  fprintf(stream, RESET_SCREEN HIDE_CURSOR);

  /* draw a frame */
  fprintf(stream, CDR);
  for (x = 0; x <= (floor->x_max)*6; x++)
    fprintf(stream, HB);
  fprintf(stream, CDL);
  for (y = 0; y < (floor->y_max)*2; y++)
    fprintf(stream, MOVE_CURSOR VB MOVE_CURSOR VB, y+2, 1, y+2, floor->x_max*6+3);
  fprintf(stream, MOVE_CURSOR CUR, floor->y_max*2+2, 1);
  for (x = 0; x <= (floor->x_max)*6; x++)
    fprintf(stream, HB);
  fprintf(stream, CUL);

  /* draw mats */
  for (x = 0; x < floor->x_max; x++)
    for (y = 0; y < floor->y_max; y++)
      sf_draw_device(stream, floor, x, y, threshold);
}


void sf_restore_terminal(FILE *stream) {
  fprintf(stream, SHOW_CURSOR "\n");
  fflush(stream);
}


void sf_refresh_floor(FILE *stream, sf_floor_t *floor, sf_message_t *msg, short threshold) {
  unsigned m;

  /* Filter floor group_ID and event messages */
  if ( (    (floor->gpid == 0)
	 || (floor->gpid && (be16toh(msg->gpid) == floor->gpid)) )
    && (    msg->def.byte == SF_DEF_AU || msg->def.byte == SF_DEF_AS
	 || msg->def.byte == SF_DEF_EU || msg->def.byte == SF_DEF_ES
	 || (    (msg->def.byte == SF_DEF_SU || msg->def.byte == SF_DEF_SS)
	      && (msg->gp2 == SF_PARA_RC || msg->gp2 == SF_PARA_RCL) )
       ) ) {

    /* Check if floor has to be resized */
    if ( (msg->modids.x > floor->x_max) || (msg->modids.y > floor->y_max) ) {
      sf_resize_floor(floor, MAX(msg->modids.x, floor->x_max),
			     MAX(msg->modids.y, floor->y_max) );
      sf_draw_floor(stream, floor, threshold);
    }

    /* update the Function Type associated to mats of the device */
    floor->mats[msg->modids.x-1][msg->modids.y-1][msg->ftid.id].ft = msg->ftid.ft;

    /* record new capacitance of the 8 mats of the device */
    for (m = 0; m < 8; m++) {
      if (msg->def.sd)
	floor->mats[msg->modids.x-1][msg->modids.y-1][m].cp = msg->data[m] - 128;
      else
	floor->mats[msg->modids.x-1][msg->modids.y-1][m].cp = msg->data[m];

      sf_draw_device(stream, floor, msg->modids.x-1, msg->modids.y-1, threshold);
    }
  }
}
