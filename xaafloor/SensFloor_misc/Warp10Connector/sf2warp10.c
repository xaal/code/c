/* sf2warp10 - SensFloor to Warp10 connector
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * This file is based on libwebsockets-test-client v2.0.3
 * by Andy Green <andy@warmcat.com>
 * under the Creative Commons CC0 1.0 Universal Public Domain Dedication
 */

#include <lws_config.h>

#define _GNU_SOURCE	// vasprintf()
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <signal.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdarg.h>
#include <sys/queue.h>

#ifdef _WIN32
#  define random rand
#  include "gettimeofday.h"
#else
#  include <syslog.h>
#  include <sys/time.h>
#  include <unistd.h>
#endif

#define _DEBUG

#include <libwebsockets.h>

#include "sensfloor.h"


static int deny_deflate;
static struct lws *wsi_warp10;
static volatile bool force_exit;

#if defined(LWS_USE_POLARSSL)
#else
#if defined(LWS_USE_MBEDTLS)
#else
#if defined(LWS_OPENSSL_SUPPORT) && defined(LWS_HAVE_SSL_CTX_set1_param)
char crl_path[1024] = "";
#endif
#endif
#endif

int max_poll_elements;
struct lws_pollfd *pollfds;
int *fd_lookup;
int count_pollfds;


enum demo_protocols {
  PROTOCOL_WARP10,
  DEMO_PROTOCOL_COUNT		/* always last */
};


/*
 * warp10 protocol
 *
 * since this also happens to be protocols[0], some callbacks that are not
 * bound to a specific protocol also turn up here.
 */

enum warp10_ws_state {
  WARP10_WS_ONERROR,
  WARP10_WS_AUTH,
  WARP10_WS_UPDATE
};


/* FIFO of websocket text messages (strings) */
typedef STAILQ_HEAD(wsmshead, wsmentry) wsms_t;
typedef struct wsmentry {
  char *txt;
  size_t sz;
  STAILQ_ENTRY(wsmentry) entries;
} wsm_t;

/* data to keep in mind across ws callbacks */
struct per_session_data__warp10 {
  enum warp10_ws_state state;
  wsms_t wsms;
};


bool warp10_onerror(struct lws *wsi) {
  const char *push = "ONERROR MESSAGE";
  char buf[LWS_PRE + strlen(push) + 1];
  char *p = &buf[LWS_PRE];
  int n, m;

  n = sprintf(p, push);
  m = lws_write(wsi, (unsigned char *)p, n, LWS_WRITE_TEXT);
  return (m >= n);
}


bool warp10_auth(struct lws *wsi) {
  const char *push = "TOKEN %s";
  char *token = lws_get_protocol(wsi)->user;
  char buf[LWS_PRE + strlen(push) + strlen(token)];
  char *p = &buf[LWS_PRE];
  int n, m;

  n = sprintf(p, push, token);
  m = lws_write(wsi, (unsigned char *)p, n, LWS_WRITE_TEXT);
  return (m >= n);
}


/* Pop a message from the FIFO and send it to the warp10 server */
bool warp10_update(struct lws *wsi, wsms_t *wsms) {
  wsm_t *wsm = STAILQ_FIRST(wsms);
  unsigned char *buf, *p;
  int m;
  
  if ( wsm == NULL )
    return true;

lwsl_info("Warp10: '%s'\n", wsm->txt);
  buf = malloc(LWS_PRE + wsm->sz);
  p = &buf[LWS_PRE];
  memcpy(p, wsm->txt, wsm->sz);
  
  m = lws_write(wsi, p, wsm->sz, LWS_WRITE_TEXT);

  STAILQ_REMOVE_HEAD(wsms, entries);
  free(wsm->txt);
  free(wsm);
  if ( !STAILQ_EMPTY(wsms) )
    lws_callback_on_writable(wsi);

  return (m >= wsm->sz);
}


/* Helper function to push text on the FIFO 
 * Input: our websocket FIFO of text messages
 *      typical printf format and data
 */
void warp10_enqueue(wsms_t *wsms, const char *fmt, ...) {
  va_list ap;
  wsm_t *wsm;
  
  wsm = (wsm_t *)malloc(sizeof(wsm_t));
  va_start(ap, fmt);
  wsm->sz = vasprintf(&(wsm->txt), fmt, ap);
  va_end(ap);
  STAILQ_INSERT_TAIL(wsms, wsm, entries);
}


/* Chunk of code that must provides such a function to push some data our websocket FIFO of text messages:
 *   void push_data(wsms_t *wsms, sf_message_t *msg) {
 *     ../..
 *     warp10_enqueue(wsms, ... );
 *   }
 */
//#include "datamodel1.c"
//#include "datamodel2.c"
//#include "datamodel3.c"
#include "datamodel4.c"


static int callback_warp10(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len) {
  struct per_session_data__warp10 *pss = (struct per_session_data__warp10 *)user;
  struct lws_pollargs *pa = (struct lws_pollargs *)in;
  int m;

  switch (reason) {

    case LWS_CALLBACK_CLIENT_ESTABLISHED:
      lwsl_info("Warp10: LWS_CALLBACK_CLIENT_ESTABLISHED\n");
      pss->state = WARP10_WS_ONERROR;
      STAILQ_INIT(&(pss->wsms));
      lws_callback_on_writable(wsi);
      break;

    case LWS_CALLBACK_CLOSED:
      lwsl_notice("Warp10: LWS_CALLBACK_CLOSED\n");
      switch (pss->state) {
	case WARP10_WS_ONERROR:
	  lwsl_err("Warp10: Server rejects stream configuration\n");
	  break;
	case WARP10_WS_AUTH:
	  lwsl_err("Warp10: Server rejects the provided write token\n");
	  break;
	case WARP10_WS_UPDATE:
	  lwsl_err("Warp10: Server rejects updated data\n");
	  break;
      }
      break;

    case LWS_CALLBACK_CLIENT_RECEIVE:
      ((char *)in)[len] = '\0';
      lwsl_info("Warp10: rx %d '%s'\n", (int)len, (char *)in);
      if (strcmp(in, "OK 0 ONERROR") == 0) {
	lwsl_info("Warp10: Stream configuration OK\n");
	pss->state = WARP10_WS_AUTH;
	lws_callback_on_writable(wsi);
      } else if (strcmp(in, "OK 1 TOKEN") == 0) {
	lwsl_info("Warp10: Authentification OK\n");
	pss->state = WARP10_WS_UPDATE;
      }
      break;

    case LWS_CALLBACK_CLIENT_WRITEABLE:
      switch (pss->state) {
	case WARP10_WS_ONERROR:
	  lwsl_info("Warp10: Send stream configuration\n");
	  if (!warp10_onerror(wsi)) {
	    lwsl_err("Warp10: Can't send stream configuration\n");
	    force_exit = true;
	  }
	  break;
	case WARP10_WS_AUTH:
	  lwsl_info("Warp10: Send authentification token\n");
	  if (!warp10_auth(wsi)) {
	    lwsl_err("Warp10: Can't send auth token\n");
	    force_exit = true;
	  }
	  break;
	case WARP10_WS_UPDATE:
	  lwsl_info("Warp10: Send data update\n");
	  if (!warp10_update(wsi, &(pss->wsms))) {
	    lwsl_err("Warp10: Can't send data update\n");
	    force_exit = true;
	  }
	  break;
      }
      break;

      /* because we are protocols[0] ... */

    case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
      lwsl_err("Warp10: LWS_CALLBACK_CLIENT_CONNECTION_ERROR\n");
      wsi_warp10 = NULL;
      break;

    case LWS_CALLBACK_CLIENT_CONFIRM_EXTENSION_SUPPORTED:
      if ((strcmp(in, "deflate-stream") == 0) && deny_deflate) {
	lwsl_notice("denied deflate-stream extension\n");
	return 1;
      }
      if ((strcmp(in, "x-webkit-deflate-frame") == 0))
	return 1;
      if ((strcmp(in, "deflate-frame") == 0))
	return 1;
      break;

    case LWS_CALLBACK_RECEIVE_CLIENT_HTTP:
      {
	char buffer[1024 + LWS_PRE];
	char *px = buffer + LWS_PRE;
	int lenx = sizeof(buffer) - LWS_PRE;

	lwsl_notice("LWS_CALLBACK_RECEIVE_CLIENT_HTTP\n");

	/*
	 * Often you need to flow control this by something
	 * else being writable.  In that case call the api
	 * to get a callback when writable here, and do the
	 * pending client read in the writeable callback of
	 * the output.
	 */
	if (lws_http_client_read(wsi, &px, &lenx) < 0)
	  return -1;
	while (lenx--)
	  putchar(*px++);
      }
      break;

    case LWS_CALLBACK_COMPLETED_CLIENT_HTTP:
      wsi_warp10 = NULL;
      force_exit = true;
      break;

#if defined(LWS_USE_POLARSSL)
#else
#if defined(LWS_USE_MBEDTLS)
#else
#if defined(LWS_OPENSSL_SUPPORT) && defined(LWS_HAVE_SSL_CTX_set1_param)
    case LWS_CALLBACK_OPENSSL_LOAD_EXTRA_CLIENT_VERIFY_CERTS:
      if (crl_path[0]) {
	/* Enable CRL checking of the server certificate */
	X509_VERIFY_PARAM *param = X509_VERIFY_PARAM_new();
	X509_VERIFY_PARAM_set_flags(param, X509_V_FLAG_CRL_CHECK);
	SSL_CTX_set1_param((SSL_CTX *) user, param);
	X509_STORE *store = SSL_CTX_get_cert_store((SSL_CTX *) user);
	X509_LOOKUP *lookup = X509_STORE_add_lookup(store, X509_LOOKUP_file());
	int n = X509_load_cert_crl_file(lookup, crl_path, X509_FILETYPE_PEM);
	X509_VERIFY_PARAM_free(param);
	if (n != 1) {
	  char errbuf[256];
	  n = ERR_get_error();
	  lwsl_err
	      ("LWS_CALLBACK_OPENSSL_LOAD_EXTRA_CLIENT_VERIFY_CERTS: SSL error: %s (%d)\n",
	       ERR_error_string(n, errbuf), n);
	  return 1;
	}
      }
      break;
#endif
#endif
#endif

    case LWS_CALLBACK_ADD_POLL_FD:
      if (count_pollfds >= max_poll_elements) {
	lwsl_err("LWS_CALLBACK_ADD_POLL_FD: too many sockets to track\n");
	return -1;
      }
      fd_lookup[pa->fd] = count_pollfds;
      pollfds[count_pollfds].fd = pa->fd;
      pollfds[count_pollfds].events = pa->events;
      pollfds[count_pollfds++].revents = 0;
      break;

    case LWS_CALLBACK_DEL_POLL_FD:
      if (!--count_pollfds)
	break;
      m = fd_lookup[pa->fd];
      /* have the last guy take up the vacant slot */
      pollfds[m] = pollfds[count_pollfds];
      fd_lookup[pollfds[count_pollfds].fd] = m;
      break;

    case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
      pollfds[fd_lookup[pa->fd]].events = pa->events;
      break;

    default:
      break;
  }

  return 0;
}


/* list of supported protocols and callbacks */

static struct lws_protocols protocols[] = {
  {
   "",
   callback_warp10,
   sizeof(struct per_session_data__warp10),
   400,
   },
  {NULL, NULL, 0, 0}		/* end */
};

static const struct lws_extension exts[] = {
  {
   "permessage-deflate",
   lws_extension_callback_pm_deflate,
   "permessage-deflate; client_max_window_bits"},
  {
   "deflate-frame",
   lws_extension_callback_pm_deflate,
   "deflate_frame"},
  {NULL, NULL, NULL /* terminator */ }
};


void sighandler(int sig) {
  force_exit = true;
}


static struct option options[] = {
  {"help", 		no_argument, 		NULL, 'h'},
  {"verbose", 		required_argument,	NULL, 'v'},
  {"ietf-version",	required_argument,	NULL, 'i'},
  {"undeflated",	no_argument,		NULL, 'u'},
  {"ssl",		no_argument,		NULL, 'S'},
  {"ssl-cert",		required_argument,	NULL, 'C'},
  {"ssl-key",		required_argument,	NULL, 'K'},
  {"ssl-ca",		required_argument,	NULL, 'A'},
#if defined(LWS_OPENSSL_SUPPORT) && defined(LWS_HAVE_SSL_CTX_set1_param)
  {"ssl-crl",		required_argument,	NULL, 'R'},
#endif
  {"token",		required_argument,	NULL, 't'},
  {"sf-device",		required_argument,	NULL, 'd'},
  {"sf-server",		required_argument,	NULL, 's'},
  {"sf-port",		required_argument,	NULL, 'p'},
  {"sf-file",		required_argument,	NULL, 'f'},
  {NULL, 0, 0, 0}
};


static int ratelimit_connects(unsigned int *last, unsigned int secs) {
  struct timeval tv;

  gettimeofday(&tv, NULL);
  if (tv.tv_sec - (*last) < secs)
    return 0;

  *last = tv.tv_sec;

  return 1;
}


int main(int argc, char **argv) {
  int n = 0, use_ssl = 0, ietf_version = -1;
  unsigned int rl_warp10 = 0;
  struct lws_context_creation_info info;
  struct lws_client_connect_info i;
  struct lws_context *context;
  const char *prot, *p;
  char w10_uri[300];
  char cert_path[1024] = "";
  char key_path[1024] = "";
  char ca_path[1024] = "";
  char *token = NULL;
  char *sf_server=NULL, *sf_port=NULL, *sf_device=NULL, *sf_file=NULL;
  int sf_fd;

  memset(&info, 0, sizeof info);

  lws_set_log_level(LLL_ERR
                //| LLL_WARN
		  | LLL_NOTICE
		  | LLL_INFO
		//| LLL_DEBUG
		//| LLL_PARSER
		//| LLL_HEADER
		//| LLL_EXT
		//| LLL_CLIENT
		//| LLL_LATENCY
		  , NULL);

  lwsl_notice("SensFloor to Warp10 connector\n");

  if (argc < 2)
    goto usage;

  while (n >= 0) {
    n = getopt_long(argc, argv, "hv:i:uSC:K:A:R:t:d:s:p:f:", options, NULL);
    if (n < 0)
      continue;
    switch (n) {
      case 'v':
	lws_set_log_level(atoi(optarg), NULL);
	break;
      case 'i':
	ietf_version = atoi(optarg);
	break;
      case 'u':
	deny_deflate = 1;
	break;
      case 'S':
	use_ssl = 2;		/* 2 = allow selfsigned */
	break;
      case 'C':
	strncpy(cert_path, optarg, sizeof(cert_path) - 1);
	cert_path[sizeof(cert_path) - 1] = '\0';
	break;
      case 'K':
	strncpy(key_path, optarg, sizeof(key_path) - 1);
	key_path[sizeof(key_path) - 1] = '\0';
	break;
      case 'A':
	strncpy(ca_path, optarg, sizeof(ca_path) - 1);
	ca_path[sizeof(ca_path) - 1] = '\0';
	break;
#if defined(LWS_USE_POLARSSL)
#else
#if defined(LWS_USE_MBEDTLS)
#else
#if defined(LWS_OPENSSL_SUPPORT) && defined(LWS_HAVE_SSL_CTX_set1_param)
      case 'R':
	strncpy(crl_path, optarg, sizeof(crl_path) - 1);
	crl_path[sizeof(crl_path) - 1] = '\0';
	break;
#endif
#endif
#endif
      case 't':
	token = optarg;
	break;
      case 'd':
	sf_device = optarg;
	break;
      case 's':
	sf_server = optarg;
	break;
      case 'p':
	sf_port = optarg;
	break;
      case 'f':
	sf_file = optarg;
	break;
      case 'h':
	goto usage;
    }
  }

  if (optind >= argc)
    goto usage;

  signal(SIGINT, sighandler);


  if (sf_device)
    sf_fd = sf_open_serial(sf_device);
  else if (sf_server && sf_port)
    sf_fd = sf_open_tcp(sf_server, sf_port);
  else if (sf_file)
    sf_fd = sf_open_file(sf_file);
  else {
    fprintf(stderr, "No SensFloor channel!\n");
    goto usage;
  }

  memset(&i, 0, sizeof(i));

  i.port = 8080;
  if (lws_parse_uri(argv[optind], &prot, &i.address, &i.port, &p)) {
    fprintf(stderr, "Error: cannot parse Warp10 uri '%s'.\n", argv[optind]);
    goto usage;
  }

  if (token == NULL) {
    fprintf(stderr, "Error: no token provided.\n");
    goto usage;
  }
  protocols[PROTOCOL_WARP10].user = token;

  /* add back the leading / on w10_uri */
  w10_uri[0] = '/';
  strncpy(w10_uri + 1, p, sizeof(w10_uri) - 2);
  w10_uri[sizeof(w10_uri) - 1] = '\0';
  i.path = w10_uri;

  if (strcmp(prot, "ws") == 0)
    use_ssl = 0;
  if ((strcmp(prot, "wss") == 0) && !use_ssl)
    use_ssl = 1;

  /*
   * create the websockets context.  This tracks open connections and
   * knows how to route any traffic and which protocol version to use,
   * and if each connection is client or server side.
   *
   * For this client-only demo, we tell it to not listen on any port.
   */

  info.port = CONTEXT_PORT_NO_LISTEN;
  info.protocols = protocols;
  info.gid = -1;
  info.uid = -1;

  if (use_ssl) {
    info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;

    /*
     * If the server wants us to present a valid SSL client certificate
     * then we can set it up here.
     */

    if (cert_path[0])
      info.ssl_cert_filepath = cert_path;
    if (key_path[0])
      info.ssl_private_key_filepath = key_path;

    /*
     * A CA cert and CRL can be used to validate the cert send by the server
     */
    if (ca_path[0])
      info.ssl_ca_filepath = ca_path;
#if defined(LWS_USE_POLARSSL)
#else
#if defined(LWS_USE_MBEDTLS)
#else
#if defined(LWS_OPENSSL_SUPPORT) && defined(LWS_HAVE_SSL_CTX_set1_param)
    else if (crl_path[0])
      lwsl_notice("WARNING, providing a CRL requires a CA cert!\n");
#endif
#endif
#endif

    if (use_ssl == 1)
      lwsl_notice
	  (" Cert must validate correctly (use -s to allow selfsigned)\n");
    else
      lwsl_notice(" Selfsigned certs allowed\n");
  }

  context = lws_create_context(&info);
  if (context == NULL) {
    fprintf(stderr, "Creating libwebsocket context failed\n");
    exit(EXIT_FAILURE);
  }

  i.context = context;
  i.ssl_connection = use_ssl;
  i.host = i.address;
  i.origin = i.address;
  i.ietf_version_or_minus_one = ietf_version;
  i.client_exts = exts;

  lwsl_notice("using %s mode (ws)\n", prot);

  /* handel poolfds */
  max_poll_elements = getdtablesize();
  pollfds = malloc(max_poll_elements * sizeof(struct lws_pollfd));
  fd_lookup = malloc(max_poll_elements * sizeof(int));
  if (pollfds == NULL || fd_lookup == NULL) {
    lwsl_err("Out of memory pollfds=%d\n", max_poll_elements);
    exit(EXIT_FAILURE);
  }

  /* add sf_fd events in pollfds */
  fd_lookup[sf_fd] = 0;
  pollfds[0].fd = sf_fd;
  pollfds[0].events = POLLIN;
  count_pollfds = 1;

  /*
   * sit there servicing the websocket context to handle incoming
   * packets
   *
   * nothing happens until the client websocket connection is
   * asynchronously established... calling lws_client_connect() only
   * instantiates the connection logically, lws_service() progresses it
   * asynchronously.
   */

  while (!force_exit) {

    if (!wsi_warp10 && ratelimit_connects(&rl_warp10, 2u)) {
      lwsl_notice("Warp10: Connecting\n");
      i.protocol = protocols[PROTOCOL_WARP10].name;
      wsi_warp10 = lws_client_connect_via_info(&i);
    }

    if (wsi_warp10) {
      // lws_service(context, 500);
      int n = 0;
      while (n >= 0 && !force_exit) {
	n = poll(pollfds, count_pollfds, -50);
	if (n < 0)
	  continue;

	if (n) {
	  for (n = 0; n < count_pollfds; n++) {
	    if (pollfds[n].revents) {

	      if ( (pollfds[n].fd == sf_fd) && (pollfds[n].revents & POLLIN) ) {
		 // SensFloor event
		 struct per_session_data__warp10 *pss = lws_wsi_user(wsi_warp10);
		 if (pss && (pss->state == WARP10_WS_UPDATE) ) {
		   sf_message_t *msg = sf_get_message(sf_fd);
		   if ( msg && (msg->def.byte == SF_DEF_EU || msg->def.byte == SF_DEF_ES) ) {
		     push_data(&(pss->wsms), msg);
		     lws_callback_on_writable(wsi_warp10);
                   }
		 }
	      } else {
		if (lws_service_fd(context, &pollfds[n]) < 0)
		  goto done; // returns immediately if the fd does not match anything under libwebsockets control
	      }
	    }
	  }
	}
      }
    }
  }

done:
  lwsl_notice("Exiting\n");
  lws_context_destroy(context);

  exit(EXIT_SUCCESS);

usage:
  fprintf(stderr, "Usage: %s "
    "[--help] [--verbose <log bitfield>] [--ietf-version <ver>] [--undeflated] "
    "[--ssl] [--ssl-cert <path>] [--ssl-key <path>] [--ssl-ca <path>] "
#if defined(LWS_OPENSSL_SUPPORT) && defined(LWS_HAVE_SSL_CTX_set1_param)
    "[--ssl-crl <path>] "
#endif
    "[--sf-device <SensFloor serial device>]|"
    "[--sf-server <SensFloor server IP>]|"
    "[--sf-port <SensFloor server port>]|"
    "[--sf-file <SensFloor data file>] "
    "--token <Warp10 write token> "
    "<Warp10 server uri>\n", basename(strdup(argv[0])));
  exit(EXIT_FAILURE);
}
