/* This file is part of sf2warp10 - SensFloor to Warp10 connector
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* A tricky data-model

  Warp10 handels location by Latitude:Longitude/Elevation (WGS84).  It is
  fine for outdoor geolocation.  However, for indoor geolocation one has to
  trick this.

  SensFloor provides coordinates from the corner of the room (according to
  basical euclidean geometry).

  The idea here is to store these coordinates as two concanated 5-digits
  numbers (almost uint16) into the elevation field (witch is suppose to be a
  java long; i.e., a signed 64 bits int (most of the time...  note that
  Java8 allows storing unsigned 64 bits into its longs)...  nevertheless, it
  is large enough for our need).

  First step: convert SensFloor tiles/sectors info into X-Y points.
  Each tile is made of 8 sectors that are right-rectangles.
  One consider the gravity-center of each right-rectangle.
  Property of right-rectangles: let's (ABC) a rectangle, right in A; the
  gravity-center G is at: AG = 1/3 AB + 1/3 AC
  For simplicity, let's assume our tiles are squares of 6 units.
  So, gravity-centers of sectors are as follow:
      _______
  Y^ |\ h| a/|   	a (4,5)		b (5,4)
   | |g_\|/_b|  	c (5,2)		d (4,1)
   | |f /|\ c|		e (2,1)		f (1,2)
   | |/_e|d_\|		g (1,4)		h (2,5)
   +-----------> X
  Tiles are numbered along XY axis starting from 1.

  Note: This is the responsability of the user to know that tiles of his
  floor 03f2 are of size 30cm x 60cm, and that tiles of his floor 0355 are
  of size 30cm x 30cm
  This could bee recorded as meta-data of the time serie:
  curl -H "X-Warp10-Token: $TOKEN" -H 'Transfer-Encoding: chunked' --data-binary 'activity{floorId=03f2}{geometry=30:60}' 'http://localhost:8080/api/v0/meta'
  curl -H "X-Warp10-Token: $TOKEN" -H 'Transfer-Encoding: chunked' --data-binary 'activity{floorId=0355}{geometry=30:30}' 'http://localhost:8080/api/v0/meta'
*/


void push_data(wsms_t *wsms, sf_message_t *msg) {
  const char *fmt = "//%05d%05d activity{floorId=%04x} %d\n";
  uint16_t floorid = be16toh(msg->gpid);
  uint16_t offset_x = (msg->modids.x - 1) * 6;
  uint16_t offset_y = (msg->modids.y - 1) * 6;
  uint16_t ax=4+offset_x, ay=5+offset_y;
  uint16_t bx=5+offset_x, by=4+offset_y;
  uint16_t cx=5+offset_x, cy=2+offset_y;
  uint16_t dx=4+offset_x, dy=1+offset_y;
  uint16_t ex=2+offset_x, ey=1+offset_y;
  uint16_t fx=1+offset_x, fy=2+offset_y;
  uint16_t gx=1+offset_x, gy=4+offset_y;
  uint16_t hx=2+offset_x, hy=5+offset_y;
  short offset = msg->def.sd ? 128 : 0;
  
  warp10_enqueue(wsms, fmt, ay, ax, floorid, msg->data[0]-offset);
  warp10_enqueue(wsms, fmt, by, bx, floorid, msg->data[1]-offset);
  warp10_enqueue(wsms, fmt, cy, cx, floorid, msg->data[2]-offset);
  warp10_enqueue(wsms, fmt, dy, dx, floorid, msg->data[3]-offset);
  warp10_enqueue(wsms, fmt, ey, ex, floorid, msg->data[4]-offset);
  warp10_enqueue(wsms, fmt, fy, fx, floorid, msg->data[5]-offset);
  warp10_enqueue(wsms, fmt, gy, gx, floorid, msg->data[6]-offset);
  warp10_enqueue(wsms, fmt, hy, hx, floorid, msg->data[7]-offset);
}
