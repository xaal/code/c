/* This file is part of sf2warp10 - SensFloor to Warp10 connector
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
A "flat" data-model.
Data from SensFloor is split into several lines.
Easy to produce, but not very easy to use afterwards; one has to retreives
PosXY values befor interpreting SectorN level
*/

void push_data(wsms_t *wsms, sf_message_t *msg) {
  uint16_t floorid = be16toh(msg->gpid);
  short offset = msg->def.sd ? 128 : 0;

  warp10_enqueue(wsms, "// PosX{FloorID=%04x} %u\n", floorid, msg->modids.x);
  warp10_enqueue(wsms, "// PosY{FloorID=%04x} %u\n", floorid, msg->modids.y);
  warp10_enqueue(wsms, "// Sector1{FloorID=%04x} %d\n", floorid, msg->data[0]-offset);
  warp10_enqueue(wsms, "// Sector2{FloorID=%04x} %d\n", floorid, msg->data[1]-offset);
  warp10_enqueue(wsms, "// Sector3{FloorID=%04x} %d\n", floorid, msg->data[2]-offset);
  warp10_enqueue(wsms, "// Sector4{FloorID=%04x} %d\n", floorid, msg->data[3]-offset);
  warp10_enqueue(wsms, "// Sector5{FloorID=%04x} %d\n", floorid, msg->data[4]-offset);
  warp10_enqueue(wsms, "// Sector6{FloorID=%04x} %d\n", floorid, msg->data[5]-offset);
  warp10_enqueue(wsms, "// Sector7{FloorID=%04x} %d\n", floorid, msg->data[6]-offset);
  warp10_enqueue(wsms, "// Sector8{FloorID=%04x} %d\n", floorid, msg->data[7]-offset);
}
