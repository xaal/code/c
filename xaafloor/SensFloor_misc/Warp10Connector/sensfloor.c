/* Minimal code to handle messages from the SensFloor® SE3-P tranciever
 *
 * Note: This library is dedicated to messages from floor to PC
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sensfloor.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netdb.h>
#include <time.h>
#include <fcntl.h>
#include <endian.h>


int sf_open_serial(const char *device) {
  const speed_t speed = B115200;
  struct termios tty;
  int fd;

  fd = open(device, O_RDWR | O_NOCTTY | O_SYNC);
  if (fd < 0) {
    fprintf(stderr, "open(%s): %s\n", device, strerror(errno));
    exit(EXIT_FAILURE);
  }

  /*baudrate 115200, 8 bits, no parity, 1 stop bit */
  if (tcgetattr(fd, &tty) < 0) {
    fprintf(stderr, "tcgetattr(%s): %s\n", device, strerror(errno));
    exit(EXIT_FAILURE);
  }

  cfsetospeed(&tty, speed);
  cfsetispeed(&tty, speed);

  tty.c_cflag |= (CLOCAL | CREAD);	/* ignore modem controls */
  tty.c_cflag &= ~CSIZE;
  tty.c_cflag |= CS8;		/* 8-bit characters */
  tty.c_cflag &= ~PARENB;	/* no parity bit */
  tty.c_cflag &= ~CSTOPB;	/* only need 1 stop bit */
  tty.c_cflag &= ~CRTSCTS;	/* no hardware flowcontrol */

  /* setup for non-canonical mode */
  tty.c_iflag &=
      ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
  tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  tty.c_oflag &= ~OPOST;

  /* fetch bytes as they become available */
  tty.c_cc[VMIN] = 1;
  tty.c_cc[VTIME] = 1;

  if (tcsetattr(fd, TCSANOW, &tty) != 0) {
    fprintf(stderr, "tcsetattr(%s): %s\n", device, strerror(errno));
    exit(EXIT_FAILURE);
  }
  return fd;
}


int sf_open_tcp(const char *host, const char *port) {
  int s, fd;
  struct addrinfo hints;
  struct addrinfo *result, *rp;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;
  hints.ai_protocol = 0;

  s = getaddrinfo(host, port, &hints, &result);
  if (s != 0) {
    fprintf(stderr, "getaddrinfo(%s,%s): %s\n", host, port, gai_strerror(s));
    exit(EXIT_FAILURE);
  }

  for (rp = result; rp != NULL; rp = rp->ai_next) {
    fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (fd == -1)
      continue;
    if (connect(fd, rp->ai_addr, rp->ai_addrlen) != -1)
      break;
    close(fd);
  }
  if (rp == NULL) {
    perror("connect");
    exit(EXIT_FAILURE);
  }
  freeaddrinfo(result);
  return fd;
}


int sf_open_file(const char *name) {
  int fd;

  if (strcmp("-", name) == 0)
    return STDIN_FILENO;

  fd = open(name, O_RDONLY);
  if (fd == -1) {
    fprintf(stderr, "open(%s): %s\n", name, strerror(errno));
    exit(EXIT_FAILURE);
  }
  return fd;
}


sf_message_t *sf_get_message(int fd) {
  static sf_message_t msg;
  uint8_t c;
  int n, i;

  do {
    n = read(fd, &c, 1);
    if (n == -1) {
      perror("read()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "SensFloor: Connexion closed\n");
      exit(EXIT_SUCCESS);
    }
  } while (c != SF_MESSAGE_START);

  i = 0;
  do {
    n = read(fd, (uint8_t *) (&msg) + i, sizeof(msg) - i);
    if (n == -1) {
      perror("read()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "SensFloor: Connexion closed\n");
      exit(EXIT_SUCCESS);
    }
    i += n;
  } while (i < sizeof(msg));
  return &msg;
}


void sf_put_message(int fd, const sf_message_t * msg) {
  uint8_t c = SF_MESSAGE_START;
  unsigned n, i;

  n = write(fd, &c, 1);
  if (n == -1) {
    perror("write()");
    exit(EXIT_FAILURE);
  } else if (n == 0) {
    fprintf(stderr, "SensFlor: Connexion closed\n");
    exit(EXIT_SUCCESS);
  }

  i = 0;
  do {
    n = write(fd, (uint8_t *) (&msg) + i, sizeof(msg) - i);
    if (n == -1) {
      perror("write()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "SensFloor: Connexion closed\n");
      exit(EXIT_SUCCESS);
    }
    i += n;
  } while (i < sizeof(msg));
}


void sf_dump_message(FILE * stream, sf_message_t * msg) {
  time_t now = time(NULL);
  fprintf(stream, "%.19s\t%04x %04x %02x %02x "
	  "%1d%1d%1d%1d%1d%1d%1d%1d %02x\t"
	  "%02x %02x %02x %02x %02x %02x %02x %02x\n",
	  ctime(&now),
	  be16toh(msg->gpid), be16toh(msg->modidr), msg->gp1,
	  *(char *)(&msg->ftid), msg->def.tr, msg->def.rc, msg->def.cc,
	  msg->def.sd, msg->def.sa, msg->def.sr, msg->def.mm, msg->def.sc,
	  msg->gp2, msg->data[0], msg->data[1], msg->data[2], msg->data[3],
	  msg->data[4], msg->data[5], msg->data[6], msg->data[7]);
}


void sf_print_message(FILE * stream, sf_message_t * msg) {
  fprintf(stream, "\tFloor:%04x  X:%u  Y:%u\n",
	  be16toh(msg->gpid), msg->modids.x, msg->modids.y);
  switch (msg->ftid.ft) {
    case 1:
      fprintf(stream, "\tPresence detected on mat %d", msg->ftid.id);
      break;
    case 2:
      fprintf(stream, "\tFall detected on mat %d", msg->ftid.id);
      break;
    default:;
  }
  fprintf(stream, "\t");
  if (msg->def.sc)
    fprintf(stream, "Sensor state changed, ");
  if (msg->def.mm)
    fprintf(stream, "Master message, ");
  if (msg->def.sr)
    fprintf(stream, "Status request, ");
  if (msg->def.sa)
    fprintf(stream, "Status answer, ");
  if (msg->def.sd)
    fprintf(stream, "Signed data values, ");
  if (msg->def.cc)
    fprintf(stream, "Change device configuration, ");
  if (msg->def.rc)
    fprintf(stream, "Recalibration, ");
  if (msg->def.tr)
    fprintf(stream, "Message from the tranceiver, ");
  fprintf(stream, "\b\b   \n");
  if (msg->def.sc) {
    if (msg->def.sd)
      fprintf(stream,
	      "\tMats: %+04d %+04d %+04d %+04d %+04d %+04d %+04d %+04d\n",
	      msg->data[0] - 128, msg->data[1] - 128, msg->data[2] - 128,
	      msg->data[3] - 128, msg->data[4] - 128, msg->data[5] - 128,
	      msg->data[6] - 128, msg->data[7] - 128);
    else
      fprintf(stream, "\tMats: %04u %04u %04u %04u %04u %04u %04u %04u\n",
	      msg->data[0], msg->data[1], msg->data[2], msg->data[3],
	      msg->data[4], msg->data[5], msg->data[6], msg->data[7]);
  }
}


/* Typical "discovering" message to send */
const sf_message_t sf_discovermsg = {
  .gpid = 0xffff,		/* wildcard GPID */
  .modidr = 0xffff,		/* wildcard MODIDR */
  .gp1 = 0x00,			/* GP1=00 for requests */
  .ftid = {0x00},		/* FTID not used? */
  .def.byte = SF_DEF_RQ,	/* Request bit */
  .gp2 = SF_PARA_RC,		/* ask for relative capacitance */
  .data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
};
