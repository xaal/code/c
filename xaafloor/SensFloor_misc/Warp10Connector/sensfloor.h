/* Minimal code to handle messages from the SensFloor® SE3-P transciever
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SENSFLOOR
#define SENSFLOOR

#include <stdint.h>
#include <stdio.h>


#define SF_MESSAGE_START	0xFD	/* Start frame delimiter */

/* Message data structure
   Note: Two bytes values are stored in big endian
 */
typedef struct {
  uint16_t gpid;		/* Group IDentification (check endianness) */
  union {
    uint16_t modidr;		/* Module Receive Id (check endianness) */
    struct {
      uint8_t x;		/*   X coordinate */
      uint8_t y;		/*   Y coordinate */
    } modids;			/* Module Send Id */
  };
  uint8_t gp1;			/* _special use_ */
  struct {
    uint8_t ft:4;		/*   Function Type */
    uint8_t id:4;		/*   mat ID */
  } ftid;			/* _special use_ */
  union {
    uint8_t byte;		/*   in raw form */
    struct {
      uint8_t sc:1;		/*   sensor State Changed */
      uint8_t mm:1;		/*   Master Message */
      uint8_t sr:1;		/*   Status Request */
      uint8_t sa:1;		/*   Status Answer */
      uint8_t sd:1;		/*   Signed Data values */
      uint8_t cc:1;		/*   Change device Configuration */
      uint8_t rc:1;		/*   Re-Calibration */
      uint8_t tr:1;		/*   message from the TRansceiver */
    };
  } def;			/* message DEFinition */
  uint8_t gp2;			/* _special use_ */
  uint8_t data[8];		/* Data - Sensor Capacity for each mat */
} sf_message_t;


/* Functions Types definitions */
#define SF_FT_OFF	0
#define SF_FT_PRESENCE	1
#define SF_FT_FALL	2


/* Typical values for messages DEFinitions */

/* messages send by one module */
#define SF_DEF_AU	0x00	/* Automatic messages for Unsigned data */
#define SF_DEF_AS	0x10	/* Automatic messages for Signed data */
#define SF_DEF_EU	0x01	/* Event message for Unsigned data */
#define SF_DEF_ES	0x11	/* Event message for Signed data */
#define SF_DEF_SU	0x08	/* Status message for Unsigned data */
#define SF_DEF_SS	0x18	/* Status message for Signed data */

/* messages send towards a module */
#define SF_DEF_MA	0x02	/* MAster message */
#define SF_DEF_RQ	0x04	/* ReQuest message */
#define SF_DEF_CC	0x20	/* module's Configuration Change */
#define SF_DEF_RT	0x40	/* Re-calibration Trigger */

/* Information blocks definition for status-request messages */
#define SF_PARA_RC	0x00	/* Relative Capacity of the sensor plates */
#define SF_PARA_RCL	0x01	/* Relative Capacity Level */
#define SF_PARA_ST	0x02	/* sensors STates */
#define SF_PARA_CT	0x03	/* CounTers */
#define SF_PARA_ACF	0x0C	/* Absolute Capacity of the First 4 sensors */
#define SF_PARA_ACL	0x0D	/* Absolute Capacity of the Last 4 sensors */


/* Open a communication channel on an USB/serial port
   Input: serial device to open (e.g., /dev/ttyUSB0
   Output: file descriptor
 */
int sf_open_serial(const char *device);


/* Open a communication channel on an IP TCP server
   Input: server hostname and TCP port (e.g., sensfloor.homnet 5000)
   Output: file descriptor
 */
int sf_open_tcp(const char *host, const char *port);


/* Open a communication channel on a file
   Input: file name
   Output: file descriptor
 */
int sf_open_file(const char *name);


/* Wait for next message
   Input: file descriptor
   Output: pointer to internal-allocated buffer with the message
      or NULL pointer in case of invalid/trunkated message
 */
sf_message_t *sf_get_message(int fd);


/* Send a message
   Input: file descriptor, pointer to the message
 */
void sf_put_message(int fd, const sf_message_t * msg);


/* Dump message in hexadecimal
   Input: FILE stream and message
 */
void sf_dump_message(FILE * stream, sf_message_t * msg);


/* Pretty print a message
   Input: FILE stream and message
 */
void sf_print_message(FILE * stream, sf_message_t * msg);

#endif
