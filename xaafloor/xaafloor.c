/* xAAL - SensFloor Fall Detector
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/timerfd.h>

#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>

#include "sensfloor.h"


#define ALIVE_PERIOD    60
#define ALERT_PERIOD	5


/* Serialize a falls data-structure into cbor xAAL body */
cbor_item_t *falls2xaal(sf_falls_t *falls) {
  cbor_item_t *cbody, *cfalls, *cfall;
  time_t now = time(NULL);
  sf_fall_t *fall;
  char buf[5];

  cfalls = cbor_new_indefinite_array();
  LIST_FOREACH(fall, falls, entries) {
    cfall = cbor_new_definite_map(4);
    (void)!cbor_map_add(cfall, (struct cbor_pair){ cbor_move(cbor_build_string("delay")), cbor_move(cbor_build_uint64(now - fall->date)) });
    sprintf(buf, "%04X", fall->gpid);
    (void)!cbor_map_add(cfall, (struct cbor_pair){ cbor_move(cbor_build_string("zone")), cbor_move(cbor_build_string(buf)) });
    (void)!cbor_map_add(cfall, (struct cbor_pair){ cbor_move(cbor_build_string("x")), cbor_move(cbor_build_float8(fall->x)) });
    (void)!cbor_map_add(cfall, (struct cbor_pair){ cbor_move(cbor_build_string("y")), cbor_move(cbor_build_float8(fall->y)) });
    (void)!cbor_array_push(cfalls, cfall);
  }
  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("falls")), cbor_move(cfalls) });
  return cbody;
}


/* Manage received xAAL message */
void manage_xaal(const xAAL_businfo_t *bus, xAAL_devinfo_t *device, sf_falls_t *falls) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  /* Recive a message */
  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (msg_type == xAAL_REQUEST) {
    if ( (strcmp(action, "is_alive") == 0)
	&& xAAL_is_aliveDevType_match(cbody, device->dev_type) ) {
      if ( !xAAL_notify_alive(bus, device) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, device, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      if ( !xAAL_write_busl(bus, device, xAAL_REPLY, "get_attributes", falls2xaal(falls), source, NULL) )
	fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/* Manage received SensFloor message */
void manage_sensfloor(int sf_fd, const xAAL_businfo_t *bus, xAAL_devinfo_t *device, sf_floors_t *floors, sf_falls_t *falls) {
  sf_update_floors(sf_fd, floors);
  if ( sf_detect_fall(floors, falls) ) {
    if ( !xAAL_write_busl(bus, device, xAAL_NOTIFY, "attributes_change", falls2xaal(falls), NULL) )
      fprintf(xAAL_error_log, "Could not send attributes_change notification\n");
  }
}


/* Periodically check for fall detection and sends alert if any */
void manage_alerts(const xAAL_businfo_t *bus, xAAL_devinfo_t *device, const sf_floors_t *floors, sf_falls_t *falls) {
  bool removed;
  
  sf_refute_falls(falls);
  sf_detect_fall(floors, falls);
  removed = sf_withdraw_falls(falls);
  
  if ( !LIST_EMPTY(falls) ) {
    if ( !xAAL_write_busl(bus, device, xAAL_NOTIFY, "alert", falls2xaal(falls), NULL) )
      fprintf(xAAL_error_log, "Could not send alert notification\n");
  }
  if ( removed ) {
    if ( !xAAL_write_busl(bus, device, xAAL_NOTIFY, "attributes_change", falls2xaal(falls), NULL) )
      fprintf(xAAL_error_log, "Could not send attributes_change notification\n");
  }
}



/* main */
int main(int argc, char **argv) {
  /* setup device info */
  xAAL_devinfo_t device = { .dev_type	= "falldetector.basic",
			  .alivemax	= 2 * ALIVE_PERIOD,
			  .vendor_id	= "IHSEV",
			  .product_id	= "SensFloor Fall Detector",
			  .version	= "0.1",
			  .hw_id		= NULL,
			  .group_id	= NULL,
			  .url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			  .schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/falldetector.basic",
			  .info		= NULL,
			  .unsupported_attributes = NULL,
			  .unsupported_methods = NULL,
			  .unsupported_notifications = NULL
			};
  xAAL_businfo_t bus;
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  sf_floors_t floors;
  sf_falls_t falls;
  fd_set rfds, rfds_;
  int fd_max;
  char *sf_server=NULL, *sf_port=NULL, *sf_device=NULL, *sf_file=NULL;
  int sf_fd, alive_fd, alert_fd;
  uint64_t exp;

  xAAL_error_log = stderr;

  uuid_clear(device.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:D:S:P:F:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, device.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(device.addr);
	}
	break;
      case 'D':
	sf_device = optarg;
	break;
      case 'S':
	sf_server = optarg;
	break;
      case 'P':
	sf_port = optarg;
	break;
      case 'F':
	sf_file = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || !passphrase) {
    fprintf(stderr, "Missing parameter for the xAAL bus\n");
    arg_error = true;
  }
  if (sf_device)
    sf_fd = sf_open_serial(sf_device);
  else if (sf_server && sf_port)
    sf_fd = sf_open_tcp(sf_server, sf_port);
  else if (sf_file)
    sf_fd = sf_open_file(sf_file);
  else {
    fprintf(stderr, "No SensFloor channel\n");
    arg_error = true;
  }
  if ( arg_error ) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] [-D <SensFloor serial device>] [-S <SensFloor server IP>] [-P <SensFloor server port>] [-F <SensFloor data file>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(device.addr) ) {
    char uuid[37];
    uuid_generate(device.addr);
    uuid_unparse(device.addr, uuid);
    printf("Device: %s\n", uuid);
  }
  xAAL_add_wanted_target(&device.addr, &bus);


  /* Set alive timer for notifications */
  {
    struct itimerspec timerspec;

    alive_fd = timerfd_create(CLOCK_REALTIME, 0);
    if (alive_fd == -1)
      perror("Could not create timer for alive messages:");
    timerspec.it_interval.tv_sec = ALIVE_PERIOD;
    timerspec.it_interval.tv_nsec = 0;
    timerspec.it_value.tv_sec = 0;
    timerspec.it_value.tv_nsec = 1;
    if ( timerfd_settime(alive_fd, 0, &timerspec, NULL) == -1 )
      fprintf(xAAL_error_log, "Could not configure timer for alive notification: %s\n", strerror(errno));
  }

  /* Set timer for alerts, if any */
  {
    struct itimerspec timerspec;

    alert_fd = timerfd_create(CLOCK_REALTIME, 0);
    if (alert_fd == -1)
      perror("Could not create timer for alert messages:");
    timerspec.it_interval.tv_sec = ALERT_PERIOD;
    timerspec.it_interval.tv_nsec = 0;
    timerspec.it_value.tv_sec = 0;
    timerspec.it_value.tv_nsec = 1;
    if ( timerfd_settime(alert_fd, 0, &timerspec, NULL) == -1 )
      fprintf(xAAL_error_log, "Could not configure timer for alert notification: %s\n", strerror(errno));
  }

  LIST_INIT(&floors);
  LIST_INIT(&falls);

  FD_ZERO(&rfds);
  FD_SET(sf_fd, &rfds);
  fd_max = sf_fd;
  FD_SET(bus.sfd, &rfds);
  fd_max = (fd_max > bus.sfd)? fd_max : bus.sfd;
  FD_SET(alive_fd, &rfds);
  fd_max = (fd_max > alive_fd)? fd_max : alive_fd;
  FD_SET(alert_fd, &rfds);
  fd_max = (fd_max > alert_fd)? fd_max : alert_fd;


  /* Main loop */
  for (;;) {
    rfds_ = rfds;
    if ( (select(fd_max+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    /* An xAAL message from the bus */
    if (FD_ISSET(bus.sfd, &rfds_))
      manage_xaal(&bus, &device, &falls);

    /* A SensFloor message */
    if (FD_ISSET(sf_fd, &rfds_))
      manage_sensfloor(sf_fd, &bus, &device, &floors, &falls);

    /* It's time to send an alive on the xAAL bus */
    if (FD_ISSET(alive_fd, &rfds_)) {
      if ( read(alive_fd, &exp, sizeof(uint64_t)) == -1 )
	fprintf(xAAL_error_log, "Alive timer: %s\n", strerror(errno));
      if ( !xAAL_notify_alive(&bus, &device) )
	fprintf(xAAL_error_log, "Could not send alive notification\n");
    }

    /* It's time to check for fall detection */
    if (FD_ISSET(alert_fd, &rfds_)) {
      if ( read(alert_fd, &exp, sizeof(uint64_t)) == -1 )
	fprintf(xAAL_error_log, "Alert timer: %s\n", strerror(errno));
      manage_alerts(&bus, &device, &floors, &falls);
    }

  }
}
