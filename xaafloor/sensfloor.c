/* Minimal code to handle messages from the SensFloor® SE3-P tranciever
 *
 * Note: This library is dedicated to messages from floor to PC
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sensfloor.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netdb.h>
#include <time.h>
#include <fcntl.h>
#include <endian.h>


/* Magic thresholds for fall detection */
#define THR_DELTA2	1.0/36.0	// Distance² between centers for 'a same fall'
#define THR_CAPA	22		// Capacitance activation
#define THR_CLUSTER	8		// Number on contigious activated mats



/* Macros for min/max.  */
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


int sf_open_serial(const char *device) {
  const speed_t speed = B115200;
  struct termios tty;
  int fd;

  fd = open(device, O_RDWR | O_NOCTTY | O_SYNC);
  if (fd < 0) {
    fprintf(stderr, "open(%s): %s\n", device, strerror(errno));
    exit(EXIT_FAILURE);
  }

  /*baudrate 115200, 8 bits, no parity, 1 stop bit */
  if (tcgetattr(fd, &tty) < 0) {
    fprintf(stderr, "tcgetattr(%s): %s\n", device, strerror(errno));
    exit(EXIT_FAILURE);
  }

  cfsetospeed(&tty, speed);
  cfsetispeed(&tty, speed);

  tty.c_cflag |= (CLOCAL | CREAD);	/* ignore modem controls */
  tty.c_cflag &= ~CSIZE;
  tty.c_cflag |= CS8;		/* 8-bit characters */
  tty.c_cflag &= ~PARENB;	/* no parity bit */
  tty.c_cflag &= ~CSTOPB;	/* only need 1 stop bit */
  tty.c_cflag &= ~CRTSCTS;	/* no hardware flowcontrol */

  /* setup for non-canonical mode */
  tty.c_iflag &=
      ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
  tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  tty.c_oflag &= ~OPOST;

  /* fetch bytes as they become available */
  tty.c_cc[VMIN] = 1;
  tty.c_cc[VTIME] = 1;

  if (tcsetattr(fd, TCSANOW, &tty) != 0) {
    fprintf(stderr, "tcsetattr(%s): %s\n", device, strerror(errno));
    exit(EXIT_FAILURE);
  }
  return fd;
}


int sf_open_tcp(const char *host, const char *port) {
  int s, fd;
  struct addrinfo hints;
  struct addrinfo *result, *rp;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;
  hints.ai_protocol = 0;

  s = getaddrinfo(host, port, &hints, &result);
  if (s != 0) {
    fprintf(stderr, "getaddrinfo(%s,%s): %s\n", host, port, gai_strerror(s));
    exit(EXIT_FAILURE);
  }

  for (rp = result; rp != NULL; rp = rp->ai_next) {
    fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (fd == -1)
      continue;
    if (connect(fd, rp->ai_addr, rp->ai_addrlen) != -1)
      break;
    close(fd);
  }
  if (rp == NULL) {
    perror("connect");
    exit(EXIT_FAILURE);
  }
  freeaddrinfo(result);
  return fd;
}


int sf_open_file(const char *name) {
  int fd;

  if (strcmp("-", name) == 0)
    return STDIN_FILENO;

  fd = open(name, O_RDONLY);
  if (fd == -1) {
    fprintf(stderr, "open(%s): %s\n", name, strerror(errno));
    exit(EXIT_FAILURE);
  }
  return fd;
}


sf_message_t *sf_get_message(int fd) {
  static sf_message_t msg;
  uint8_t c;
  int n, i;

  do {
    n = read(fd, &c, 1);
    if (n == -1) {
      perror("read()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "SensFloor: Connexion closed\n");
      exit(EXIT_SUCCESS);
    }
  } while (c != SF_MESSAGE_START);

  i = 0;
  do {
    n = read(fd, (uint8_t *) (&msg) + i, sizeof(msg) - i);
    if (n == -1) {
      perror("read()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "SensFloor: Connexion closed\n");
      exit(EXIT_SUCCESS);
    }
    i += n;
  } while (i < sizeof(msg));
  return &msg;
}


void sf_put_message(int fd, const sf_message_t * msg) {
  uint8_t c = SF_MESSAGE_START;
  unsigned n, i;

  n = write(fd, &c, 1);
  if (n == -1) {
    perror("write()");
    exit(EXIT_FAILURE);
  } else if (n == 0) {
    fprintf(stderr, "SensFlor: Connexion closed\n");
    exit(EXIT_SUCCESS);
  }

  i = 0;
  do {
    n = write(fd, (uint8_t *) (&msg) + i, sizeof(msg) - i);
    if (n == -1) {
      perror("write()");
      exit(EXIT_FAILURE);
    } else if (n == 0) {
      fprintf(stderr, "SensFloor: Connexion closed\n");
      exit(EXIT_SUCCESS);
    }
    i += n;
  } while (i < sizeof(msg));
}


void sf_dump_message(FILE * stream, sf_message_t * msg) {
  time_t now = time(NULL);
  fprintf(stream, "%.19s\t%04x %04x %02x %02x "
	  "%1d%1d%1d%1d%1d%1d%1d%1d %02x\t"
	  "%02x %02x %02x %02x %02x %02x %02x %02x\n",
	  ctime(&now),
	  be16toh(msg->gpid), be16toh(msg->modidr), msg->gp1,
	  *(char *)(&msg->ftid), msg->def.tr, msg->def.rc, msg->def.cc,
	  msg->def.sd, msg->def.sa, msg->def.sr, msg->def.mm, msg->def.sc,
	  msg->gp2, msg->data[0], msg->data[1], msg->data[2], msg->data[3],
	  msg->data[4], msg->data[5], msg->data[6], msg->data[7]);
}


void sf_print_message(FILE * stream, sf_message_t * msg) {
  fprintf(stream, "\tFloor:%04x  X:%u  Y:%u\n",
	  be16toh(msg->gpid), msg->modids.x, msg->modids.y);
  switch (msg->ftid.ft) {
    case 1:
      fprintf(stream, "\tPresence detected on mat %d", msg->ftid.id);
      break;
    case 2:
      fprintf(stream, "\tFall detected on mat %d", msg->ftid.id);
      break;
    default:;
  }
  fprintf(stream, "\t");
  if (msg->def.sc)
    fprintf(stream, "Sensor state changed, ");
  if (msg->def.mm)
    fprintf(stream, "Master message, ");
  if (msg->def.sr)
    fprintf(stream, "Status request, ");
  if (msg->def.sa)
    fprintf(stream, "Status answer, ");
  if (msg->def.sd)
    fprintf(stream, "Signed data values, ");
  if (msg->def.cc)
    fprintf(stream, "Change device configuration, ");
  if (msg->def.rc)
    fprintf(stream, "Recalibration, ");
  if (msg->def.tr)
    fprintf(stream, "Message from the tranceiver, ");
  fprintf(stream, "\b\b   \n");
  if (msg->def.sc) {
    if (msg->def.sd)
      fprintf(stream,
	      "\tMats: %+04d %+04d %+04d %+04d %+04d %+04d %+04d %+04d\n",
	      msg->data[0] - 128, msg->data[1] - 128, msg->data[2] - 128,
	      msg->data[3] - 128, msg->data[4] - 128, msg->data[5] - 128,
	      msg->data[6] - 128, msg->data[7] - 128);
    else
      fprintf(stream, "\tMats: %04u %04u %04u %04u %04u %04u %04u %04u\n",
	      msg->data[0], msg->data[1], msg->data[2], msg->data[3],
	      msg->data[4], msg->data[5], msg->data[6], msg->data[7]);
  }
}


/* Typical "discovering" message to send */
const sf_message_t sf_discovermsg = {
  .gpid = 0xffff,		/* wildcard GPID */
  .modidr = 0xffff,		/* wildcard MODIDR */
  .gp1 = 0x00,			/* GP1=00 for requests */
  .ftid = {0x00},		/* FTID not used? */
  .def.byte = SF_DEF_RQ,	/* Request bit */
  .gp2 = SF_PARA_RC,		/* ask for relative capacitance */
  .data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
};


sf_floor_t *sf_init_floor(const unsigned gpid, unsigned x_max, unsigned y_max) {
  sf_floor_t *floor = (sf_floor_t *)malloc(sizeof(sf_floor_t));
  floor->gpid = gpid;
  floor->x_max = x_max;
  floor->y_max = y_max;
  floor->mats = (short *)calloc(x_max * y_max * 8, sizeof(short));
  return floor;
}


void sf_resize_floor(sf_floor_t *floor, unsigned x_max, unsigned y_max) {
  short *new_mats;
  unsigned x, y, w, h, m;

  new_mats = (short *) calloc(x_max * y_max * 8, sizeof(short));
  w = MIN(floor->x_max, x_max);
  h = MIN(floor->y_max, y_max);
  for (x = 0; x < w; x++)
    for (y = 0; y < h; y++)
      for (m = 0; m < 8; m++)
	new_mats[x + y*x_max + m*x_max*y_max] = floor->mats[x + y*floor->x_max + m*floor->x_max*floor->y_max];
  free(floor->mats);
  floor->mats = new_mats;
  floor->x_max = x_max;
  floor->y_max = y_max;
}


void sf_free_floor(sf_floor_t *floor) {
  free(floor->mats);
  free(floor);
}


sf_floor_t *sf_get_floor(const sf_floors_t *floors, unsigned gpid) {
  sf_floor_t *np;
  LIST_FOREACH(np, floors, entries)
    if ( np->gpid == gpid )
      return np;
  return NULL;
}


void sf_print_floor(const sf_floor_t *floor) {
  unsigned x, y, m;
  for (y=0; y < floor->y_max; y++) {
    printf("|");
    for (x=0; x < floor->x_max; x++) {
      printf("\b|");
      for (m=0; m < 8; m++) {
	printf("%d ", floor->mats[x + y*floor->x_max + m*floor->x_max*floor->y_max]);
      }
    }
    printf("|\n");
  }
}

void sf_update_floors(int fd, sf_floors_t *floors) {
  sf_message_t *msg;
  unsigned gpid, m;
  sf_floor_t *floor;

  msg = sf_get_message(fd);
  if ( !msg || (msg->def.byte != SF_DEF_EU && msg->def.byte != SF_DEF_ES) )
    return;

  gpid = be16toh(msg->gpid);
  floor = sf_get_floor(floors, gpid);
  if ( floor ) {
    if ( (msg->modids.x > floor->x_max) || (msg->modids.y > floor->y_max) )
      sf_resize_floor(floor, MAX(msg->modids.x, floor->x_max),
			     MAX(msg->modids.y, floor->y_max) );
  } else {
    floor = sf_init_floor(gpid, msg->modids.x, msg->modids.y);
    LIST_INSERT_HEAD(floors, floor, entries);
  }

  for (m = 0; m < 8; m++) {
    if (msg->def.sd)
      floor->mats[ (msg->modids.x-1) + (msg->modids.y-1)*floor->x_max + m*floor->x_max*floor->y_max ] = msg->data[m] - 128;
    else
      floor->mats[ (msg->modids.x-1) + (msg->modids.y-1)*floor->x_max + m*floor->x_max*floor->y_max ] = msg->data[m];
  }
}


bool sf_add_neigh(sf_neighs_t *neighs, unsigned x, unsigned y, unsigned m, short c) {
  sf_neigh_t *neigh;

  LIST_FOREACH(neigh, neighs, entries) {
    if ( (x==neigh->x) && (y==neigh->y) && (m==neigh->m) )
      return false;
  }

  neigh = malloc(sizeof(sf_neigh_t));
  neigh->x = x;
  neigh->y = y;
  neigh->m = m;
  neigh->c = c;
  LIST_INSERT_HEAD(neighs, neigh, entries);
  return true;
}


void sf_print_neighs(sf_neighs_t *neighs) {
  sf_neigh_t *n;
  printf("Neighbors: ");
  LIST_FOREACH(n,  neighs, entries)
    printf("(%d,%d,%d,%d) ", n->x, n->y, n->m, n->c);
  printf("\n");
}


sf_neighs_t *sf_cluster(const sf_floor_t *floor, unsigned x, unsigned y, unsigned m, short threshold) {
  /* Each 'tile' of SensFloor is a square (or rectangle) made of 8 triangle 'mats'
     Mats are numbered clockwise from 0 to 7
     Each tile has for coordinate x,y; each mat has for coordinate x,y,m
     For each mat one considers 4 neighbors belonging to the same tile or to adjacent ones:
     the mats adjacent to each of its 3 sides, plus the mat touching its right vertex.
	| /4|3\ |
      _\|/__|__\|/_
       /|\ 7|0 /|\	E.g., Let's consider the central tile.
      / | \ | / | \	Neighbors of mat #6 are mats #5 and #7,
      1_|6_\|/_1|_6	plus mats #1&2 of the left tile (at coordonate x-1, y)
      2 |5 /|\ 2| 5
      \ | / | \ | /	Bellow is the neighborhood definition for each
      _\|/_4|3_\|/_	of the 8 mats of the central tile.
       /|\  |  /|\	(each adjacents mat is considered clockwise)
	| \7|0/ |

    Clustering Principle: One manage a list of 'neighbors', initiated with a given mat of a given tile
    if the capacitance of this mat is more than the threshold.
    One look at each mat of the list and add its adjascents mats (as defined above),
    regardless of the capacitance of these mats.
    Then one iterate on this neighbors list until stabilization (i.e., no more mat added).
   */

  const struct { short x, y, m; } neighborhood[8][4] = {
    { {+0,+1,3}, {+0,+0,1}, {+0,+0,7}, {+0,+1,4} }, // 0
    { {+0,+0,0}, {+1,+0,6}, {+0,+0,2}, {+1,+0,6} }, // 1
    { {+0,+0,1}, {+1,+0,5}, {+0,+0,3}, {+1,+0,5} }, // 2
    { {+0,+0,2}, {+0,-1,0}, {+0,+0,4}, {+0,-1,7} }, // 3
    { {+0,+0,5}, {+0,+0,3}, {+0,-1,7}, {+0,-1,0} }, // 4
    { {+0,+0,6}, {+0,+0,4}, {-1,+0,2}, {-1,+0,1} }, // 5
    { {+0,+0,7}, {+0,+0,5}, {-1,+0,1}, {-1,+0,2} }, // 6
    { {+0,+1,4}, {+0,+0,0}, {+0,+0,6}, {+0,+1,3} }  // 7
  };

  sf_neighs_t *neighs;
  sf_neigh_t *neigh;
  short c, i;
  bool onceagain;
  int neigh_x, neigh_y;
  unsigned neigh_m;

  neighs = malloc(sizeof(sf_neighs_t));
  LIST_INIT(neighs);
  c = floor->mats[x + y*floor->x_max + m*floor->x_max*floor->y_max];
  sf_add_neigh(neighs, x, y, m, c);
  onceagain = true;

  while (onceagain) {
    onceagain = false;
    LIST_FOREACH(neigh, neighs, entries) {
      if ( neigh->c >= threshold) {
	for (i = 0; i < 4; i++) {
	  neigh_x = neigh->x + neighborhood[neigh->m][i].x;
	  neigh_y = neigh->y + neighborhood[neigh->m][i].y;
	  neigh_m = neighborhood[neigh->m][i].m;
	  if ( (neigh_x >= 0) && (neigh_x < floor->x_max) && (neigh_y >= 0) && (neigh_y < floor->y_max) ) {
	    c = floor->mats[neigh_x + neigh_y*floor->x_max + neigh_m*floor->x_max*floor->y_max];
	    if ( sf_add_neigh(neighs, neigh_x, neigh_y, neigh_m, c) )
	      onceagain = true;
	  }
	}
      }
    }
  }

  return neighs;
}


unsigned sf_cluster_size(sf_neighs_t *neighs, short threshold) {
  sf_neigh_t *neigh;
  unsigned sz = 0;
  LIST_FOREACH(neigh, neighs, entries)
    if ( neigh->c >= threshold)
      sz++;
  return sz;
}


void sf_free_neighs(sf_neighs_t *neighs) {
  sf_neigh_t *n1, *n2;
  n1 = LIST_FIRST(neighs);
  while (n1 != NULL) {
    n2 = LIST_NEXT(n1, entries);
    free(n1);
    n1 = n2;
  }
  free(neighs);
}


sf_fall_t *find_fall(const sf_falls_t *falls, unsigned gpid, double x, double y, double delta2) {
  sf_fall_t *fall;
  double X, Y, D;
  LIST_FOREACH(fall, falls, entries) {
    if ( fall->gpid == gpid ) {
      X = x - fall->x;
      Y = y - fall->y;
      D = X*X + Y*Y;
      if ( D <= delta2 )
	return fall;
    }
  }
  return NULL;
}


bool sf_update_fall(sf_falls_t *falls, const sf_neighs_t *neighs, unsigned gpid) {
  const struct {double x,y;} offset[8] = {
   {4.0/6.0, 5.0/6.0}, // Let point (0,0) be the bottom-left corner of the very
   {5.0/6.0, 4.0/6.0}, // first 'tile', and point (1,1) its top-right corner
   {5.0/6.0, 2.0/6.0}, // So, the center of each of the 8 'mats' (right-rectangles)
   {4.0/6.0, 1.0/6.0}, // is at the given offset (x,y goes from 0,0 to max_x,max_y)
   {2.0/6.0, 1.0/6.0}, // (take a pencil and a paper and draw figures to convince
   {1.0/6.0, 2.0/6.0}, // yourself)
   {1.0/6.0, 4.0/6.0}, // Use it as an offset for the gravity-center of each mat
   {2.0/6.0, 5.0/6.0}  // of each tile of the floor.
  };
  sf_neigh_t *neigh;
  sf_fall_t *fall;
  double X=0.0, Y=0.0, C=0.0;
  bool change = false;

  LIST_FOREACH(neigh, neighs, entries) {
    X += (neigh->x + offset[neigh->m].x) * neigh->c;
    Y += (neigh->y + offset[neigh->m].y) * neigh->c;
    C += neigh->c;
  }
  X /= C;
  Y /= C;

  fall = find_fall(falls, gpid, X, Y, THR_DELTA2);
  if ( !fall ) {
    fall = malloc(sizeof(sf_fall_t));
    LIST_INSERT_HEAD(falls, fall, entries);
    fall->date = time(NULL);
    fall->gpid = gpid;
    change = true;
  }
  fall->x = X;
  fall->y = Y;
  fall->confirmed = true;
  return change;
}


void sf_empty_falls(sf_falls_t *falls) {
  sf_fall_t *n1, *n2;
  n1 = LIST_FIRST(falls);
  while (n1 != NULL) {
    n2 = LIST_NEXT(n1, entries);
    free(n1);
    n1 = n2;
  }
  LIST_INIT(falls);
}


void sf_refute_falls(sf_falls_t *falls) {
  sf_fall_t *fall;
  LIST_FOREACH(fall, falls, entries)
    fall->confirmed = false;
}


bool sf_withdraw_falls(sf_falls_t *falls) {
  sf_fall_t *fall;
  bool removed = false;
  LIST_FOREACH(fall, falls, entries) {
    if ( !fall->confirmed) {
      LIST_REMOVE(fall, entries);
      free(fall);
      removed = true;
    }
  }
  return removed;
}



bool sf_detect_fall(const sf_floors_t *floors, sf_falls_t *falls) {
  sf_floor_t *floor;
  sf_neighs_t *neighs;
  unsigned x, y, m;
  bool change = false;

  LIST_FOREACH(floor, floors, entries) {
    for (x = 0; x < floor->x_max; x++) {
      for (y = 0; y < floor->y_max; y++) {
	for (m = 0; m < 8; m++) {
	  if ( floor->mats[x + y*floor->x_max + m*floor->x_max*floor->y_max] >= THR_CAPA ) {
	    neighs = sf_cluster(floor, x, y, m, THR_CAPA);
	    if ( sf_cluster_size(neighs, THR_CAPA) >= THR_CLUSTER)
	      change |= sf_update_fall(falls, neighs, floor->gpid);
	    sf_free_neighs(neighs);
	  }
	}
      }
    }
  }
  return change;
}
