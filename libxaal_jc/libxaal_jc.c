/* libxaal_jc
 * Auxiliary json2cbor & cbor2json transcoders for xAAL
 *
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#include <cbor.h>
#include <json-c/json.h>
#include <uuid/uuid.h>

#include <xaal.h>
#include <xaal_jc.h>


/* The (hidden) maxdepth parameter may prevents infinit recursion on
 * self-referencing data structure... Awful but secure. */
#define MAXDEPTH	30


/*
 * cbor2json
 */

json_object *xAAL_bytestring2json(cbor_item_t *item) {
  unsigned char *bstr;
  size_t len;

  bstr = xAAL_cbor_bytestring_dup(item, &len);
  if ( len == 16 ) { /* Assume it is an uuid - xAAL transcoding choice */
    char uuid[37];
    uuid_unparse(bstr, uuid);
    free(bstr);
    return json_object_new_string(uuid);

  } else {
    char hex[len*2];
    for (size_t i = 0; i<len; i++)
      sprintf(&(hex[2*i]), "%02x", bstr[i]);
    free(bstr);
    return json_object_new_string_len(hex, len*2);;
  }
}


json_object *xAAL_cbor2json_ex(cbor_item_t *item, unsigned maxdepth) {
  if (!maxdepth--)
    return NULL;
  switch ( cbor_typeof(item) ) {
    case CBOR_TYPE_UINT:
      return json_object_new_int64(cbor_get_int(item));
    case CBOR_TYPE_NEGINT:
      return json_object_new_int64(-cbor_get_int(item)-1);
    case CBOR_TYPE_BYTESTRING:
      return xAAL_bytestring2json(item);
    case CBOR_TYPE_STRING: {
      size_t len;
      char *str = xAAL_cbor_string_dup(item, &len);
      json_object *jobj = json_object_new_string_len(str, len);
      free(str);
      return jobj; }
    case CBOR_TYPE_ARRAY: {
      size_t i, len = cbor_array_size(item);
      json_object *jobj = json_object_new_array();
      for (i=0; i < len; i++)
	json_object_array_add(jobj, xAAL_cbor2json_ex(cbor_move(cbor_array_get(item, i)), maxdepth));
      return jobj; }
    case CBOR_TYPE_MAP: {
      char *key;
      size_t i, len, sz = cbor_map_size(item);
      struct cbor_pair *map = cbor_map_handle(item);
      json_object *jobj = json_object_new_object();
      for (i=0; i < sz; i++) {
	key = xAAL_cbor_string_dup(map[i].key, &len);
	if ( key ) {
	  json_object_object_add(jobj, key, xAAL_cbor2json_ex(map[i].value, maxdepth));
	  free(key);
	}
      }
      return jobj; }
    case CBOR_TYPE_TAG:
      return xAAL_cbor2json_ex(cbor_move(cbor_tag_item(item)), maxdepth); 
    case CBOR_TYPE_FLOAT_CTRL:
      if ( cbor_is_float(item) )
        return json_object_new_double(cbor_float_get_float(item));
      else {
        switch ( cbor_ctrl_value(item) ) {
        case CBOR_CTRL_NONE:
          return NULL; //?
        case CBOR_CTRL_FALSE:
          return json_object_new_boolean(false);
        case CBOR_CTRL_TRUE:
          return json_object_new_boolean(true);
        case CBOR_CTRL_NULL:
          return NULL;
        case CBOR_CTRL_UNDEF:
          return NULL; //?
        default:
          return NULL; //?
        }
      }
      break;
    default:
      return NULL; //?
  }
}

json_object *xAAL_cbor2json(cbor_item_t *item) {
  return xAAL_cbor2json_ex(item, MAXDEPTH);
}



/*
 * json2cbor
 */

cbor_item_t *xAAL_jsonstring2cbor(json_object *jobj) {
  const char *str = json_object_get_string(jobj);
  int len = json_object_get_string_len(jobj);
  uuid_t uuid;

  if ( (len == 36) && (uuid_parse(str, uuid)==0) )
    return cbor_build_bytestring(uuid, 16);
  else
    return cbor_build_stringn(str, len);
}


cbor_item_t *xAAL_json2cbor_ex(json_object *jobj, unsigned maxdepth) {
  if (!maxdepth--)
    return cbor_new_undef();
  switch ( json_object_get_type(jobj) ) {
    case json_type_null:
      return cbor_new_null();
    case json_type_boolean:
      return cbor_build_bool(json_object_get_boolean(jobj));
    case json_type_double:
      return xAAL_cbor_build_float(json_object_get_double(jobj));
    case json_type_int:
      return xAAL_cbor_build_int(json_object_get_int64(jobj));
    case json_type_object: {
      cbor_item_t *cmap = cbor_new_indefinite_map();
      json_object_object_foreach(jobj, key, val) {
	(void)!cbor_map_add(cmap, (struct cbor_pair){ cbor_move(cbor_build_string(key)), cbor_move(xAAL_json2cbor_ex(val, maxdepth)) });
      }
      return cmap; }
    case json_type_array: {
      int i, sz = json_object_array_length(jobj);
      cbor_item_t *carray = cbor_new_definite_array(sz);
      for (i=0; i < sz; i++) 
	(void)!cbor_array_push(carray, xAAL_json2cbor_ex(json_object_array_get_idx(jobj, i), maxdepth));
      return carray; }
    case json_type_string:
      return xAAL_jsonstring2cbor(jobj);
  }
  return cbor_new_undef();
}

cbor_item_t *xAAL_json2cbor(json_object *jobj) {
  return xAAL_json2cbor_ex(jobj, MAXDEPTH);
}
