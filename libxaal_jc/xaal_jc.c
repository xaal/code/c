/* libxaal_jc
 * Auxiliary json2cbor & cbor2json transcoders for xAAL
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <cbor.h>
#include <json-c/json.h>

#include <xaal_jc.h>


void cbor_to_json(char *file_in, char *file_out) {
  int fd;
  fd = open(file_in, O_RDONLY);
  if (fd == -1) {
    perror(file_in);
    exit(EXIT_FAILURE);
  }
  
  struct stat st;
  size_t sz;
  fstat(fd, &st);
  sz = st.st_size;
  
  unsigned char *map;
  map = mmap(0, sz, PROT_READ, MAP_SHARED, fd, 0);
  if (map == MAP_FAILED) {
    close(fd);
    perror(file_in);
    exit(EXIT_FAILURE);
  }

  cbor_item_t *citem;
  struct cbor_load_result cresult;
  citem = cbor_load(map, sz, &cresult);
  if (citem == NULL) {
    fprintf(stderr,  "CBOR error; code %d, position %lu\n", cresult.error.code, cresult.error.position);
    munmap(map, sz);
    close(fd);
    exit(EXIT_FAILURE);
  }
  
  json_object *jitem;
  jitem = xAAL_cbor2json(citem);
  cbor_decref(&citem);
  munmap(map, sz);
  close(fd);
  
  if (json_object_to_file_ext(file_out, jitem, JSON_C_TO_STRING_PRETTY | JSON_C_TO_STRING_SPACED) == -1) {
    perror(file_out);
    json_object_put(jitem);
    exit(EXIT_FAILURE);
  } else
    json_object_put(jitem);
}


void json_to_cbor(char *file_in, char *file_out) {
  json_object *jitem;  
  jitem = json_object_from_file(file_in);
  if (json_object_is_type(jitem, json_type_null)) {
    perror(file_in);
    fprintf(stderr, "Error parsing json file %s\n", file_in);
    exit(EXIT_FAILURE);
  }

  cbor_item_t *citem;
  citem = xAAL_json2cbor(jitem);
  json_object_put(jitem);

  size_t sz, len;
  cbor_mutable_data buf;
  len = cbor_serialize_alloc(citem, &buf, &sz);
  cbor_decref(&citem);

  int fd;
  fd = creat(file_out, (mode_t)0666);
  if (fd == -1) {
    perror(file_out);
    exit(EXIT_FAILURE);
  }
  
  if (write(fd, buf, len) == -1) {
    perror(file_out);
    close(fd);
    exit(EXIT_FAILURE);
  } else
    close(fd);
}
  

int main(int argc, char *argv[]) {
  bool j2c = true;
  char *file_in = NULL, *file_out = NULL;
  int opt;
  bool arg_error = false;
  
  while ((opt = getopt(argc, argv, "1i:o:")) != -1) {
    switch (opt) {
      case '1':
        j2c = false;
        break;
      case 'i':
        file_in = optarg;
        break;
      case 'o':
        file_out = optarg;
        break;
      default:
        arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (arg_error || !file_in || !file_out || arg_error) {
    fprintf(stderr, "Usage: %s [-1] -i <file_in> -o <file_out>\n"
      "Do its best to convert a json file into a cbor file, according to xAAL rules.\n"
      "Options:\n"
      "  -1		Do the reverse, i.e. cbor to json\n"
      "  -i <file_in>	Input file\n"
      "  -o <file_out>	Output file\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  if (j2c)
    json_to_cbor(file_in, file_out);
  else
    cbor_to_json(file_in, file_out);

  exit(EXIT_SUCCESS);  
}
