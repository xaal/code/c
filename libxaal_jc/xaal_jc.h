/* libxaal_jc
 * Auxiliary json2cbor & cbor2json transcoders for xAAL
 *
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LIBXAAL_JC
#define LIBXAAL_JC

#define xAAL_JC_VERSION	"0.2"

#include <cbor.h>
#include <json-c/json.h>

/*
 * cbor2json
 */

/* Transcode a cbor bytestring into json string, with hexadecimal notation
 * Except bytestring[16] that are encoded as uuid (i.e. hexa with dash) */
json_object *xAAL_bytestring2json(cbor_item_t *item);

/* Do its best to transcode cbor data into json
 * Strings are converted as above, tags are ignored */
json_object *xAAL_cbor2json(cbor_item_t *item);


/*
 * json2cbor
 */

/* Transcode a json string into cbor
 * Note: if the string represents an uuid, parse it and return a bytestring[16],
 * else return a cbor string */
cbor_item_t *xAAL_jsonstring2cbor(json_object *jobj);

/* Do its best to transcode json data into cbor
 * Strings are converted as above */
cbor_item_t *xAAL_json2cbor(json_object *jobj);

/* Explicitly transcode a base64 data json string into a cbor bytestring */
cbor_item_t *xAAL_jsonb64_to_bytestring(json_object *jobj);


#endif
