/* Lamp Commander - xAAL dummy hmi in command line for lamps
 * (c) 2020 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <sys/queue.h>
#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>



/* Some global variables nedded for the sig handler */
xAAL_businfo_t bus;
xAAL_devinfo_t cli;

/* List of detected lamps */
typedef LIST_HEAD(listhead, entry) lamps_t;
typedef struct entry {
  uuid_t addr;
  char *type;
  bool selected;
  time_t timeout;
  LIST_ENTRY(entry) entries;
} lamp_t;



#define ALIVE_PERIOD	60

/* SIGALRM handler that sends alive messages */
void alive_sender(int sig) {
  if (!xAAL_notify_alive(&bus, &cli) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* Send a request to a set of lamps*/
/* Return true if success */
bool bulk_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *cli, lamps_t *lamps, const char *action) {
  cbor_item_t *ctargets = cbor_new_indefinite_array();
  lamp_t *np;

  /* Build a list of targets to query */
  LIST_FOREACH(np, lamps, entries)
    if ( np->timeout && (time(NULL) > np->timeout) ) {
      // Too old, clean it
      LIST_REMOVE(np, entries);
      free(np->type);
      free(np);
    } else if (np->selected)
      (void)!cbor_array_push(ctargets, cbor_move(cbor_build_bytestring(np->addr, 16)));
  if ( !cbor_array_size(ctargets) )
    fprintf(xAAL_error_log, "Warning: broadcasted \"%s\" request.\n", action);

  return xAAL_write_bus(bus, cli, xAAL_REQUEST, action, NULL, ctargets);
}


/* Send is_alive request */
/* Return true if success */
bool request_is_alive(const xAAL_businfo_t *bus, const xAAL_devinfo_t *cli) {
  cbor_item_t *cbody, *cdev_types;

  cdev_types = cbor_new_definite_array(1);
  (void)!cbor_array_push(cdev_types, cbor_move(cbor_build_string("lamp.any")));

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("dev_types")), cbor_move(cdev_types) });

  return xAAL_write_busl(bus, cli, xAAL_REQUEST, "is_alive", cbody, &xAAL_is_alive_target, NULL);
}


#define CLI_MENU	"\nMenu: (1) Select lamps  (2) Send on  (3) Send off  (4) Quit\nYour choice?  "

/* Command Line Interface */
void cli_menu(const xAAL_businfo_t *bus, const xAAL_devinfo_t *cli, lamps_t *lamps) {
  int menu, choice, i;
  lamp_t *np;

  if ( scanf("%d", &menu) == 1 ) {
    switch (menu) {
    case 1:
      i = 0;
      printf("Detected lamps:\n");
      LIST_FOREACH(np, lamps, entries) {
	if ( np->timeout && (time(NULL) > np->timeout) ) {
	  LIST_REMOVE(np, entries);
	  free(np->type);
	  free(np);
	} else {
	  char str[37];
	  uuid_unparse(np->addr, str);
	  printf("%2d: %c %s %s %s", i++, np->selected?'*':' ', str, np->type, ctime(&np->timeout));
	}
      }
      if (i) {
	printf("Toggle which one? ");
	fflush(stdout);
	if ( scanf("%d", &choice) == 1 ) {
	  i = 0;
	  bool yes = false;
	  LIST_FOREACH(np, lamps, entries)
	    if ( choice == i++ ) {
	      yes = true;
	      np->selected = !np->selected;
	      break;
	    }
	  if (!yes)
	    printf("Sorry, can't find it.\n");
	} else {
	  scanf("%*s");
	  printf("Sorry.\n");
	}
      }
      break;
    case 2:
      if (!bulk_request(bus, cli, lamps, "turn_on"))
	fprintf(xAAL_error_log, "Could not send 'turn_on' request\n");
      break;
    case 3:
      if (!bulk_request(bus, cli, lamps, "turn_off"))
	fprintf(xAAL_error_log, "Could not send 'turn_off' request\n");
      break;
    case 4:
      exit(EXIT_SUCCESS);
    default:
      printf("Sorry, %d is not on the menu.\n", menu);
    }
  } else {
    char *str;
    scanf("%ms", &str);
    if ( str[1] == '\0' && ( (str[0]=='x') || (str[0]=='q') ) )
      exit(EXIT_SUCCESS);
    else {
      printf("What is '%s'?\n", str);
      free(str);
    }
  }
  fflush(stdin);
  printf(CLI_MENU);
  fflush(stdout);
}



/* manage received message */
void manage_msg(const xAAL_businfo_t *bus, const xAAL_devinfo_t *cli, lamps_t *lamps) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  lamp_t *np;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (    (msg_type == xAAL_REQUEST)
       && xAAL_targets_match(ctargets, &cli->addr) ) {

    if ( (strcmp(action, "is_alive") == 0)
	&& xAAL_is_aliveDevType_match(cbody, cli->dev_type) ) {
      if ( !xAAL_notify_alive(bus, cli) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, cli, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      /* I have no attributes */

    }
  } else if (strncmp(dev_type, "lamp.", strlen("lamp.")) == 0) {
    /* A lamp is talking */
    bool exists = false;
    LIST_FOREACH(np, lamps, entries)
      if ( uuid_compare(np->addr, *source) == 0 ) {
	if ( (msg_type == xAAL_NOTIFY) && (strcmp(action, "alive") == 0) )
	  np->timeout = xAAL_read_aliveTimeout(cbody);
	else
	  np->timeout = 0;
	exists = true;
	break;
      }
    if (!exists) {
      np = malloc(sizeof(lamp_t));
      uuid_copy(np->addr, *source);
      np->type = strdup(dev_type);
      if ( (msg_type == xAAL_NOTIFY) && (strcmp(action, "alive") == 0) )
	np->timeout = xAAL_read_aliveTimeout(cbody);
      else
	np->timeout = 0;
      np->selected = false;
      LIST_INSERT_HEAD(lamps, np, entries);
    }

  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  struct sigaction act_alarm;
  fd_set rfds, rfds_;
  lamps_t lamps;

  uuid_clear(cli.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:u:s:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 'u':
	if ( uuid_parse(optarg, cli.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(cli.addr);
	}
	break;
      case 's':
	passphrase = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (addr==NULL || port==NULL || arg_error) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  xAAL_error_log = stderr;
  if (!xAAL_join_bus(addr, port, hops, 1, &bus))
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/
  bus.key = xAAL_pass2key(passphrase);

  /* Generate device address if needed */
  if ( uuid_is_null(cli.addr) ) {
    char str[37];
    uuid_generate(cli.addr);
    uuid_unparse(cli.addr, str);
    printf("Device: %s\n", str);
  }

  /* Setup 'cli' device info */
  cli.dev_type    = "hmi.basic";
  cli.alivemax   = 2 * ALIVE_PERIOD;
  cli.vendor_id   = "IHSEV";
  cli.product_id  = "Lamp Commander";
  cli.hw_id	 = NULL;
  cli.version    = "0.5";
  cli.group_id    = NULL;
  cli.url	 = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  cli.schema	 = "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/hmi.basic";
  cli.info	 = NULL;
  cli.unsupported_attributes = NULL;
  cli.unsupported_methods = (char *[]){ "get_attributes", NULL };
  cli.unsupported_notifications = (char *[]){ "attributes_change", NULL };

  /* Setup 'lamps' list */
  LIST_INIT(&lamps);

  /* Manage periodic alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND | SA_RESTART;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &cli) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");

  if ( !request_is_alive(&bus, &cli) )
    fprintf(xAAL_error_log, "Could not send is_alive request.\n");

  printf(CLI_MENU);
  fflush(stdout);

  FD_ZERO(&rfds);
  FD_SET(STDIN_FILENO, &rfds);
  FD_SET(bus.sfd, &rfds);

  /* Main loop */
  for (;;) {
    rfds_ = rfds;
    if ( (select(bus.sfd+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(STDIN_FILENO, &rfds_)) {
      /* User wake up */
      cli_menu(&bus, &cli, &lamps);

    } else if (FD_ISSET(bus.sfd, &rfds_)) {
      /* Recive a message */
      manage_msg(&bus, &cli, &lamps);

    }
  }
}
