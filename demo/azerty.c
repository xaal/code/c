/* Azerty - A (dummy) board of xAAL button switches
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <termios.h>

#include <sys/queue.h>
#include <cbor.h>
#include <json-c/json.h>
#include <uuid/uuid.h>

#include <xaal.h>

#define ALIVE_PERIOD    60

/*****************/
/* Chapter: data */
/*****************/

/* List of all devices */
typedef TAILQ_HEAD(listhead, entry) devices_t;

typedef struct entry {
  xAAL_devinfo_t devinfo;
  char button;
  devices_t *embedded; // attribute of the composite
  bool position; // attribute of swiches
  TAILQ_ENTRY(entry) entries;
} device_t;


const uuid_t *add_device(devices_t *devices, char button, const uuid_t *addr,
		       uuid_t *group_id) {
  device_t *device;

  if (button < 'a' || button > 'z')
    return NULL;

  TAILQ_FOREACH(device, devices, entries)
    if (device->button == button)
      return &device->devinfo.addr;

  device = (device_t *)malloc(sizeof(device_t));

  /* Generate address if needed */
  if (uuid_is_null(*addr))
    uuid_generate(device->devinfo.addr);
  else
    uuid_copy(device->devinfo.addr, *addr);

  /* Generate group_id if needed */
  if (uuid_is_null(*group_id))
    uuid_generate(*group_id);

  { char s[30];
    sprintf(s, "Button '%c'", button);
    device->devinfo.info      = strdup(s);
  }
  device->devinfo.dev_type   = "switch.basic";
  device->devinfo.product_id = "Dummy Button-switch";
  device->devinfo.group_id   = group_id;
  device->button	    = button;
  device->position	    = false;
  device->devinfo.hw_id      = NULL;
  device->devinfo.alivemax  = 2 * ALIVE_PERIOD;
  device->devinfo.vendor_id  = "IHSEV";
  device->devinfo.version   = "0.4";
  device->devinfo.url       = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  device->devinfo.schema    = "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/basic.basic";
  device->devinfo.info	    = NULL;
  device->devinfo.unsupported_attributes = NULL;
  device->devinfo.unsupported_methods = NULL;
  device->devinfo.unsupported_notifications = NULL;

  TAILQ_INSERT_TAIL(devices, device, entries);

  return &device->devinfo.addr;
}



/*****************/
/* Chapter: xAAL */
/*****************/


/* Some global variables nedded for the sig handler */
xAAL_businfo_t *p_bus;
devices_t *p_devices;


/* SIGALRM handler that sends alive messages */
void alive_sender(int sig) {
  device_t *device;
  TAILQ_FOREACH(device, p_devices, entries)
    if ( !xAAL_notify_alive(p_bus, &(device->devinfo)) )
      fprintf(xAAL_error_log, "Could not send alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* notify_attributes_change */
/* Return true if success */
bool notify_attributes_change_button(const xAAL_businfo_t *bus,
				    const device_t *button) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("position")), cbor_move(cbor_build_bool(button->position)) });
  return xAAL_write_bus(bus, &(button->devinfo), xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


/* reply_get_attributes (button) */
bool reply_get_attributes(const xAAL_businfo_t *bus,
			 const device_t *device,
			 const uuid_t *source) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);

  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("position")), cbor_move(cbor_build_bool(device->position)) });

  return xAAL_write_busl(bus, &(device->devinfo), xAAL_REPLY,
			 "get_attributes", cbody, source, NULL);
}


/* Manage received message */
void manage_msg(const xAAL_businfo_t *bus, devices_t *devices) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  device_t *device;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (msg_type == xAAL_REQUEST) {
    TAILQ_FOREACH(device, devices, entries) {
      if (xAAL_targets_match(ctargets, &device->devinfo.addr)) {

	if ( (strcmp(action, "is_alive") == 0)
	     && xAAL_is_aliveDevType_match(cbody, device->devinfo.dev_type) ) {
	  if ( !xAAL_notify_alive(bus, &(device->devinfo)) )
	    fprintf(xAAL_error_log, "Could not reply to is_alive\n");

	} else if ( strcmp(action, "get_description") == 0 ) {
	  if ( !xAAL_reply_get_description(bus, &(device->devinfo), source) )
	    fprintf(xAAL_error_log, "Could not reply to get_description\n");

	} else if ( strcmp(action, "get_attributes") == 0 ) {
	  if ( !reply_get_attributes(bus, device, source) )
	    fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

	}
      }
    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}


/****************/
/* Chapter: hmi */
/****************/


/* Manage silent stdin */
struct termios initial_term;

void setsilent() {
  struct termios term;
  if ( tcgetattr(STDIN_FILENO, &initial_term) == -1) {
    perror("tcgetattr");
    return;
  }
  term = initial_term;
  term.c_lflag &= ~(ICANON | ECHO);
  term.c_cc[VMIN] = 1;
  term.c_cc[VTIME] = 0;
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &term) == -1)
    perror("tcsetattr");
}

void restore_term() {
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &initial_term) == -1)
    perror("tcsetattr");
}


/* Display buttons position */
void display_buttons(devices_t *devices) {
  device_t *device;
  printf("  ");
  TAILQ_FOREACH(device, devices, entries)
    if (strcmp(device->devinfo.dev_type, "switch.basic") == 0)
      printf("%c:%s ", device->button, device->position?"on ":"off");
  printf("\r");
  fflush(stdout);
}


/* Manage user input */
void user_input(xAAL_businfo_t *bus, devices_t *devices) {
  device_t *device;
  char c;

  if (read(STDIN_FILENO, &c, 1) == -1) {
    fprintf(xAAL_error_log, "read/stdin: %s\n", strerror(errno));
    return;
  }

  TAILQ_FOREACH(device, devices, entries)
    if (device->button == c) {
      device->position = !device->position;
      if (!notify_attributes_change_button(bus, device))
       fprintf(xAAL_error_log, "Could not notify attributes_change\n");
    }

  display_buttons(devices);
}


/*************************/
/* Chapter: User Options */
/*************************/

/* Options from cmdline or conffile */
typedef struct {
  char *addr;
  char *port;
  int  hops;
  char *passphrase;
  uuid_t group_id;
  char *conffile;
  bool immutable;
  bool daemon;
  char *logfile;
  char *pidfile;
  devices_t *devices;
} options_t;

/* Parse cmdline */
void parse_cmdline(int argc, char **argv, options_t *opts) {
  int opt;
  bool arg_error = false;

  while ((opt = getopt(argc, argv, "a:p:h:s:g:c:idl:P:D:U:")) != -1) {
    switch (opt) {
      case 'a':
	opts->addr = optarg;
	break;
      case 'p':
	opts->port = optarg;
	break;
      case 'h':
	opts->hops = atoi(optarg);
	break;
      case 's':
	opts->passphrase = optarg;
	break;
      case 'g':
	if ( uuid_parse(optarg, opts->group_id) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(opts->group_id);
	}
	break;
      case 'c':
	opts->conffile = optarg;
	break;
      case 'i':
       opts->immutable = true;
	break;
      case 'd':
	opts->daemon = true;
	break;
      case 'l':
	opts->logfile = optarg;
	break;
      case 'P':
	opts->pidfile = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  while (optind < argc)
    fprintf(stderr, "Unknown parameter %s\n", argv[optind++]);
  if (arg_error) {
    fprintf(stderr, "Usage: %s [-a <addr>] [-p <port>] [-h <hops>] [-s <secret>] [-g <group_id>]\n"
		"               [-c <conffile>] [-d] [-l <logfile>] [-P <pidfile>]\n"
		"-a <addr>      multicast IPv4 or IPv6 address of the xAAL bus\n"
		"-p <port>      UDP port of the xAAL bus\n"
		"-h <hops>      Hops limit for multicast packets\n"
		"-s <secret>	Secret passphrase\n"
		"-g <group_id>   UUID of the group of keys; random by default\n"
		"-c <conffile>  Filename of the configuration file (cson format)\n"
		"               Use 'azerty.conf' by default\n"
		"-i             Immutable config file (do not re-write it)\n"
		"-d             Start as a daemon\n"
		"-l <logfile>   Filename to write errors; stderr by default\n"
		"-P <pidfile>   Filename to write pid; none by default\n"
		, argv[0]);
    exit(EXIT_FAILURE);
  }
}


/* Read a config file (json format) */
void read_config(options_t *opts) {
  struct json_object *jconf, *jaddr, *jport, *jhops, *jpassphrase, *jgroupid,
		     *jconffile, *jimmutable, *jdaemon, *jlogfile, *jpidfile,
		     *jboard, *jbutton, *jbuttonname, *jbuttonaddr;
  const char *buttonname, *buttonaddr, *group_id;
  int board_len, i;
  uuid_t uuid;

  /* read file */
  jconf = json_object_from_file(opts->conffile);
  if (json_object_is_type(jconf, json_type_null)) {
    fprintf(stderr, "Could not parse config file %s\n", opts->conffile);
    return;
  }

  /* parse bus addr */
  if (json_object_object_get_ex(jconf, "addr", &jaddr)
      && json_object_is_type(jaddr, json_type_string))
    opts->addr = strdup(json_object_get_string(jaddr));

  /* parse bus port */
  if (json_object_object_get_ex(jconf, "port", &jport)
      && json_object_is_type(jport, json_type_string))
    opts->port = strdup(json_object_get_string(jport));

  /* parse bus hops */
  if (json_object_object_get_ex(jconf, "hops", &jhops)
      && json_object_is_type(jhops, json_type_int))
    opts->hops = json_object_get_int(jhops);

  /* parse passphrase */
  if (json_object_object_get_ex(jconf, "passphrase", &jpassphrase)
      && json_object_is_type(jpassphrase, json_type_string))
    opts->passphrase = strdup(json_object_get_string(jpassphrase));

  /* parse group_id for keys (uuid) */
  if (json_object_object_get_ex(jconf, "group_id", &jgroupid)
      && json_object_is_type(jgroupid, json_type_string)) {
    group_id = json_object_get_string(jgroupid);
    if ( uuid_parse(group_id, opts->group_id) == -1 )
      uuid_clear(opts->group_id);
  }

  /* parse config file name  */
  if (json_object_object_get_ex(jconf, "conffile", &jconffile)
      && json_object_is_type(jconffile, json_type_string))
    opts->conffile = strdup(json_object_get_string(jconffile));

  /* parse immutable flag  */
  if (json_object_object_get_ex(jconf, "immutable", &jimmutable)
      && json_object_is_type(jimmutable, json_type_boolean))
    opts->immutable = json_object_get_boolean(jimmutable);

  /* parse daemon flag  */
  if (json_object_object_get_ex(jconf, "daemon", &jdaemon)
      && json_object_is_type(jdaemon, json_type_boolean))
    opts->daemon = json_object_get_boolean(jdaemon);

  /* parse pid file name  */
  if (json_object_object_get_ex(jconf, "pidfile", &jpidfile)
      && json_object_is_type(jpidfile, json_type_string))
    opts->pidfile = strdup(json_object_get_string(jpidfile));

  /* parse log file name  */
  if (json_object_object_get_ex(jconf, "logfile", &jlogfile)
      && json_object_is_type(jlogfile, json_type_string))
    opts->logfile = strdup(json_object_get_string(jlogfile));

  /* parse list of button-switches */
  if (json_object_object_get_ex(jconf, "buttons", &jboard)
      && json_object_is_type(jboard, json_type_array)) {
    board_len = json_object_array_length(jboard);
    for (i=0; i<board_len; i++) {
      jbutton = json_object_array_get_idx(jboard, i);
      if (json_object_is_type(jbutton, json_type_object)
	  && json_object_object_get_ex(jbutton, "name", &jbuttonname)
	  && json_object_is_type(jbuttonname, json_type_string)
	  && json_object_object_get_ex(jbutton, "addr", &jbuttonaddr)
	  && json_object_is_type(jbuttonaddr, json_type_string) ) {
	buttonname = json_object_get_string(jbuttonname);
	buttonaddr = json_object_get_string(jbuttonaddr);
	if ( uuid_parse(buttonaddr, uuid) == -1 )
	  uuid_clear(uuid);
	add_device(opts->devices, buttonname[0], &uuid, &opts->group_id);
      }
    }
  }

  json_object_put(jconf);
}


/* Re-write config file (json format) */
void write_config(options_t *opts) {
  struct json_object *jconf, *jboard, *jbutton;
  device_t *device;
  char uuid_str[37];

  jconf = json_object_new_object();
  jconf = json_object_new_object();
  json_object_object_add(jconf, "addr",      json_object_new_string(opts->addr));
  json_object_object_add(jconf, "port",      json_object_new_string(opts->port));
  json_object_object_add(jconf, "hops",      json_object_new_int(opts->hops));
  json_object_object_add(jconf, "passphrase",json_object_new_string(opts->passphrase));
  uuid_unparse(opts->group_id, uuid_str);
  json_object_object_add(jconf, "group_id",   json_object_new_string(uuid_str));
  json_object_object_add(jconf, "conffile",  json_object_new_string(opts->conffile));
  json_object_object_add(jconf, "immutable", json_object_new_boolean(opts->immutable));
  json_object_object_add(jconf, "daemon",    json_object_new_boolean(opts->daemon));
  if (opts->logfile)
    json_object_object_add(jconf, "logfile", json_object_new_string(opts->logfile));
  if (opts->pidfile)
    json_object_object_add(jconf, "pidfile", json_object_new_string(opts->pidfile));

  jboard = json_object_new_array();

  TAILQ_FOREACH(device, opts->devices, entries)
    if (strcmp(device->devinfo.dev_type, "switch.basic")==0) {
      char s[] = { device->button, '\0' };
      jbutton = json_object_new_object();
      json_object_object_add(jbutton, "name", json_object_new_string(s));
      uuid_unparse(device->devinfo.addr, uuid_str);
      json_object_object_add(jbutton, "addr", json_object_new_string(uuid_str));
      json_object_array_add(jboard, jbutton);
    }
  json_object_object_add(jconf, "buttons", jboard);

  if (json_object_to_file_ext(opts->conffile, jconf, JSON_C_TO_STRING_PRETTY
			      | JSON_C_TO_STRING_SPACED) == -1)
    fprintf(xAAL_error_log, "Writing config file: %s\n", strerror(errno));

  json_object_put(jconf);
}



/*****************/
/* Chapter: main */
/*****************/

/* Global variable for the handler */
options_t *opts;

/* Called at exit */
void terminate() {
  if (!opts->immutable)
    write_config(opts);
  if (opts->pidfile)
    unlink(opts->pidfile);
}

/* Handler for Ctrl-C &co. */
void cancel(int s) {
  terminate();
  exit(EXIT_SUCCESS);
}


/* main */
int main(int argc, char **argv) {
  devices_t devices;
  xAAL_businfo_t bus;
  fd_set rfds, rfds_;
  options_t options = { .addr=NULL, .port=NULL, .hops=-1, .passphrase=NULL,
			.conffile="azerty.conf",
			.immutable=false,
			.daemon=false, .logfile=NULL, .pidfile=NULL,
			.devices=&devices };

  p_bus = &bus;
  p_devices = &devices;
  TAILQ_INIT(&devices);

  uuid_clear(options.group_id);

  /* Parse cmdline arguments */
  parse_cmdline(argc, argv, &options);

  /* Load config */
  read_config(&options);

  /* Manage logfile */
  if (options.logfile) {
    xAAL_error_log = fopen(options.logfile, "a");
    if (xAAL_error_log == NULL) {
      perror("Opening logfile");
      xAAL_error_log = stderr;
    }
  } else
    xAAL_error_log = stderr;

  /* Join the xAAL bus */
  if ( !options.addr || !options.port || !options.passphrase) {
    fprintf(xAAL_error_log, "Please provide the address, the port and the passphrase of the xAAL bus.\n");
    exit(EXIT_FAILURE);
  } else if ( !xAAL_join_bus(options.addr, options.port, options.hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(options.passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Create switches */
  add_device(&devices, 'a', NULL, &options.group_id);
  add_device(&devices, 'z', NULL, &options.group_id);
  add_device(&devices, 'e', NULL, &options.group_id);
  add_device(&devices, 'r', NULL, &options.group_id);
  add_device(&devices, 't', NULL, &options.group_id);
  add_device(&devices, 'y', NULL, &options.group_id);

  /* Build the list of wanted targets for the bus layer */
  { device_t *device;
    TAILQ_FOREACH(device, &devices, entries)
    xAAL_add_wanted_target(&device->devinfo.addr, &bus);
  }

  /* Start as a daemon */
  if (options.daemon && (daemon(1,1) == -1) )
    fprintf(xAAL_error_log, "daemon: %s\n", strerror(errno));

  /* Write pidfile */
  {
    FILE *pfile;

    if (options.pidfile) {
      pfile = fopen(options.pidfile, "w");
      if (pfile == NULL)
	fprintf(xAAL_error_log, "Opening pidfile: %s\n", strerror(errno));
      else {
	fprintf(pfile, "%d\n", getpid());
	fclose(pfile);
      }
    }
  }

  /* Manage alive notifications */
  {
    struct sigaction act_alarm;

    act_alarm.sa_handler = alive_sender;
    act_alarm.sa_flags = ~SA_RESETHAND;
    sigemptyset(&act_alarm.sa_mask);
    sigaction(SIGALRM, &act_alarm, NULL);
    alive_sender(0);
  }

  /* Manage Ctrl-C &co. */
  opts = &options;
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);
  atexit(terminate);

  setsilent();
  atexit(restore_term);

  FD_ZERO(&rfds);
  FD_SET(STDIN_FILENO, &rfds);
  FD_SET(bus.sfd, &rfds);

   /* Main loop */
  display_buttons(&devices);
  for (;;) {
    rfds_ = rfds;
    if ( (select(bus.sfd+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(STDIN_FILENO, &rfds_))
      user_input(&bus, &devices);

    if (FD_ISSET(bus.sfd, &rfds_))
      manage_msg(&bus, &devices);
  }
}
