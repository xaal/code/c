/* Thermo PC - Themperature of your CPU(s) on xAAL
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <sys/timerfd.h>

#include <sys/queue.h>
#include <sensors/sensors.h>
#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>


#define ALIVE_PERIOD	60
#define	THEMP_PERIOD	1
#define THEMP_THRESHOLD 2


/* List of devices */
typedef LIST_HEAD(listhead, entry) devices_t;

typedef struct entry {
  xAAL_devinfo_t devinfo;
  void *data;
  LIST_ENTRY(entry) entries;
} device_t;


/* Data managed by an embedded thermometer device */
/* Should be casted while using a (device_t).data */
typedef struct {
  double t;
  char *label;
  const sensors_chip_name *cn;
  int subfeat;
} thermometer_data;




/* Get cpu(s) sensors and build xAAL thermometers devices */
void init_sensors(devices_t *devices, const uuid_t *group_id) {
  int nr, f, s, rc;
  const sensors_chip_name *cn;
  const sensors_feature *feat;
  const sensors_subfeature *subf;
  thermometer_data *th;
  device_t *np;
  char *label;

  if (sensors_init(NULL)) {
    if (xAAL_error_log)
      fprintf(xAAL_error_log, "Error: trouble with default configuration file."
	      " (See sensors.conf(5).)\n");
    exit(EXIT_FAILURE);
  }

  nr = 0;
  while ( (cn=sensors_get_detected_chips(0, &nr)) != NULL ) {
    f = 0;
    while ( (feat = sensors_get_features(cn, &f)) != NULL ) {
      s = 0;
      while ( (subf = sensors_get_all_subfeatures(cn, feat, &s)) != NULL ) {
	label = sensors_get_label(cn, feat);
	if ( (subf->flags & SENSORS_MODE_R)
	    && (strncmp(label, "Core", strlen("Core")) == 0)
	    && strstr(subf->name, "_input") ) {
	  np = malloc(sizeof(device_t));
	  uuid_generate(np->devinfo.addr);
	  np->devinfo.dev_type   = "thermometer.basic";
	  np->devinfo.vendor_id  = "IHSEV";
	  np->devinfo.product_id = "CPU Thermal Sensor";
	  np->devinfo.hw_id	= NULL;
	  np->devinfo.version   = "0.5";
	  np->devinfo.alivemax  = 2 * ALIVE_PERIOD;
	  np->devinfo.group_id   = (uuid_t *)group_id;
	  np->devinfo.url       = "http://recherche.imt-atlantique.fr/xaal/documentation/";
	  np->devinfo.schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/thermometer.basic";
	  np->devinfo.info	= label;
	  np->devinfo.unsupported_attributes = NULL;
	  np->devinfo.unsupported_methods = NULL;
	  np->devinfo.unsupported_notifications = NULL;
	  th = (thermometer_data *)malloc(sizeof(thermometer_data));
	  rc = sensors_get_value(cn, subf->number, &th->t);
	  if ( (rc == -1) && xAAL_error_log )
	    fprintf(xAAL_error_log, "Could not get value of %s\n", label);
	  th->label = label;
	  th->cn = cn;
	  th->subfeat = subf->number;
	  np->data = th;
	  LIST_INSERT_HEAD(devices, np, entries);
	}
      }
    }
  }
}



/* notify_attributes_change (temperature) */
/* Return true if success */
bool notify_attributes_change_thermometer(const xAAL_businfo_t *bus,
					 const device_t *thermometer) {
  cbor_item_t *cbody;
  thermometer_data *th = thermometer->data;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("temperature")), cbor_move(xAAL_cbor_build_float(th->t)) });
  return xAAL_write_bus(bus, &thermometer->devinfo, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


/* reply_get_attributes (Themperature) */
/* Return true if success */
bool reply_get_attributes_thermometer(const xAAL_businfo_t *bus,
				     const device_t *thermometer,
				     const uuid_t *target) {
  cbor_item_t *cbody;
  thermometer_data *th = thermometer->data;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("temperature")), cbor_move(xAAL_cbor_build_float(th->t)) });
  return xAAL_write_busl(bus, &thermometer->devinfo, xAAL_REPLY, "get_attributes", cbody, target, NULL);
}


/* check_sensors */
void check_sensors(const xAAL_businfo_t *bus, const devices_t *devices) {
  device_t *np;
  thermometer_data *th;
  double t;
  int rc;

  LIST_FOREACH(np, devices, entries) {
    th = np->data;
    rc = sensors_get_value(th->cn, th->subfeat, &t);
    if (rc < 0) {
      if (xAAL_error_log)
	fprintf(xAAL_error_log, "Could not get temperature of %s: %d", th->label, rc);
    } else {
	if ( (th->t > t + THEMP_THRESHOLD) || (th->t < t-THEMP_THRESHOLD)) {
	th->t = t;
	if ( !notify_attributes_change_thermometer(bus, np) )
	 if ( xAAL_error_log )
	  fprintf(xAAL_error_log, "Could not send attributes_change\n");
      }
    }
  }
}



/* Manage received message */
void manage_msg(const xAAL_businfo_t *bus, const devices_t *devices) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  device_t *np;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  LIST_FOREACH(np, devices, entries) {
    if (xAAL_targets_match((const cbor_item_t *)ctargets, &np->devinfo.addr)) {

      if ( msg_type == xAAL_REQUEST ) {

	if ( (strcmp(action, "is_alive") == 0)
	     && xAAL_is_aliveDevType_match(cbody, np->devinfo.dev_type) ) {
	  if ( !xAAL_notify_alive(bus, &np->devinfo) )
	    fprintf(xAAL_error_log, "Could not reply to is_alive\n");

	} else if ( strcmp(action, "get_description") == 0 ) {
	  if ( !xAAL_reply_get_description(bus, &np->devinfo, source) )
	    fprintf(xAAL_error_log, "Could not reply to get_description\n");

	} else if ( strcmp(action, "get_attributes") == 0 ) {
	  if ( !reply_get_attributes_thermometer(bus, np, source) )
	    fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

	}
      }
    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}




/* main */
int main(int argc, char **argv) {
  xAAL_businfo_t bus;
  devices_t devices;
  device_t *np;
  int alive_fd, sensors_fd;
  int opt;
  char *passphrase = NULL;
  char *addr=NULL, *port=NULL;
  uuid_t group_id;
  int hops = -1;
  bool arg_error = false;
  fd_set rfds, rfds_;
  int fd_max;
  struct itimerspec timerspec;
  uint64_t exp;

  /* Setup the list of devices */
  LIST_INIT(&devices);

  uuid_clear(group_id);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:g:s:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 'g':
	if ( uuid_parse(optarg, group_id) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(group_id);
	}
	break;
      case 's':
	passphrase = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (addr==NULL || port==NULL || arg_error || passphrase==NULL) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-g <group_id>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  xAAL_error_log = stderr;
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(group_id) ) {
    char str[37];
    uuid_generate(group_id);
    uuid_unparse(group_id, str);
    printf("Group Id: %s\n", str);
  }

  /* Fill the list of devices with thermometers */
  init_sensors(&devices, &group_id);

  /* Record therometers as wanted targets */
  LIST_FOREACH(np, &devices, entries)
    xAAL_add_wanted_target(&np->devinfo.addr, &bus);


  /* Set alive timer */
  alive_fd = timerfd_create(CLOCK_REALTIME, 0);
  if (alive_fd == -1)
    perror("Could not create timer for alive messages");
  timerspec.it_interval.tv_sec = ALIVE_PERIOD;
  timerspec.it_interval.tv_nsec = 0;
  timerspec.it_value.tv_sec = 0;
  timerspec.it_value.tv_nsec = 1;
  if ( timerfd_settime(alive_fd, 0, &timerspec, NULL) == -1 )
    perror("Could not configure timer for alive messages");

  /* Set sensors polling timer */
  sensors_fd = timerfd_create(CLOCK_REALTIME, 0);
  if (sensors_fd == -1)
    perror("Could not create timer for sensors polling");
  timerspec.it_interval.tv_sec = THEMP_PERIOD;
  timerspec.it_interval.tv_nsec = 0;
  timerspec.it_value.tv_sec = THEMP_PERIOD;
  timerspec.it_value.tv_nsec = 0;
  if ( timerfd_settime(sensors_fd, 0, &timerspec, NULL) == -1 )
    perror("Could not configure timer for sensors polling");

  FD_ZERO(&rfds);
  FD_SET(bus.sfd, &rfds);
  fd_max = bus.sfd;
  FD_SET(alive_fd, &rfds);
  fd_max = (fd_max > alive_fd)? fd_max : alive_fd;
  FD_SET(sensors_fd, &rfds);
  fd_max = (fd_max > sensors_fd)? fd_max : sensors_fd;

  /* Main loop */
  for (;;) {

    rfds_ = rfds;
    if ( (select(fd_max+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(alive_fd, &rfds_)) {
      if ( read(alive_fd, &exp, sizeof(uint64_t)) == -1 )
	fprintf(xAAL_error_log, "Alive timer\n");
      LIST_FOREACH(np, &devices, entries)
	if ( !xAAL_notify_alive(&bus, &np->devinfo) )
	  fprintf(xAAL_error_log, "Could not send alive notifications.\n");
    }

    if (FD_ISSET(sensors_fd, &rfds_)) {
      if ( read(sensors_fd, &exp, sizeof(uint64_t)) == -1 )
	fprintf(xAAL_error_log, "Sensors timer\n");
      check_sensors(&bus, &devices);
    }

    if (FD_ISSET(bus.sfd, &rfds_))
      manage_msg(&bus, &devices);
  }
}
