/* xAAL dummy shutter
 * (c) 2020 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/select.h>

#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>


#define STEP	5.0	/* Percent per second of the aperture of the shutter */


#define ALIVE_PERIOD    60


/* setup shutter device info */
xAAL_devinfo_t shutter = { .dev_type	= "shutter.position",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "IHSEV",
			.product_id	= "Dummy shutter",
			.hw_id		= NULL,
			.version	= "0.5",
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.schema		= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/shutter.position",
			.info		= NULL,
			.unsupported_attributes = NULL,
			.unsupported_methods = NULL,
			.unsupported_notifications = NULL
		      };

xAAL_businfo_t bus;


typedef enum { up, down, stop } action_t;


void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &shutter) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


bool reply_get_attributes(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *shutter,
			 action_t action,
			 double position,
			 const uuid_t *target) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(2);
  switch (action) {
    case up:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("up")) });
      break;
    case down:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("down")) });
      break;
    case stop:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("stop")) });
      break;
  }
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("position")), cbor_move(xAAL_cbor_build_float(position)) });
  return xAAL_write_busl(bus, shutter, xAAL_REPLY, "get_attributes", cbody, target, NULL);
}


bool send_attribute_action(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *shutter,
			 action_t action) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  switch (action) {
    case up:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("up")) });
      break;
    case down:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("down")) });
      break;
    case stop:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("stop")) });
      break;
  }
  return xAAL_write_busl(bus, shutter, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


bool send_attribute_position(const xAAL_businfo_t *bus,
			  const xAAL_devinfo_t *shutter,
			  double position) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("position")), cbor_move(xAAL_cbor_build_float(position)) });
  return xAAL_write_busl(bus, shutter, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


bool send_attributes_action_position(const xAAL_businfo_t *bus,
				  const xAAL_devinfo_t *shutter,
				  action_t action, double position) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(2);
  switch (action) {
    case up:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("up")) });
      break;
    case down:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("down")) });
      break;
    case stop:
      (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("action")), cbor_move(cbor_build_string("stop")) });
      break;
  }
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("position")), cbor_move(xAAL_cbor_build_float(position)) });
  return xAAL_write_busl(bus, shutter, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}




double get_position_request(cbor_item_t *cbody) {
  cbor_item_t *ctarget;

  if (cbody) {
    ctarget = xAAL_cbor_map_get(cbody, "target");
    switch (cbor_typeof(ctarget)) {
    case CBOR_TYPE_UINT:	return cbor_get_int(ctarget);
    case CBOR_TYPE_NEGINT:	return -cbor_get_int(ctarget) - 1;
    case CBOR_TYPE_FLOAT_CTRL:	return cbor_float_get_float(ctarget);
    default: return -1;
    }
  } else
    return -1.0;
}


void manage_msg(xAAL_businfo_t *bus, xAAL_devinfo_t *shutter,
		action_t *attribute_action,
		double *attribute_position,
		double *attribute_target) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (msg_type == xAAL_REQUEST) {
    if ( (strcmp(action, "is_alive") == 0)
	&& xAAL_is_aliveDevType_match(cbody, shutter->dev_type) ) {
      if ( !xAAL_notify_alive(bus, shutter) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, shutter, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      if ( !reply_get_attributes(bus, shutter, *attribute_action, *attribute_position, source) )
	fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

    } else if ( strcmp(action, "up") == 0 ) {
      if (*attribute_position < 100.0) {
	*attribute_action = up;
	*attribute_target = 100.0;
      }
      if ( !send_attribute_action(bus, shutter, *attribute_action) )
	fprintf(xAAL_error_log, "Could not send attributes_change\n");

    } else if ( strcmp(action, "down") == 0 ) {
      if (*attribute_position > 0.0) {
	*attribute_action = down;
	*attribute_target = 0.0;
      }
      if ( !send_attribute_action(bus, shutter, *attribute_action) )
	fprintf(xAAL_error_log, "Could not send attributes_change\n");

    } else if ( strcmp(action, "stop") == 0 ) {
      *attribute_action = stop;
      *attribute_target = *attribute_position;
      if ( !send_attributes_action_position(bus, shutter, *attribute_action, *attribute_position) )
	fprintf(xAAL_error_log, "Could not send attributes_change\n");

    } else if ( strcmp(action, "set_position") == 0 ) {
      double target = get_position_request(cbody);
      if ( target >= 0.0 && target <= 100.0 ) {

	if (target < *attribute_position) {
	  *attribute_target = target;
	  *attribute_action = down;
	  if ( !send_attribute_action(bus, shutter, *attribute_action) )
	    fprintf(xAAL_error_log, "Could not send attributes_change\n");

	} else if (target > *attribute_position) {
	  *attribute_target = target;
	  *attribute_action = up;
	  if ( !send_attribute_action(bus, shutter, *attribute_action) )
	    fprintf(xAAL_error_log, "Could not send attributes_change\n");
	}
      }

    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}


void show_shutter(action_t action, double position) {
  const char foo[] = "                    ";
  const char bar[] = "####################";
  int len = position/5;

  switch (action) {
  case up:
    printf(" ^ Up  ");
    break;
  case down:
    printf(" v Down");
    break;
  case stop:
    printf(" - Stop");
    break;
  }
  printf("  % 6.1f%% [%.*s%.*s] \r", position, len, foo, 20-len, bar);
  fflush(stdout);
}


/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  action_t attribute_action = stop;
  double attribute_position = 100.0;
  double attribute_target = 100.0;
  struct sigaction act_alarm;
  fd_set rfds, rfds_;
  struct timeval second;

  xAAL_error_log = stderr;

  uuid_clear(shutter.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, shutter.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(shutter.addr);
	}
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }


  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(shutter.addr) ) {
    char str[37];
    uuid_generate(shutter.addr);
    uuid_unparse(shutter.addr, str);
    printf("Device: %s\n", str);
  }
  xAAL_add_wanted_target(&shutter.addr, &bus);

  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &shutter) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");

  FD_ZERO(&rfds);
  FD_SET(bus.sfd, &rfds);

  /* Main loop */
  for (;;) {
    rfds_ = rfds;
    second.tv_sec = 1;
    second.tv_usec = 0;

    show_shutter(attribute_action, attribute_position);

    if ( (select(bus.sfd+1, &rfds_, NULL, NULL, &second) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(bus.sfd, &rfds_)) {
      /* Recive a message */
      manage_msg(&bus, &shutter, &attribute_action, &attribute_position, &attribute_target);

    } else {
      double elapsed = 1.0 - second.tv_sec - second.tv_usec * 10e-7;
      if (elapsed < 0.0) elapsed = 0.0;

      switch (attribute_action) {
      case up:
	if (attribute_position >= attribute_target) {
	  attribute_position = attribute_target;
	  attribute_action = stop;
	  if ( !send_attributes_action_position(&bus, &shutter, attribute_action, attribute_position) )
	    fprintf(xAAL_error_log, "Could not send attributes_change\n");

	} else {
	  attribute_position += STEP * elapsed;
	  if ( attribute_position >= 100.0 ) {
	    attribute_position = attribute_target = 100.0;
	    attribute_action = stop;
	    if ( !send_attributes_action_position(&bus, &shutter, attribute_action, attribute_position) )
	      fprintf(xAAL_error_log, "Could not send attributes_change\n");
	  } else
	    if ( !send_attribute_position(&bus, &shutter, attribute_position) )
	      fprintf(xAAL_error_log, "Could not send attributes_change\n");
	}
	break;
      case down:
	if (attribute_position <= attribute_target) {
	  attribute_position = attribute_target;
	  attribute_action = stop;
	  if ( !send_attributes_action_position(&bus, &shutter, attribute_action, attribute_position) )
	    fprintf(xAAL_error_log, "Could not send attributes_change\n");

	} else {
	  attribute_position -= STEP * elapsed;
	  if ( attribute_position <= 0.0 ) {
	    attribute_position = attribute_target = 0.0;
	    attribute_action = stop;
	    if ( !send_attributes_action_position(&bus, &shutter, attribute_action, attribute_position) )
	      fprintf(xAAL_error_log, "Could not send attributes_change\n");
	  } else
	  if ( !send_attribute_position(&bus, &shutter, attribute_position) )
	    fprintf(xAAL_error_log, "Could not send attributes_change\n");
	}
	break;
      default:
	;
      }
    }

  }

}
