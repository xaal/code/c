/* xAAL bus dumper
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cbor.h>

#include <xaal.h>


void print_targets(cbor_item_t *ctargets) {
  size_t i, sz = cbor_array_size(ctargets);
  char str[37];
  uuid_t uuid;

  printf("targets: [ ");
  for (i=0; i<sz; i++) {
    if ( xAAL_cbor_is_uuid(cbor_move(cbor_array_get(ctargets, i)), &uuid) ) {
      uuid_unparse(uuid, str);
      printf("%s ", str);
    }
  }
  printf("]\n");
}


int main(int argc, char **argv) {
  xAAL_businfo_t bus;
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  cbor_item_t *ctargets, *cbody;
  uuid_t *source;
  char source_str[37];
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  unsigned long n = 0;
  time_t now;

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret>\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  xAAL_error_log = stderr;
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  for (;;) {
    /* Recive a message */
    if (!xAAL_read_bus(&bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
      continue;

    uuid_unparse(*source, source_str);
    now = time(NULL);
    printf("\n--%ld-- %s", ++n, ctime(&now));
    print_targets(ctargets);
    printf("source: %s\n", source_str);
    printf("dev_type: %s\n", dev_type);
    switch (msg_type) {
      case xAAL_NOTIFY:  printf("msg_type: notify\n");  break;
      case xAAL_REQUEST: printf("msg_type: request\n"); break;
      case xAAL_REPLY:   printf("msg_type: reply\n");   break;
      default: printf("msg_type: %d INVALID\n", msg_type); break;
    }
    printf("action: %s\n", action);
    printf("body:"); if (cbody) cbor_describe(cbody, stdout); else printf("\n");
    xAAL_free_msg(ctargets, source, dev_type, action, cbody);
  }
}
