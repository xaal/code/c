/* xAAL dummy dimmer lamp
 * (c) 2020 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>




#define ALIVE_PERIOD    60

/* setup lamp device info */
xAAL_devinfo_t lamp = { .dev_type	= "lamp.dimmer",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "IHSEV",
			.product_id	= "Dummy dimmer lamp",
			.hw_id		= NULL,
			.version	= "0.5",
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.schema		= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/lamp.dimmer",
			.info		= NULL,
			.unsupported_attributes = (char *[]){ "color_themperature", NULL },
			.unsupported_methods = (char *[]){ "set_color_themperature", NULL },
			.unsupported_notifications = NULL
		      };

xAAL_businfo_t bus;


void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &lamp) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


bool reply_get_attributes(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *lamp,
			 bool light, double brightness,
			 const uuid_t *target) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(2);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("light")), cbor_move(cbor_build_bool(light)) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("brightness")), cbor_move(xAAL_cbor_build_float(brightness)) });
  return xAAL_write_busl(bus, lamp, xAAL_REPLY, "get_attributes", cbody, target, NULL);
}


bool send_attribute_light(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *lamp,
			 bool light) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("light")), cbor_move(cbor_build_bool(light)) });
  return xAAL_write_busl(bus, lamp, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


bool send_attribute_brightness(const xAAL_businfo_t *bus,
			  const xAAL_devinfo_t *lamp,
			  double brightness) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("brightness")), cbor_move(xAAL_cbor_build_float(brightness)) });
  return xAAL_write_busl(bus, lamp, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


double get_brightness_request(cbor_item_t *cbody) {
  cbor_item_t *cbrightness;

  if (cbody) {
    cbrightness = xAAL_cbor_map_get(cbody, "brightness");
    switch (cbor_typeof(cbrightness)) {
    case CBOR_TYPE_UINT:	return cbor_get_int(cbrightness);
    case CBOR_TYPE_NEGINT:	return -cbor_get_int(cbrightness) - 1;
    case CBOR_TYPE_FLOAT_CTRL:	return cbor_float_get_float(cbrightness);
    default: return -1.0;
    }
  } else
    return -1.0;
}


void show_lamp(bool light, double brightness) {
  const char foo[] = "                    ";
  const char bar[] = "||||||||||||||||||||";
  int len = brightness/5;

  if (light)
    printf(" (O) On ");
  else
    printf("  .  Off");
  printf("  % 6.1f%% [%.*s%.*s] \r", brightness, len, bar, 20-len, foo);
  fflush(stdout);
}


/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  bool attribute_light = false;
  double attribute_brightness = 100.0;
  struct sigaction act_alarm;
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  xAAL_error_log = stderr;

  uuid_clear(lamp.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, lamp.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(lamp.addr);
	}
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }


  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(lamp.addr) ) {
    char str[37];
    uuid_generate(lamp.addr);
    uuid_unparse(lamp.addr, str);
    printf("Device: %s\n", str);
  }
  xAAL_add_wanted_target(&lamp.addr, &bus);


  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &lamp) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");


  /* Main loop */
  for (;;) {

    /* Show lamp */
    show_lamp(attribute_light, attribute_brightness);

    /* Recive a message */
    if (!xAAL_read_bus(&bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
      continue;

    if (msg_type == xAAL_REQUEST) {
      if ( (strcmp(action, "is_alive") == 0)
	  && xAAL_is_aliveDevType_match(cbody, lamp.dev_type) ) {
	if ( !xAAL_notify_alive(&bus, &lamp) )
	  fprintf(xAAL_error_log, "Could not reply to is_alive\n");

      } else if ( strcmp(action, "get_description") == 0 ) {
	if ( !xAAL_reply_get_description(&bus, &lamp, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_description\n");

      } else if ( strcmp(action, "get_attributes") == 0 ) {
	if ( !reply_get_attributes(&bus, &lamp, attribute_light, attribute_brightness, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

      } else if ( strcmp(action, "turn_on") == 0 ) {
	attribute_light = true;
	if ( !send_attribute_light(&bus, &lamp, attribute_light) )
	  fprintf(xAAL_error_log, "Could not send attributes_change\n");

      } else if ( strcmp(action, "turn_off") == 0 ) {
	attribute_light = false;
	if ( !send_attribute_light(&bus, &lamp, attribute_light) )
	  fprintf(xAAL_error_log, "Could not send attributes_change\n");

      } else if ( strcmp(action, "set_brightness") == 0 ) {
	double target = get_brightness_request(cbody);
	if ( target >= 0.0 && target <= 100.0 ) {
	  attribute_brightness = target;
	  if ( !send_attribute_brightness(&bus, &lamp, attribute_brightness) )
	    fprintf(xAAL_error_log, "Could not send attributes_change\n");

	}
      }
    }
    xAAL_free_msg(ctargets, source, dev_type, action, cbody);
  }
}
