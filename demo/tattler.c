/* Tattler
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Tattler - A dummy xAAL device that look at 'neighbors' and notify
 * when they arrive or leave the neighborhood...
 * Interesting neighbors may be a smartphone, tablet, TV, or any device
 * that may have an activity on the home network and that give an idea
 * about the presence or the activity of users at home. (This may be
 * useful for smart xAAL scenarios).
 * Those neighbors are identified by their Ethernet address.
 *
 * The best way to know about presence of device on the home network is
 * certainly to look at DHCP leases. However, there is no standard way to
 * query a dhcp server about its active leases.
 * A more obvious way is to set network interfaces in promiscuous, dump
 * traffic and look at Ethernet addresses in packets. The libpcap can do
 * that. However, this require privileged capabilities (CAP_NET_ADMIN,
 * CAP_NET_RAW).
 * Another approach could be to do some arp-ping periodically (ARP in IPv4
 * plus Neighbor Discovery Protocol for IPv6). Note that this also require
 * some privileged capabilities (at least CAP_NET_RAW).
 * The last but not the least way is to deal with the local neighbor table
 * maintained by the OS about its peers plus all arp queries seen on
 * the local link. One just have to query it. This is a little less
 * accurate, but it is reachable for a non-privileged application.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/timerfd.h>
#include <libmnl/libmnl.h>
#include <linux/rtnetlink.h>
#include <sys/queue.h>
#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>

#define ALIVE_PERIOD    120


/****************************/
/* Chapter "Data Structure" */
/****************************/

/**
 * The neighbor table of an OS is "IP" oriented (the dst attribute).  Given
 * an IP (v4/v6), what is the corresponding Ethernet address (link layer
 * addr)?  Remember that an Ethernet interface may have several IP (v4/v6).
 * Moreover, when a host become unreachable, the corresponding entry in the
 * neighbor table loose the lladdr (eth addr) for this dst (IP).
 *
 * The concern of the Tattler device is the reverse: the table is oriented
 * towards the Ethernet address of neighbor (which is a usable identifier).
 * One keep in mind associated IPs. This is why one have a list of lists.
 *
 * If an entry of the neigh table fall in 'stale' state, one can send an udp
 * probe to force an underlying arp request.  (The 'sendprobe' attribute in
 * the config file is the delay in seconds before sending those probes, or a
 * negative value for no probe).  This is globally fine (in the context of
 * simple devices plugged in a home network)...  but it's not perfect...  Do
 * not expect the moon.
 */

/* List of dst(s) for a neigh identified by its lladdr */
typedef LIST_HEAD(dsthead, dstentry) dsts_t;

typedef struct dstentry {
  size_t sz;
  unsigned char ip[16];
  LIST_ENTRY(dstentry) entries;
} dst_t;


/* List of neighbors */
typedef TAILQ_HEAD(neighead, neighentry) neighbors_t;

typedef struct neighentry {
  unsigned char lladdr[6];
  char *nickname;
  dsts_t dsts;
  bool reachable;
  int sendprobe;
  time_t timestamp;
  TAILQ_ENTRY(neighentry) entries;
} neigh_t;



/************************/
/* Chapter "UPD Probes" */
/************************/

/**
 *  Manage a list of udp-probes to send in batch mode at specific time
 */

/* List of udp-probes to send */
typedef CIRCLEQ_HEAD(probeshead, probeentry) probes_t;

typedef struct probeentry {
  size_t sz;
  unsigned char ip[16];
  int delay;
  CIRCLEQ_ENTRY(probeentry) entries;
} probe_t;


/* Synchronize timer on the next probe to send */
void adjust_timer(probes_t *probes, int timerfd) {
  struct itimerspec timerspec;
  int delay;

  if (probes->cqh_first != (void *)probes) /* Pickup first probe's delay*/
    delay = probes->cqh_first->delay;
  else /* Empty queue: unarms the timer. */
    delay = 0;

  timerspec.it_interval.tv_sec = 0;
  timerspec.it_interval.tv_nsec = 0;
  timerspec.it_value.tv_sec = delay;
  timerspec.it_value.tv_nsec = 0;
  if ( timerfd_settime(timerfd, 0, &timerspec, NULL) == -1 )
    fprintf(xAAL_error_log, "Could not adjust timer for probes: %s\n", strerror(errno));
}


/* Retrun how many seconds remain on this timer */
int remaining_timer(int timerfd) {
  struct itimerspec timerspec;

  if (timerfd_gettime(timerfd, &timerspec) == -1) {
    fprintf(xAAL_error_log, "timerfd_gettime: %s\n", strerror(errno));
    return 0;
  } else
    return timerspec.it_value.tv_sec;
}


/* Add a IP to the list of probes to send */
/* The list is sorted by 'delay'; try to maintain this */
void probes_queue_add(size_t sz, unsigned char *ip, int delay, probes_t *probes, int timerfd) {
  probe_t *probe, *np;
  int prev_delay = -1;

  /* Build probe data */
  probe = (probe_t *)malloc(sizeof(probe_t));
  probe->sz = sz;
  memcpy(probe->ip, ip, sz);
  probe->delay = delay;

  if (probes->cqh_first == (void *)probes) {
    /* The list of probes is empty */
    CIRCLEQ_INSERT_HEAD(probes, probe, entries);

  } else {
    /* Synchronize the delay of the new probe */
    probe->delay -= remaining_timer(timerfd);
    if (probe->delay < 0)
      probe->delay = 0;

    /* Insert the new probe at its place */
    CIRCLEQ_FOREACH(np, probes, entries) {
      if ( (probe->delay >= prev_delay) && (probe->delay < np->delay) ) {
	CIRCLEQ_INSERT_BEFORE(probes, np, probe, entries);
	break;
      }
      prev_delay = np->delay;
    }
  }

  /* One need to ajust timer if the probe was inserted at the head */
  adjust_timer(probes, timerfd);
}


/* Remove an IP from the list of probes */
void probes_queue_del(size_t sz, unsigned char *ip, probes_t *probes, int timerfd) {
  probe_t *np;

  /* Search this ip in the list of probes, then remove it */
  CIRCLEQ_FOREACH(np, probes, entries)
    if ( (np->sz == sz) && (memcmp(np->ip, ip, sz) == 0) ) {
      CIRCLEQ_REMOVE(probes, np, entries);
      free(np);
    }

  /* In case one removed the first probe, adjust timer */
  adjust_timer(probes, timerfd);
}


/* Send UDP echo request message (RFC862)
 * Even if one do not expect an answer this cause an ARP request */
void udp_probe(unsigned dst_sz, unsigned char *dst) {
  struct sockaddr_in sin;
  struct sockaddr_in6 sin6;
  int s;

  switch (dst_sz) {
    case 4:
      s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
      if (s == -1) {
	fprintf(xAAL_error_log, "udp socket: %s\n", strerror(errno));
	return;
      }
      sin.sin_family = AF_INET;
      sin.sin_port = htons(7);
      memcpy(&sin.sin_addr, dst, dst_sz);
      if (sendto(s, "\0", 1, 0, (struct sockaddr *)&sin, sizeof(sin)) == -1)
	fprintf(xAAL_error_log, "udp sendto: %s\n", strerror(errno));
      break;
    case 16:
      s = socket(PF_INET6, SOCK_DGRAM, IPPROTO_UDP);
      if (s == -1) {
	fprintf(xAAL_error_log, "udp socket: %s\n", strerror(errno));
	return;
      }
      sin6.sin6_family = AF_INET6;
      sin6.sin6_port = htons(7);
      sin6.sin6_flowinfo = 0;
      memcpy(&sin6.sin6_addr, dst, dst_sz);
      sin6.sin6_scope_id = 0;
      if (sendto(s, "\0", 1, 0, (struct sockaddr *)&sin6, sizeof(sin6)) == -1)
	fprintf(xAAL_error_log, "udp sendto: %s\n", strerror(errno));
      break;
  }
  close(s);
}


/* It's time to send a probe */
void probes_queue_run(probes_t *probes, int timerfd) {
  probe_t *np;
  int delay;

  if (probes->cqh_first == (void *)probes)
   return;  /* Empty queue, this should not happen */

  delay = probes->cqh_first->delay;
  CIRCLEQ_FOREACH(np, probes, entries) {
    np->delay -= delay;
    if (np->delay <= 0) {
      CIRCLEQ_REMOVE(probes, np, entries);
      udp_probe(np->sz, np->ip);
      free(np);
    }
  }

  /* In case one removed the first probe, adjust timer */
  adjust_timer(probes, timerfd);
}



/******************/
/* Chapter "xAAL" */
/******************/

/* Minimal cbor serialisation of a neigh_t */
cbor_item_t * neigh_to_cbor(neigh_t *neigh) {
  cbor_item_t *cneigh = cbor_new_indefinite_map();
  char eth[18];

  if (neigh) {
    sprintf(eth, "%02x:%02x:%02x:%02x:%02x:%02x", neigh->lladdr[0],
	    neigh->lladdr[1], neigh->lladdr[2], neigh->lladdr[3],
	    neigh->lladdr[4], neigh->lladdr[5]);
    (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("eth")), cbor_move(cbor_build_string(eth)) });
    (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("nickname")), cbor_move(cbor_build_string(neigh->nickname)) });
  }
  return cneigh;
}


/* Notify neighbor change on xAAL bus */
bool notify_attributes_change(const xAAL_businfo_t *bus, const xAAL_devinfo_t *tattler,
			     neigh_t *neigh) {
  cbor_item_t *cbody, *cneighbors, *cneigh;

  cneigh = neigh_to_cbor(neigh);
  (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("reachable")), cbor_move(cbor_build_bool(neigh->reachable)) });
  (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("timestamp")), cbor_move(cbor_build_string(ctime(&(neigh->timestamp)))) });

  cneighbors = cbor_new_definite_array(1);
  (void)!cbor_array_push(cneighbors, cbor_move(cneigh));

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("neighbors")), cbor_move(cneighbors) });

  return xAAL_write_bus(bus, tattler, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


/* Reply to get_attributes */
bool reply_get_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *tattler,
			 neighbors_t *neighbors, const uuid_t *target) {
  cbor_item_t *cbody, *cneighbors, *cneigh;
  neigh_t *neigh;

  cneighbors = cbor_new_indefinite_array();
  TAILQ_FOREACH(neigh, neighbors, entries) {
    cneigh = neigh_to_cbor(neigh);
    (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("reachable")), cbor_move(cbor_build_bool(neigh->reachable)) });
    (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("timestamp")), cbor_move(cbor_build_string(ctime(&(neigh->timestamp)))) });
    (void)!cbor_array_push(cneighbors, cbor_move(cneigh));
  }

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("neighbors")), cbor_move(cneighbors) });

  return xAAL_write_busl(bus, tattler, xAAL_REPLY, "get_attributes", cbody, target, NULL);
}


/* Reply to getNeighbor */
bool reply_getNeighbor(const xAAL_businfo_t *bus, const xAAL_devinfo_t *tattler,
		      neigh_t *neigh, const uuid_t *target) {
  cbor_item_t *cbody, *cneigh;

  cneigh = neigh_to_cbor(neigh);
  if (neigh) {
    (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("reachable")), cbor_move(cbor_build_bool(neigh->reachable)) });
    (void)!cbor_map_add(cneigh, (struct cbor_pair){ cbor_move(cbor_build_string("timestamp")), cbor_move(cbor_build_string(ctime(&(neigh->timestamp)))) });
  }

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("neighbor")), cbor_move(cneigh) });

  return xAAL_write_busl(bus, tattler, xAAL_REPLY, "getNeighbor", cbody, target, NULL);
}


/* Reply to addNeighbor */
bool reply_addNeighbor(const xAAL_businfo_t *bus, const xAAL_devinfo_t *tattler,
		      neigh_t *neigh, const uuid_t *target) {
  cbor_item_t *cbody, *cneigh;

  cneigh = neigh_to_cbor(neigh);
  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("neighbor")), cbor_move(cneigh) });

  return xAAL_write_busl(bus, tattler, xAAL_REPLY, "addNeighbor", cbody, target, NULL);
}


/* Reply to delNeighbor */
bool reply_delNeighbor(const xAAL_businfo_t *bus, const xAAL_devinfo_t *tattler,
		      neigh_t *neigh, const uuid_t *target) {
  cbor_item_t *cbody, *cneigh;

  cneigh = neigh_to_cbor(neigh);
  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("neighbor")), cbor_move(cneigh) });

  return xAAL_write_busl(bus, tattler, xAAL_REPLY, "delNeighbor", cbody, target, NULL);
}


/* Retrieve a neighbor in the list from eth and/or nickname provided in msg*/
neigh_t *retreive_neigh(cbor_item_t *cbody, neighbors_t *neighbors) {
  cbor_item_t *cneigh, *ceth, *cnickname;
  neigh_t *neigh;
  char *eth;
  const char *nickname;
  unsigned char lladdr[6];
  bool have_eth = false;
  size_t nickname_sz = 0;;

  cneigh = xAAL_cbor_map_get(cbody, "neighbor");
  if ( !cneigh || !cbor_isa_map(cneigh) )
    return NULL;

  ceth = xAAL_cbor_map_get(cneigh, "eth");
  if ( ceth && cbor_isa_string(ceth) && cbor_string_is_definite(ceth) ) {
    eth = strndup((const char*)cbor_string_handle(ceth), cbor_string_length(ceth));
    if (sscanf(eth, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", lladdr, lladdr + 1,
	       lladdr + 2, lladdr + 3, lladdr + 4, lladdr + 5) == 6)
      have_eth = true;
    free(eth);
  }

  cnickname = xAAL_cbor_map_get(cneigh, "nickname");
  if ( cnickname && cbor_isa_string(cnickname) && cbor_string_is_definite(cnickname) ) {
    nickname = (const char*)cbor_string_handle(cnickname);
    nickname_sz = cbor_string_length(cnickname);
  }

  if (!have_eth && !nickname_sz)
    return NULL;

  TAILQ_FOREACH(neigh, neighbors, entries) {
    if (have_eth && (memcmp(neigh->lladdr, lladdr, 6) == 0) ) {
      if (nickname_sz && strncmp(neigh->nickname, nickname, nickname_sz))
	return NULL;
      else
	return neigh;
    }
  }
  return NULL;
}


/* Add a neighbor (described in cbor) to the list */
neigh_t *add_neighbor(cbor_item_t *cneigh, neighbors_t *neighbors) {
  cbor_item_t *ceth, *cnickname, *cprobe;
  unsigned char lladdr[6];
  char *eth;
  const char *nickname;
  int sendprobe;
  neigh_t *neigh;
  size_t nickname_sz;

  /* Parse eth attribute */
  ceth = xAAL_cbor_map_get(cneigh, "eth");
  if ( !ceth || !cbor_isa_string(ceth) || !cbor_string_is_definite(ceth) ) {
    fprintf(xAAL_error_log, "Could not get 'eth' variable while adding a neighbor\n");
    return NULL;
  }

  eth = strndup((const char*)cbor_string_handle(ceth), cbor_string_length(ceth));
  if (sscanf(eth, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", lladdr, lladdr + 1,
	     lladdr + 2, lladdr + 3, lladdr + 4, lladdr + 5) != 6) {
    fprintf(xAAL_error_log, "Malformed ethernet address '%s' while adding a neighbor\n", eth);
    return NULL;
  }
  free(eth);

  /* Check if the eth address is already in our list, then return this one */
  TAILQ_FOREACH(neigh, neighbors, entries)
    if (memcmp(neigh->lladdr, lladdr, 6) == 0)
      return neigh;

  /* Get nickname */
  cnickname = xAAL_cbor_map_get(cneigh, "nickname");
  if ( !cnickname || !cbor_isa_string(cnickname) || !cbor_string_is_definite(cnickname) ) {
    fprintf(xAAL_error_log, "Could not get 'nickname' variable while adding a neighbour\n");
    nickname = "";
  } else {
    nickname = (const char*)cbor_string_handle(cnickname);
    nickname_sz = cbor_string_length(cnickname);
  }

  /* Get sendprobe */
  cprobe = xAAL_cbor_map_get(cneigh, "sendprobe");
  if ( !cprobe || !cbor_isa_uint(cprobe) ) {
    fprintf(xAAL_error_log, "Could not get 'sendprobe' variable while adding a neighbour\n");
    sendprobe = 120;
  } else
    sendprobe = cbor_get_int(cprobe);

  /* Build the neighbor entry */
  neigh = (neigh_t *) malloc(sizeof(neigh_t));
  memcpy(neigh->lladdr, lladdr, 6);
  LIST_INIT(&neigh->dsts);
  neigh->timestamp = 0;
  neigh->nickname = strndup(nickname, nickname_sz);
  neigh->reachable = false;
  neigh->sendprobe = sendprobe;
  TAILQ_INSERT_TAIL(neighbors, neigh, entries);

  return neigh;
}


/* Add a neighbor described within a msg */
neigh_t *add_neighbor_from_msg(cbor_item_t *cbody, neighbors_t *neighbors) {
  cbor_item_t *cneigh;

  cneigh = xAAL_cbor_map_get(cbody, "neighbor");
  if ( !cneigh || !cbor_isa_map(cneigh) )
    return NULL;

  return add_neighbor(cneigh, neighbors);
}


/* Manage received message */
void manage_msg(const xAAL_businfo_t *bus, const xAAL_devinfo_t *tattler,
		neighbors_t *neighbors) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (   (msg_type == xAAL_REQUEST)
      && xAAL_targets_match(ctargets, &tattler->addr) ) {

    if ( (strcmp(action, "is_alive") == 0)
	 && xAAL_is_aliveDevType_match(cbody, tattler->dev_type) ) {
      if ( !xAAL_notify_alive(bus, tattler) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, tattler, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      if ( !reply_get_attributes(bus, tattler, neighbors, source) )
	fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

    } else if ( strcmp(action, "addNeighbor") == 0 ) {
      if ( !reply_addNeighbor(bus, tattler,
			      add_neighbor_from_msg(cbody, neighbors), source) )
	fprintf(xAAL_error_log, "Could not reply to addNeighbor\n");

    } else if ( strcmp(action, "delNeighbor") == 0 ) {
      neigh_t *neigh = retreive_neigh(cbody, neighbors);
      if ( !reply_delNeighbor(bus, tattler, neigh, source) )
	fprintf(xAAL_error_log, "Could not reply to delNeighbor\n");
      if (neigh) {
	free(neigh->nickname);
	while (neigh->dsts.lh_first != NULL) {
	  LIST_REMOVE(neigh->dsts.lh_first, entries);
	  free(neigh->dsts.lh_first);
	}
	TAILQ_REMOVE(neighbors, neigh, entries);
	free(neigh);
      }

    } else if ( strcmp(action, "getNeighbor") == 0 ) {
      if ( !reply_getNeighbor(bus, tattler,
			      retreive_neigh(cbody, neighbors), source) )
	fprintf(xAAL_error_log, "Could not reply to getNeighbor\n");
    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/*****************************/
/* Chapter "Netlink Sockets" */
/*****************************/

/* Context data for mnl calls-back and others */
typedef struct {
  xAAL_businfo_t bus;
  xAAL_devinfo_t tattler;
  neighbors_t neighbors;
  probes_t probes;
  int timerfd;
} context_t;



/* Compare this ARP table entry with my list
 * Return the matching neighbor and dst if any */
bool watch_neighborhood(struct nlattr *tb[], neighbors_t *neighbors,
			neigh_t **neigh, dst_t **dst) {
  unsigned char *lladdr = NULL;
  unsigned char *dstadr = NULL;

  size_t dst_sz = 0;
  /* Pick up attributes */
  if (tb[NDA_LLADDR]
      && (mnl_attr_get_payload_len(tb[NDA_LLADDR]) == 6))
    lladdr = mnl_attr_get_payload(tb[NDA_LLADDR]);
  if (tb[NDA_DST]) {
    dstadr = mnl_attr_get_payload(tb[NDA_DST]);
    dst_sz = mnl_attr_get_payload_len(tb[NDA_DST]);
  }

  // Look at this lladdr (ethernet addr) in my list
  if (lladdr) {
    TAILQ_FOREACH(*neigh, neighbors, entries) {
      if (memcmp((*neigh)->lladdr, lladdr, 6) == 0) {	// Found this lladr in my list
	if (dstadr) {
	  // Look at this dst in the list of the neighbor
	  LIST_FOREACH(*dst, &((*neigh)->dsts), entries)
	    if ((*dst)->sz == dst_sz
		&& (memcmp((*dst)->ip, dstadr, dst_sz) == 0))
	      return true;	// Found this dst
	  // Could not find dst; update the list with it
	  *dst = (dst_t *) malloc(sizeof(dst_t));
	  (*dst)->sz = dst_sz;
	  memcpy((*dst)->ip, dstadr, dst_sz);
	  LIST_INSERT_HEAD(&(*neigh)->dsts, *dst, entries);
	  return true;
	} else
	  // An arp table without dst, this should not happen!
	  return false;
      }
    }

  } else if (dstadr) {
    // The arp table has a dst without lladdr: host unreachable
    // Look at all dsts of all neighs of my list
    TAILQ_FOREACH(*neigh, neighbors, entries)
      LIST_FOREACH(*dst, &((*neigh)->dsts), entries)
	if ((*dst)->sz == dst_sz && (memcmp((*dst)->ip, dstadr, dst_sz) == 0))
	  return true; // Found
  }
  // Not in my list
  return NULL;
}


/* Validate attr */
static int data_attr_cb(const struct nlattr *attr, void *data) {
  const struct nlattr **tb = data;
  int type = mnl_attr_get_type(attr);
  unsigned len = mnl_attr_get_payload_len(attr);

  /* skip unsupported attribute in user-space */
  if (mnl_attr_type_valid(attr, RTA_MAX) < 0)
    return MNL_CB_OK;

  /* only consider dst (ipv4/ipv6) and lladdr (ethernet addr) */
  switch (type) {
    case NDA_DST:
      if (len != 16 && len != 4) {
	return MNL_CB_ERROR;
      }
      break;
    case NDA_LLADDR:
      if (len != 6) {
	return MNL_CB_ERROR;
      }
      break;
  }

  tb[type] = attr;
  return MNL_CB_OK;
}


/* Parse attr of routing messages (neigh table) */
static int data_cb(const struct nlmsghdr *nlh, void *data) {
  context_t *context = data;
  struct nlattr *tb[RTA_MAX + 1] = { };
  struct ndmsg *nm = mnl_nlmsg_get_payload(nlh);
  neigh_t *neigh;
  dst_t *dst;
  bool state_chage = false;

  if (nm->ndm_state & (NUD_REACHABLE | NUD_STALE | NUD_FAILED)) {
    mnl_attr_parse(nlh, sizeof(*nm), data_attr_cb, tb);
    if (watch_neighborhood(tb, &(context->neighbors), &neigh, &dst)) {
      if ((nm->ndm_state & NUD_REACHABLE) == NUD_REACHABLE) {
	if (!neigh->reachable) {
	  neigh->reachable = true;
	  state_chage = true;
	  if (neigh->sendprobe > 0)
	    probes_queue_del(dst->sz, dst->ip, &(context->probes), context->timerfd);
	}
	neigh->timestamp = time(NULL);
      } else if ((nm->ndm_state & NUD_FAILED) == NUD_FAILED) {
	if (neigh->reachable) {
	  neigh->reachable = false;
	  state_chage = true;
	  if (neigh->sendprobe > 0)
	    probes_queue_del(dst->sz, dst->ip, &(context->probes), context->timerfd);
	}
	neigh->timestamp = time(NULL);
      } else if (neigh->sendprobe > 0)
	probes_queue_add(dst->sz, dst->ip, neigh->sendprobe, &(context->probes), context->timerfd);
      if (state_chage) {	// State change
	if (!notify_attributes_change(&(context->bus), &(context->tattler), neigh) )
	  fprintf(xAAL_error_log, "Could not notify attributes_change\n");
      }
    }
  }
  return MNL_CB_OK;
}



/*************************/
/* Chapter "Config file" */
/*************************/

/* Options read&written to conffile */
typedef struct {
  char *addr;
  char *port;
  int hops;
  char *passphrase;
  char *conffile;
  bool immutable;
  bool daemon;
  char *logfile;
  char *pidfile;
} options_t;


/* Minimal json serialisation of a neigh_t */
struct json_object * neigh_to_json(neigh_t *neigh) {
  struct json_object *jneigh = json_object_new_object();
  char eth[18];

  if (neigh) {
    sprintf(eth, "%02x:%02x:%02x:%02x:%02x:%02x", neigh->lladdr[0],
	    neigh->lladdr[1], neigh->lladdr[2], neigh->lladdr[3],
	    neigh->lladdr[4], neigh->lladdr[5]);
    json_object_object_add(jneigh, "eth", json_object_new_string(eth));
    json_object_object_add(jneigh, "nickname", json_object_new_string(neigh->nickname));
  }
  return jneigh;
}


/* Add a neighbor (described in json) to the list */
neigh_t *add_json_neighbor(struct json_object *jneigh, neighbors_t *neighbors) {
  struct json_object *jeth, *jnickname, *jprobe;
  unsigned char lladdr[6];
  const char *eth, *nickname;
  int sendprobe;
  neigh_t *neigh;

  /* Parse eth attribute */
  if (!json_object_object_get_ex(jneigh, "eth", &jeth)
     || !json_object_is_type(jeth, json_type_string)) {
    fprintf(xAAL_error_log, "Could not get 'eth' variable while adding a neighbor\n");
    return NULL;
  }

  eth = json_object_get_string(jeth);
  if (sscanf(eth, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", lladdr, lladdr + 1,
	     lladdr + 2, lladdr + 3, lladdr + 4, lladdr + 5) != 6) {
    fprintf(xAAL_error_log, "Malformed ethernet address '%s' while adding a neighbor\n", eth);
    return NULL;
  }

  /* Check if the eth address is already in our list, then return this one */
  TAILQ_FOREACH(neigh, neighbors, entries)
    if (memcmp(neigh->lladdr, lladdr, 6) == 0)
      return neigh;

  /* Get nickname */
  if (!json_object_object_get_ex(jneigh, "nickname", &jnickname)
     || !json_object_is_type(jnickname, json_type_string)) {
    fprintf(xAAL_error_log, "Could not get 'nickname' variable while adding a neighbour\n");
    nickname = "";
  } else
    nickname = json_object_get_string(jnickname);

  /* Get sendprobe */
  if (!json_object_object_get_ex(jneigh, "sendprobe", &jprobe)
     || !json_object_is_type(jprobe, json_type_int)) {
    fprintf(xAAL_error_log, "Could not get 'sendprobe' variable while adding a neighbour\n");
    sendprobe = 120;
  } else
    sendprobe = json_object_get_int(jprobe);

  /* Build the neighbor entry */
  neigh = (neigh_t *) malloc(sizeof(neigh_t));
  memcpy(neigh->lladdr, lladdr, 6);
  LIST_INIT(&neigh->dsts);
  neigh->timestamp = 0;
  neigh->nickname = strdup(nickname);
  neigh->reachable = false;
  neigh->sendprobe = sendprobe;
  TAILQ_INSERT_TAIL(neighbors, neigh, entries);

  return neigh;
}


/* Read a config file (json format); Build list of interesting neighbors */
bool read_config(context_t *ctx, options_t *opts) {
  struct json_object *jconf, *jaddr, *jport, *jhops, *jpassphrase, *juuid, *jconffile,
		     *jimmutable, *jdaemon, *jlogfile, *jpidfile, *jneighbors;
  int neighbors_len, i;

  /* read file */
  jconf = json_object_from_file(opts->conffile);
  if (json_object_is_type(jconf, json_type_null)) {
    fprintf(xAAL_error_log, "Could not parse config file %s\n", opts->conffile);
    return false;
  }

  /* parse bus addr */
  if (json_object_object_get_ex(jconf, "addr", &jaddr)
      && json_object_is_type(jaddr, json_type_string))
    opts->addr = strdup(json_object_get_string(jaddr));

  /* parse bus port */
  if (json_object_object_get_ex(jconf, "port", &jport)
      && json_object_is_type(jport, json_type_string))
    opts->port = strdup(json_object_get_string(jport));

  /* parse bus hops */
  if (json_object_object_get_ex(jconf, "hops", &jhops)
      && json_object_is_type(jhops, json_type_int))
    opts->hops = json_object_get_int(jhops);

  /* parse passphrase */
  if (json_object_object_get_ex(jconf, "passphrase", &jpassphrase)
      && json_object_is_type(jpassphrase, json_type_string))
    opts->passphrase = strdup(json_object_get_string(jpassphrase));

  /* parse tattler xAAL address (uuid) */
  if (json_object_object_get_ex(jconf, "uuid", &juuid)
      && json_object_is_type(juuid, json_type_string)) {
    if ( !uuid_parse(json_object_get_string(juuid), ctx->tattler.addr) )
      uuid_clear(ctx->tattler.addr);
  }

  /* parse config file name  */
  if (json_object_object_get_ex(jconf, "conffile", &jconffile)
      && json_object_is_type(jconffile, json_type_string))
    opts->conffile = strdup(json_object_get_string(jconffile));

  /* parse immutable flag  */
  if (json_object_object_get_ex(jconf, "immutable", &jimmutable)
      && json_object_is_type(jimmutable, json_type_boolean))
    opts->immutable = json_object_get_boolean(jimmutable);

  /* parse daemon flag  */
  if (json_object_object_get_ex(jconf, "daemon", &jdaemon)
      && json_object_is_type(jdaemon, json_type_boolean))
    opts->daemon = json_object_get_boolean(jdaemon);

  /* parse pid file name  */
  if (json_object_object_get_ex(jconf, "pidfile", &jpidfile)
      && json_object_is_type(jpidfile, json_type_string))
    opts->pidfile = strdup(json_object_get_string(jpidfile));

  /* parse log file name  */
  if (json_object_object_get_ex(jconf, "logfile", &jlogfile)
      && json_object_is_type(jlogfile, json_type_string))
    opts->logfile = strdup(json_object_get_string(jlogfile));


  /* parse neighbors config */
  if (!json_object_object_get_ex(jconf, "neighbors", &jneighbors)
      && !json_object_is_type(jneighbors, json_type_array)) {
    fprintf(xAAL_error_log, "Invalid conf file: no \"neighbors\" object\n");
    json_object_put(jconf);
    return false;
  }

  neighbors_len = json_object_array_length(jneighbors);
  for (i=0; i<neighbors_len; i++)
    add_json_neighbor(json_object_array_get_idx(jneighbors, i), &ctx->neighbors);

  json_object_put(jconf);
  return true;
}


/* Re-write config file (json format) */
bool write_config(context_t *ctx, options_t *opts) {
  struct json_object *jconf, *jneighbors, *jneigh;
  neigh_t *neigh;
  char uuid_str[37];

  jconf = json_object_new_object();
  json_object_object_add(jconf, "addr",      json_object_new_string(opts->addr));
  json_object_object_add(jconf, "port",      json_object_new_string(opts->port));
  json_object_object_add(jconf, "hops",      json_object_new_int(opts->hops));
  json_object_object_add(jconf, "passphrase",json_object_new_string(opts->passphrase));
  uuid_unparse(ctx->tattler.addr, uuid_str);
  json_object_object_add(jconf, "uuid",      json_object_new_string(uuid_str));
  json_object_object_add(jconf, "conffile",  json_object_new_string(opts->conffile));
  json_object_object_add(jconf, "immutable", json_object_new_boolean(opts->immutable));
  json_object_object_add(jconf, "daemon",    json_object_new_boolean(opts->daemon));
  if (opts->logfile)
    json_object_object_add(jconf, "logfile", json_object_new_string(opts->logfile));
  if (opts->pidfile)
    json_object_object_add(jconf, "pidfile", json_object_new_string(opts->pidfile));

  jneighbors = json_object_new_array();
  TAILQ_FOREACH(neigh, &(ctx->neighbors), entries) {
    jneigh = neigh_to_json(neigh);
    json_object_object_add(jneigh, "sendprobe", json_object_new_int(neigh->sendprobe));
    json_object_array_add(jneighbors, jneigh);
  }

  json_object_object_add(jconf, "neighbors", jneighbors);
  if (json_object_to_file_ext(opts->conffile, jconf, JSON_C_TO_STRING_PRETTY
			      | JSON_C_TO_STRING_SPACED) == -1) {
    fprintf(xAAL_error_log, "Writing config file: %s\n", strerror(errno));
    json_object_put(jconf);
    return false;
  }
  json_object_put(jconf);
  return true;
}



/*************************************/
/* Chapter "Main of the application" */
/*************************************/

/* Global variable for the handler */
context_t *ctx;
options_t *opts;

/* Called at exit */
void terminate() {
  if (!opts->immutable)
    write_config(ctx, opts);
  if (opts->pidfile)
    unlink(opts->pidfile);
}

/* Handler for Ctrl-C &co. */
void cancel(int s) {
  terminate();
  exit(EXIT_SUCCESS);
}


/* Main */
int main(int argc, char **argv) {
  context_t context;
  options_t options = { .addr=NULL, .port=NULL, .hops=-1, .passphrase=NULL,
		      .conffile="tattler.conf", .immutable=false,
		      .daemon=false, .logfile=NULL, .pidfile=NULL };
  struct mnl_socket *nl;
  int nlfd;
  char buf[MNL_SOCKET_BUFFER_SIZE];
  struct nlmsghdr *nlh;
  struct rtmsg *rtm;
  int ret = 0;
  unsigned int seq, portid;
  fd_set rfds, rfds_;
  int fd_max;
  int alive_fd;
  uint64_t exp;

  uuid_clear(context.tattler.addr);
  /* Parse cmdline arguments */
  {
    int opt;
    bool arg_error = false;

    while ((opt = getopt(argc, argv, "a:p:h:u:c:idl:P:s:")) != -1) {
      switch (opt) {
	case 'a':
	  options.addr = optarg;
	  break;
	case 'p':
	  options.port = optarg;
	  break;
	case 'h':
	  options.hops = atoi(optarg);
	  break;
	case 'u':
	  if ( !uuid_parse(optarg, context.tattler.addr) ) {
	    fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	    uuid_clear(context.tattler.addr);
	  }
	  break;
	case 'c':
	  options.conffile = optarg;
	  break;
	case 'i':
	  options.immutable = true;
	  break;
	case 'd':
	  options.daemon = true;
	  break;
	case 'l':
	  options.logfile = optarg;
	  break;
	case 'P':
	  options.pidfile = optarg;
	  break;
	case 's':
	  options.passphrase = optarg;
	  break;
	default: /* '?' */
	  arg_error = true;
      }
    }
    if (optind < argc) {
      fprintf(stderr, "Unknown argument %s\n", argv[optind]);
      arg_error = true;
    }
    if (arg_error) {
      fprintf(stderr, "Usage: %s [-a <addr>] [-p <port>] [-h <hops>] [-s <secret>]\n"
		  "		 [-u <uuid>] [-c <conffile>] [-i] [-d] [-l <logfile>] [-P <pidfile>]\n"
		  "-a <addr>	multicast IPv4 or IPv6 address of the xAAL bus\n"
		  "-p <port>	UDP port of the xAAL bus\n"
		  "-h <hops>	Hops limit for multicast packets\n"
		  "-s <secret>  Secret passphrase\n"
		  "-u <uuid>	xAAL address of the device; random by default\n"
		  "-c <conffile>	Filename of the configuration file (cson format)\n"
		  "		Use 'tattler.conf' by default\n"
		  "-i		Immutable config file (do not re-write it)\n"
		  "-d		Start as a daemon\n"
		  "-l <logfile>	Filename to write errors; stderr by default\n"
		  "-P <pidfile>	Filename to write pid; none by default\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  /* Setup 'context' data */
  TAILQ_INIT(&context.neighbors);
  CIRCLEQ_INIT(&context.probes);
  context.timerfd = timerfd_create(CLOCK_REALTIME, 0);
  if (context.timerfd == -1)
    perror("Could not create timer for probes");

  /* Load config */
  xAAL_error_log = stderr;
  if (!read_config(&context, &options))
    fprintf(stderr, "Error while reading config file.\n");

  /* Manage logfile */
  if (options.logfile) {
    xAAL_error_log = fopen(options.logfile, "a");
    if (xAAL_error_log == NULL) {
      perror("Opening logfile");
      xAAL_error_log = stderr;
    }
  } else
    xAAL_error_log = stderr;

  /* Join the xAAL bus */
  if ( !options.addr || !options.port || !options.passphrase) {
    fprintf(xAAL_error_log, "Please provide the address, the port and the passphrase of the xAAL bus.\n");
    exit(EXIT_FAILURE);
  } else if ( !xAAL_join_bus(options.addr, options.port, options.hops, 1, &context.bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  context.bus.maxAge = 2*60; /*seconds*/;
  context.bus.key = xAAL_pass2key(options.passphrase);
  if (context.bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(context.tattler.addr) ) {
    char str[37];
    uuid_generate(context.tattler.addr);
    uuid_unparse(context.tattler.addr, str);
    fprintf(xAAL_error_log, "Device: %s\n", str);
  }
  xAAL_add_wanted_target(&context.tattler.addr, &context.bus);

  /* Setup tattler device info */
  context.tattler.dev_type    = "notifier.experimental";
  context.tattler.alivemax   = 2 * ALIVE_PERIOD;
  context.tattler.vendor_id   = "IHSEV";
  context.tattler.product_id  = "Tattler";
  context.tattler.hw_id	     = NULL;
  context.tattler.version    = "0.4";
  context.tattler.group_id    = NULL;
  context.tattler.url        = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  context.tattler.url        = NULL;
  context.tattler.info	     = NULL;
  context.tattler.unsupported_attributes = NULL;
  context.tattler.unsupported_methods = NULL;
  context.tattler.unsupported_notifications = NULL;

  /* Start as a daemon */
  if (options.daemon && (daemon(1,1) == -1) )
    fprintf(xAAL_error_log, "daemon: %s\n", strerror(errno));

  /* Write pidfile */
  {
    FILE *pfile;

    if (options.pidfile) {
      pfile = fopen(options.pidfile, "w");
      if (pfile == NULL)
	fprintf(xAAL_error_log, "Opening pidfile: %s\n", strerror(errno));
      else {
	fprintf(pfile, "%d\n", getpid());
	fclose(pfile);
      }
    }
  }

  /* Manage Ctrl-C &co. */
  ctx = &context;
  opts = &options;
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);
  atexit(terminate);

  /**
   * Dump neigh table - Initial stage
   */

  nlh = mnl_nlmsg_put_header(buf);
  nlh->nlmsg_type = RTM_GETNEIGH;
  nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
  nlh->nlmsg_seq = seq = time(NULL);
  rtm = mnl_nlmsg_put_extra_header(nlh, sizeof(struct rtmsg));
  rtm->rtm_family = AF_UNSPEC;
  nl = mnl_socket_open(NETLINK_ROUTE);
  if (nl == NULL) {
    fprintf(xAAL_error_log, "mnl_socket_open: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0) {
    fprintf(xAAL_error_log, "mnl_socket_bind: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  portid = mnl_socket_get_portid(nl);
  if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0) {
    fprintf(xAAL_error_log, "mnl_socket_send: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  ret = mnl_socket_recvfrom(nl, buf, sizeof(buf));
  while (ret > 0) {
    ret = mnl_cb_run(buf, ret, seq, portid, data_cb, &context);
    if (ret <= MNL_CB_STOP)
      break;
    ret = mnl_socket_recvfrom(nl, buf, sizeof(buf));
  }
  if (ret == -1) {
    fprintf(xAAL_error_log, "error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  mnl_socket_close(nl);
  /* End of neigh table dump */



  /**
   * Now wait for event from neighbor table - Ongoing stage
   */

  /* netlink socket */
  nl = mnl_socket_open(NETLINK_ROUTE);
  if (nl == NULL) {
    fprintf(xAAL_error_log, "mnl_socket_open: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  if (mnl_socket_bind(nl, RTM_GETNEIGH, MNL_SOCKET_AUTOPID) < 0) {
    fprintf(xAAL_error_log, "mnl_socket_bind: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  nlfd = mnl_socket_get_fd(nl);

  /* Set alive timer */
  {
    struct itimerspec timerspec;

    alive_fd = timerfd_create(CLOCK_REALTIME, 0);
    if (alive_fd == -1)
      perror("Could not create timer for alive messages:");
    timerspec.it_interval.tv_sec = ALIVE_PERIOD;
    timerspec.it_interval.tv_nsec = 0;
    timerspec.it_value.tv_sec = 0;
    timerspec.it_value.tv_nsec = 1;
    if ( timerfd_settime(alive_fd, 0, &timerspec, NULL) == -1 )
      fprintf(xAAL_error_log, "Could not configure timer for alive messages:: %s\n", strerror(errno));
  }

  /* prepare the fd set for the following select */
  FD_ZERO(&rfds);
  FD_SET(context.timerfd, &rfds);
  fd_max = context.timerfd;
  FD_SET(nlfd, &rfds);
  fd_max = (fd_max > nlfd)? fd_max : nlfd;
  FD_SET(context.bus.sfd, &rfds);
  fd_max = (fd_max > context.bus.sfd)? fd_max : context.bus.sfd;
  FD_SET(alive_fd, &rfds);
  fd_max = (fd_max > alive_fd)? fd_max : alive_fd;

  /* main loop */
  for (;;) {

    rfds_ = rfds;
    if ( (select(fd_max+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select: %s\n", strerror(errno));

    /* An event from the neighbor table */
    if (FD_ISSET(nlfd, &rfds_)) {
      ret = mnl_socket_recvfrom(nl, buf, sizeof(buf));
      if (ret > 0)
	ret = mnl_cb_run(buf, ret, 0, 0, data_cb, &context);
      if (ret <= MNL_CB_STOP)
	break;
    }

    /* It's time to send a probe */
    if (FD_ISSET(context.timerfd, &rfds_)) {
      if ( read(context.timerfd, &exp, sizeof(uint64_t)) == -1 )
	fprintf(xAAL_error_log, "Probe timer: %s\n", strerror(errno));
      probes_queue_run(&context.probes, context.timerfd);
    }

    /* An xAAL message from the bus */
    if (FD_ISSET(context.bus.sfd, &rfds_))
      manage_msg(&context.bus, &context.tattler, &context.neighbors);

    /* It's time to send an alive on the xAAL bus */
    if (FD_ISSET(alive_fd, &rfds_)) {
      if ( read(alive_fd, &exp, sizeof(uint64_t)) == -1 )
	fprintf(xAAL_error_log, "Alive timer: %s\n", strerror(errno));
      if ( !xAAL_notify_alive(&context.bus, &context.tattler) )
	fprintf(xAAL_error_log, "Could not send alive notification\n");
    }

  }
  if (ret == -1) {
    fprintf(xAAL_error_log, "error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  /* ...dead code... for now */
  mnl_socket_close(nl);
  return EXIT_SUCCESS;
}
