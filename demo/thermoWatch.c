/* Thermo Watch - Collect values from xAAL thermometers
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <termios.h>

#include <sys/queue.h>
#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>



typedef LIST_HEAD(listhead, entry) thermometers_t;
typedef struct entry {
  uuid_t addr;
  char *type;
  double t;
  time_t timeout;
  time_t timestamp;
  LIST_ENTRY(entry) entries;
} thermometer_t;


/* Some global variables nedded for the sig handler */
xAAL_businfo_t bus;
xAAL_devinfo_t me;


#define ALIVE_PERIOD    60

/* SIGALRM handler that sends alive messages */
void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* Send is_alive request */
/* Return true if success */
bool request_is_alive(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me) {
  cbor_item_t *cbody, *cdev_types;

  cdev_types = cbor_new_definite_array(1);
  (void)!cbor_array_push(cdev_types, cbor_move(cbor_build_string("thermometer.any")));

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("dev_types")), cbor_move(cdev_types) });

  return xAAL_write_busl(bus, me, xAAL_REQUEST, "is_alive", cbody, &xAAL_is_alive_target, NULL);
}


/* Send 'get_attributes' request */
/* Return true if success */
bool send_get_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
			const uuid_t *target) {
  return xAAL_write_busl(bus, me, xAAL_REQUEST, "get_attributes", NULL, target, NULL);
}


/* Extract temperature from messages */
/* Return -273.15 in case of error */
double extract_temperature(cbor_item_t *cbody) {
  cbor_item_t *ctemperature;

  if (cbody) {
    ctemperature = xAAL_cbor_map_get(cbody, "temperature");
    return cbor_float_get_float(ctemperature);
  } else
    return -273.15;
}


/* Manage received message */
void manage_msg(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, thermometers_t *thermometers) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  thermometer_t *np;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (   (msg_type == xAAL_REQUEST)
      && xAAL_targets_match((const cbor_item_t *)ctargets, &me->addr) ){

    if ( (strcmp(action, "is_alive") == 0)
	 && xAAL_is_aliveDevType_match(cbody, me->dev_type) ) {
      if ( !xAAL_notify_alive(bus, me) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, me, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      /* I have no attributes */

    }

  } else if (strncmp(dev_type, "thermometer.", strlen("thermometer.")) == 0) {
    if ( msg_type == xAAL_REPLY ) {
      if ( strcmp(action, "get_attributes") == 0 ) {
	bool exists = false;
	LIST_FOREACH(np, thermometers, entries)
	  if ( uuid_compare(np->addr, *source) == 0 ) {
	    np->t = extract_temperature(cbody);
	    np->timestamp = time(NULL);
	    exists = true;
	    break;
	  }
	if (!exists) {
	  np = malloc(sizeof(thermometer_t));
	  uuid_copy(np->addr, *source);
	  np->type = strdup(dev_type);
	  np->timeout = 0;
	  np->t = extract_temperature(cbody);
	  np->timestamp = time(NULL);
	  LIST_INSERT_HEAD(thermometers, np, entries);
	}
      }

    } else if ( msg_type == xAAL_NOTIFY ) {
      if ( strcmp(action, "alive") == 0 ) {
	/* A thermometer is alive */
	bool exists = false;
	LIST_FOREACH(np, thermometers, entries)
	  if ( uuid_compare(np->addr, *source) == 0 ) {
	    np->timeout = xAAL_read_aliveTimeout(cbody);
	    exists = true;
	    break;
	  }
	if (!exists) {
	  np = malloc(sizeof(thermometer_t));
	  uuid_copy(np->addr, *source);
	  np->type = strdup(dev_type);
	  np->timeout = xAAL_read_aliveTimeout(cbody);
	  np->t = -273.15;
	  np->timestamp = 0;
	  LIST_INSERT_HEAD(thermometers, np, entries);
	  if ( !send_get_attributes(bus, me, source) )
	    fprintf(xAAL_error_log, "Could not send get_attributes\n");
	}

      } else if ( strcmp(action, "attributes_change") == 0 ) {
	bool exists = false;
	LIST_FOREACH(np, thermometers, entries)
	  if ( uuid_compare(np->addr, *source) == 0 ) {
	    np->t = extract_temperature(cbody);
	    np->timestamp = time(NULL);
	    exists = true;
	    break;
	  }
	if (!exists) {
	  np = malloc(sizeof(thermometer_t));
	  uuid_copy(np->addr, *source);
	  np->type = strdup(dev_type);
	  np->timeout = 0;
	  np->t = extract_temperature(cbody);
	  np->timestamp = time(NULL);
	  LIST_INSERT_HEAD(thermometers, np, entries);
	}
      }

    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/* Manage silent stdin */
struct termios initial_term;

void setsilent() {
  struct termios term;
  if ( tcgetattr(STDIN_FILENO, &initial_term) == -1) {
    perror("tcgetattr");
    return;
  }
  term = initial_term;
  term.c_lflag &= ~(ICANON | ECHO);
  term.c_cc[VMIN] = 1;
  term.c_cc[VTIME] = 0;
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &term) == -1)
    perror("tcsetattr");
}

void restore_term() {
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &initial_term) == -1)
    perror("tcsetattr");
}



/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  struct sigaction act_alarm;
  fd_set rfds, rfds_;
  char c;
  thermometers_t thermometers; /* List of detected thermometers */
  thermometer_t *np;

  uuid_clear(me.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:u:s:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 'u':
	if ( uuid_parse(optarg, me.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(me.addr);
	}
	break;
      case 's':
	passphrase = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (addr==NULL || port==NULL || arg_error || passphrase==NULL) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  xAAL_error_log = stderr;
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(me.addr) ) {
    char str[37];
    uuid_generate(me.addr);
    uuid_unparse(me.addr, str);
    printf("Device: %s\n", str);
  }

  /* Setup 'me' device info */
  me.dev_type    = "hmi.basic";
  me.alivemax   = 2 * ALIVE_PERIOD;
  me.vendor_id   = "IHSEV";
  me.product_id  = "Thermometers Watcher";
  me.hw_id	= NULL;
  me.version    = "0.5";
  me.group_id    = NULL;
  me.url        = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  me.schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/hmi.basic";
  me.info	= NULL;
  me.unsupported_attributes = NULL;
  me.unsupported_methods = NULL;
  me.unsupported_notifications = NULL;

  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND | SA_RESTART;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");

  if ( !request_is_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send is_alive request.\n");


  LIST_INIT(&thermometers);

  setsilent();
  atexit(restore_term);
  printf("Press a key to show thermometers values\n(Or 'G' to send get_attributes requests to them)\n");

  FD_ZERO(&rfds);
  FD_SET(STDIN_FILENO, &rfds);
  FD_SET(bus.sfd, &rfds);

  /* Main loop */
  for (;;) {

    rfds_ = rfds;
    if ( (select(bus.sfd+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(STDIN_FILENO, &rfds_)) {
      /* User wake up */
      switch ( read(STDIN_FILENO, &c, 1) ) {
      case -1:
	fprintf(xAAL_error_log, "read/stdin: %s\n", strerror(errno));
	break;
      case 0:
	exit(EXIT_SUCCESS);
	break;
      }
      printf("Detected thermometers:\n");
      LIST_FOREACH(np, &thermometers, entries)
	if ( np->timeout && (time(NULL) > np->timeout) ) {
	  LIST_REMOVE(np, entries);
	  free(np->addr);
	  free(np->type);
	  free(np);
	} else {
	  char str[37];
	  uuid_unparse(np->addr, str);
	  printf("%s %+3.2lf°C at %s", str, np->t, ctime(&np->timestamp));
	  if ( (c == 'G') && !send_get_attributes(&bus, &me, &np->addr) )
	    fprintf(xAAL_error_log, "Could not send get_attributes\n");
	}
      printf("\n");

    } else if (FD_ISSET(bus.sfd, &rfds_))
      manage_msg(&bus, &me, &thermometers);

  }
}
