/* xAAL bus monitoring
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <limits.h>
#include <float.h>
#include <errno.h>

#include <sys/queue.h>
#include <cbor.h>

#include <xaal.h>



/*
 * list of detected devices
 */
typedef LIST_HEAD(listhead, entry) seen_devs_t;
typedef struct entry {
  uuid_t addr;
  char *type;
  char *ip;
  unsigned long long bytes, msgs;
  LIST_ENTRY(entry) entries;
} seen_dev_t;


seen_dev_t *append_dev(seen_devs_t *seen_devs, const uuid_t *addr, const char *dev_type) {
  seen_dev_t *np = NULL;

  LIST_FOREACH(np, seen_devs, entries)
    if ( uuid_compare(np->addr, *addr) == 0 )
      return np;

  np = (seen_dev_t*) malloc( sizeof(seen_dev_t) );
  uuid_copy(np->addr, *addr);
  np->type = strdup(dev_type);
  np->bytes = 0;
  np->msgs = 0;
  np->ip = NULL;
  LIST_INSERT_HEAD(seen_devs, np, entries);
  return np;
}


/*
 * global variables
 * functions on global variables
 */

struct timeval start;
seen_devs_t *seen_devs = NULL;
unsigned long long bytes_count;
unsigned long long msgs_count;

void init_data() {
  seen_dev_t *np;

  bytes_count = 0;
  msgs_count = 0;

  if (seen_devs)
    LIST_FOREACH(np, seen_devs, entries) {
      LIST_REMOVE(np, entries);
      free(np->type);
      free(np->ip);
      free(np);
    }
  else {
    seen_devs = (seen_devs_t *) malloc( sizeof(seen_devs_t) );
    LIST_INIT(seen_devs);
  }

  gettimeofday(&start, NULL);
  printf("Initialization %s\n", ctime(&(start.tv_sec)));
}


void print_report(int s) {
  seen_dev_t *np;
  struct timeval now, elapsed;
  unsigned long sec, min, hr, day, t, elapsed_sec;
  double bw, bw_dev;
  char addr[37];

  gettimeofday(&now, NULL);
  printf("%s", ctime(&(now.tv_sec)));
  timersub(&now, &start, &elapsed);

  t = elapsed.tv_sec;
  sec = t % 60;
  t /= 60;
  min = t % 60;
  t /= 60;
  hr = t % 24;
  day = t / 24;
  printf("uptime: %lu days %lu hours %lu minutes %lu seconds\n",  day, hr, min, sec);

  elapsed_sec = elapsed.tv_sec + elapsed.tv_usec*1E-6;
  bw = (double)(bytes_count) / (double)(elapsed_sec);
  printf("bandwidth: %.3g kB/s %.3g msg/s (%llu messages, mean size: %.3gB)\n", bw/1024, (double)(msgs_count)/elapsed_sec, msgs_count, (double)(bytes_count)/msgs_count);

  LIST_FOREACH(np, seen_devs, entries) {
    bw_dev = (double)(np->bytes) / (double)(elapsed.tv_sec + elapsed.tv_usec*1E-6);
    uuid_unparse(np->addr, addr);
    printf("%s %s %s bw:%.3g%% count:%.3g%%\n", addr, np->ip, np->type, bw_dev/bw*100.0, (double)(np->msgs)/(double)(msgs_count)*100.0);
  }
  printf("\n");

  switch (s) {
    case SIGHUP:
    case SIGINT:
      exit(EXIT_SUCCESS);
    case SIGALRM:
      alarm(3600);
    default:
      signal(s, print_report);
  }
}


int main(int argc, char **argv) {
  xAAL_businfo_t bus;
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  ssize_t msglen;
  char *ip;
  seen_dev_t *seen_dev;

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret>\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  xAAL_error_log = stderr;
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  init_data();

  signal(SIGHUP, print_report);
  signal(SIGINT, print_report);
  signal(SIGUSR1, print_report);
  signal(SIGUSR2, print_report);
  signal(SIGALRM, print_report);
  alarm(3600);

  for (;;) {
    /* Recive a message */
    if (!xAAL_read_bus(&bus, &ctargets, &source, &dev_type, &msg_type, &action,
		       &cbody, &msglen, &ip))
      continue;

    if (bytes_count > (ULLONG_MAX-msglen)) {
      init_data();
      continue;
    }

    bytes_count += msglen;
    msgs_count++;

    seen_dev = append_dev(seen_devs, source, dev_type);
    seen_dev->bytes += msglen;
    seen_dev->msgs++;
    if ( (seen_dev->ip == NULL) && (ip) )
      seen_dev->ip = strdup(ip);

    print_report(SIGUSR1);

    xAAL_free_msg(ctargets, source, dev_type, action, cbody);
  }
}
