/* xAAL in Natural Command
 *   Interpret a command in natural language (e.g., French)
 *   and execute the corresponding xAAL action.
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <libintl.h>
#include <json-c/json.h>

#include <fstrcmp.h>

#include <sys/queue.h>

#include "natural_cmd.h"
#include "tts.h"

#define MAJORDOM	"majordome"



/*
 * Dictionnary
 */

void add_word(words_t *words, const char *word) {
  word_t *wd = (word_t *)malloc(sizeof(word_t));
  wd->word = strdup(word);
  LIST_INSERT_HEAD(words, wd, entries);
}

translate_t *add_translate(translates_t *trs, char *target, nat_t nature) {
  translate_t *tr = (translate_t *)malloc(sizeof(translate_t));
  tr->target = strdup(target);
  tr->nature = nature;
  LIST_INIT(&(tr->sources));
  LIST_INSERT_HEAD(trs, tr, entries);
  return tr;
}


void load_dictionnary(const char *dictionnary, translates_t *trs) {
  struct json_object *jdic = json_object_from_file(dictionnary);
  struct json_object *jsources, *jnature;
  const char *s_nature;
  nat_t nature;
  int i, len;
  translate_t *tr;


  if ( !json_object_is_type(jdic, json_type_object))
    return;

  json_object_object_foreach(jdic, target, jtranslate) {
    if ( json_object_object_get_ex(jtranslate, "nature", &jnature)
	&& json_object_is_type(jnature, json_type_string)
	&& json_object_object_get_ex(jtranslate, "sources", &jsources)
	&& json_object_is_type(jsources, json_type_array) ) {

      s_nature = json_object_get_string(jnature);
      if (strcmp(s_nature, "dev_type")==0)
	nature = nat_dev_type;
      else if (strcmp(s_nature, "method")==0)
	nature = nat_method;
      else if (strcmp(s_nature, "tag")==0)
	nature = nat_tag;
      else
	continue;

      len = json_object_array_length(jsources);
      if (len == 0 )
	continue;

      tr = add_translate(trs, target, nature);

      for (i = 0; i < len; i++)
	 add_word(&(tr->sources), json_object_get_string( json_object_array_get_idx(jsources, i) ) );
    }
  }
}



/*
 * Chapter Searches in Dictionnary
 */

void add_seach_word(search_words_t *swds, char *word, translate_t *tr, double score) {
  search_word_t *swd = (search_word_t *)malloc(sizeof(search_word_t));
  swd->word = word;
  swd->translated = tr;
  swd->score = score;
  LIST_INSERT_HEAD(swds, swd, entries);
}

search_token_t *add_search_token(search_tokens_t *stoks, char *tok) {
  search_token_t *token = (search_token_t *)malloc(sizeof(search_token_t));
  token->tok = tok;
  LIST_INIT(&(token->candidats));
  LIST_INSERT_HEAD(stoks, token, entries);
  return token;
}

/* Compute distance (score) from tok to each word of the dic */
void search_tok_in_dic(search_tokens_t *stoks, translates_t *dic, char *tok) {
  search_token_t *token = add_search_token(stoks, tok);
  translate_t *tr;
  word_t *wd;

  LIST_FOREACH(tr, dic, entries)
    LIST_FOREACH(wd, &(tr->sources), entries)
      add_seach_word(&(token->candidats), wd->word, tr, fstrcmp(wd->word, tok));
}


void free_search_tokens(search_tokens_t *stoks) {
  search_token_t *token;
  search_word_t *swd;

  LIST_FOREACH(token, stoks, entries) {
    LIST_FOREACH(swd, &(token->candidats), entries) {
      LIST_REMOVE(swd, entries);
      free(swd);
    }
    LIST_REMOVE(token, entries);
    free(token);
  }
  free(stoks);
}


/* Compute score for each tok of the sentence */
search_tokens_t *search_sentence_in_dic(char *sentence, translates_t *dic) {
  search_tokens_t *stoks = (search_tokens_t *)malloc(sizeof(search_tokens_t));
  char *saveptr;
  char *tok = strtok_r(sentence, " \n", &saveptr);

  LIST_INIT(stoks);

  if (!tok || strcasecmp(tok, MAJORDOM)!=0) {
    fprintf(stderr, "Error: The command does not start with '%s'\n", MAJORDOM);
    free(stoks);
    return NULL;
  }

  while ((tok = strtok_r(NULL, " \n", &saveptr)))
    search_tok_in_dic(stoks, dic, tok);

  return stoks;
}


double get_score(search_tokens_t *stoks, char *word) {
  search_token_t *stok;
  search_word_t *swd;
  double score = 0;

  LIST_FOREACH(stok, stoks, entries)
    LIST_FOREACH(swd, &(stok->candidats), entries)
      if ( (strcmp(swd->translated->target, word)==0)
	   && (swd->score > score) )
	score = swd->score;
  return score;
}


void print_stoks(search_tokens_t *stoks) {
  search_token_t *stok;
  search_word_t *swd;

  LIST_FOREACH(stok, stoks, entries) {
    printf("%s:\n", stok->tok);
    LIST_FOREACH(swd, &(stok->candidats), entries)
      printf("  %s [%f] -> %s\n", swd->word, swd->score, swd->translated->target);
  }
}




/*
 * Expected Commands
 */

void add_cmd(cmds_t *cmds, xaal_cmd_t xaal_cmd, char *word1, ...) {
  cmd_t *cmd = (cmd_t *)malloc(sizeof(cmd_t));
  va_list ap;
  char *word = word1;

  cmd->xaal_cmd = xaal_cmd;
  LIST_INIT(&(cmd->words));
  va_start(ap, word1);
  while (word != NULL) {
    add_word(&(cmd->words), word);
    word = va_arg(ap, char*);
  }
  va_end(ap);
  LIST_INSERT_HEAD(cmds, cmd, entries);
}



/* Build the list of best tags (i.e. values) translating tokens of the initial sentence */
void best_tags(search_tokens_t *stoks, vals_t *vals) {
  search_token_t *stock;
  search_word_t *sword;
  char *best_tag;
  double best_score;

  LIST_FOREACH(stock, stoks, entries) {
    best_score = 0.0;
    LIST_FOREACH(sword, &(stock->candidats), entries) {
      if (    (sword->translated->nature == nat_tag)
	   && (sword->score > best_score) ) {
	best_score = sword->score;
	best_tag = sword->translated->target;
      }
    }
    if (best_score >= FSTRCMP_THRESHOLD) {
      insert_value(vals, best_tag);
    }
  }
}


void match_stocks_cmd(search_tokens_t *stoks, cmds_t *cmds, xaal_ctx_t *xaal_ctx) {
  cmd_t *cmd, *best_cmd;
  word_t *word;
  double cmd_score, best_score = 0;
  vals_t vals;

  LIST_FOREACH(cmd, cmds, entries) {
    cmd_score = 1.0;
    LIST_FOREACH(word, &(cmd->words), entries)
      cmd_score *= get_score(stoks, word->word);
    if (cmd_score > best_score) {
      best_score = cmd_score;
      best_cmd = cmd;
    }
  }

  if (best_score>=FSTRCMP_THRESHOLD) {
    TAILQ_INIT(&vals);
    best_tags(stoks, &vals);
    // printf("tags: "); print_vals(&vals);
    tts_speak(gettext("I have understood: "));
    (best_cmd->xaal_cmd)(xaal_ctx, &(best_cmd->words), &vals);
    delete_vals(&vals);
  } else
    tts_speak(gettext("Sorry, I did not understand.\n"));
}
