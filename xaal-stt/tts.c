/* (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <espeak/speak_lib.h>
#include <string.h>
#include <stdio.h>


void tts_init_fr() {
  espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
  espeak_SetVoiceByName("mb-fr1");
  espeak_SetParameter(espeakRATE,   120, 0);
  espeak_SetParameter(espeakVOLUME, 100, 0);
  espeak_SetParameter(espeakPITCH,   40, 0);
  espeak_SetParameter(espeakWORDGAP,  0,0);
}


void tts_init_en() {
  espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
  espeak_SetVoiceByName("mb-en1");
  espeak_SetParameter(espeakRATE,   120, 0);
  espeak_SetParameter(espeakVOLUME, 100, 0);
  espeak_SetParameter(espeakPITCH,   40, 0);
  espeak_SetParameter(espeakWORDGAP,  0, 0);
}


void tts_speak(char *str) {
  printf("%s\n", str);
  espeak_Synth(str, strlen(str)+1, 0, POS_CHARACTER, 0, espeakCHARS_AUTO, NULL, NULL);
  //espeak_Synchronize();
}
