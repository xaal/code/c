/* majordom
 *   Process sentences recognized by pocketsphinx
 *   and execute the corresponding xAAL action.
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <locale.h>
#include <libintl.h>

#include <sphinxbase/cmd_ln.h>
#include <sphinxbase/err.h>


#include "agent.h"
#include "natural_cmd.h"
#include "tts.h"
#include <xaal.h>
#include <uuid/uuid.h>
#include <signal.h>


/* i18n */
#define PACKAGE         "majordom"
#define LOCALEDIR       "."


/* needed global variable */
xaal_ctx_t *p_xaal_ctx;

/* Called at exit */
void terminate() {
  dump_db(p_xaal_ctx);
}

/* Handler for Ctrl-C &co. */
void cancel(int s) {
  exit(EXIT_SUCCESS);
}


/*
 * Majordom init
 */
void majordom_init(cmd_ln_t *config, translates_t *dic, cmds_t *cmds, xaal_ctx_t *xaal_ctx) {
  pthread_t thread;
  char *locale;

  LIST_INIT(dic);
  load_dictionnary(cmd_ln_str_r(config, "-translatedic"), dic);

  LIST_INIT(cmds);
  populate_cmds(cmds);

  /* i18n */
  locale = setlocale(LC_ALL, "");
  bindtextdomain(PACKAGE, LOCALEDIR);
  textdomain(PACKAGE);

  if (locale && strncmp(locale, "fr", 2)==0)
    tts_init_fr();
  else
    tts_init_en();

  xAAL_error_log = err_get_logfp();
//  err_set_debug_level(0); // ???

  /* parse the keynames argument */
  {
    char *str, *token;
    unsigned nb = 0;

    xaal_ctx->keylist = NULL;
    for (str = strdup(cmd_ln_str_r(config, "-translatedic")); (token = strtok(str, ", ")); str = NULL) {
      xaal_ctx->keylist = realloc(xaal_ctx->keylist, sizeof(char*)*(nb++));
      xaal_ctx->keylist[nb-1] = token;
    }
    xaal_ctx->keylist = realloc(xaal_ctx->keylist, sizeof(char*)*(nb));
    xaal_ctx->keylist[nb] = NULL;
  }

  if ( !xAAL_join_bus( cmd_ln_str_r(config, "-address"), cmd_ln_str_r(config, "-port"),
		       cmd_ln_int32_r(config, "-hops"), 1, &(xaal_ctx->bus)) )
    E_FATAL("Could not join xAAL bus.");

   /* Setup security of the bus */
  xaal_ctx->bus.maxAge = 2*60; /*seconds*/;
  xaal_ctx->bus.key = xAAL_pass2key(cmd_ln_str_r(config, "-secret"));
  if (xaal_ctx->bus.key == NULL)
    E_FATAL("Could not compute key from passphrase");

  /* Build device uuid addr if needed */
  if ( uuid_parse(cmd_ln_str_r(config, "-uuid"), xaal_ctx->majordom.addr) != 0 ) {
    char uuid[37];
    uuid_generate(xaal_ctx->majordom.addr);
    uuid_unparse(xaal_ctx->majordom.addr, uuid);
    printf("Device: %s\n", uuid);
  }
  xAAL_add_wanted_target(&(xaal_ctx->majordom.addr), &(xaal_ctx->bus));

  xaal_ctx->majordom.dev_type	= "hmi.basic";
  xaal_ctx->majordom.alivemax	= 0;
  xaal_ctx->majordom.vendor_id	= "IHSEV";
  xaal_ctx->majordom.product_id	= "Majordom - xAAL Speech to Text";
  xaal_ctx->majordom.hw_id	= NULL;
  xaal_ctx->majordom.version	= "0.4";
  xaal_ctx->majordom.group_id	= NULL;
  xaal_ctx->majordom.url	= "http://recherche.imt-atlantique.fr/xaal/documentation/";
  xaal_ctx->majordom.schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/hmi.basic";
  xaal_ctx->majordom.info	= NULL;
  xaal_ctx->majordom.unsupported_attributes = NULL;
  xaal_ctx->majordom.unsupported_methods = NULL;
  xaal_ctx->majordom.unsupported_notifications = NULL;

  xaal_ctx->dbfile = cmd_ln_str_r(config, "-dbfile");
  load_db(xaal_ctx);

  p_xaal_ctx = xaal_ctx;
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);
  signal(SIGUSR1, cancel);
  atexit(terminate);

  pthread_create(&thread, NULL, xaal_agent, xaal_ctx);
}


/*
 * Majordom taks
 * Interpret natural commands
 */
void majordomus(const char *hyp, translates_t *dic, cmds_t *cmds, xaal_ctx_t *xaal_ctx) {
  char *cmd;
  search_tokens_t *stoks;

  if (strlen(hyp)) {
    cmd = strdup(hyp);
    // printf("----\nInput: %s\n", cmd);
    stoks = search_sentence_in_dic(cmd, dic);
    if (stoks) {
      // print_stoks(stoks);
      match_stocks_cmd(stoks, cmds, xaal_ctx);
      free_search_tokens(stoks);
    }
    free(cmd);
  }
}
