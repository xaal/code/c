/* db - Database of xAAL devices
 * Part of the 'majordom' software
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef AGENT
#define AGENT

#include <stdbool.h>
#include <sys/queue.h>
#include <cbor.h>
#include <xaal.h>


/*
 * Section Definition of Data Structures
 */

typedef TAILQ_HEAD(valshead, valentry) vals_t;
typedef struct valentry {
  char *value;
  TAILQ_ENTRY(valentry) entries;
} val_t;


typedef TAILQ_HEAD(kvshead, kventry) kvs_t;
typedef struct kventry {
  char *key, *value;
  TAILQ_ENTRY(kventry) entries;
} kv_t;


typedef TAILQ_HEAD(deviceshead, deviceentry) devices_t;
typedef struct deviceentry {
  uuid_t addr;
  char *dev_type;
  time_t timeout;
  kvs_t kvs;
  TAILQ_ENTRY(deviceentry) entries;
} device_t;


/* xAAL Context */
typedef struct {
  xAAL_businfo_t bus;
  xAAL_devinfo_t majordom;
  devices_t devices;
  pthread_rwlock_t rwlock;
  const char *dbfile;
  const char **keylist;
} xaal_ctx_t;



/*
 * Section Keys-Values of a Device
 */


/* insert a value in the list */
void insert_value(vals_t *vals, const char *value);

/* delete a value in the list */
void delete_value(vals_t *vals, const char *value);

/* delete the list of values */
void delete_vals(vals_t *vals);

void print_vals(vals_t *vals);


/* insert a kv in the list */
void insert_kv(kvs_t *kvs, const char *key, const char *value);

/* delete a kv in the list */
void delete_kv(kvs_t *kvs, const char *key);

/* delete the list of kvs */
void delete_kvs(kvs_t *kvs);

void print_kvs(kvs_t *kvs);

/* check if a value is in the list of keys-values */
bool check_value(kvs_t *kvs, const char *value);

/* check if all values of subset are presents into a kv overset */
bool matching_values(kvs_t *overset, vals_t *subset);

/* get kvs of a device */
cbor_item_t *get_kvs(device_t *device);

/* get kvs of a device by addr */
cbor_item_t *get_kvs_by_addr(devices_t *devices, const uuid_t *addr);

/* update the list of kvs (merge) with values of wanted keys */
void update_kvs(kvs_t *kvs, const char **keylist, cbor_item_t *cmap);



/*
 * Section Devices
 */

/* validate the name of a dev_type */
bool validate_dev_type(const char *dev_type);

/* select a device */
device_t *select_device(devices_t *devices, const uuid_t *addr);

/* select or insert a device (its addr) */
device_t *add_device(devices_t *devices, const uuid_t *addr);

/* update a device */
void update_device(devices_t *devices, const char **keylist, cbor_item_t *cdevice);

/* search device having given values */
cbor_item_t *matching_device(devices_t *devices, const char *dev_type, vals_t *vals);

/* serialize a device into json */
cbor_item_t *get_device(device_t *device);

/* delete a device */
void delete_device(devices_t * devices, device_t *device);



/*
 * Section Database Itself
 */

/* read-write mutex */
void rwinit(xaal_ctx_t *xaal_ctx);
void wrlock(xaal_ctx_t *xaal_ctx);
void rdlock(xaal_ctx_t *xaal_ctx);
void unlock(xaal_ctx_t *xaal_ctx);
void rwdestroy(xaal_ctx_t *xaal_ctx);

/* Init DB and load a file (if any) */
void load_db(xaal_ctx_t *xaal_ctx);

/* Dump DB to a file */
void dump_db(xaal_ctx_t *xaal_ctx);



/*
 * xAAL threads
 */

void *xaal_agent(void *data);

#endif
