/* xAAL in Natural Command
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This piece of code contains hardcoded stuff relating to:
 * - French language
 * - xAAL commands to be expected
 *
 * Edit this according to your needs.
 */


#include <stdio.h>
#include <libintl.h>

#include "natural_cmd.h"
#include "tts.h"
#include "xaal.h"


/* Switch on lamps */
void xaal_light_on(xaal_ctx_t *xaal_ctx, words_t *cmd_words, vals_t *vals) {
  cbor_item_t *ctargets = matching_device(&(xaal_ctx->devices), "lamp.", vals);

  if (cbor_array_size(ctargets)) {
    tts_speak(gettext("I switch on the lights.\n"));
    xAAL_write_bus(&(xaal_ctx->bus), &(xaal_ctx->majordom), xAAL_REQUEST, "on", NULL, ctargets);
  } else
    tts_speak(gettext("Unfortunately I haven't found which lamp to switch on.\n"));
}


/* Switch off lamps */
void xaal_light_off(xaal_ctx_t *xaal_ctx, words_t *cmd_words, vals_t *vals) {
  cbor_item_t *ctargets = matching_device(&(xaal_ctx->devices), "lamp.", vals);

  if (cbor_array_size(ctargets)) {
    tts_speak(gettext("I switch off the lights.\n"));
    xAAL_write_bus(&(xaal_ctx->bus), &(xaal_ctx->majordom), xAAL_REQUEST, "off", NULL, ctargets);
  } else
    tts_speak(gettext("Unfortunately I haven't found which lamp to switch off.\n"));
}


/* Move up shutters */
void xaal_shutter_up(xaal_ctx_t *xaal_ctx, words_t *cmd_words, vals_t *vals) {
  cbor_item_t *ctargets = matching_device(&(xaal_ctx->devices), "shutter.", vals);

  if (cbor_array_size(ctargets)) {
    tts_speak(gettext("I open the shutters.\n"));
    xAAL_write_bus(&(xaal_ctx->bus), &(xaal_ctx->majordom), xAAL_REQUEST, "up", NULL, ctargets);
  } else
    tts_speak(gettext("Unfortunately I haven't found which shutter to open.\n"));
}


/* Move down shutters */
void xaal_shutter_down(xaal_ctx_t *xaal_ctx, words_t *cmd_words, vals_t *vals) {
  cbor_item_t *ctargets = matching_device(&(xaal_ctx->devices), "shutter.", vals);

  if (cbor_array_size(ctargets)) {
    tts_speak(gettext("I close the shutters.\n"));
    xAAL_write_bus(&(xaal_ctx->bus), &(xaal_ctx->majordom), xAAL_REQUEST, "down", NULL, ctargets);
  } else
    tts_speak(gettext("Unfortunately I haven't found which shutter to close.\n"));
}


/* Stop shutters */
void xaal_shutter_stop(xaal_ctx_t *xaal_ctx, words_t *cmd_words, vals_t *vals) {
  cbor_item_t *ctargets = matching_device(&(xaal_ctx->devices), "shutter.", vals);

  if (cbor_array_size(ctargets)) {
    tts_speak(gettext("I stop the shutter.\n"));
    xAAL_write_bus(&(xaal_ctx->bus), &(xaal_ctx->majordom), xAAL_REQUEST, "stop", NULL, ctargets);
  } else
    tts_speak(gettext("Unfortunately I haven't found which shutter to stop.\n"));
}


/* Hardcoded list of expected commands */
void populate_cmds(cmds_t *cmds) {
  add_cmd(cmds, xaal_light_off, "lamp.any", "off", NULL);
  add_cmd(cmds, xaal_light_on, "lamp.any", "on", NULL);

  add_cmd(cmds, xaal_shutter_up,   "shutter.any", "up", NULL);
  add_cmd(cmds, xaal_shutter_down, "shutter.any", "down", NULL);
  add_cmd(cmds, xaal_shutter_stop, "shutter.any", "stop", NULL);
}
