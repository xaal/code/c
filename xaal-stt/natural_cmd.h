/* xAAL in Natural Command
 *   Interpret a command in natural language (e.g., French)
 *   and execute the corresponding xAAL action.
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NATURAL_COMMANDER
#define NATURAL_COMMANDER

#include <sys/queue.h>

#include "agent.h"

/*
 * Dictionnary
 */

/* List of words */
typedef LIST_HEAD(wordshead, wordentry) words_t;
typedef struct wordentry {
  char *word;
  LIST_ENTRY(wordentry) entries;
} word_t;

void add_word(words_t *words, const char *word);


/* List of translations: a target word corresponding to a list of source words */
typedef enum { nat_dev_type, nat_method, nat_tag } nat_t;

typedef LIST_HEAD(translateshead, transentry) translates_t;
typedef struct transentry {
  char *target;
  nat_t nature;
  words_t sources;
  LIST_ENTRY(transentry) entries;
} translate_t;


translate_t *add_translate(translates_t *trs, char *target, nat_t nature);

void load_dictionnary(const char *dictionnary, translates_t *trs);



/*
 * Chapter Searches in Dictionnary
 */

/* A word in dictionnary, its translation, its maching score */
typedef LIST_HEAD(swdshead, swdentry) search_words_t;
typedef struct swdentry {
  char *word;
  translate_t *translated;
  double score;
  LIST_ENTRY(swdentry) entries;
} search_word_t;


void add_seach_word(search_words_t *swds, char *word, translate_t *tr, double score);


/* For each token of the initial sentence, the 'distance'(score) to words of the dictionnary */
typedef LIST_HEAD(stokshead, stokentry) search_tokens_t;
typedef struct stokentry {
  char *tok;
  search_words_t candidats;
  LIST_ENTRY(stokentry) entries;
} search_token_t;


search_token_t *add_search_token(search_tokens_t *stoks, char *tok);


/* Compute distance (score) from tok to each word of the dic */
void search_tok_in_dic(search_tokens_t *stoks, translates_t *dic, char *tok);


void free_search_tokens(search_tokens_t *stoks);


/* Compute score for each tok of the sentence */
search_tokens_t *search_sentence_in_dic(char *sentence, translates_t *dic);

void print_stoks(search_tokens_t *stoks);




/*
 * Expected Commands
 */


/* Definition of xAAL commands */
typedef void (*xaal_cmd_t)(xaal_ctx_t *xaal_ctx, words_t *cmd_words, vals_t *vals);


/* List of expected commands in the target langage */
typedef LIST_HEAD(cmdshead, cmdentry) cmds_t;
typedef struct cmdentry {
  xaal_cmd_t xaal_cmd;
  words_t words;
  LIST_ENTRY(cmdentry) entries;
} cmd_t;


void add_cmd(cmds_t *cmd, xaal_cmd_t xaal_cmd, char *word1, ...);


/* Hardcoded list of expected commands */
void populate_cmds(cmds_t *cmds);


void match_stocks_cmd(search_tokens_t *stoks, cmds_t *cmds, xaal_ctx_t *xaal_ctx);


#endif
