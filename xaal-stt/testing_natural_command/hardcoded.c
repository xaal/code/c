/* xAAL in Natural Command
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This piece of code contains hardcoded stuff relating to:
 * - French language
 * - xAAL commands to be expected
 *
 * Edit this according to your needs.
 */


#include <stdio.h>

#include "natural_cmd.h"

/* Hardcoded dictionnary of translations */
void populate_dic(translates_t *dic) {
  translate_t *tr;

  tr = add_translate(dic, "lamp.*");
  add_word(&(tr->sources), "lampe");
  add_word(&(tr->sources), "lumière");

  tr = add_translate(dic, "on");
  add_word(&(tr->sources), "allume");

  tr = add_translate(dic, "off");
  add_word(&(tr->sources), "éteins");
}



/* Switch on lamps */
void xaal_light_on(xaal_ctx_t *xaal_ctx, words_t *words) {
  word_t *word;
  printf("Switch on lamps ( ");
  LIST_FOREACH(word, words, entries)
    printf("%s ", word->word);
  printf(")\n");
}


/* Switch off lamps */
void xaal_light_off(xaal_ctx_t *xaal_ctx, words_t *words) {
  word_t *word;
  printf("Switch off lamps ( ");
  LIST_FOREACH(word, words, entries)
    printf("%s ", word->word);
  printf(")\n");
}



/* Hardcoded list of expected commands */
void populate_cmds(cmds_t *cmds) {
  add_cmd(cmds, xaal_light_off, "lamp.*", "off", NULL);
  add_cmd(cmds, xaal_light_on, "lamp.*", "on", NULL);
}
