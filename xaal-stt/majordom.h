/* majordom
 *   Process sentences recognized by pocketsphinx
 *   and execute the corresponding xAAL action.
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAJORDOM
#define MAJORDOM

#include "agent.h"
#include "natural_cmd.h"


/*
 * Majordom init
 */
void majordom_init(cmd_ln_t *config, translates_t *dic, cmds_t *cmds, xaal_ctx_t *xaal_ctx);


/*
 * Majordom taks
 * Interpret natural commands
 */
void majordomus(const char *hyp, translates_t *dic, cmds_t *cmds, xaal_ctx_t *xaal_ctx);

#endif
