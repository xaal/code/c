
#include <stdio.h>
#include <string.h>
#include <assert.h>


#include <sphinxbase/err.h>
#include <sphinxbase/ad.h>

#include "pocketsphinx.h"


#include "majordom.h"


static const arg_t cont_args_def[] = {
    POCKETSPHINX_OPTIONS,
    /* Argument file. */
    {"-argfile",
     ARG_STRING,
     NULL,
     "Argument file giving extra arguments."},
    {"-adcdev",
     ARG_STRING,
     NULL,
     "Name of audio device to use for input."},
    {"-infile",
     ARG_STRING,
     NULL,
     "Audio file to transcribe."},
    {"-inmic",
     ARG_BOOLEAN,
     "no",
     "Transcribe audio from microphone."},
    {"-time",
     ARG_BOOLEAN,
     "no",
     "Print word times in file transcription."},
    {"-address",
     ARG_STRING,
     "224.0.29.200",
     "IP multicast address of the xAAL bus." },
    {"-port",
     ARG_STRING,
     "1234",
     "UDP port of the xAAL bus." },
    {"-hops",
     ARG_INT32,
     "-1",
     "IP packets hops count of the xAAL bus."},
    {"-secret",
     ARG_STRING,
     "",
     "Secret passphrase the xAAL bus."},
    {"-uuid",
     ARG_STRING,
     "ddbaccfd-fa43-43c7-9eb3-6a21379590a5",
     "xAAL address (uuid) of the majordom device."},
    {"-dbfile",
     ARG_STRING,
     "majordom.db",
     "Database of xAAL devices discoverd by majordom."},
    {"-translatedic",
     ARG_STRING,
     "translate.dic",
     "Dictionnary of transaltion from natural commands to xAAL."},
    CMDLN_EMPTY_OPTION
};

static cmd_ln_t *config;



int main(int argc, char *argv[]) {
  translates_t dictionnary;
  cmds_t cmds;
  xaal_ctx_t xaal_ctx;
  char *hyp;
  size_t n;

  config = cmd_ln_parse_r(NULL, cont_args_def, argc, argv, TRUE);
	
  majordom_init(config, &dictionnary, &cmds, &xaal_ctx);

  for (;;) {
    hyp = NULL; n = 0;
    if (getline(&hyp, &n, stdin)<=1)
      break;

    printf("%s\n", hyp);
    majordomus(hyp, &dictionnary, &cmds, &xaal_ctx);
  }
  return 0;
}
