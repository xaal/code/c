#!/bin/bash

ADDR="224.0.29.200"
PORT="1234"
SECRET="my_secret"


METADATA=7db980ca-5b4a-475e-9fce-7ea36a093680

LAMPE_CUISINE=438e2c05-42f2-402b-b5b3-52db031d2ea2
LAMPE_SALON=3de17f84-e08a-4444-9e0c-38d372f0025b
LAMPE_WC=51711a83-b766-44f0-b500-63752ed3756b
LAMPE_ENTREE=bc400652-c820-4aef-a9eb-fe06e4f0965d

VOLET_CUISINE=21dac201-5a46-48f2-a0e1-24f83e077061
VOLET_WC=9d7151f6-16fe-4454-9663-1e2005cdb28e

xfce4-terminal \
        -e "bash -c \"LD_LIBRARY_PATH+=:. ../libxaal_sec/metadatadb   -a $ADDR -p $PORT -s $SECRET -u $METADATA\""	-T "MetaData DB" \
  --tab -e "bash -c \"LD_LIBRARY_PATH+=:. ../libxaal_sec/dummyDimmer  -a $ADDR -p $PORT -s $SECRET -u $LAMPE_CUISINE\""	-T "Lampe cuisine" \
  --tab -e "bash -c \"LD_LIBRARY_PATH+=:. ../libxaal_sec/dummyDimmer  -a $ADDR -p $PORT -s $SECRET -u $LAMPE_SALON\""	-T "Lampe salon" \
  --tab -e "bash -c \"LD_LIBRARY_PATH+=:. ../libxaal_sec/dummyDimmer  -a $ADDR -p $PORT -s $SECRET -u $LAMPE_WC\""	-T "Lampe wc" \
  --tab -e "bash -c \"LD_LIBRARY_PATH+=:. ../libxaal_sec/dummyDimmer  -a $ADDR -p $PORT -s $SECRET -u $LAMPE_ENTREE\""	-T "Lampe entrée" \
  --tab -e "bash -c \"LD_LIBRARY_PATH+=:. ../libxaal_sec/dummyShutter -a $ADDR -p $PORT -s $SECRET -u $VOLET_CUISINE\""	-T "Volet cuisine" \
  --tab -e "bash -c \"LD_LIBRARY_PATH+=:. ../libxaal_sec/dummyShutter -a $ADDR -p $PORT -s $SECRET -u $VOLET_WC\""	-T "Volet WC"

sleep 1

cat << EOF | LD_LIBRARY_PATH+=:. ../baseservices/updateKV -a "$ADDR" -p "$PORT" -s "$SECRET"
$LAMPE_CUISINE	location cuisine
$LAMPE_SALON	location salon
$LAMPE_WC	location wc
$LAMPE_ENTREE	location entree
$VOLET_CUISINE	location cuisine
$VOLET_WC	location wc
EOF
