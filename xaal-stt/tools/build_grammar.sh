#!/bin/bash

function build_corpus() {
  A=( "majordome" )
  B=( "allume" "éteins" )
  C=( "la lumière" "la lampe" )
  D=( "" "de la cuisine" "du salon" "des wc" "des toilettes" \
	"de l'entrée" "<unk>" )

  for a in "${A[@]}"; do
    for b in "${B[@]}"; do
      for c in "${C[@]}"; do
        for d in "${D[@]}"; do
	  echo "<s> $a $b $c $d </s>"
        done
      done
    done
  done
}

function build_corpus2() {
  A=( "majordome" )
  B=( "monte" "ouvre" "relève" "descends" "ferme" "referme" "baisse" \
	"arrête" "stoppe" )
  C=( "le volet" "le rideau" "le store" )
  D=( "" "de la cuisine" \
 	"des wc" "des toilettes" "de la douche" \
 	"de l'entrée" "<unk>" )

  for a in "${A[@]}"; do
    for b in "${B[@]}"; do
      for c in "${C[@]}"; do
        for d in "${D[@]}"; do
	  echo "<s> $a $b $c $d </s>"
        done
      done
    done
  done
}


function build_vocab_idngram_lm() {
  if [ ! -s "$1.txt" ] ; then
    echo "Error: no corpus file $1.txt" >&2
    exit -1
  fi
 
  text2wfreq < $1.txt | wfreq2vocab > $1.vocab
  text2idngram -vocab $1.vocab -idngram $1.idngram < $1.txt
  idngram2lm -vocab_type 0 -idngram $1.idngram -vocab $1.vocab -arpa $1.lm
}



function build_dic() {
  if [ ! -s "$1.vocab" ] ; then
    echo "Error: no vocabulary file $1.vocab" >&2
    exit -1
  fi
  if [ ! -s "$2" ] ; then
    echo "Error: no reference dictionnary $2" >&2
    exit -1
  fi

  grep -v '^#' $1.vocab | while read WORD ; do
    if ! grep -e "^$WORD " -e "^$WORD(" $2 ; then
      echo >&2
      echo "Unknown word: $WORD" >&2
      if grep -v '<' <<< "$WORD" &>/dev/null ; then
        echo "You may arrange something like:" >&2
        agrep -2 "^$WORD " $2 | head -5 >&2
      fi
    fi
  done > $1.dic
}

function patch_dic_majordome() {
  cat << EOF >> $1.dic
éteins ai tt in
l'entrée ll an tt rr ei
wc(3) vv ei ss ei
descends dd ai ss an
EOF
}



CORPUS="corpus"
DIC_REF=/usr/share/pocketsphinx/model/lm/fr_FR/frenchWords62K.dic

build_corpus > $CORPUS.txt
build_corpus2 >> $CORPUS.txt
build_vocab_idngram_lm $CORPUS
build_dic $CORPUS $DIC_REF
patch_dic_majordome $CORPUS

exit 0

echo "<s> majordome </s>" > keyword.txt
build_vocab_idngram_lm keyword
build_dic keyword $DIC_REF
