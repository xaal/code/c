#!/bin/bash

if [ "$#" -ne 1 -a "$#" -ne 2 ]; then
  echo "Usage: $0 <corpus.txt> [<wave files prefix>]"
  exit -1
fi

sed -e "s,<s> ,,g;s, </s>,,g;s,<unk>,,g" $1 | while read TEXT; do
  let n++
  WAVE="`printf "$2%0.4d.wav" $n`"
  curl -L -G "http://voxygen.fr/sites/all/modules/voxygen_voices/assets/proxy/index.php?method=redirect&voice=Damien&ts=$(date +%s)" --data-urlencode "text=$TEXT" | mpg123 --mono --rate 16000 -w $WAVE -
done
