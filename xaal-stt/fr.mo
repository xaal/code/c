��          �      �       0     1     H     \     r     �     �     �  8   �  7     6   I  5   �  5   �  �  �     {     �     �     �     �     �     �  <     :   \  7   �  7   �  9                     	                
                        I close the shutters.
 I have understood:  I open the shutters.
 I stop the shutter.
 I switch off the lights.
 I switch on the lights.
 Sorry, I did not understand.
 Unfortunately I haven't found which lamp to switch off.
 Unfortunately I haven't found which lamp to switch on.
 Unfortunately I haven't found which shutter to close.
 Unfortunately I haven't found which shutter to open.
 Unfortunately I haven't found which shutter to stop.
 Project-Id-Version: majordom 1
Report-Msgid-Bugs-To: christophe.lohr@telecom-bretagne.eu
POT-Creation-Date: 2015-05-16 15:57+0200
PO-Revision-Date: 2015-05-16 15:57+0200
Last-Translator: clohr <christophe.lohr@telecom-bretagne.eu>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Je ferme les volets.
 J'ai compris:  J'ouvre les volets.
 J'arrête les volets.
 J'éteins les lumières.
 J'allume les lumières.
 Désolé, je n'ai pas compris.
 Malheureusement je n'ai pas trouvé quelle lampe éteindre.
 Malheureusement je n'ai pas trouvé quelle lampe allumer.
 Malheureusement je n'ai pas trouvé quel volet fermer.
 Malheureusement je n'ai pas trouvé quel volet ouvrir.
 Malheureusement je n'ai pas trouvé quel volet arrêter.
 