/* xAAL generic-sender
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>


#define ALIVE_PERIOD    60

/* setup device info */
xAAL_devinfo_t device = { .dev_type	= "basic.basic",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "IHSEV",
			.product_id	= "Dummy generic-sender device",
			.version	= "0.4",
			.hw_id		= NULL,
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.schema		= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/basic.basic",
			.info		= NULL,
			.unsupported_attributes = NULL,
			.unsupported_methods = NULL,
			.unsupported_notifications = NULL
		      };

char *pipe_name = "generic-sender.pipe";
int fd_pipe = -1;

xAAL_businfo_t bus;



void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &device) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


bool reply_get_attributes(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *device,
			 const uuid_t *target,
			 cbor_item_t *attribute,
			 time_t date) {
  cbor_item_t *cbody;

  cbody = cbor_incref(attribute);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("date")),  cbor_move(xAAL_cbor_build_int(date)) });
  return xAAL_write_busl(bus, device, xAAL_REPLY, "get_attributes", cbody, target, NULL);
}


bool send_attribute(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *device,
			 cbor_item_t *attribute) {
  cbor_item_t *cbody;

  cbody = cbor_incref(attribute);
  return xAAL_write_bus(bus, device, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}



/* Manage received xAAL message */
void manage_msg(const xAAL_businfo_t *bus, xAAL_devinfo_t *device, cbor_item_t *attribute, time_t date) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  /* Recive a message */
  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (msg_type == xAAL_REQUEST) {
    if ( (strcmp(action, "is_alive") == 0)
	&& xAAL_is_aliveDevType_match(cbody, device->dev_type) ) {
      if ( !xAAL_notify_alive(bus, device) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, device, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      if ( !reply_get_attributes(bus, device, source, attribute, date) )
	fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}


/* Manage received message on the pipe */
void manage_pipe(int fd_pipe, const xAAL_businfo_t *bus, xAAL_devinfo_t *device, cbor_item_t *attribute, time_t *date) {
  unsigned char buf[xAAL_BUF_SIZE];
  ssize_t nread;
  struct cbor_load_result cresult;
  cbor_item_t *cpipe;

  nread = read(fd_pipe, buf, xAAL_BUF_SIZE);
  if (nread == -1)
    return;

  /* Use the json-c library */
  cpipe = cbor_load(buf, nread, &cresult);
  if ( !cpipe ) {
    fprintf(xAAL_error_log,  "CBOR error; code %d, position %lu\n", cresult.error.code, cresult.error.position);
    return;
  }

  if ( cbor_isa_map(cpipe) ) {
    cbor_decref(&attribute);
    attribute = cbor_incref(cpipe);
    time(date);
    if (!send_attribute(bus, device, attribute))
      fprintf(xAAL_error_log, "Could not send attributes_change\n");
  }
  cbor_decref(&cpipe);
}


/* Called at exit */
void terminate() {
  close(fd_pipe);
  unlink(pipe_name);
}

/* Handler for Ctrl-C &co. */
void cancel(int s) {
  terminate();
  exit(EXIT_SUCCESS);
}



/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  cbor_item_t *attribute = cbor_new_indefinite_map();
  time_t date;
  struct sigaction act_alarm;
  fd_set rfds, rfds_;
  int fd_max;

  xAAL_error_log = stderr;

  uuid_clear(device.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:P:D:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, device.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(device.addr);
	}
	break;
      case 'P':
	pipe_name = optarg;
	break;
      case 'D':
	device.dev_type = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] [-P <pipe name>] [-D <dev_type>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Make named pipe */
  if (mkfifo(pipe_name, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP) == -1) {
    perror("mkfifo");
    exit(EXIT_FAILURE);
  }
  atexit(terminate);
  fd_pipe = open(pipe_name, O_RDWR);
  if (fd_pipe < 0) {
    perror("open pipe");
    exit(EXIT_FAILURE);
  }

  /* Manage Ctrl-C &co. */
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);

  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(device.addr) ) {
    char uuid[37];
    uuid_generate(device.addr);
    uuid_unparse(device.addr, uuid);
    printf("Device: %s\n", uuid);
  }
  xAAL_add_wanted_target(&device.addr, &bus);


  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &device) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");

  FD_ZERO(&rfds);
  FD_SET(fd_pipe, &rfds);
  fd_max = fd_pipe;
  FD_SET(bus.sfd, &rfds);
  fd_max = (fd_max > bus.sfd)? fd_max : bus.sfd;

  /* Main loop */
  for (;;) {
    rfds_ = rfds;
    if ( (select(fd_max+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(bus.sfd, &rfds_))
      manage_msg(&bus, &device, attribute, date);

    if (FD_ISSET(fd_pipe, &rfds_))
      manage_pipe(fd_pipe, &bus, &device, attribute, &date);
  }
}
