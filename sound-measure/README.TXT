# Sound Pressure Level Sensor


## Objective
The purpose here is to illustrate xAAL schemas dedicated to sensors measuring something that evolve "a lot" and potentially frequently.
As a proof of concept I choose to measure the Sound Pressure Level in db(A) using the microphone of the PC. (So, this can't be a really serious measure!)


## Content
- splsensor: an xAAL sensor measuring the Sound Pressure Level
- splog: a (silent) xAAL app recording values sended by splsensor on the xAAL bus
- beep: a tool helping to calibrate SPL measurements
- draw_xxx.gnuplot: some gnuplot scripts to display logged data



## Choosing between three behaviors
According to the "best effort" philosophy of xAAL, a sensor computes a value in a way it claims to be interesting, and decides itself to send values when it thinks this is interesting.

Therfore three profiles are proposed here for "splsensor":

- "Exact": By design, the sensor computes an SPL value almost every 20ms (0.1857 seconds to be precise, see below).
  If this value differs of the previous cycle, the sensor sends it via an attributes_change message.
  This may flood the xAAL bus (well, much less than video streaming ;-) ) and the database of the logger, but the logger may draw an almost "exact" curve of the thing measured.
  The sensor uses the "spl.basic" schema.

- "Mean": The sensor computes the SP mean on a period of almost 1mn (323 measures to be precise), and sends the new value if it changed via an attributes_change message.
  The logger has much fewer values to collect and store, however the drawn curve is an approximation.
  The sensor uses also the "spl.basic" schema.

- "Adaptive": The sensor computes mean / min / max in real time. If this differs for more than 10% of the previous report, it sends an new report giving:
  { last exact SPL, mean, min, max, standard deviation, number of samples, duration of mesures }
  It sends this via an attributes_change message of the "spl.advanced" schema.
  By doing so, the device adapts its behavior according to interest of values to exhibit.
  When the measured thing evolves a lot and quickly, the device sends frequent reports.
  When things are stables over a long period, the device sends very vew reports, gathering information with some statistical figures (mean, st-dev, etc.).
  So that the logger may have not so many data to manages, but it is still able to draw an almost precise curve.

  This adaptive behavior could be enhanced by adapting in real time this threshold of 10% according to something else, eg. the current number of samples or the last SPL value.
  (One may be interested by a fine granularity close to alerts levels, and a coarse granularity outside those scopes.)



## Notes about measuring a Sound Pressure Level
I coded it according to my understanding of Wikipedia pages:  http://en.wikipedia.org/wiki/Sound_pressure#Sound_pressure_level  http://en.wikipedia.org/wiki/A-weighting

I also thank the author of the following blog page giving very practical information: http://davidegironi.blogspot.fr/2014/02/a-simple-sound-pressure-level-meter-spl.html

To sum-up:
- Use the alsa "plughw:" driver that directly provides a mono capture of the microphone in the form of a buffer of floats (from -1.0 to 1.0).
  The rate is 44.1kHz. The number of frames is 8192 (8*1024), which correspond to the actual memory size of typical sound-cards on PCs.
  This means a measure every 0.1857 seconds (frames/rate).
- Apply FFT to get the sound amplitude in the frequency domain
- Apply the A-Weighting with a precomputed table according to Wikipedia's formula
- Computes the Root Mean Square
- Apply a time weighting (moving average) on 0.125s according to the "Fast" profile of the A-weighting specification
- Finally apply the log10 to have figures in dB, and scale it according to a given "calibration" (see below)

Any comments or corrections will be appreciated.



## Calibration
First of all, be aware that this is almost impossible to measure the SPL without a calibrated/dedicated hardware.
However, let's try to calibrate it by a naive process using the "beep" application provided here.
1) Find a reference SPL-sensor. (To be honest, I use my smartphone with a dedicated application)
2) Put it side to your microphone. Places your speakers in front of them.
3) Start the "beep" application. This will play a long beep (a sin wave of 1kHz) at two levels of volume, while measuring the SPL.
    In the mean time, read figures on your reference SPL-sensor, and give it to the beep application.
4) The beep application will then calculate a "Boost" factor (that should be very close to 1.0) and a "Ref" value (that may be around 80dB)
5) Uses those Boost and Ref figures with the "splsensor" application.



## Possible issues with alsamixer
If you are not comfortable with alsamixer please accept to spend some time to play with it.
Things are not so complicated but you first have to understand how your sound card works.
(Note that you may have to disable Pulseaudio and JACK sound servers on your
system. The code proposed here works at a lower level without upper layer
framework.)

For instance on my PC, I use the Rear Mic.
- First, I set the Boost level (capture) to 100%
- Then I choose Rear Mic as Input Source 0
- I set to 100% the level of Input Source 0
- I mute other Input Sources
- I mute the playback of the Rear Mic

In command line:
  amixer -D hw:0 sset 'Rear Mic Boost',0 0%
  amixer -D hw:0 sset 'Input Source',0 'Rear Mic'
  amixer -D hw:0 sset 'Capture',0 100% on
  amixer -D hw:0 sset 'Capture',1 0% off
  amixer -D hw:0 sset 'Capture',2 0% off
  amixer -D hw:0 sset 'Rear Mic' 0% off


## Logging values
The "splog" is an xAAL application recording values sent by "splsensor" xAAL devices.
Logfiles are in a format suitable for gnuplot.
Some examples are provided (eg., try "gnuplot draw_adaptive.gnuplot").


## Dependencies
- libxaal (including package uuid-dev libcbor libsodium-dev)
- libasound2-dev libfftw3-single3 libfftw3-dev
