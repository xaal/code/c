set xlabel "Seconds since Epoch"
set ylabel "db(A)"

set bars small

# Date	last	min	max	mean	samples	stdev	duration
set style fill solid 0.25 border
plot "profile_adaptive.log" \
	using ($1-$8/2):5:8 title "mean" with boxes, \
	'' using ($1-$8/2):5:7 title "stdev" with yerrorbars pt 0, \
	'' using ($1-$8/2):3:($1-$8):1 title "min" with xerrorbars pt 0, \
	'' using ($1-$8/2):4:($1-$8):1 title "max" with xerrorbars pt 0

pause -1 "Press Enter"
