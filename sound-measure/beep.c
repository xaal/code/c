/* beep - Calibrating the measurement of the Sound Pressure Level
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>
#include <math.h>
#include <stdbool.h>
#include <termios.h>
#include <complex.h>

#include <fftw3.h>

#include <alsa/asoundlib.h>
#include <alsa/mixer.h>

#include "spl.h"


/*
 * Play
 */


void generate_sin(float *buffer, snd_pcm_sframes_t frames, unsigned int rate, float freq, float *_phase) {
  static double max_phase = 2.0 * M_PI;
  double step = max_phase*freq/rate;
  unsigned int i;
  float phase = *_phase;

  for (i = 0; i < frames; i++) {
    buffer[i] = sinf(phase);
    phase += step;
    if (phase >= max_phase)
      phase -= max_phase;
  }
  *_phase = phase;
}


typedef struct {
  float *samples;
  unsigned int frames;
  unsigned int rate;
  float freq;
  float phase;
} playback_private_data;



void playback_callback(snd_async_handler_t *ahandler) {
  snd_pcm_t *handle = snd_async_handler_get_pcm(ahandler);
  playback_private_data *data = snd_async_handler_get_callback_private(ahandler);
  snd_pcm_sframes_t frames = data->frames;
  float *samples = data->samples;
  snd_pcm_sframes_t avail;
  int err;

  avail = snd_pcm_avail_update(handle);
  while (avail >= frames) {
    generate_sin(samples, frames, data->rate, data->freq, &(data->phase));
    err = snd_pcm_writei(handle, samples, frames);
    if (err < 0)
      err = snd_pcm_recover(handle, err, 1);
    if (err < 0) {
      fprintf(stderr, "Write error: %s\n", snd_strerror(err));
      exit(EXIT_FAILURE);
    }
    if (err != frames) {
      fprintf(stderr, "Playback error: write %i expected %li frames\n", err, frames);
      exit(EXIT_FAILURE);
    }
    avail = snd_pcm_avail_update(handle);
  }
}



snd_pcm_t *playback_init(char *device, playback_private_data *data) {
  const unsigned int rate = 44100;
  const snd_pcm_sframes_t frames = 1024 * 4;
  snd_async_handler_t *ahandler;
  float samples[frames];
  snd_pcm_t *handle;
  int err;

  if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
    fprintf(stderr, "Playback open error: %s\n", snd_strerror(err));
    exit(EXIT_FAILURE);
  }

  if ((err = snd_pcm_set_params(handle, SND_PCM_FORMAT_FLOAT, SND_PCM_ACCESS_RW_INTERLEAVED, 1, rate, 1, 500000)) < 0) {
    fprintf(stderr, "Playback parameters error: %s\n", snd_strerror(err));
    exit(EXIT_FAILURE);
  }

  if (output) {
    printf("\n** Playback device **\n");
    snd_pcm_dump(handle, output);
  }

  data->frames = frames;
  data->samples = (float *)malloc(frames * sizeof(float));
  data->rate = rate;
  data->phase = 0;

  err = snd_async_add_pcm_handler(&ahandler, handle, playback_callback, data);
  if (err < 0) {
    fprintf(stderr, "Unable to register playback handler\n");
    exit(EXIT_FAILURE);
  }

  generate_sin(samples, frames, data->rate, data->freq, &(data->phase));
  err = snd_pcm_writei(handle, samples, frames);
  if (err < 0)
    err = snd_pcm_recover(handle, err, 1);
  if (err < 0) {
    fprintf(stderr, "Write error: %s\n", snd_strerror(err));
    exit(EXIT_FAILURE);
  }
  if (err != frames) {
    fprintf(stderr, "Playback error: write %i expected %li frames\n", err, frames);
    exit(EXIT_FAILURE);
  }

  if (snd_pcm_state(handle) == SND_PCM_STATE_PREPARED) {
    err = snd_pcm_start(handle);
    if (err < 0) {
      fprintf(stderr, "Playback start error: %s\n", snd_strerror(err));
      exit(EXIT_FAILURE);
    }
  }

  return handle;
}


void playback_close(snd_pcm_t *handle, playback_private_data *data) {
  snd_pcm_close(handle);
  free(data->samples);
}



/*
 * Mixer
 * http://stackoverflow.com/questions/6787318/set-alsa-master-volume-from-c-code
 */


void set_volume(const char *device, const char *selem, long volume) {
  snd_mixer_selem_id_t *sid;
  snd_mixer_elem_t* elem;
  snd_mixer_t *handle;
  long min, max;
  int err;

  if ((err = snd_mixer_open(&handle, 0)))
    fprintf(stderr, "Warning snd_mixer_open: %s\n", snd_strerror(err));

  if ((err = snd_mixer_attach(handle, device)))
    fprintf(stderr, "Warning snd_mixer_attach: %s\n", snd_strerror(err));

  if ((err = snd_mixer_selem_register(handle, NULL, NULL)))
    fprintf(stderr, "Warning snd_mixer_selem_register: %s\n", snd_strerror(err));

  if ((err = snd_mixer_load(handle)))
    fprintf(stderr, "Warning snd_mixer_load: %s\n", snd_strerror(err));


  snd_mixer_selem_id_alloca(&sid);
  snd_mixer_selem_id_set_index(sid, 0);
  snd_mixer_selem_id_set_name(sid, selem);

  if (!(elem = snd_mixer_find_selem(handle, sid)))
    fprintf(stderr, "Warning snd_mixer_find_selem: sound element '%s' not found\n", selem);


  if ((err = snd_mixer_selem_get_playback_volume_range(elem, &min, &max)))
    fprintf(stderr, "Warning snd_mixer_selem_get_playback_volume_range: %s\n", snd_strerror(err));

  if ((err = snd_mixer_selem_set_playback_volume_all(elem, volume * max / 100)))
    fprintf(stderr, "Warning snd_mixer_selem_set_playback_volume_all: %s\n", snd_strerror(err));


  if ((err = snd_mixer_close(handle)))
    fprintf(stderr, "Warning snd_mixer_close: %s\n", snd_strerror(err));
}


/*
 * Capture
 * see spl.h spl.c
 */

void display_result(capture_private_data *data) {
  printf("\rSound Pressure Level: % 6.2f dBA  %6.2f/%6.2f/%6.2f/%6.2f",
	 data->SPL, data->Min, data->Mean, sqrtf(data->Variance), data->Max);
  fflush(stdout);
}



/*
 * Main
 */


/* Manage silent stdin */
struct termios initial_term;

void setsilent() {
  struct termios term;
  if ( tcgetattr(STDIN_FILENO, &initial_term) == -1) {
    perror("tcgetattr");
    return;
  }
  term = initial_term;
  term.c_lflag &= ~(ICANON | ECHO);
  term.c_cc[VMIN] = 1;
  term.c_cc[VTIME] = 0;
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &term) == -1)
    perror("tcsetattr");
}

void restore_term() {
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &initial_term) == -1)
    perror("tcsetattr");
  printf("\n");
}



char keystroke() {
  char c, k;
  int flag = fcntl(STDIN_FILENO, F_GETFL, 0);

  fcntl(0, F_SETFL, flag | O_NONBLOCK);
  while (read(STDIN_FILENO, &c, 1) > 0);
  fcntl(0, F_SETFL, flag & ~O_NONBLOCK);

  read(STDIN_FILENO, &k, 1);

  fcntl(0, F_SETFL, flag | O_NONBLOCK);
  while (read(STDIN_FILENO, &c, 1) > 0);
  fcntl(0, F_SETFL, flag & ~O_NONBLOCK);

  return k;
}



void usage(char *argv0, char *playback_device, char *capture_device, float freq) {
  fprintf(stderr, "Usage: %s [-i <input_dev>] [-o <output_dev>] [-f <frequency>] [-v] [-h]\n"
    "    -i <input_dev>   Input device for audio capture (%s)\n"
    "    -o <output_dev>  Output device for audio playback (%s)\n"
    "    -f <frequency>   Frequency to play (%.1f Hz)\n"
    "    -v               Be verbose\n"
    "    -h               This help\n"
    , basename(argv0), playback_device, capture_device, freq);
}



int main(int argc, char *argv[]) {
  char *playback_device = "plughw:0,0";
  char *capture_device = "plughw:0,0";
  float freq = 1000.0; // Hz
  float Ref = 0.0; // dB
  float Boost = 1.0; // ratio
  float Y1, Y2, X1, X2;
  int opt, err;
  bool verbose = false;
  bool argerr = false;
  snd_pcm_t *capture_handle;
  snd_pcm_t *playback_handle;
  capture_private_data capture_data;
  playback_private_data playback_data;
  float volume;

  while ((opt = getopt(argc, argv, "f:i:o:vh")) != -1) {
    switch (opt) {
      case 'f':
	freq = atof(optarg);
	break;
      case 'i':
	capture_device = optarg;
	break;
      case 'o':
	playback_device = optarg;
	break;
      case 'v':
	verbose = true;
	break;
      case 'h':
	usage(argv[0], playback_device, capture_device, freq);
	exit(EXIT_SUCCESS);
	break;
      default:
	argerr = true;
    }
  }
  while (optind < argc) {
    fprintf(stderr, "Unexpected argument '%s'\n", argv[optind++]);
    argerr = true;
  }
  if (argerr)
    usage(argv[0], playback_device, capture_device, freq);

  if (verbose) {
    printf("Input device (capture): %s\n"
	   "Output device (playback): %s\n"
	   "Frequency: %.1f Hz\n", capture_device, playback_device, freq);
    err = snd_output_stdio_attach(&output, stdout, 0);
    if (err < 0) {
      fprintf(stderr, "Output failed: %s\n", snd_strerror(err));
      exit(EXIT_FAILURE);
    }
  }


  /* Introduction */

  setsilent();
  atexit(restore_term);

  printf("I will take two measures to calibrate your microphone.\n"
	 "Please use alsamixer to setup your audio card in order to:\n"
	 "  - Select which microphone to capture\n"
	 "  - Setup boost level of this microphone\n"
	 "  - Setup capture level\n"
	 "  - Avoid this microphone to be remixed to the output\n"
	 "  - Unmute output\n"
	 "Now, I will play a long beep (a sin wave at %.1fHz).\n"
	 "Put your speaker in front of your microphone, and your reference sonometer side to your microphone.\n"
	 "Press a key when you are ready.\n\n", freq);
  keystroke();


  /* First measure */

  printf("Read figures on your reference sonometer.\n"
	 "Press a key when you are happy with that.\n");

  set_volume("default", "Master", 100.0);

  playback_data.freq =  freq;
  playback_handle = playback_init(playback_device, &playback_data);

  capture_data.Ref = Ref;
  capture_data.Boost = Boost;
  capture_data.send_result = display_result;
  capture_handle = capture_init(capture_device, &capture_data);

  keystroke();

  X1 = capture_data.SPL;
  set_volume("default", "Master", 0.01);
  playback_close(playback_handle, &playback_data);
  capture_close(capture_handle, &capture_data);

  printf("\nWhat figure did you read on your reference sonometer? "); fflush(stdout);
  restore_term();
  Y1 = 0.0;
  scanf("%f", &Y1);
  setsilent();


  /* Second measure */

  printf("\nNow let's do it again at an other volume level.\n"
	 "Press a key when you are ready.\n");
  keystroke();

  set_volume("default", "Master", 30);
  printf("Read figures on your reference sonometer.\n"
	 "Press a key when you are happy with that.\n");

  playback_data.freq =  freq;
  playback_handle = playback_init(playback_device, &playback_data);

  capture_data.Ref = Ref;
  capture_data.Boost = Boost;
  capture_data.send_result = display_result;
  capture_handle = capture_init(capture_device, &capture_data);

  keystroke();

  X2 = capture_data.SPL;
  set_volume("default", "Master", 0.01);
  playback_close(playback_handle, &playback_data);
  capture_close(capture_handle, &capture_data);

  printf("\nWhat figure did you read on your reference sonometer? "); fflush(stdout);
  restore_term();
  Y2 = 0.0;
  scanf("%f", &Y2);
  setsilent();

  /* Compute parameters and play */
  Boost = (Y1-Y2)/(X1-X2);
  Ref = (X1*Y2-X2*Y1)/(X1-X2);
  printf("\nThank you.\n");
  printf("X1=%f	Y1=%f\nX2=%f	Y2=%f\n", X1, Y1, X2, Y2);
  printf("Here are calibration parameters I have computed:\n	Boost=%f	Ref=%f\n", Boost, Ref);

  printf("Now you can play with this.\n\nPress '+' '-' to change volume.\n");

  set_volume("default", "Master", volume = 50.0);

  playback_data.freq =  freq;
  playback_handle = playback_init(playback_device, &playback_data);

  capture_data.Ref = Ref;
  capture_data.Boost = Boost;
  capture_data.send_result = display_result;
  capture_handle = capture_init(capture_device, &capture_data);
  for (;;) {
    switch (keystroke()) {
      case '+': volume += 1.0; break;
      case '-': volume -= 1.0; break;
      case 'q': case 'Q': case 27: case '\n': goto end;
    }
    if (volume <= 0.0) volume = 0.001;
    if (volume > 100.0) volume = 100.0;
    set_volume("default", "Master", volume);
  }

end:
  playback_close(playback_handle, &playback_data);
  capture_close(capture_handle, &capture_data);

  exit(EXIT_SUCCESS);
}
