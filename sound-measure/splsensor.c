/* splsensor - An xAAL sensor measuring the Sound Pressure Level in db(A)
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>

#include "spl.h"


#define ALIVE_PERIOD    60

/* setup sensor device info */
xAAL_devinfo_t sensor = { .dev_type	= "spl.basic",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "IHSEV",
			.product_id	= "Almost serious Sound Pressure Level sensor",
			.hw_id		= NULL,
			.version	= "0.4",
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.schema		= NULL,
			.info		= NULL,
			.unsupported_attributes = NULL,
			.unsupported_methods = NULL,
			.unsupported_notifications = NULL
		      };

xAAL_businfo_t bus;

void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &sensor) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* The "Exact" (flooding) profile */

bool send_attributeSPL_exact(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *sensor,
			 capture_private_data *data,
			 const uuid_t *target) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("SPL")), cbor_move(xAAL_cbor_build_float(data->SPL)) });
  if (target)
    return xAAL_write_busl(bus, sensor, xAAL_REPLY, "get_attributes", cbody, target, NULL);
  else
    return xAAL_write_bus(bus, sensor, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}

void send_result_exact(capture_private_data *data) {
  static float last = 0.0;

  if (data->SPL != last) {
    last = data->SPL;
    send_attributeSPL_exact(data->bus, data->sensor, data, NULL);
  }
}



/* The "Mean" (losy) profile */

bool send_attributeSPL_mean(const xAAL_businfo_t *bus,
			  const xAAL_devinfo_t *sensor,
			  capture_private_data *data,
			  const uuid_t *target) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("SPL")), cbor_move(xAAL_cbor_build_float(data->Mean)) });
  if (target)
    return xAAL_write_busl(bus, sensor, xAAL_REPLY, "get_attributes", cbody, target, NULL);
  else
    return xAAL_write_busl(bus, sensor, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}

#define COUNT_MAX	323	//  1mn * rate / frames
void send_result_mean(capture_private_data *data) {
  static unsigned count;

  if (count++ == COUNT_MAX) { // Almost 1mn
    send_attributeSPL_mean(data->bus, data->sensor, data, NULL);
    data->N = 0;
    data->Mean = 0.0;
    count = 0;
  }
}


/* The "Adaptive" profile */

bool send_attributeSPL_adaptive(const xAAL_businfo_t *bus,
			  const xAAL_devinfo_t *sensor,
			  capture_private_data *data,
			  const uuid_t *target) {
  cbor_item_t *cbody, *cSPL;

  cSPL = cbor_new_definite_map(7);
  (void)!cbor_map_add(cSPL, (struct cbor_pair){ cbor_move(cbor_build_string("last")), cbor_move(xAAL_cbor_build_float(data->SPL)) });
  (void)!cbor_map_add(cSPL, (struct cbor_pair){ cbor_move(cbor_build_string("mean")), cbor_move(xAAL_cbor_build_float(data->Mean)) });
  (void)!cbor_map_add(cSPL, (struct cbor_pair){ cbor_move(cbor_build_string("min")), cbor_move(xAAL_cbor_build_float(data->Min)) });
  (void)!cbor_map_add(cSPL, (struct cbor_pair){ cbor_move(cbor_build_string("max")), cbor_move(xAAL_cbor_build_float(data->Max)) });
  (void)!cbor_map_add(cSPL, (struct cbor_pair){ cbor_move(cbor_build_string("stdev")), cbor_move(xAAL_cbor_build_float(sqrtf(data->Variance))) });
  (void)!cbor_map_add(cSPL, (struct cbor_pair){ cbor_move(cbor_build_string("samples")), cbor_move(xAAL_cbor_build_int(data->N)) });
  (void)!cbor_map_add(cSPL, (struct cbor_pair){ cbor_move(cbor_build_string("duration")), cbor_move(xAAL_cbor_build_float(data->N * data->frames / data->rate)) });

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("SPL")), cbor_move(cSPL) });

  if (target)
    return xAAL_write_busl(bus, sensor, xAAL_REPLY, "get_attributes", cbody, target, NULL);
  else
    return xAAL_write_bus(bus, sensor, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}


#define RATIO_MAX	0.1	//  10%
void send_result_adaptive(capture_private_data *data) {
  static float Mean, Min, Max;

  if (     (fabs(Mean - data->Mean) / Mean) > RATIO_MAX
	|| (fabs(Min - data->Min) / Min) > RATIO_MAX
	|| (fabs(Max - data->Max) / Max) > RATIO_MAX  ) {
    // fprintf(stderr, "ΔSPL:%f  ΔMean:%f  ΔMin:%f  ΔMax:%f  ΔVar:%f\n", (fabs(SPL - data->SPL) / SPL), (fabs(Mean - data->Mean) / Mean), (fabs(Min - data->Min) / Min), (fabs(Max - data->Max) / Max), (fabs(Variance - data->Variance) / Variance));
    send_attributeSPL_adaptive(data->bus, data->sensor, data, NULL);
    Mean = data->Mean;
    Min = data->Min;
    Max = data->Max;
    data->N = 0;
    data->Mean = 0.0;
    data->Min = INFINITY;
    data->Max = -INFINITY;
    data->Variance  = 0.0;
  }
}



typedef enum { EXACT, MEAN, ADAPTIVE } profile_t;

bool reply_get_attributes(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *sensor,
			 capture_private_data *data,
			 profile_t profile,
			 const uuid_t *target) {
  switch (profile) {
    case EXACT:
      return send_attributeSPL_exact(bus, sensor, data, target);
    case MEAN:
      return send_attributeSPL_mean(bus, sensor, data, target);
    case ADAPTIVE:
      return send_attributeSPL_adaptive(bus, sensor, data, target);
    default:
      return false;
  }
}




/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  profile_t profile = ADAPTIVE;
  float Boost = 1.0;
  float Ref = 80.0;
  bool arg_error = false;
  struct sigaction act_alarm;
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  capture_private_data capture_data;
  char *capture_device = "plughw:0,0";

  xAAL_error_log = stderr;

  uuid_clear(sensor.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:P:B:R:D:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, sensor.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(sensor.addr);
	}
	break;
      case 'P':
	if (strcasecmp(optarg, "exact") == 0)
	  profile = EXACT;
	else if (strcasecmp(optarg, "mean") == 0)
	  profile = MEAN;
	else if (strcasecmp(optarg, "adaptive") == 0)
	  profile = ADAPTIVE;
	break;
      case 'B':
	Boost = atof(optarg);
	break;
      case 'R':
	Ref = atof(optarg);
	break;
      case 'D':
	capture_device = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] [-P exact|mean|adaptive] [-B <boost>] [-R <dB Ref>] [-D <alsa device>]\n"
	    "An xAAL device to Monitor Sound Pressure Level in db(A)\n"
	    "	-a <addr>	IP multicast address of the xAAL bus\n"
	    "	-p <port>	UDP port of the xAAL bus\n"
	    "	-h <hops>	IP packets hops count of the xAAL bus\n"
	    "	-s <secret>	Secret passphrase of the xAAL bus\n"
	    "	-u <uuid>	xAAL address (uuid) of the sensor device\n"
	    "	-D <snd dev>	Alsa capture device (plughw:0,0)\n"
	    "	-P <profile>	Profile of the sensor\n"
	    "			exact: new message when SPL change (e.g. almost every 20ms)\n"
	    "			mean: the SPL mean on almost one minute\n"
	    "			adaptive: reports min/max/mean/stdev/duration only if it is 10%% different than previous report\n"
	    "	-B <boost>	A boosting factor on SPL measure (usualy close to 1.0)\n"
	    "	-R <dB Ref>	Reference of the SPL measure (need to be calibrated, usualy arround 80dB)\n" , argv[0]);
    exit(EXIT_FAILURE);
  }


  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(sensor.addr) ) {
    char uuid[37];
    uuid_generate(sensor.addr);
    uuid_unparse(sensor.addr, uuid);
    printf("Device: %s\n", uuid);
  }
  xAAL_add_wanted_target(&sensor.addr, &bus);

  /* Each profile has it own behavior */
  switch (profile) {
    case EXACT:
      capture_data.send_result = send_result_exact;
      break;
    case MEAN:
      capture_data.send_result = send_result_mean;
      break;
    case ADAPTIVE:
      capture_data.send_result = send_result_adaptive;
      sensor.dev_type = "spl.advanced";
      break;
  }
  capture_data.Ref = Ref;
  capture_data.Boost = Boost;
  capture_data.bus = &bus;
  capture_data.sensor = &sensor;
  capture_init(capture_device, &capture_data);


  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &sensor) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");


  /* Main loop */
  for (;;) {

    /* Recive a message */
    if (!xAAL_read_bus(&bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
      continue;

    if (msg_type == xAAL_REQUEST) {
      if ( (strcmp(action, "is_alive") == 0)
	  && xAAL_is_aliveDevType_match(cbody, sensor.dev_type) ) {
	if ( !xAAL_notify_alive(&bus, &sensor) )
	  fprintf(xAAL_error_log, "Could not reply to is_alive\n");

      } else if ( strcmp(action, "get_description") == 0 ) {
	if ( !xAAL_reply_get_description(&bus, &sensor, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_description\n");

      } else if ( strcmp(action, "get_attributes") == 0 ) {
	if ( !reply_get_attributes(&bus, &sensor, &capture_data, profile, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

      }
    }
    xAAL_free_msg(ctargets, source, dev_type, action, cbody);
  }
}
