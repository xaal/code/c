set xlabel "Seconds since Epoch"
set ylabel "db(A)"

plot "profile_mean.log" using 1:2 with lines

pause -1 "Press Enter"
