/* Measuring Sound Pressure Level
 * This file is part of the "splsensor" and "beep" applications
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>

#include <fftw3.h>

#include <alsa/asoundlib.h>

#include "spl.h"


snd_output_t *output = NULL;

void capture_callback(snd_async_handler_t *ahandler) {
  snd_pcm_t *handle = snd_async_handler_get_pcm(ahandler);
  capture_private_data *data = snd_async_handler_get_callback_private(ahandler);
  snd_pcm_sframes_t frames = data->frames;
  float *samples = data->samples;
  fftwf_complex *spectrum = data->spectrum;
  float *Aweighting = data->Aweighting;
  snd_pcm_sframes_t avail;
  int err;
  unsigned int i;
  float x, y, magnitude, RMS_new, Delta, M2;
  static float RMS_global = 0;
  static unsigned not_enought = 0;

  avail = snd_pcm_avail_update(handle);
  while (avail >= frames) {
    err = snd_pcm_readi(handle, samples, frames);

    if (err < 0) {
      fprintf(stderr, "Read error: %s\n", snd_strerror(err));
      exit(EXIT_FAILURE);
    }
    if (err != frames) {
      fprintf(stderr, "Capture error: read %i expected %li frames\n", err, frames);
      exit(EXIT_FAILURE);
    }
    not_enought = 0;

    fftwf_execute(data->plan);

    /* Apply A-weighing and get Root Mean Square */
    magnitude = 0.0;
    for (i = 0; i < frames/2; i++) {
      x = crealf(spectrum[i]) * Aweighting[i];
      y = cimagf(spectrum[i]) * Aweighting[i];
      magnitude += x*x + y*y;
    }
    RMS_new = sqrtf( 2.0 / (frames*frames) * magnitude );

    /* Time weighting (moving average) */
    /* A-weighting specs: Slow=1s Fast=0.125s */
    RMS_global = (1 - data->alpha) * RMS_global + data->alpha * RMS_new;

    /* Sound Pressure Level in dB(A) */
    data->SPL = 20.0 * log10f( RMS_global ) * data->Boost + data->Ref;

    data->N++;
    Delta = data->SPL - data->Mean;
    data->Mean += Delta / data->N;
    M2 += Delta * (data->SPL - data->Mean);
    data->Variance = M2 / data->N;

    if (data->SPL < data->Min) data->Min = data->SPL;
    if (data->SPL > data->Max) data->Max = data->SPL;

    data->send_result(data);

    avail = snd_pcm_avail_update(handle);
  }
  not_enought++;
  if (not_enought >= 10) {
    fprintf(stderr, "Error: This is the %uth time there's not enough available frames (want %u).\n", not_enought, (unsigned)frames);
    exit(EXIT_FAILURE);
  }
}


void fill_Aweighting_table(float *Aweighting, unsigned int rate, unsigned int frames) {
  // http://en.wikipedia.org/wiki/A-weighting#A
  //  Ra(f)=  12200^2*f^4 / (f^2+20.6^2)(f^2+12200^2)((f^2+107.7^2)^0.5)((f^2+737.9^2)^0.5)
  const float c1 = 12200.0 * 12200.0;
  const float c2 = 20.6 * 20.6;
  const float c3 = 107.7 * 107.7;
  const float c4 = 737.9 * 737.9;
  float step = rate/frames;
  float f, f2, f4;
  unsigned int i;

  for (i = 0; i < frames/2; i++) {
    f = step * i;
    f2 = f * f;
    f4 = f2 * f2;
    Aweighting[i] = (c1 * f4) / ( (f2 + c2) * (f2 + c1 ) * sqrtf( (f2 + c3) * (f2 + c4) ) );
    // printf("A(%f)=%f\n", f, Aweighting[i]);
  }
}


snd_pcm_t *capture_init(char *device, capture_private_data *data) {
  const unsigned int rate = 44100;
  const snd_pcm_sframes_t frames = 1024 * 8;
  const float time_window = 0.125; /* A-weighting specs: Slow=1s Fast=0.125s */
  snd_async_handler_t *ahandler;
  snd_pcm_t *handle;
  int err;

  if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
    fprintf(stderr, "Capture open error: %s\n", snd_strerror(err));
    exit(EXIT_FAILURE);
  }

  if ((err = snd_pcm_set_params(handle, SND_PCM_FORMAT_FLOAT, SND_PCM_ACCESS_RW_INTERLEAVED, 1, rate, 1, 500000)) < 0) {
    fprintf(stderr, "Capture parameters error: %s\n", snd_strerror(err));
    exit(EXIT_FAILURE);
  }

  if (output) {
    printf("\n** Capture device **\n");
    snd_pcm_dump(handle, output);
  }

  data->rate = rate;
  data->frames = frames;
  data->samples = fftwf_alloc_real(frames);
  if (!data->samples) {
    fprintf(stderr, "Unable to allocate buffer for samples.\n");
    exit(EXIT_FAILURE);
  }
  data->spectrum = fftwf_alloc_complex(frames/2+1);
  if (!data->spectrum) {
    fprintf(stderr, "Unable to allocate buffer to compute the spectrum.\n");
    exit(EXIT_FAILURE);
  }
  data->plan = fftwf_plan_dft_r2c_1d(frames, data->samples, data->spectrum, FFTW_MEASURE);
  if (!data->plan) {
    fprintf(stderr, "Unable to create the FFTW plan.\n");
    exit(EXIT_FAILURE);
  }

  data->Aweighting = fftwf_alloc_real(frames/2+1);
  fill_Aweighting_table(data->Aweighting, rate, frames);

  data->alpha = time_window / ( frames/rate  + time_window );	//  alpha = Window / (framing duration + Window)

  data->N = 0;
  data->Mean = 0.0;
  data->Min = INFINITY;
  data->Max = -INFINITY;

  err = snd_async_add_pcm_handler(&ahandler, handle, capture_callback, data);
  if (err < 0) {
    fprintf(stderr, "Unable to register capture handler\n");
    exit(EXIT_FAILURE);
  }

  if (snd_pcm_state(handle) == SND_PCM_STATE_PREPARED) {
    err = snd_pcm_start(handle);
    if (err < 0) {
      fprintf(stderr, "Capture start error: %s\n", snd_strerror(err));
      exit(EXIT_FAILURE);
    }
  }

  return handle;
}


void capture_close(snd_pcm_t *handle, capture_private_data *data) {
  snd_pcm_close(handle);
  fftwf_free(data->samples);
  fftwf_free(data->spectrum);
  fftwf_free(data->Aweighting);
}
