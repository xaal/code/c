/* Measuring Sound Pressure Level
 * This file is part of the "splsensor" and "beep" applications
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPL_H
#define SPL_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>

#include <fftw3.h>

#include <alsa/asoundlib.h>

#include <xaal.h>


extern snd_output_t *output;


typedef struct capture_private_data {
  float rate;
  float *samples;
  unsigned int frames;
  float SPL, Boost, Ref, alpha;
  unsigned int N;
  float Mean, Variance, Min, Max;
  fftwf_complex *spectrum;
  fftwf_plan plan;
  float *Aweighting;
  const xAAL_businfo_t *bus;
  const xAAL_devinfo_t *sensor;
  void (*send_result)(struct capture_private_data *data);
} capture_private_data;


void capture_callback(snd_async_handler_t *ahandler);

void fill_Aweighting_table(float *Aweighting, unsigned int rate, unsigned int frames);

snd_pcm_t *capture_init(char *device, capture_private_data *data);

void capture_close(snd_pcm_t *handle, capture_private_data *data);

#endif
