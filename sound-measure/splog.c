/* splog - Log Sound Presure Level reported by an xAAL spl-sensor
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/queue.h>
#include <signal.h>

#include <cbor.h>

#include <xaal.h>


typedef LIST_HEAD(listhead, entry) sensors_t;
typedef struct entry {
  uuid_t addr;
  char *dev_type;
  FILE *log;
  LIST_ENTRY(entry) entries;
} sensor_t;


FILE *select_sensor(sensors_t *sensors, const uuid_t *addr, const char *dev_type) {
  enum { BASIC, ADVANCED } type;
  sensor_t *sensor;
  char *name;
  char uuid[37];

  if (strcmp(dev_type, "spl.basic")==0)
    type = BASIC;
  else if (strcmp(dev_type, "spl.advanced")==0)
    type = ADVANCED;
  else
    return NULL;

  LIST_FOREACH(sensor, sensors, entries)
    if (uuid_compare(sensor->addr, *addr)==0 && strcmp(sensor->dev_type, dev_type)==0)
      return sensor->log;

  sensor = (sensor_t *)malloc(sizeof(sensor_t));
  uuid_copy(sensor->addr, *addr);
  sensor->dev_type = strdup(dev_type);
  uuid_unparse(*addr, uuid);
  name = malloc(strlen(uuid)+strlen(dev_type)+5);
  sprintf(name, "%s_%s.log", dev_type, uuid);
  sensor->log = fopen(name, "a");
  free(name);
  LIST_INSERT_HEAD(sensors, sensor, entries);
  switch (type) {
    case BASIC:
      fprintf(sensor->log, "# Date	SPL db(A)\n");
      break;
    case ADVANCED:
      fprintf(sensor->log, "# Date	last	min	max	mean	samples	stdev	duration\n");
      break;
  }
  return sensor->log;
}


void log_values(FILE *log, cbor_item_t *cbody) {
  cbor_item_t *cSPL, *cLast, *cMean, *cMin, *cMax, *cSamples, *cStDev, *cDuration;
  struct timeval tv;

  cSPL = xAAL_cbor_map_get(cbody, "SPL");
  if ( !cSPL)
    return;

  gettimeofday(&tv, NULL);

  if ( cbor_is_float(cSPL) ) {
    fprintf(log, "%lu.%06lu	%f\n", tv.tv_sec, tv.tv_usec, cbor_float_get_float(cSPL));
    printf("%lu.%06lu	%f\n", tv.tv_sec, tv.tv_usec, cbor_float_get_float(cSPL));

  } else if ( cbor_isa_map(cSPL) ) {
    cLast = xAAL_cbor_map_get(cSPL, "last");
    cMean = xAAL_cbor_map_get(cSPL, "mean");
    cMin = xAAL_cbor_map_get(cSPL, "min");
    cMax = xAAL_cbor_map_get(cSPL, "max");
    cStDev = xAAL_cbor_map_get(cSPL, "stdev");
    cDuration = xAAL_cbor_map_get(cSPL, "duration");
    cSamples = xAAL_cbor_map_get(cSPL, "samples");
    if (   cLast && cbor_is_float(cLast)
	&& cMean && cbor_is_float(cMean)
	&& cMin && cbor_is_float(cMin)
	&& cMax && cbor_is_float(cMax)
	&& cStDev && cbor_is_float(cStDev)
	&& cDuration && cbor_is_float(cDuration)
	&& cSamples && cbor_isa_uint(cSamples) ) {
      //# Date      last    min     max     mean    samples stdev   duration
      fprintf(log, "%lu.%06lu	%f	%f	%f	%f	%ld	%f	%f\n",
	    tv.tv_sec, tv.tv_usec,
	    cbor_float_get_float(cLast),
	    cbor_float_get_float(cMin),
	    cbor_float_get_float(cMax),
	    cbor_float_get_float(cMean),
	    cbor_get_int(cSamples),
	    cbor_float_get_float(cStDev),
	    cbor_float_get_float(cDuration));
      printf("%lu.%06lu	%f	%f	%f	%f	%ld	%f	%f\n",
	    tv.tv_sec, tv.tv_usec,
	    cbor_float_get_float(cLast),
	    cbor_float_get_float(cMin),
	    cbor_float_get_float(cMax),
	    cbor_float_get_float(cMean),
	    cbor_get_int(cSamples),
	    cbor_float_get_float(cStDev),
	    cbor_float_get_float(cDuration));
    }
  }
}


sensors_t *p_sensors;
void cancel(int s) {
  sensor_t *sensor;
  LIST_FOREACH(sensor, p_sensors, entries)
    fclose(sensor->log);
  exit(EXIT_SUCCESS);
}



int main(int argc, char **argv) {
  xAAL_businfo_t bus;
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  sensors_t sensors;
  FILE *log;

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret>\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  xAAL_error_log = stderr;
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  LIST_INIT(&sensors);

  p_sensors = &sensors;
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);

  /* main loop */
  for (;;) {
    if (!xAAL_read_bus(&bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
      continue;

    log = select_sensor(&sensors, source, dev_type);
    if (log)
      log_values(log, cbody);

    xAAL_free_msg(ctargets, source, dev_type, action, cbody);
  }
}
