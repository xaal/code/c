/* xaaws - xAAL web interface
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Acknowledgement: Andy Green <andy@warmcat.com>
 * libwebsockets-test-server (c) 2010-2016 - CC0 1.0
 */

#ifndef _XAAWS_
#define _XAAWS_


#include <libwebsockets.h>
#include <xaal.h>

#include "db.h"


extern int max_poll_elements;
extern struct lws_pollfd *pollfds;
extern int *fd_lookup;
extern int count_pollfds;

extern volatile int force_exit;
extern struct lws_context *context;

extern char *resource_path;

extern db_t db;
extern xAAL_businfo_t bus;
extern xAAL_devinfo_t me;

#endif
