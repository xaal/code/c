/* xaaws - xAAL web interface
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Acknowledgement: Andy Green <andy@warmcat.com>
 * libwebsockets-test-server (c) 2010-2016 - CC0 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <syslog.h>
#include <sys/time.h>
#include <unistd.h>
#include <netdb.h>
#include <uuid/uuid.h>

#include <libwebsockets.h>

#include <xaal.h>

#include "xaaws.h"
#include "options.h"
#include "proto-http.h"
#include "proto-xaal.h"
#include "xaagent.h"
#include "db.h"


int max_poll_elements;
struct lws_pollfd *pollfds;
int *fd_lookup;
int count_pollfds;

volatile int force_exit = 0;
struct lws_context *context;

char *resource_path;

db_t db;
xAAL_businfo_t bus;
xAAL_devinfo_t me;



/* list of supported protocols and callbacks */
static struct lws_protocols protocols[] = {
  { "http-only", callback_http, sizeof(struct per_session_data__http), 0 },
  { "xaal-ctl", callback_xaal_ctl, sizeof(struct per_session_data__xaal_ctl), 0 },
  { "xaal-dump", callback_xaal_dump, sizeof(struct per_session_data__xaal_dump), 0 },
  { NULL, NULL, 0, 0 }
};


static const struct lws_extension exts[] = {
  { "permessage-deflate", lws_extension_callback_pm_deflate, "permessage-deflate" },
  { "deflate-frame", lws_extension_callback_pm_deflate, "deflate_frame" },
  { NULL, NULL, NULL }
};


void sighandler(int sig) {
  force_exit = 1;
  lws_cancel_service(context);
}


void emit_log(int level, const char *line) {
  char buf[50];
 lwsl_timestamp(level, buf, sizeof(buf));
 fprintf(xAAL_error_log, "%s%s", buf, line);
}


int service_port(const char *name) {
  struct servent *serv = getservbyname(name, NULL);
  if (serv)
    return serv->s_port;
  else
    return atoi(name);
}


int main(int argc, char **argv) {
  struct lws_context_creation_info info;
  options_t options = { .addr=NULL, .port=NULL, .hops=-1, .passphrase=NULL,
			.uuid=&me.addr, .conffile="./xaaws.conf", .immutable=false,
			.daemon=false, .logfile=NULL, .lockfile="/tmp/.xaaws.lock",
			.uid=-1, .gid=-1, .verbose=7, .http_port="8888", .http_iface=NULL,
			.resource_path="./www", .dbfile="./xaaws.db" };
  int alive_fd, checking_fd;
  uint64_t exp;


  uuid_clear(me.addr);
  parse_cmdline(argc, argv, &options);
  read_config(&options);
  init_schemas_path(&options, SCHEMAS_CACHE);

  if (options.daemon) {
    if (lws_daemonize(options.lockfile)) {
      fprintf(stderr, "Failed to daemonize\n");
      exit(EXIT_FAILURE);
    }
    /* we will only try to log things according to our debug_level */
    setlogmask(LOG_UPTO(LOG_DEBUG));
    openlog(NULL, LOG_PID, LOG_DAEMON);
    /* tell the library what debug level to emit and to send it to syslog */
    lws_set_log_level(options.verbose, lwsl_emit_syslog);
  } else {
    lws_set_log_level(options.verbose, emit_log);
  }

  signal(SIGINT, sighandler);

  init_xaagent(&options, &bus, &me, &alive_fd, &checking_fd);

  init_db(&db);
  load_devices(&(db.devices), options.dbfile);
  load_schemas(&(db.schemas), options.schemas);


  max_poll_elements = getdtablesize();
  pollfds = malloc(max_poll_elements * sizeof(struct lws_pollfd));
  fd_lookup = malloc(max_poll_elements * sizeof(int));
  if (pollfds == NULL || fd_lookup == NULL) {
    lwsl_err("Out of memory pollfds=%d\n", max_poll_elements);
    exit(EXIT_FAILURE);
  }

  /* add xAAL events in pollfds */
  fd_lookup[bus.sfd] = 0;
  pollfds[0].fd = bus.sfd;
  pollfds[0].events = POLLIN;
  fd_lookup[alive_fd] = 1;
  pollfds[1].fd = alive_fd;
  pollfds[1].events = POLLIN;
  fd_lookup[checking_fd] = 2;
  pollfds[2].fd = checking_fd;
  pollfds[2].events = POLLIN;
  count_pollfds = 3;

  memset(&info, 0, sizeof info);
  info.port = service_port(options.http_port);
  info.iface = options.http_iface;
  info.protocols = protocols;
  info.gid = options.gid;
  info.uid = options.uid;
  info.max_http_header_pool = 32;
  info.options = LWS_SERVER_OPTION_VALIDATE_UTF8;
  info.extensions = exts;
  info.timeout_secs = 600;

  context = lws_create_context(&info);
  if (context == NULL) {
    lwsl_err("libwebsocket init failed\n");
    exit(EXIT_FAILURE);
  }

  resource_path = options.resource_path;
  lwsl_info("Using resource path \"%s\"\n", resource_path);

  write_config(&options);

  int n = 0;
  while (n >= 0 && !force_exit) {

    n = poll(pollfds, count_pollfds, -50);
    if (n < 0)
      continue;

    if (n) {
      for (n = 0; n < count_pollfds; n++) {
	if (pollfds[n].revents) {

	  if ( (pollfds[n].fd == bus.sfd) && (pollfds[n].revents & POLLIN) ) {
	    callback_xaal(&bus, &me, &db);

	  } else if ( (pollfds[n].fd == alive_fd) && (pollfds[n].revents & POLLIN) ) {
	    if ( read(alive_fd, &exp, sizeof(uint64_t)) == -1 )
	      lwsl_err("Error on Alive timer\n");
	    if ( !xAAL_notify_alive(&bus, &me) )
	      lwsl_err("Could not send alive notification.\n");

	  } else if ( (pollfds[n].fd == checking_fd) && (pollfds[n].revents & POLLIN) ) {
	    if ( read(checking_fd, &exp, sizeof(uint64_t)) == -1 )
	     lwsl_err("Error on Checking timer\n");
	    callback_checking(&bus, &me, &db, &options);

	  } else {
	    /*
	     * returns immediately if the fd does not
	     * match anything under libwebsockets
	     * control
	     */
	    if (lws_service_fd(context, &pollfds[n]) < 0)
	      goto done;
	  }
	}
      }
    }
  }

 done:

  lws_context_destroy(context);

  lwsl_notice("xaaws exited cleanly\n");

  dump_devices(&(db.devices), options.dbfile);

  closelog();

  exit(EXIT_SUCCESS);
}
