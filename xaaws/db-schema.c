/* db - Database for xAAL Agent
 * Part of the xaaws software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <regex.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <curl/curl.h>

#include "db-schema.h"


/* validate the name of an attribute/type/methode/notification identifier */
bool validate_identifier(const char *identifier) {
  static regex_t *preg = NULL;

  if (!preg) {
    preg = (regex_t *)malloc(sizeof(regex_t));
    int r = regcomp(preg, "^[a-zA-Z][a-zA-Z0-9_-]*$", REG_NOSUB);
    if ( r != 0 ) {
      preg = NULL;
      fprintf(xAAL_error_log, "Error regcomp: %d\n", r);
      return false;
    }
  }
  return regexec(preg, identifier, 0, NULL, 0) != REG_NOMATCH;
}





/*
 * Section Schemas
 */



/* validate the name of a schema (dev_type) */
bool validate_dev_type(const char *dev_type) {
  static regex_t *preg = NULL;

  if (!preg) {
    preg = (regex_t *)malloc(sizeof(regex_t));
    int r = regcomp(preg, "^[a-zA-Z][a-zA-Z0-9_-]*.[a-zA-Z][a-zA-Z0-9_-]*$", REG_NOSUB);
    if ( r != 0 ) {
      preg = NULL;
      fprintf(xAAL_error_log, "Error regcomp: %d\n", r);
      return false;
    }
  }
  return regexec(preg, dev_type, 0, NULL, 0) != REG_NOMATCH;
}


/* load all schemas in the cache */
void load_schemas(schemas_t *schemas, const char *path) {
  DIR *dir;
  struct dirent *entry;

  if ((dir = opendir(path)) == NULL)
    fprintf(xAAL_error_log, "%s: %s\n", strerror(errno), path);
  else {
    while ((entry = readdir(dir)) != NULL) {
      if ( strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..") ) {
        json_object *jschema = load_json_file(path, entry->d_name);
        if (jschema) {
          add_json_schema(schemas, jschema);
          json_object_put(jschema);
        }
      }
    }
    closedir(dir);
  }  
}

 
/* load json file (e.g. a schema) */
json_object *load_json_file(const char *dir, const char *name) {
  struct json_object *jobj;
  char *path;

  path = malloc(strlen(dir)+1+strlen(name)+1);
  sprintf(path, "%s/%s", dir, name);
  if (access(path, R_OK)) {
    free(path);
    return NULL;
  }

  jobj = json_object_from_file(path);
  if ( !json_object_is_type(jobj, json_type_null)) {
    free(path);
    return jobj;
  } else {
    /* This file is not a json object */
    unlink(path); /* Delete corrupted file */
    free(path);
    return NULL;
  }
}


/* curl call-back to write data while downloading */
static size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  size_t written = fwrite(ptr, size, nmemb, stream);
  return written;
}

/* download a file (e.g. a schema) */
bool download_file(const char *url, const char *name, const char *destdir) {
  char *dest;
  CURL *curl_handle;
  char curl_errbuf[CURL_ERROR_SIZE];
  int err;
  FILE *destfile;

  dest = malloc(strlen(destdir)+1+strlen(name)+1);
  sprintf(dest, "%s/%s", destdir, name);
  destfile = fopen(dest,"w+b");
  if ( !destfile ) {
    fprintf(xAAL_error_log, "%s: %s\n", strerror(errno), dest);
    free(dest);
    return false;
  }

  curl_global_init(CURL_GLOBAL_ALL);
  curl_handle = curl_easy_init();
  if (!curl_handle ) {
    fprintf(xAAL_error_log, "Can't init curl for downloading %s to %s\n", url, dest);
    free(dest);
    return false;
  }

  curl_easy_setopt(curl_handle, CURLOPT_URL, url);

  curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);
  //curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0L);
  //curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);
  //curl_easy_setopt(curl_handle, CURLOPT_CAPATH, capath);
  //curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);
  //curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYHOST, 0L);
  curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, true);
  curl_easy_setopt(curl_handle, CURLOPT_ERRORBUFFER, curl_errbuf);
  curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
  curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, destfile);
  err = curl_easy_perform(curl_handle);
  curl_easy_cleanup(curl_handle);

  if ( fclose(destfile) )
    fprintf(xAAL_error_log, "%s: %s\n", strerror(errno), dest);

  if ( !err ) 
    free(dest);
  else {
    fprintf(xAAL_error_log, "%s: %s\n", curl_errbuf, url);
    if ( unlink(dest) )
      fprintf(xAAL_error_log, "%s: %s/%s\n", strerror(errno), destdir, name);
    return false;
  }

  return true;
}



/* select a schema by its name */
schema_t *select_schema(schemas_t *schemas, const char *title) {
  schema_t *np;

  if (title) 
    TAILQ_FOREACH(np, schemas, entries)
      if (strcmp(title, np->title) == 0)
        return np;
  return NULL;
}


/* add a schema from json data */
void add_json_schema(schemas_t *schemas, json_object *jschema) {
  json_object *jtitle, *jdescription, *jlang, *jdoc, *jref, *jlicense,
              *jextends, *jattributes, *jmethods, *jnotifications, *jdatamodel;
  schema_t *schema;
  const char *dev_type;

  if ( !json_object_object_get_ex(jschema, "title", &jtitle)
      || !json_object_is_type(jtitle, json_type_string) )
    return;
  dev_type = json_object_get_string(jtitle);
  if (!validate_dev_type(dev_type) )
    return;

  schema = select_schema(schemas, dev_type);
  if (schema)
    return;

  schema = (schema_t *)malloc(sizeof(schema_t));
  TAILQ_INSERT_TAIL(schemas, schema, entries);

  schema->title = strdup(dev_type);
  
  if ( json_object_object_get_ex(jschema, "description", &jdescription) && json_object_is_type(jdescription, json_type_string) )
    schema->description = strdup(json_object_get_string(jdescription));
  else
    schema->description = NULL;

  if ( json_object_object_get_ex(jschema, "lang", &jlang) && json_object_is_type(jlang, json_type_string) )
    schema->lang = strdup(json_object_get_string(jlang));
  else
    schema->lang = NULL;

  if ( json_object_object_get_ex(jschema, "documentation", &jdoc) && json_object_is_type(jdoc, json_type_string) )
      schema->documentation = strdup(json_object_get_string(jdoc));
  else
    schema->documentation = NULL;

  if ( json_object_object_get_ex(jschema, "ref", &jref) && json_object_is_type(jref, json_type_string) )
    schema->ref = strdup(json_object_get_string(jref));
  else
    schema->ref = NULL;

  if ( json_object_object_get_ex(jschema, "license", &jlicense) && json_object_is_type(jlicense, json_type_string) )
    schema->license = strdup(json_object_get_string(jlicense));
  else
    schema->license = NULL;

  if ( json_object_object_get_ex(jschema, "extends", &jextends) && json_object_is_type(jextends, json_type_string) ) {
    const char *extends = json_object_get_string(jextends);
    if ( validate_dev_type(extends) )
      schema->extends = strdup(extends);
    else
      schema->extends = NULL;
  } else
    schema->extends = NULL;

  TAILQ_INIT(&(schema->attributes));
  if ( json_object_object_get_ex(jschema, "attributes", &jattributes) && json_object_is_type(jattributes, json_type_object) )
    add_json_parameters(&(schema->attributes), jattributes);

  TAILQ_INIT(&(schema->methods));
  if ( json_object_object_get_ex(jschema, "methods", &jmethods) && json_object_is_type(jmethods, json_type_object) ) {
    json_object_object_foreach(jmethods, key, val)
      add_json_method(&(schema->methods), key, val);
  }

  TAILQ_INIT(&(schema->notifications));
  if ( json_object_object_get_ex(jschema, "notifications", &jnotifications) && json_object_is_type(jnotifications, json_type_object) ) {
    json_object_object_foreach(jnotifications, key, val)
      add_json_notification(&(schema->notifications), key, val);
  }
  
  TAILQ_INIT(&(schema->datamodel));
  if ( json_object_object_get_ex(jschema, "datamodel", &jdatamodel) && json_object_is_type(jdatamodel, json_type_object) ) {
    json_object_object_foreach(jdatamodel, key, val)
      add_json_datatype(&(schema->datamodel), key, val);
  }
}



/*
 * Section Parameters
 * used for Attributes Methodes Notifications
 */

/* select a parameter by its name */
parameter_t *select_parameter(parameters_t *parameters, const char *name) {
  parameter_t *np;

  TAILQ_FOREACH(np, parameters, entries)
    if (strcmp(name, np->name) == 0)
      return np;
  return NULL;
}


/* add parameters from a json map { * name:type } */
void add_json_parameters(parameters_t *parameters, json_object *jparameters) {
  const char *type;

  json_object_object_foreach(jparameters, name, jtype) {
    if ( validate_identifier(name) && json_object_is_type(jtype, json_type_string) && !select_parameter(parameters, name) && validate_identifier(type=json_object_get_string(jtype)) ) {
      parameter_t *parameter = (parameter_t *)malloc(sizeof(parameter_t));
      parameter->name = strdup(name);
      parameter->type = strdup(type);
      TAILQ_INSERT_TAIL(parameters, parameter, entries);
    }
  }
}


/* serialize a parameters list into cbor */
cbor_item_t *get_cbor_parameters(parameters_t *parameters) {
  cbor_item_t *cparameters = cbor_new_indefinite_map();
  parameter_t *parameter;

  TAILQ_FOREACH(parameter, parameters, entries)
    (void)!cbor_map_add(cparameters, (struct cbor_pair){ cbor_move(cbor_build_string(parameter->name)), cbor_move(cbor_build_string(parameter->type)) });
  return cparameters;
}


/* serialize a parameters list into json */
json_object *get_json_parameters(parameters_t *parameters) {
  json_object *jparameters = json_object_new_object();
  parameter_t *parameter;

  TAILQ_FOREACH(parameter, parameters, entries)
    json_object_object_add(jparameters, parameter->name, json_object_new_string(parameter->type));
  return jparameters;
}




/*
 * Section Attributes of a schema
 */

/* select an attribute by its name in a schema or in extended ones */
parameter_t *select_attribute(schemas_t *schemas, schema_t *schema, const char *name) {
  schema_t *p_schema = schema;
  parameter_t *attribute;

  while (p_schema) {
    attribute = select_parameter(&(p_schema->attributes), name);
    if (attribute)
      return attribute;
    p_schema = select_schema(schemas, p_schema->extends);
  }
  return NULL;
}


/* serialize attributes of a schema into cbor, including extended schemas recursively */
cbor_item_t *get_cbor_attributes(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return cbor_new_indefinite_map();
  else {
    cbor_item_t *cattributes = get_cbor_attributes(schemas, select_schema(schemas, schema->extends));
    parameter_t *attribute;
    TAILQ_FOREACH(attribute, &(schema->attributes), entries)
      xAAL_cbor_map_update(cattributes, attribute->name, cbor_build_string(attribute->type));
    return cattributes;
  }
}


/* serialize attributes of a schema into json, including extended schemas recursively */
json_object *get_json_attributes(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return json_object_new_object();
  else {
    json_object *jattributes = get_json_attributes(schemas, select_schema(schemas, schema->extends));
    parameter_t *attribute;
    TAILQ_FOREACH(attribute, &(schema->attributes), entries) {
      if (json_object_object_get_ex(jattributes, attribute->name, NULL))
	json_object_object_del(jattributes, attribute->name);
      json_object_object_add(jattributes, attribute->name, json_object_new_string(attribute->type));
    }
    return jattributes;
  }
}


/* Check if an attribute is querable by get_attributes() 
   i.e. Attributes of basic.basic are used with get_description() and not get_attributes() */
bool not_basic_attribute(const char *name) {
  static char *basic[] = { "dev_type", "address", "vendor_id", "product_id", "hw_id",
			  "version", "group_id", "url", "schema", "info",
			  "unsupported_attributess", "unsupported_methods",
			  "unsupported_notifications", NULL };
  char **p;
  for (p = basic; *p != NULL; p++)
    if (strcmp(name, *p) == 0)
      return false;
  return true;
}




/*
 * Section Methods of a schema
 */


/* select a method by its name */
method_t *select_method(methods_t *methods, const char *name) {
  method_t *np;

  TAILQ_FOREACH(np, methods, entries)
    if (strcmp(name, np->name) == 0)
      return np;
  return NULL;
}


/* add a method from json data */
void add_json_method(methods_t *methods, const char *name, json_object *jmethod) {
  json_object *jdescription, *jin, *jout;
  method_t *method;
  
  if (!validate_identifier(name))
    return;
    
  if (select_method(methods, name))
    return;
    
  method = (method_t *)malloc(sizeof(method_t));
  TAILQ_INSERT_TAIL(methods, method, entries);
  
  method->name = strdup(name);

  if ( json_object_object_get_ex(jmethod, "description", &jdescription) && json_object_is_type(jdescription, json_type_string) )
    method->description = strdup(json_object_get_string(jdescription));
  else
    method->description = NULL;

  TAILQ_INIT(&(method->in));
  if ( json_object_object_get_ex(jmethod, "in", &jin) && json_object_is_type(jin, json_type_object) )
    add_json_parameters(&(method->in), jin);

  TAILQ_INIT(&(method->out));
  if ( json_object_object_get_ex(jmethod, "out", &jout) && json_object_is_type(jout, json_type_object) )
    add_json_parameters(&(method->out), jout);
}


/* serialize a method into cbor */
cbor_item_t *get_cbor_method(method_t *method) {
  cbor_item_t *cmethod = cbor_new_indefinite_map();

  if (method->description)
    (void)!cbor_map_add(cmethod, (struct cbor_pair){ cbor_move(cbor_build_string("description")), cbor_move(cbor_build_string(method->description)) });
  (void)!cbor_map_add(cmethod, (struct cbor_pair){ cbor_move(cbor_build_string("in")), cbor_move(get_cbor_parameters(&(method->in))) });
  (void)!cbor_map_add(cmethod, (struct cbor_pair){ cbor_move(cbor_build_string("out")), cbor_move(get_cbor_parameters(&(method->out))) });
  return cmethod;
}


/* serialize a method into json */
json_object *get_json_method(method_t *method) {
  json_object *jmethod = json_object_new_object();

  if (method->description)
    json_object_object_add(jmethod, "description", json_object_new_string(method->description));
  json_object_object_add(jmethod, "in", get_json_parameters(&(method->in)));
  json_object_object_add(jmethod, "out", get_json_parameters(&(method->out)));
  return jmethod;
}


/* serialize methods of a schema into cbor, including extended schemas recursively */
cbor_item_t *get_cbor_methods(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return cbor_new_indefinite_map();
  else {
    cbor_item_t *cmethods = get_cbor_methods(schemas, select_schema(schemas, schema->extends));
    method_t *method;
    TAILQ_FOREACH(method, &(schema->methods), entries)
      xAAL_cbor_map_update(cmethods, method->name, get_cbor_method(method));
    return cmethods;
  }
}


/* serialize methods of a schema into json, including extended schemas recursively */
json_object *get_json_methods(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return json_object_new_object();
  else {
    json_object *jmethods = get_json_methods(schemas, select_schema(schemas, schema->extends));
    method_t *method;
    TAILQ_FOREACH(method, &(schema->methods), entries) {
      if (json_object_object_get_ex(jmethods, method->name, NULL))
	json_object_object_del(jmethods, method->name);
      json_object_object_add(jmethods, method->name, get_json_method(method));
    }
    return jmethods;
  }
}



/*
 * Section Notifications of a schema
 */

/* select a notification by its name */
bool select_notification(notifications_t *notifications, const char *name) {
  notification_t *np;

  TAILQ_FOREACH(np, notifications, entries)
    if (strcmp(name, np->name) == 0)
      return np;
  return NULL;
}


/* add a notification from json data */
void add_json_notification(notifications_t *notifications, const char *name, json_object *jnotification) {
  json_object *jdescription, *jout;
  notification_t *notification;
  
  if (!validate_identifier(name))
    return;

  if (select_notification(notifications, name))
    return;

  notification = (notification_t *)malloc(sizeof(notification_t));
  TAILQ_INSERT_TAIL(notifications, notification, entries);

  notification->name = strdup(name);

  if ( json_object_object_get_ex(jnotification, "description", &jdescription) && json_object_is_type(jdescription, json_type_string) )
    notification->description = strdup(json_object_get_string(jdescription));
  else
    notification->description = NULL;
    
  TAILQ_INIT(&(notification->out));
  if ( json_object_object_get_ex(jnotification, "out", &jout) && json_object_is_type(jout, json_type_object) )
    add_json_parameters(&(notification->out), jout);
}


/* serialize a notification into cbor */
cbor_item_t *get_cbor_notification(notification_t *notification) {
  cbor_item_t *cnotification = cbor_new_indefinite_map();

  if (notification->description)
    (void)!cbor_map_add(cnotification, (struct cbor_pair){ cbor_move(cbor_build_string("description")), cbor_move(cbor_build_string(notification->description)) });
  (void)!cbor_map_add(cnotification, (struct cbor_pair){ cbor_move(cbor_build_string("out")), cbor_move(get_cbor_parameters(&(notification->out))) });
  return cnotification;
}


/* serialize a notification into json */
json_object *get_json_notification(notification_t *notification) {
  json_object *jnotification = json_object_new_object();

  if (notification->description)
    json_object_object_add(jnotification, "description", json_object_new_string(notification->description));
  json_object_object_add(jnotification, "out", get_json_parameters(&(notification->out)));
  return jnotification;
}


/* serialize notifications of a schema into cbor, including extended schemas recursively */
cbor_item_t *get_cbor_notifications(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return cbor_new_indefinite_map();
  else {
    cbor_item_t *cnotifications = get_cbor_notifications(schemas, select_schema(schemas, schema->extends));
    notification_t *notification;
    TAILQ_FOREACH(notification, &(schema->notifications), entries)
      xAAL_cbor_map_update(cnotifications, notification->name, get_cbor_notification(notification));
    return cnotifications;
  }
}


/* serialize notifications of a schema into json, including extended schemas recursively */
json_object *get_json_notifications(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return json_object_new_object();
  else {
    json_object *jnotifications = get_json_notifications(schemas, select_schema(schemas, schema->extends));
    notification_t *notification;
    TAILQ_FOREACH(notification, &(schema->notifications), entries) {
      if (json_object_object_get_ex(jnotifications, notification->name, NULL))
	json_object_object_del(jnotifications, notification->name);
      json_object_object_add(jnotifications, notification->name, get_json_notification(notification));
    }
    return jnotifications;
  }
}



/*
 * Section DataModel of a schema
 */

/* select a datatype by its name */
datatype_t *select_datatype(datamodel_t *datamodel, const char *name) {
  datatype_t *np;

  TAILQ_FOREACH(np, datamodel, entries)
    if (strcmp(name, np->name) == 0)
      return np;
  return NULL;
}


/* add a datatype from json */
void add_json_datatype(datamodel_t *datamodel, const char *name, json_object *jdatatype) {
  json_object *jdescription, *junit, *jcddl;
  datatype_t *datatype;
  
  if ( !validate_identifier(name) )
    return;

  if ( select_datatype(datamodel, name) )
    return;

  datatype = (datatype_t *)malloc(sizeof(datatype_t));
  TAILQ_INSERT_TAIL(datamodel, datatype, entries);

  datatype->name = strdup(name);

  if ( json_object_object_get_ex(jdatatype, "description", &jdescription) && json_object_is_type(jdescription, json_type_string) )
    datatype->description = strdup(json_object_get_string(jdescription));
  else
    datatype->description = NULL;

  if ( json_object_object_get_ex(jdatatype, "unit", &junit) && json_object_is_type(junit, json_type_string) )
    datatype->unit = strdup(json_object_get_string(junit));
  else
    datatype->unit = NULL;

  if ( json_object_object_get_ex(jdatatype, "type", &jcddl) && json_object_is_type(jcddl, json_type_string) )
    datatype->cddl = strdup(json_object_get_string(jcddl));
  else
    datatype->cddl = NULL;
}


/* serialize a datatype to cbor */
cbor_item_t *get_cbor_datatype(datatype_t *datatype) {
  cbor_item_t *cdatatype = cbor_new_indefinite_map();
  if (datatype->description)
    (void)!cbor_map_add(cdatatype, (struct cbor_pair){ cbor_move(cbor_build_string("description")), cbor_move(cbor_build_string(datatype->description)) });
  if (datatype->unit)
    (void)!cbor_map_add(cdatatype, (struct cbor_pair){ cbor_move(cbor_build_string("unit")), cbor_move(cbor_build_string(datatype->unit)) });
  if (datatype->cddl)
    (void)!cbor_map_add(cdatatype, (struct cbor_pair){ cbor_move(cbor_build_string("type")), cbor_move(cbor_build_string(datatype->cddl)) });
  return cdatatype;
}


/* serialize a data type to json */
json_object *get_json_datatype(datatype_t *datatype) {
  json_object *jdatatype = json_object_new_object();
  if (datatype->description)
    json_object_object_add(jdatatype, "description", json_object_new_string(datatype->description));
  if (datatype->unit)
    json_object_object_add(jdatatype, "unit", json_object_new_string(datatype->unit));
  if (datatype->cddl)
    json_object_object_add(jdatatype, "type", json_object_new_string(datatype->cddl));
  return jdatatype;
}


/* serialize datamodel of a schema to cbor, including extended schemas recursively */
cbor_item_t *get_cbor_datamodel(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return cbor_new_indefinite_map();
  else {
    cbor_item_t *cdatamodel = get_cbor_datamodel(schemas, select_schema(schemas, schema->extends));
    datatype_t *datatype;
    TAILQ_FOREACH(datatype, &(schema->datamodel), entries)
      xAAL_cbor_map_update(cdatamodel, datatype->name, get_cbor_datatype(datatype));
    return cdatamodel;
  }
}


/* serialize datamodel of a schema to json, including extended schemas recursively */
json_object *get_json_datamodel(schemas_t *schemas, schema_t *schema) {
  if (schema == NULL)
    return json_object_new_object();
  else {
    json_object *jdatamodel = get_json_datamodel(schemas, select_schema(schemas, schema->extends));
    datatype_t *datatype;
    TAILQ_FOREACH(datatype, &(schema->datamodel), entries) {
      if (json_object_object_get_ex(jdatamodel, datatype->name, NULL))
	json_object_object_del(jdatamodel, datatype->name);
      json_object_object_add(jdatamodel, datatype->name, get_json_datatype(datatype));
    }
    return jdatamodel;
  }
}

