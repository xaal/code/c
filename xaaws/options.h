/* xaaws - xAAL web interface
 * Part of the 'xaaws' software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPTIONS_
#define _OPTIONS_

#include <stdbool.h>
#include <uuid/uuid.h>

/* Options from cmdline or conffile */
typedef struct {
  char *addr;
  char *port;
  int   hops;
  char *passphrase;
  uuid_t *uuid;
  char *conffile;
  bool  immutable;
  bool  daemon;
  char *logfile;
  char *lockfile;
  int   uid;
  int   gid;
  int   verbose;
  char *http_port;
  char *http_iface;
  char *resource_path;
  char *dbfile;
  char *schemas;
} options_t;


/* Parse cmdline */
void parse_cmdline(int argc, char **argv, options_t *opts);

/* Read a config file (json format) */
void read_config(options_t *opts);

/* Init the path of schemas cache */
void init_schemas_path(options_t *opts, const char *cache);

/* Re-write config file (json format) */
void write_config(options_t *opts);

#endif
