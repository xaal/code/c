/* db - Database for xAAL Agent
 * Part of the xaaws software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <regex.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "db.h"

/*
 * Section Database
 */

/* Init DB */
void init_db(db_t *db) {
  TAILQ_INIT(&(db->devices));
  TAILQ_INIT(&(db->schemas));
  TAILQ_INIT(&(db->jmsgreqs));
  TAILQ_INIT(&(db->cmsgreqs));
  TAILQ_INIT(&(db->jattrreqs));
  TAILQ_INIT(&(db->cattrreqs));
}




/*
 * Section mixing Schemas and Devices
 */

/* Build a list of attributes names that are missing in the list of values */
/* according to schema and extended ones */
cbor_item_t *missing_values(device_t *device, schemas_t *schemas) {
  cbor_item_t *cattributes = cbor_new_indefinite_array();
  schema_t *p_schema = select_schema(schemas, device->devinfo.dev_type);
  parameter_t *attribute;

  while (p_schema) {
    TAILQ_FOREACH(attribute, &(p_schema->attributes), entries)
      if (   not_basic_attribute(attribute->name) 
          && !select_value(&(device->vals), attribute->name)
	  && !xAAL_strings_get(device->devinfo.unsupported_attributes, attribute->name) )
	(void)!cbor_array_push(cattributes, cbor_move(cbor_build_string(attribute->name)));
    p_schema = select_schema(schemas, p_schema->extends);
  }
  return cattributes;
}



/*
 * Section Request Queues (http streams or websockets)
 */

/* Remove a wsi from requests queues */
void remove_wsi(db_t *db, struct lws *wsi) {
  req_t *req;

  TAILQ_FOREACH(req, &(db->jmsgreqs), entries)
    if (req->wsi == wsi) {
      TAILQ_REMOVE(&(db->jmsgreqs), req, entries);
      free(req);
      return;
    }
  TAILQ_FOREACH(req, &(db->cmsgreqs), entries)
    if (req->wsi == wsi) {
      TAILQ_REMOVE(&(db->cmsgreqs), req, entries);
      free(req);
      return;
    }
  TAILQ_FOREACH(req, &(db->jattrreqs), entries)
    if (req->wsi == wsi) {
      TAILQ_REMOVE(&(db->jattrreqs), req, entries);
      free(req);
      return;
    }
  TAILQ_FOREACH(req, &(db->cattrreqs), entries)
    if (req->wsi == wsi) {
      TAILQ_REMOVE(&(db->cattrreqs), req, entries);
      free(req);
      return;
    }
}
