var dev_addr;
var methods;
var datamodel;
var socket;


function populate_device_table(device) {
  if ('address' in device)
    $('#address').html(device.address);
  if ('dev_type' in device)
    $('#dev_type').html('<a href="/schemas/'+device.dev_type+'">'+device.dev_type+'</a>');
  if ('vendor_id' in device)
    $('#vendor_id').html(device.vendor_id);
  if ('product_id' in device)
    $('#product_id').html(device.product_id);
  if ('hw_id' in device)
    $('#hw_id').append($('<pre/>').html(JSON.stringify(device.hw_id)));
  if ('version' in device)
    $('#version').html(device.version);
  if ('group_id' in device)
    $('#group_id').html('<a href="/device.html?group_id='+device.group_id+'">'+device.group_id+'</a>');
  if ('url' in device)
    $('#url').html('<a href="'+device.url+'">'+device.url+'</a>');
  if ('schema' in device)
    $('#schema').html('<a href="'+device.schema+'">'+device.schema+'</a>');
  if ('info' in device)
    $('#info').html(device.info);
  if ('timeout' in device)
    $('#timeout').html(device.timeout);
  if ('unsupported_attributes' in device)
    $('#unsupported_attributes').append($('<pre/>').html(JSON.stringify(device.unsupported_attributes)));
  if ('unsupported_methods' in device)
    $('#unsupported_methods').append($('<pre/>').html(JSON.stringify(device.unsupported_methods)));
  if ('unsupported_notifications' in device)
    $('#unsupported_notifications').append($('<pre/>').html(JSON.stringify(device.unsupported_notifications)));

  $.ajax({
    url: '/json/map?device='+device.address,
    dataType: 'json',
    data: null,
    success: function(map){
      $.each(map, function(key, value) {
	if (key)
	  $('#map').append(' ');
	$('#map').append('<a href="/index.html?map='+key+'">'+key+'</a>'+':"'+value+'"');
      });
    }
  });

};


function populate_attributes_table(attributes){
  $.each(attributes, function(i, attribute) {
    $('#attributes').append($('<tr/>')
      .append($('<td/>').html(attribute.name))
      .append($('<td/>').html(attribute.type))
      .append($('<td/>').append($('<pre/>').html(JSON.stringify(attribute.value, null, 2)).attr('id', attribute.name+'_value')))
      .append($('<td/>').html(attribute.date).attr('id', attribute.name+'_date'))
    );
  });
};



function populate_methods_table(data) {
  methods = data;

  $.each(methods, function(name, method) {

    var in_parameters_layout = $('<p/>');
    for (var parameter in method.in)
      in_parameters_layout.append($('<div id="'+name+'_in_'+parameter+'"/>').html(parameter+':'+method.in[parameter]));
    
    var out_parameters_layout = $('<p/>');
    for (var parameter in method.out)
      out_parameters_layout.append($('<div id="'+name+'_out_'+parameter+'"/>').html(parameter+':'+method.out[parameter]));

    $('#methods').append($('<tr/>')
      .append($('<td/>').html('<button id="'+name+'" onClick="send(this.id)">'+name+'</button>'))
      .append($('<td/>').html(method.description))
      .append($('<td/>').html(in_parameters_layout))
      .append($('<td/>').html(out_parameters_layout))
    );

    for (var parameter in method.in) {
      var typename = method.in[parameter];
      method.in[parameter] = {};
      method.in[parameter].typename = typename;
      const container = document.getElementById(name+'_in_'+parameter);
      const options = { schema: {title:' '} }; //datamodel[typename].type ...
      method.in[parameter].editor = new JSONEditor(container, options);
    }

  });
};



function populate_datamodel_table(data){
  datamodel = data;
  $.each(datamodel, function(typename, datatype) {
    $('#datamodel').append($('<tr/>')
      .append($('<td/>').html(typename))
      .append($('<td/>').html(datatype.description))
      .append($('<td/>').html(datatype.unit))
      .append($('<td/>').append($('<pre/>').html(datatype.type)))
    );
  });
};



function send(name) {
  var msg = { targets:[dev_addr], action:name };

  if ('in' in methods[name])
    for (parameter in methods[name].in)
      if ('editor' in methods[name].in[parameter]) {
        if (!('body' in msg)) msg.body = {};
        msg.body[parameter] = methods[name].in[parameter].editor.get_value();
      }

  socket.send(JSON.stringify(msg));
}



dev_addr = $.url().param('device');
$('#title1').append(' <em>'+dev_addr+'</em>');

$.getJSON('/json/description?device='+dev_addr, populate_device_table);
$.getJSON('/json/attributes?device='+dev_addr, populate_attributes_table);
$.getJSON('/json/datamodel?device='+dev_addr, populate_datamodel_table);
$.getJSON('/json/methods?device='+dev_addr, populate_methods_table);

socket = new WebSocket( "ws://" + document.domain + ':' + location.port, "xaal-ctl" );
socket.onerror = function() { console.log(event.data); };

socket.onmessage = function(event) { 
  var newAttr = JSON.parse(event.data);
  if (newAttr.device == dev_addr) {
    $.each(newAttr.attributes, function(i, attribute) {
      document.getElementById(attribute.name+'_value').innerHTML = JSON.stringify(attribute.value, null, 2);
      document.getElementById(attribute.name+'_date').innerHTML = attribute.date;
    });
  }
};
