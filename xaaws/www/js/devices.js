function populate_devices_table(devices) {
  $.each(devices, function(i, device) {

    var td_map = $('<td/>');
    $.ajax({
      url: '/json/map?device='+device.address,
      dataType: 'json',
      data: null,
      success: function(map){
	$.each(map, function(key, value) {
	  if (key)
	    td_map.append(' ');
	  td_map.append('<a href="/index.html?key='+key+'">'+key+'</a>'+':"'+value+'"');
	});
      }
    });

    $('#devices').append($('<tr/>')
      .append($('<td/>').html('<a href="/device.html?device='+device.address+'">'+device.address+'</a>'))
      .append($('<td/>').html('<a href="/schemas/'+device.dev_type+'">'+device.dev_type+'</a>'))
      .append(td_map)
      .append($('<td/>').html(device.timeout))
    );
  });
};


$(document).ready(function() {
  key = $.url().param('key');
  if (key) {
    jqurl = '/json/devices?key='+key;
    $('#title1').append(' with key <em>'+key+'</em>');
    $('#footer').append($('<p style="float:left;"/>').html('<a href="index.html">Home &larr;</a>'));
  } else {
    jqurl = '/json/devices';
  }

  $.getJSON(jqurl, populate_devices_table);
});
