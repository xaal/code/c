try{
  var socket = new WebSocket( "ws://" + document.domain + ':' + location.port, "xaal-dump" );

  socket.onmessage = function(event) { 
    var theDiv = document.getElementById('dump');
    var newNode = document.createElement('pre');
    newNode.innerHTML = Date() + '<br>' + JSON.stringify(JSON.parse(event.data), null, 2);
    theDiv.insertBefore(newNode, theDiv.firstChild);

  };

  socket.onerror = function(event) {
     console.log(event.message);
  };

} catch(e) {
  theDiv.innerHTML = 'Not supported';
}
