/* db - Database for xAAL Agent
 * Part of the xaaws software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DB_
#define _DB_

#include <sys/queue.h>
#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>
#include <libwebsockets.h>
#include <sys/queue.h>

#include <xaal.h>

#include "db-device.h"
#include "db-schema.h"


#define SCHEMAS_CACHE	"schemas"
// #define SCHEMAS_SERVER	"http://recherche.imt-atlantique.fr/xaal/documentation"
#define SCHEMAS_SERVER	"https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7"

/*
 * Section Definition of Data Structures
 */

typedef TAILQ_HEAD(reqhead, reqentry) reqs_t;
typedef struct reqentry {
  struct lws *wsi;
  TAILQ_ENTRY(reqentry) entries;
} req_t;


typedef struct {
  devices_t devices;
  schemas_t schemas;
  reqs_t jmsgreqs;	/* request queue for xAAL msg in json */
  reqs_t cmsgreqs;	/* request queue for xAAL msg in cbor */
  reqs_t jattrreqs;	/* request queue for attributes_change in json */
  reqs_t cattrreqs;	/* request queue for attributes_change in cbor */
} db_t;



/*
 * Section Database Itself
 */

/* Init DB */
void init_db(db_t *db);



/*
 * Section mixing Schemas and Devices
 */

/* Build a list of attributes names that are missing in the list of values */
/* according to schema and extended ones */
cbor_item_t *missing_values(device_t *device, schemas_t *schemas);




/*
 * Section Request Queues (http streams or websockets)
 */

/* Remove a wsi from requests queues */
void remove_wsi(db_t *db, struct lws *wsi);

#endif
