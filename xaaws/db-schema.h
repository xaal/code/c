/* db - Database for xAAL Agent
 * Part of the xaaws software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DBSCHEMA_
#define _DBSCHEMA_

#include <sys/queue.h>
#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>
#include <xaal.h>



/*
 * Section Definition of Data Structures
 */

typedef TAILQ_HEAD(parametershead, parametersentry) parameters_t;
typedef struct parametersentry {
  char *name;
  char *type;
  TAILQ_ENTRY(parametersentry) entries;
} parameter_t;


typedef TAILQ_HEAD(methodshead, methodentry) methods_t;
typedef struct methodentry {
  char *name;
  char *description;
  parameters_t in;
  parameters_t out;
  TAILQ_ENTRY(methodentry) entries;
} method_t;


typedef TAILQ_HEAD(notificationshead, notificationentry) notifications_t;
typedef struct notificationentry {
  char *description;
  char *name;
  parameters_t out;
  TAILQ_ENTRY(notificationentry) entries;
} notification_t;


typedef TAILQ_HEAD(datamodelhead, datamodelentry) datamodel_t;
typedef struct datamodelentry {
  char *name;
  char *description;
  char *unit;
  char *cddl;
  TAILQ_ENTRY(datamodelentry) entries;
} datatype_t;


typedef TAILQ_HEAD(schemashead, schemaentry) schemas_t;
typedef struct schemaentry {
  char *title;
  char *description;
  char *lang;
  char *documentation;
  char *ref;
  char *license;
  char *extends;
  parameters_t attributes;
  methods_t methods;
  notifications_t notifications;
  datamodel_t datamodel;
  TAILQ_ENTRY(schemaentry) entries;
} schema_t;



/* validate the name of an attribute/type/methode/notification identifier */
bool validate_identifier(const char *identifier);



/*
 * Section Schemas
 */

/* validate the name of a schema (dev_type) */
bool validate_dev_type(const char *dev_type);

/* load all schemas in the cache */
void load_schemas(schemas_t *schemas, const char *dir);

/* load json file (e.g. a schema) */
json_object *load_json_file(const char *dir, const char *name);

/* download a file (e.g. a schema) */
bool download_file(const char *baseurl, const char *name, const char *destdir);

/* add a schema from json data */
void add_json_schema(schemas_t *schemas, json_object *jschema);

/* select a schema by its name */
schema_t *select_schema(schemas_t *schemas, const char *title);



/*
 * Section Parameters
 * used for Attributes Methodes Notifications
 */

/* select a parameter by its name */
parameter_t *select_parameter(parameters_t *parameters, const char *name);

/* add parameters from a json map { * name:type } */
void add_json_parameters(parameters_t *parameters, json_object *jparameters);

/* serialize a parameters list to cbor */
cbor_item_t *get_cbor_parameters(parameters_t *parameters);

/* serialize a parameters list to json */
json_object *get_json_parameters(parameters_t *parameters);



/*
 * Section Attributes of a schema
 *
 * Note: schema's attributes are a parameters data structure
 * with the schema's extention process
 */ 
 
/* select an attribute by its name in a schema or in extended ones */
parameter_t *select_attribute(schemas_t *schemas, schema_t *schema, const char *name);

/* serialize attributes of a schema to cbor, including extended schemas recursively */
cbor_item_t *get_cbor_attributes(schemas_t *schemas, schema_t *schema);

/* serialize attributes of a schema to json, including extended schemas recursively */
json_object *get_json_attributes(schemas_t *schemas, schema_t *schema);

/* Check if an attribute is querable by get_attributes() 
   i.e. Attributes of basic.basic are used with get_description() and not get_attributes() */
bool not_basic_attribute(const char *name);



/*
 * Section Methods of a schema
 */

/* select a method by its name */
method_t *select_method(methods_t *methods, const char *name);

/* add a method from json data */
void add_json_method(methods_t *methods, const char *name, json_object *jmethod);

/* serialize a method to cbor */
cbor_item_t *get_cbor_method(method_t *method);

/* serialize a method to json */
json_object *get_json_method(method_t *method);

/* serialize methods of a schema to cbor, including extended schemas recursively */
cbor_item_t *get_cbor_methods(schemas_t *schemas, schema_t *schema);

/* serialize methods of a schema to json, including extended schemas recursively */
json_object *get_json_methods(schemas_t *schemas, schema_t *schema);



/*
 * Section Notifications of a schema
 */

/* select a notification by its name */
bool select_notification(notifications_t *notifications, const char *name);

/* add a notification from json data */
void add_json_notification(notifications_t *notifications, const char *name, json_object *jnotification);

/* serialize a notification to cbor */
cbor_item_t *get_cbor_notification(notification_t *notification);

/* serialize a notification to json */
json_object *get_json_notification(notification_t *notification);

/* serialize notifications of a schema to cbor, including extended schemas recursively */
cbor_item_t *get_cbor_notifications(schemas_t *schemas, schema_t *schema);

/* serialize notifications of a schema to json, including extended schemas recursively */
json_object *get_json_notifications(schemas_t *schemas, schema_t *schema);



/* 
 * Section DataModel of a schema
 */

/* select a datatype by its name */
datatype_t *select_datatype(datamodel_t *datamodel, const char *name);

/* add a datatype from json */
void add_json_datatype(datamodel_t *datamodel, const char *name, json_object *jdatatype);

/* serialize a datatype to cbor */
cbor_item_t *get_cbor_datatype(datatype_t *datatype);

/* serialize a datatype to json */
json_object *get_json_datatype(datatype_t *datatype);

/* serialize datamodel of a schema to cbor, including extended schemas recursively */
cbor_item_t *get_cbor_datamodel(schemas_t *schemas, schema_t *schema);

/* serialize datamodel of a schema to json, including extended schemas recursively */
json_object *get_json_datamodel(schemas_t *schemas, schema_t *schema);


#endif
