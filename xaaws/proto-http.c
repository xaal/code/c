/* xaaws - xAAL web interface
 * Part of the 'xaaws' software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Acknowledgement: Andy Green <andy@warmcat.com>
 * libwebsockets-test-server (c) 2010-2016 - CC0 1.0
 */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <libwebsockets.h>
#include <sys/queue.h>
#include <stdio.h>

#include "proto-http.h"
#include "xaaws.h"
#include "db.h"
#include "xaagent.h"


const char *get_mimetype(const char *file) {
  int n = strlen(file);

  if ((n>=3) && !strcasecmp(&file[n - 3], ".js"))
    return "application/javascript";

  if ((n>=5) && !strcasecmp(&file[n - 5], ".html"))
    return "text/html";

  if ((n>=4) && !strcasecmp(&file[n - 4], ".css"))
    return "text/css";

  if ((n>=4) && !strcasecmp(&file[n - 4], ".ico"))
    return "image/x-icon";

  if ((n>=4) && !strcasecmp(&file[n - 4], ".png"))
    return "image/png";

  if ((n>=4) && !strcasecmp(&file[n - 4], ".jpg"))
    return "image/jpeg";

  if ((n>=4) && !strcasecmp(&file[n - 4], ".svg"))
    return "image/svg+xml";

  if (strstr(file, SCHEMAS_CACHE)) //ugly
    return "application/json";

  return NULL;
}



/* My own implementation of lws_get_urlarg_by_name() provided by more recent lib */
char* get_urlarg_by_name(struct lws *wsi, const char *name) {
  int name_sz = strlen(name);
  char name_eq[ name_sz ];
  char buf[256];
  int n, min, len = sizeof(buf);

  memcpy(name_eq, name, name_sz);
  name_eq[name_sz++] = '=';
  min = (name_sz > len) ? len : name_sz;

  for (n=0; lws_hdr_copy_fragment(wsi, buf, len, WSI_TOKEN_HTTP_URI_ARGS, n) > 0; n++)
    if ( strncmp(buf, name_eq, min) == 0 )
      return strdup(buf+name_sz);

  return NULL;
}


/* this protocol server (always the first one) handles HTTP,
 *
 * Some misc callbacks that aren't associated with a protocol also turn up only
 * here on the first protocol server.
 */

int callback_http(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len) {
  struct per_session_data__http *pss = (struct per_session_data__http *)user;
  struct lws_pollargs *pa = (struct lws_pollargs *)in;
  ssize_t rest_sz = strlen(REST_JSON_PATH);
  const char *mimetype;
  char buf[256];
  int n, m;

  switch (reason) {
    case LWS_CALLBACK_HTTP:
      pss->janswer = NULL;
      pss->post_uri = NULL;
      pss->post_msg = NULL;
      pss->post_len = 0;
      pss->complete = true;

      if (len < 1) {
	lws_return_http_status(wsi, HTTP_STATUS_BAD_REQUEST, NULL);
	return -1;
      }

      /* if a legal POST URL, let it continue and accept data */
      if (lws_hdr_total_length(wsi, WSI_TOKEN_POST_URI)) {
	pss->post_uri = strdup(in);
	break;
      }

      /* Our pseudo-REST API */
      if ( strncmp(in, REST_JSON_PATH, rest_sz) == 0) {

	if ( strcmp(in+rest_sz, "devices") == 0) {
	  char *query_key = get_urlarg_by_name(wsi, "key");
	  pss->janswer = get_json_devices(&(db.devices), query_key);
	  if (query_key) free(query_key);

	} else if (strcmp(in+rest_sz, "description") == 0) {
	  char *query_dev = get_urlarg_by_name(wsi, "device");
	  if (query_dev) {
	    uuid_t uuid;
	    if ( uuid_parse(query_dev, uuid) == 0 )
	      pss->janswer = get_json_device_by_addr(&(db.devices), &uuid);
	    free(query_dev);
	  }

	} else if (strcmp(in+rest_sz, "attributes") == 0) {
	  char *query_dev = get_urlarg_by_name(wsi, "device");
	  if (query_dev) {
	    uuid_t uuid;
	    if ( uuid_parse(query_dev, uuid) == 0 )
	      pss->janswer = get_json_values_by_addr(&(db.devices), &uuid);
	    free(query_dev);
	  }

	} else if (strcmp(in+rest_sz, "map") == 0) {
	  char *query_dev = get_urlarg_by_name(wsi, "device");
	  if (query_dev) {
	    uuid_t uuid;
	    if ( uuid_parse(query_dev, uuid) == 0 )
	      pss->janswer = get_json_kvs_by_addr(&(db.devices), &uuid);
	    free(query_dev);
	  }

	} else if (strcmp(in+rest_sz, "methods") == 0) {
	  char *query_dev = get_urlarg_by_name(wsi, "device");
	  if (query_dev) {
	    uuid_t uuid;
	    if ( uuid_parse(query_dev, uuid) == 0 ) {
	      device_t *device = select_device(&(db.devices), &uuid);
	      if (device)
	        pss->janswer = get_json_methods( &(db.schemas), select_schema(&(db.schemas), device->devinfo.dev_type) );
            }
	    free(query_dev);
	  }

	} else if (strcmp(in+rest_sz, "datamodel") == 0) {
	  char *query_dev = get_urlarg_by_name(wsi, "device");
	  if (query_dev) {
	    uuid_t uuid;
	    if ( uuid_parse(query_dev, uuid) == 0 ) {
	      device_t *device = select_device(&(db.devices), &uuid);
	      if (device)
	        pss->janswer = get_json_datamodel( &(db.schemas), select_schema(&(db.schemas), device->devinfo.dev_type) );
            }
	    free(query_dev);
	  }

	} else if (strcmp(in+rest_sz, "dump") == 0) {
	  req_t *req = (req_t *) malloc( sizeof(req_t) );
	  req->wsi = wsi;
	  TAILQ_INSERT_TAIL(&(db.jmsgreqs), req, entries);
	  pss->complete = false;
	  lws_callback_on_writable(wsi);
	  break;

	} else if (strcmp(in+rest_sz, "attributes_change") == 0) {
	  req_t *req = (req_t *) malloc( sizeof(req_t) );
	  req->wsi = wsi;
	  TAILQ_INSERT_TAIL(&(db.jattrreqs), req, entries);
	  pss->complete = false;
	  lws_callback_on_writable(wsi);
	  break;

	}

	if (pss->janswer) {
	  lws_callback_on_writable(wsi);
	  break;
	} else {
	  lws_return_http_status(wsi, HTTP_STATUS_BAD_REQUEST, NULL);
	  return -1;
	}
      }

      /* send a file the easy way */
      strcpy(buf, resource_path);

      if (strcmp(in, "/")) {
	if (*((const char *)in) != '/')
	  strcat(buf, "/");
	strncat(buf, in, sizeof(buf) - strlen(buf) - 1);
      } else			/* default file to serve */
	strcat(buf, "/index.html");
      buf[sizeof(buf) - 1] = '\0';

      /* refuse to serve files we don't understand */
      mimetype = get_mimetype(buf);
      if (!mimetype) {
	lwsl_err("Unknown mimetype for %s\n", buf);
	lws_return_http_status(wsi, HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE, NULL);
	return -1;
      }

      n = lws_serve_http_file(wsi, buf, mimetype, NULL, 0);
      if (n < 0 || ((n > 0) && lws_http_transaction_completed(wsi)))
	return -1;	/* error or can't reuse connection: close the socket */
      break;		/*the sending of the file completes asynchronously */

    case LWS_CALLBACK_HTTP_BODY:
      pss->post_msg = realloc(pss->post_msg, pss->post_len+=len);
      memcpy(pss->post_msg, in, pss->post_len);
      break;

    case LWS_CALLBACK_HTTP_BODY_COMPLETION:
      if (!pss->post_uri || !pss->post_msg) {
	free(pss->post_uri);
	free(pss->post_msg);
	goto try_to_reuse;
      }

      if ( strcmp(pss->post_uri, REST_JSON_PATH "send") == 0 )
	pss->janswer = post_xAAL_json_request(&bus, &me, pss->post_msg, pss->post_len);
      else
	pss->janswer = NULL;

      free(pss->post_uri);
      free(pss->post_msg);
      pss->post_uri = NULL;
      pss->post_msg = NULL;
      pss->post_len = 0;
      pss->complete = true;

      if (pss->janswer) {
	lws_callback_on_writable(wsi);
	break;
      } else {
	lws_return_http_status(wsi, HTTP_STATUS_BAD_REQUEST, NULL);
	return -1;
      }


    case LWS_CALLBACK_HTTP_WRITEABLE:
      if (!pss->janswer && !pss->complete) {
	if ( hdr_http_stream(wsi) ) {
	  //lws_client_http_body_pending(wsi, 1);
	  break;
	} else {
	  lwsl_err("Bad REST\n");
	  lws_return_http_status(wsi, HTTP_STATUS_INTERNAL_SERVER_ERROR, NULL);
	  return -1;
	}
      }

      if (pss->janswer) {
	if (pss->complete) {
	  if ( serve_http_json(wsi, pss->janswer) ) {
	    json_object_put(pss->janswer);
	    pss->janswer = NULL;
	    //lws_client_http_body_pending(wsi, 0);
	    goto try_to_reuse;
	  } else {
	    lwsl_err("Bad REST\n");
	    lws_return_http_status(wsi, HTTP_STATUS_INTERNAL_SERVER_ERROR, NULL);
	    json_object_put(pss->janswer);
	    pss->janswer = NULL;
	    return -1;
	  }

	} else {
	  if ( serve_http_json_stream(wsi, pss->janswer) ) {
	    json_object_put(pss->janswer);
	    pss->janswer = NULL;
	    //lws_client_http_body_pending(wsi, 1);
	    break;
	  } else {
	    lwsl_err("Bad REST\n");
	    lws_return_http_status(wsi, HTTP_STATUS_INTERNAL_SERVER_ERROR, NULL);
	    return -1;
	  }
	}
      }
      break;

    case LWS_CALLBACK_WSI_DESTROY:
      remove_wsi(&db, wsi);
      break;

    case LWS_CALLBACK_ADD_POLL_FD:
      if (count_pollfds >= max_poll_elements) {
	lwsl_err("LWS_CALLBACK_ADD_POLL_FD: too many sockets to track\n");
	goto try_to_reuse;
      }
      fd_lookup[pa->fd] = count_pollfds;
      pollfds[count_pollfds].fd = pa->fd;
      pollfds[count_pollfds].events = pa->events;
      pollfds[count_pollfds++].revents = 0;
      break;

    case LWS_CALLBACK_DEL_POLL_FD:
      if (!--count_pollfds)
	break;
      m = fd_lookup[pa->fd];
      /* have the last guy take up the vacant slot */
      pollfds[m] = pollfds[count_pollfds];
      fd_lookup[pollfds[count_pollfds].fd] = m;
      break;

    case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
      pollfds[fd_lookup[pa->fd]].events = pa->events;
      break;

    default:
      break;
  }

  return 0;

  /* if we're on HTTP1.1 or 2.0, will keep the idle connection alive */
try_to_reuse:
  if (lws_http_transaction_completed(wsi))
    return -1;
  return 0;
}


/* send (cbor|json) data to an http connexion */
bool serve_http(struct lws *wsi, const unsigned char *buf, size_t len, const char *mime) {
  unsigned char buffer[4096 + LWS_PRE];
  unsigned char *p, *end, *start;

  /* Build and send HTTP header */
  p = buffer + LWS_PRE;
  start = p;
  end = p + sizeof(buffer) - LWS_PRE;

  if (lws_add_http_header_status(wsi, HTTP_STATUS_OK, &p, end))
    return false;

  if (lws_add_http_header_by_token(wsi, WSI_TOKEN_HTTP_CONTENT_TYPE, (unsigned char *)mime, strlen(mime), &p, end))
    return false;

  if (lws_add_http_header_by_token(wsi, WSI_TOKEN_HTTP_CACHE_CONTROL, (unsigned char *)"no-cache", strlen("no-cache"), &p, end))
    return false;

  if (lws_add_http_header_content_length(wsi, len, &p, end))
    return false;

  if (lws_finalize_http_header(wsi, &p, end))
    return false;

  if ( lws_write(wsi, start, p - start, LWS_WRITE_HTTP_HEADERS) < 0 )
    return false;

  /* Build and send json body */
  if ( lws_write_http(wsi, buf, len) < 0 )
    return false;

  return true;
}


/* send serialized json data to an http connexion */
bool serve_http_json(struct lws *wsi, json_object *jobj) {
  const char *json = json_object_to_json_string_ext(jobj, JSON_C_TO_STRING_PLAIN|JSON_C_TO_STRING_NOZERO);
  return serve_http(wsi, (const unsigned char *)json, strlen(json), "application/json");
}


/* send serialized cbor data to an http connexion */
bool serve_http_cbor(struct lws *wsi, cbor_item_t *item) {
  unsigned char *cbor;
  size_t sz, len;
  bool r;

  len = cbor_serialize_alloc(item, &cbor, &sz);
  if (!len)
    return false;
    
  r = serve_http(wsi, cbor, len, "application/cbor");
  free(cbor);
  return r;
}



#define STREAM_PREFIX	"data: "
#define STREAM_SUFFIX	"\r\n\r\n"

/* send serialized json data to an http streaming connexion */
bool serve_http_json_stream(struct lws *wsi, json_object *jobj) {
  const char *json = json_object_to_json_string_ext(jobj, JSON_C_TO_STRING_PLAIN|JSON_C_TO_STRING_NOZERO);
  char buf[ strlen(STREAM_PREFIX) + strlen(json) + strlen(STREAM_SUFFIX) + 1];
  int len;

  len = sprintf(buf, STREAM_PREFIX "%s" STREAM_SUFFIX, json);
  return ( lws_write_http(wsi, buf, len) >= 0 );
}


/* send serialized cbor data to an http streaming connexion */
/* note: this is binary data */
bool serve_http_cbor_stream_binary(struct lws *wsi, cbor_item_t *item) {
  unsigned char *cbor;
  char *buf;
  size_t sz, len, len2;
  bool r;
  
  len = cbor_serialize_alloc(item, &cbor, &sz);
  if (!len)
    return false;

  buf = malloc(strlen(STREAM_PREFIX) + len + strlen(STREAM_SUFFIX) + 1);
  len2 = sprintf(buf, STREAM_PREFIX "%s" STREAM_SUFFIX, cbor);
  r = ( lws_write_http(wsi, buf, len2) >= 0 );
  free(buf);
  free(cbor);  

  return r;
}


/* send pretty-printed cbor describe data to an http streaming connexion */
/* note: this is textual data */
bool serve_http_cbor_stream(struct lws *wsi, cbor_item_t *item) {
  char *buf;
  size_t len;
  FILE *stream;
  bool r;
  
  stream = open_memstream (&buf, &len);
  fprintf(stream, STREAM_PREFIX);
  cbor_describe(item, stream);
  fprintf(stream, STREAM_SUFFIX);
  fflush(stream);
  r = ( lws_write_http(wsi, buf, len) >= 0 );
  fclose (stream);
  
  return r;
}


/* build headers for replying to an http streaming connexion */
bool hdr_http_stream(struct lws *wsi) {
  unsigned char buffer[4096 + LWS_PRE];
  unsigned char *p, *end, *start;

  p = buffer + LWS_PRE;
  start = p;
  end = p + sizeof(buffer) - LWS_PRE;

  if (lws_add_http_header_status(wsi, HTTP_STATUS_OK, &p, end))
    return false;

  if (lws_add_http_header_by_token(wsi,
		  WSI_TOKEN_HTTP_CONTENT_TYPE,
		  (unsigned char *)"text/event-stream",
		  strlen("text/event-stream"), &p, end))
    return false;

  if (lws_add_http_header_by_token(wsi,
		  WSI_TOKEN_HTTP_CACHE_CONTROL,
		  (unsigned char *)"no-cache",
		  strlen("no-cache"), &p, end))
    return false;

  if (lws_add_http_header_by_token(wsi,
		  WSI_TOKEN_CONNECTION,
		  (unsigned char *)"keep-alive",
		  strlen("keep-alive"), &p, end))
    return false;

  if (lws_finalize_http_header(wsi, &p, end))
    return false;

  return ( lws_write(wsi, start, p - start, LWS_WRITE_HTTP_HEADERS) >= 0 );
}
