/* xAAL Agent
 *   Collects info on an xAAL bus and store it on its internal db
 *   Answers via Rest or WebSockets
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _XAAGENT_
#define _XAAGENT_

#include <json-c/json.h>
#include "options.h"
#include "db.h"

/* Global init of the xAAL agent */
void init_xaagent(options_t *options, xAAL_businfo_t *bus, xAAL_devinfo_t *me,
		 int *alive_fd, int *checking_fd);


/* Manage xAAL received message */
void callback_xaal(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db);


/* Time to time, check missing info in db and send xAAL queries to know more */
void callback_checking(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db, options_t *options);


/* Send an xAAL request comming from the web (cbor) API */
cbor_item_t *post_xAAL_cbor_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, const unsigned char *msg, size_t len);

/* Send an xAAL request comming from the web (json) API */
json_object *post_xAAL_json_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, const char *msg, size_t len);

/* Send an xAAL request comming from the WebSocket (cbor) API */
bool ws_xAAL_cbor_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, cbor_item_t *cinput);

/* Send an xAAL request comming from the WebSocket (json) API */
bool ws_xAAL_json_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, json_object *jinput);

/* Feed http (cbor) stream or web sockets with interesting data from xAAL */
void streams_cbor_feed(reqs_t *reqs, cbor_item_t *cobj);

/* Feed http (json) stream or web sockets with interesting data from xAAL */
void streams_json_feed(reqs_t *reqs, json_object *jobj);

#endif
