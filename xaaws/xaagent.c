/* xAAL Agent
 *   Collects info on an xAAL bus and store it on its internal db
 *   Answers via Rest or WebSockets
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/timerfd.h>
#include <sys/queue.h>

#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>
#include <libwebsockets.h>

#include <xaal.h>
#include <xaal_jc.h>

#include "xaagent.h"
#include "options.h"
#include "db.h"

#define ALIVE_PERIOD		120
#define CHECKING_PERIOD		30
#define GRACE_DELAY		120






/* Send is_alive request */
bool request_is_alive(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
		     cbor_item_t *ctargets) {
  cbor_item_t *cbody, *cdev_types;
  bool r;

  cdev_types = cbor_new_definite_array(1);
  (void)!cbor_array_push(cdev_types, cbor_move(cbor_build_string("any.any")));

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("dev_types")), cbor_move(cdev_types) });

  if (ctargets)
    r = xAAL_write_bus(bus, me, xAAL_REQUEST, "is_alive", cbody, ctargets);
  else
    r = xAAL_write_busl(bus, me, xAAL_REQUEST, "is_alive", cbody, &xAAL_is_alive_target, NULL);
  return r;
}



/* Manage is_alive notifications */
void receive_is_alive(const uuid_t *source, const char *dev_type, cbor_item_t *cbody, db_t *db) {
  device_t *device;

  device = add_device(&(db->devices), source);
  if (!device->devinfo.dev_type)
    device->devinfo.dev_type = strdup(dev_type);
  else if (strcmp(device->devinfo.dev_type, dev_type) == 0)
    device->timeout = xAAL_read_aliveTimeout(cbody);
}



/* Reply to get_attributes (empty answer, for now) */
bool reply_get_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
			 const uuid_t *target) {
  return xAAL_write_busl(bus, me, xAAL_REPLY, "get_attributes", NULL, target, NULL);
}



/* Manage get_keys_values replies and
   keys_values_changed notifications (metadatadb.any) */
void receive_keysValues(cbor_item_t *creplyBody, db_t *db) {
  cbor_item_t *cdev, *cmap;
  uuid_t dev;
  device_t *device;

  if ( !creplyBody || !cbor_isa_map(creplyBody) )
    return; /* get empty reply !? */

  cdev = xAAL_cbor_map_get(creplyBody, "device");
  if ( !xAAL_cbor_is_uuid(cdev, &dev) )
    return;

  cmap = xAAL_cbor_map_get(creplyBody, "map");
  if ( !cbor_isa_map(cmap) )
    return;

  device = add_device(&(db->devices), &dev);
  update_map(&(device->map), cmap);
}


/* Send get_keys_values request */
bool request_get_keys_values(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
		     cbor_item_t *ctargets, const uuid_t *device) {
  cbor_item_t *cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(cbor_build_bytestring(*device, 16)) });
  return xAAL_write_bus(bus, me, xAAL_REQUEST, "get_keys_values", cbody, ctargets);
}



/* manage get_device_attributes replies from cache */
void receive_cachedDeviceAttributes(cbor_item_t *creplyBody, db_t *db) {
  cbor_item_t *cache, *cattribute, *cvalue, *cdate;
  char *name;
  uuid_t dev;
  time_t date, now = time(NULL);
  device_t *device;
  size_t sz, i, len;
  
  if ( !creplyBody || !cbor_isa_map(creplyBody) )
    return; /* get empty reply !? */

  if ( !xAAL_cbor_is_uuid(xAAL_cbor_map_get(creplyBody, "device"), &dev) )
    return;

  cache = xAAL_cbor_map_get(creplyBody, "attributes");
  if ( !cache || !cbor_isa_array(cache) )
    return;

  sz = cbor_array_size(cache);
  for (i = 0; i < sz; i++) {
    cattribute = cbor_move(cbor_array_get(cache, i));

    name = xAAL_cbor_string_dup(xAAL_cbor_map_get(cattribute, "name"), &len);
    if ( !name )
      continue;

    if ( !validate_identifier(name) ) {
      free(name);
      continue;
    }

    cvalue = xAAL_cbor_map_get(cattribute, "value");
    if ( !cvalue ) {
      free(name);
      continue;
    }

    cdate = xAAL_cbor_map_get(cattribute, "date");
    if ( cdate && cbor_is_int(cdate) )
      date = cbor_get_int(cdate);
    else
      date = now;

    device = add_device(&(db->devices), &dev);
  
    parameter_t *parameter = select_attribute(&(db->schemas), select_schema(&(db->schemas), device->devinfo.dev_type), name);
    update_value(&(device->vals), name, parameter?parameter->type:NULL, cvalue, date);
  }

  /* build attributes info for http cbor streams */
  streams_cbor_feed(&(db->cattrreqs), creplyBody);

  /* build attributes info for http json streams */
  streams_json_feed(&(db->jattrreqs), xAAL_cbor2json(creplyBody));
}



/* manage a get_description reply */
void receive_get_description(const uuid_t *dev, cbor_item_t *creplyBody, db_t *db) {
  device_t *device;

  device = add_device(&(db->devices), dev);
  
  if (creplyBody && cbor_isa_map(creplyBody) )
    update_description(device, creplyBody);
}



/* manage attributes_change notification or get_attributes reply */
void receive_deviceAttributes(const uuid_t *dev, const char *dev_type, cbor_item_t *creplyBody, db_t *db) {
  json_object *jobj;
  cbor_item_t *cattribute, *cattributes, *cobj;
  char uuid[37], *attribute;
  time_t now = time(NULL);
  device_t *device;
  struct cbor_pair *pairs;
  size_t len, i, sz;

  if ( !validate_dev_type(dev_type) )
    return;

  device = add_device(&(db->devices), dev);
  if (!device->devinfo.dev_type)
    device->devinfo.dev_type = strdup(dev_type);
  else if (strcmp(device->devinfo.dev_type, dev_type) != 0)
    return;

  if ( !creplyBody || !cbor_isa_map(creplyBody) )
    return; /* get empty reply !? */

  cattributes = cbor_new_indefinite_array();
  pairs = cbor_map_handle(creplyBody);
  sz = cbor_map_size(creplyBody);
  for (i = 0; i < sz; i++) {
    attribute = xAAL_cbor_string_dup(pairs[i].key, &len);
    if (!attribute)
      continue;

    if (validate_identifier(attribute)) {
      parameter_t *parameter = select_attribute(&(db->schemas), select_schema(&(db->schemas), device->devinfo.dev_type), attribute);
      update_value(&(device->vals), attribute, parameter?parameter->type:NULL, pairs[i].value, now);

      cattribute = cbor_new_definite_map(3);
      (void)!cbor_map_add(cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("name")), cbor_move(cbor_build_string(attribute)) });
      (void)!cbor_map_add(cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("value")), pairs[i].value });
      (void)!cbor_map_add(cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("date")), cbor_move(xAAL_cbor_build_int(now)) });
      (void)!cbor_array_push(cattributes, cbor_move(cattribute));
    }
    free(attribute);
  }

  /* buid attributes info for http cbor streams */
  cobj = cbor_new_definite_map(2);
  (void)!cbor_map_add(cobj, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(cbor_build_bytestring(*dev, 16)) });
  (void)!cbor_map_add(cobj, (struct cbor_pair){ cbor_move(cbor_build_string("attributes")), cbor_move(cattributes) });
  streams_cbor_feed(&(db->cattrreqs), cobj);

  /* buid attributes info for http json streams */
  jobj = json_object_new_object();
  uuid_unparse(*dev, uuid);
  json_object_object_add(jobj, "device", json_object_new_string(uuid));
  json_object_object_add(jobj, "attributes", xAAL_cbor2json(cattributes));
  streams_json_feed(&(db->jattrreqs), jobj);
}



/* Send get_attributes request to the device */
bool request_get_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
			   const uuid_t *target, cbor_item_t *cattributes) {
  cbor_item_t *cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("attributes")), cbor_move(cattributes) });
  return xAAL_write_busl(bus, me, xAAL_REQUEST, "get_attributes", cbody, target, NULL);
}



/* Send get_device_attributes request to caches */
bool request_getCachedAttributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
				 cbor_item_t *ctargets, const uuid_t *addr,
				 cbor_item_t *cattributes) {
  cbor_item_t *cbody = cbor_new_definite_map(2);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(cbor_build_bytestring(*addr, 16)) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("attributes")), cbor_move(cattributes) });
  return xAAL_write_bus(bus, me, xAAL_REQUEST, "get_device_attribute", cbody, ctargets);
}


/* Rebuild a virtual xAAL message to be displayed in a browser (from the cipherd one) */
json_object *rebuild_json_msg(cbor_item_t *ctargets,
				     const uuid_t *source, char *dev_type,
				     xAAL_msg_type_t msg_type, char *action,
				     cbor_item_t *cbody) {
  json_object *jmsg = json_object_new_object();
  char uuid[37];

  json_object_object_add(jmsg, "targets", xAAL_cbor2json(ctargets));
  uuid_unparse(*source, uuid);
  json_object_object_add(jmsg, "source",  json_object_new_string(uuid));
  json_object_object_add(jmsg, "dev_type", json_object_new_string(dev_type));
  switch (msg_type) {
    case xAAL_NOTIFY:	json_object_object_add(jmsg, "msg_type", json_object_new_string("notify")); 	break;
    case xAAL_REQUEST:	json_object_object_add(jmsg, "msg_type", json_object_new_string("request"));	break;
    case xAAL_REPLY:	json_object_object_add(jmsg, "msg_type", json_object_new_string("reply"));	break;
    default:		json_object_object_add(jmsg, "msg_type", json_object_new_string("invalid"));	break;
  }
  json_object_object_add(jmsg, "action", json_object_new_string(action));
  if (cbody) json_object_object_add(jmsg, "body", xAAL_cbor2json(cbody));
  return jmsg;
}



/* Manage received message */
void callback_xaal(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  /* send msg to http streams, if there are clients */
  if ( db->jmsgreqs.tqh_first != NULL )
    streams_json_feed(&(db->jmsgreqs), rebuild_json_msg(ctargets, source, dev_type, msg_type, action, cbody));

  /* manage msg */
  if (xAAL_targets_match(ctargets, &me->addr)) {

    if (msg_type == xAAL_REQUEST) {

      if ( (strcmp(action, "is_alive") == 0)
	   && xAAL_is_aliveDevType_match(cbody, me->dev_type) ) {
	if ( !xAAL_notify_alive(bus, me) )
	  fprintf(xAAL_error_log, "Could not reply to is_alive\n");

      } else if ( strcmp(action, "get_description") == 0 ) {
	if ( !xAAL_reply_get_description(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_description\n");

      } else if ( strcmp(action, "get_attributes") == 0 ) {
	if ( !reply_get_attributes(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

      }

    }
  }

  /* I am interested by replies to me or to others */
  if (msg_type == xAAL_REPLY) {

    if (strcmp(action, "get_description") == 0)
	receive_get_description(source, cbody, db);

    else if (strcmp(action, "get_attributes") == 0)
	receive_deviceAttributes(source, dev_type, cbody, db);

    if (strncmp(dev_type, "metadatadb.", strlen("metadatadb.")) == 0) {
      if (strcmp(action, "get_keys_values") == 0)
	receive_keysValues(cbody, db);

    } else if (strncmp(dev_type, "cache.", strlen("cache.")) == 0) {
      if (strcmp(action, "get_device_attributes") == 0)
	receive_cachedDeviceAttributes(cbody, db);

    }

  }

  /* Manage interesting notifications */
  if (msg_type == xAAL_NOTIFY) {
    if (strcmp(action, "alive") == 0)
      receive_is_alive(source, dev_type, cbody, db);

    else if (strcmp(action, "attributes_change") == 0)
      receive_deviceAttributes(source, dev_type, cbody, db);

    else if (strcmp(action, "keys_values_changed") == 0)
      receive_keysValues(cbody, db);
  }

  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/* * */

/* check for new devices and send get_description and get_attributes */
void checking_newdevices(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db) {
  cbor_item_t *ctargets_desc = cbor_new_indefinite_array();
  cbor_item_t *ctargets_attr = cbor_new_indefinite_array();
  device_t *device;

  TAILQ_FOREACH(device, &(db->devices), entries)
    if (   !(device->devinfo.vendor_id)
	&& !(device->devinfo.product_id)
	&& !(device->devinfo.hw_id)
	&& !(device->devinfo.group_id)
	&& !(device->devinfo.version)
	&& !(device->devinfo.url)
	&& !(device->devinfo.schema)
	&& !(device->devinfo.info) ) {
      if ( !xAAL_strings_get(device->devinfo.unsupported_attributes, "get_description") )
	(void)!cbor_array_push(ctargets_desc, cbor_move(cbor_build_bytestring(device->devinfo.addr, 16)));
      if ( !xAAL_strings_get(device->devinfo.unsupported_methods, "get_attributes") )
	(void)!cbor_array_push(ctargets_attr, cbor_move(cbor_build_bytestring(device->devinfo.addr, 16)));
    }

  if ( cbor_array_size(ctargets_desc) != 0 )
    xAAL_write_bus(bus, me, xAAL_REQUEST, "get_description", NULL, ctargets_desc);
  else
    cbor_decref(&ctargets_desc);

  if ( cbor_array_size(ctargets_attr) != 0 )
    xAAL_write_bus(bus, me, xAAL_REQUEST, "get_attributes", NULL, ctargets_attr);
  else
    cbor_decref(&ctargets_attr);
  
}


/* check for map of key-values (metadata) */
void checking_map(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db) {
  cbor_item_t *ctargets = cbor_new_indefinite_array();
  size_t len = strlen("metadatadb.");
  device_t *device;

  TAILQ_FOREACH(device, &(db->devices), entries)
    if (device->devinfo.dev_type && strncmp(device->devinfo.dev_type, "metadatadb.", len) == 0)
      (void)!cbor_array_push(ctargets, cbor_move(cbor_build_bytestring(device->devinfo.addr, 16)));

  if (cbor_array_size(ctargets) != 0) {
    TAILQ_FOREACH(device, &(db->devices), entries)
      if (device->map.tqh_first == NULL)
	request_get_keys_values(bus, me, cbor_incref(ctargets), &(device->devinfo.addr));
  }
  cbor_decref(&ctargets);
}



/* send get_device_attributes to caches on missing values */
void checking_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db) {
  cbor_item_t *ctargets = cbor_new_indefinite_array();
  size_t len = strlen("cache.");
  device_t *device;

  TAILQ_FOREACH(device, &(db->devices), entries)
    if (device->devinfo.dev_type && strncmp(device->devinfo.dev_type, "cache.", len) == 0)
      (void)!cbor_array_push(ctargets, cbor_move(cbor_build_bytestring(device->devinfo.addr, 16)));

  if (cbor_array_size(ctargets) != 0) {
    TAILQ_FOREACH(device, &(db->devices), entries) {
      cbor_item_t *cattributes = missing_values(device, &(db->schemas));
      if (cbor_array_size(cattributes) != 0) {
        if ( !xAAL_strings_get(device->devinfo.unsupported_methods, "get_attributes") )
          request_get_attributes(bus, me, &(device->devinfo.addr), cbor_incref(cattributes));
        if ( (cbor_array_size(ctargets) != 0)
            && !xAAL_strings_get(device->devinfo.unsupported_notifications, "attributes_changed") )
          request_getCachedAttributes(bus, me, cbor_incref(ctargets), &(device->devinfo.addr), cattributes);
      }
    }
  }
  cbor_decref(&ctargets);
}


/* delete dead devices */
void checking_deaddevices(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db) {
  device_t *device;
  time_t deadline;

  deadline = time(NULL) - GRACE_DELAY;
  TAILQ_FOREACH(device, &(db->devices), entries)
    if (device->timeout && (device->timeout < deadline))
      delete_device(&(db->devices), device);
}


/* download new schemas, i.e. dev_type of known devices not yet loaded in the schemas list */
void checking_schemas(db_t *db, options_t *options) {
  device_t *device;
  
  /* in case device description has a schema url */
  TAILQ_FOREACH(device, &(db->devices), entries) {
    if ( !select_schema(&(db->schemas), device->devinfo.dev_type) ) {
      if ( device->devinfo.schema && download_file(device->devinfo.schema, device->devinfo.dev_type, options->schemas) ) {
        json_object *jschema = load_json_file(options->schemas, device->devinfo.dev_type);
        if (jschema) {
          add_json_schema(&(db->schemas), jschema);
          json_object_put(jschema);
          continue;
        }
      }
    }
  }
  
  /* else, download from our server */
  TAILQ_FOREACH(device, &(db->devices), entries) {
    if ( !select_schema(&(db->schemas), device->devinfo.dev_type) ) {
      if ( download_file(SCHEMAS_SERVER, device->devinfo.dev_type, options->schemas) ) {
        json_object *jschema = load_json_file(options->schemas, device->devinfo.dev_type);
        if (jschema) {
          add_json_schema(&(db->schemas), jschema);
          json_object_put(jschema);
          continue;
        }
      }
    }
  }  
}


/* Time to time, check missing info in db and send xAAL queries to know more */
void callback_checking(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, db_t *db, options_t *options) {
  checking_newdevices(bus, me, db);
  checking_map(bus, me, db);
  checking_attributes(bus, me, db);
  checking_deaddevices(bus, me, db);
  checking_schemas(db, options);
}




/* Global init of the xAAL agent */
void init_xaagent(options_t *options, xAAL_businfo_t *bus, xAAL_devinfo_t *me, int *alive_fd, int *checking_fd) {
  /* Manage logfile */
  if (options->logfile) {
    xAAL_error_log = fopen(options->logfile, "a");
    if (xAAL_error_log == NULL) {
      perror("Opening logfile");
      xAAL_error_log = stderr;
    }
  } else
    xAAL_error_log = stderr;

 /* Join the xAAL bus */
  if ( !options->addr || !options->port || !options->passphrase) {
    lwsl_err("Please provide the address, the port and the passphrase of the xAAL bus.\n");
    exit(EXIT_FAILURE);
  } else if ( !xAAL_join_bus(options->addr, options->port, options->hops, 1, bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus->maxAge = 2*60; /*seconds*/;
  bus->key = xAAL_pass2key(options->passphrase);
  if (bus->key == NULL) {
    lwsl_err("Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(me->addr) ) {
    char str[37];
    uuid_generate(me->addr);
    uuid_unparse(me->addr, str);
    printf("Device: %s\n", str);
  }

  /* Setup device info */
  me->dev_type   = "hmi.basic";
  me->alivemax  = 2 * ALIVE_PERIOD;
  me->vendor_id  = "IHSEV";
  me->product_id = "xAAL Agent with websockets";
  me->hw_id      = NULL;
  me->version   = "0.2";
  me->group_id   = NULL;
  me->url       = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  me->schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/hmi.basic";
  me->info	= NULL;
  me->unsupported_attributes = NULL;
  me->unsupported_methods = NULL;
  me->unsupported_notifications = NULL;

  {
    struct itimerspec timerspec;

    /* Set alive timer */
    *alive_fd = timerfd_create(CLOCK_REALTIME, 0);
    if (*alive_fd == -1)
      lwsl_err("Could not create timer for alive messages: %s\n", strerror(errno));
    timerspec.it_interval.tv_sec = ALIVE_PERIOD;
    timerspec.it_interval.tv_nsec = 0;
    timerspec.it_value.tv_sec = 0;
    timerspec.it_value.tv_nsec = 1;
    if ( timerfd_settime(*alive_fd, 0, &timerspec, NULL) == -1 )
      lwsl_err("Could not configure timer for alive messages: %s\n", strerror(errno));


    /* Set timer for 'checking queries' */
    *checking_fd = timerfd_create(CLOCK_REALTIME, 0);
    if (*checking_fd == -1)
      lwsl_err("Could not create timer for checking: %s\n", strerror(errno));
    timerspec.it_interval.tv_sec = CHECKING_PERIOD;
    timerspec.it_interval.tv_nsec = 0;
    timerspec.it_value.tv_sec = 2;
    timerspec.it_value.tv_nsec = 0;
    if ( timerfd_settime(*checking_fd, 0, &timerspec, NULL) == -1 )
      lwsl_err("Could not configure timer for checking: %s\n", strerror(errno));
  }

  /* sends a broadcast is_alive */
  request_is_alive(bus, me, NULL);
}



/* Forward an xAAL request received from the web (json) API to the xAAL bus */
/* Expect: { "targets":[<*uuid>], "action":"<string>", "body":{<object>} } */
bool ws_xAAL_json_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, json_object *jinput) {
  json_object *jtargets, *jtarget, *jaction, *jbody;
  cbor_item_t *ctargets, *cbody;
  uuid_t uuid;
  const char *action;
  int sz, i;

  if ( !json_object_object_get_ex(jinput, "targets", &jtargets)
     || !json_object_is_type(jtargets, json_type_array) 
     || !json_object_object_get_ex(jinput, "action", &jaction) 
     || !json_object_is_type(jaction, json_type_string) ) {
    return false;
  }

  action = json_object_get_string(jaction);
  if (!validate_identifier(action))
    return false;

  sz = json_object_array_length(jtargets);
  ctargets = cbor_new_definite_array(sz);
  for (i = 0; i < sz; i++) {
    jtarget = json_object_array_get_idx(jtargets, i);
    if (   json_object_is_type(jtarget, json_type_string)
	&& (uuid_parse(json_object_get_string(jtarget), uuid)==0) )
      (void)!cbor_array_push(ctargets, cbor_move(cbor_build_bytestring(uuid, 16)));
  }

  json_object_object_get_ex(jinput, "body", &jbody);
  cbody = jbody? xAAL_json2cbor(jbody) : NULL;

  return xAAL_write_bus(bus, me, xAAL_REQUEST, action, cbody, ctargets);
}



/* Forward an xAAL request received from the WebSocket (json) API */
/* Expect: { "targets":[<*uuid>], "action":"<string>", "body":{<object>} } */
/* Return: (json)({ "success": true|false }) */
json_object *post_xAAL_json_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, const char *msg, size_t len) {
  json_object *jinput, *janswer;
  struct json_tokener *tok;
  enum json_tokener_error jerr;

  tok = json_tokener_new_ex(JSON_TOKENER_DEFAULT_DEPTH);
  jinput = json_tokener_parse_ex(tok, msg, len);
  jerr = json_tokener_get_error(tok);
  json_tokener_free(tok);

  janswer = json_object_new_object();
  if (jerr == json_tokener_success) {
    json_object_object_add(janswer, "success", json_object_new_boolean(ws_xAAL_json_request(bus, me, jinput)));
    json_object_put(jinput);
  } else {
    json_object_object_add(janswer, "success", json_object_new_boolean(false));
  }
  return janswer;
}



/* Forward an xAAL request received from the WebSocket (cbor) API */
/* Expect: [ [<*uuid>], "<string>", {<object>} ] */
bool ws_xAAL_cbor_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, cbor_item_t *cinput) {
  cbor_item_t *caction, *ctargets, *cbody;
  char *action;
  size_t szi, szt, i;
  uuid_t uuid;

  if ( !cbor_isa_map(cinput)
     || (szi=cbor_array_size(cinput)<2)
     || (szi>3) ) {
    return false;
  }

  ctargets = cbor_move(cbor_array_get(cinput, 0));
  if ( cbor_isa_map(ctargets) ) {
    szt = cbor_array_size(ctargets);
    for (i = 0; i < szt; i++) {
      if ( !xAAL_cbor_is_uuid(cbor_move(cbor_array_get(ctargets, i)), &uuid) ) {
        cbor_decref(&ctargets);
	return false;
      }
    }
  }

  caction = cbor_move(cbor_array_get(cinput, 1));
  if ( !cbor_isa_string(caction) || cbor_string_is_indefinite(caction)) {
    cbor_decref(&caction);
    cbor_decref(&ctargets);
    return false;
  }
  action = strndup((const char *)cbor_string_handle(caction), cbor_string_length(caction));
  cbor_decref(&caction);

  if ( !validate_identifier(action) ) {
    cbor_decref(&ctargets);
    free(action);
    return false;
  }

  cbody = (szi==3) ? cbor_move(cbor_array_get(cinput, 2)) : NULL;

  bool ret = xAAL_write_bus(bus, me, xAAL_REQUEST, action, cbody, ctargets);
  free(action);
  return ret;
}


/* Forward an xAAL request received from the web (cbor) API to the xAAL bus */
/* Expect: [ [<*uuid>], "<string>", {<object>} ] 
   Return: (cbor)(true|false) */
cbor_item_t *post_xAAL_cbor_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me, const unsigned char *msg, size_t msglen) {
  struct cbor_load_result cresult;
  cbor_item_t *cinput, *canswer;

  cinput = cbor_load(msg, msglen, &cresult);
  if ( !cinput )
    return cbor_build_bool(false);

  canswer = cbor_build_bool(ws_xAAL_cbor_request(bus, me, cinput));
  cbor_decref(&cinput);
  
  return canswer;
}



/* Feed http (json) stream or web sockets with interesting data from xAAL */
void streams_json_feed(reqs_t *reqs, json_object *jobj) {
  req_t *req;
  struct per_session_data__generic {
    json_object *janswer;
  } *user_space;

  TAILQ_FOREACH(req, reqs, entries) {
    user_space = lws_wsi_user(req->wsi);
    user_space->janswer = json_object_get(jobj);
    lws_callback_on_writable(req->wsi);
  }
}


/* Feed http (cbor) stream or web sockets with interesting data from xAAL */
void streams_cbor_feed(reqs_t *reqs, cbor_item_t *item) {
  req_t *req;
  struct per_session_data__generic {
    cbor_item_t *canswer;
  } *user_space;

  TAILQ_FOREACH(req, reqs, entries) {
    user_space = lws_wsi_user(req->wsi);
    user_space->canswer = cbor_incref(item);
    lws_callback_on_writable(req->wsi);
  }
}
