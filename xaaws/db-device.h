/* db - Database for xAAL Agent
 * Part of the xaaws software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DBDEVICE_
#define _DBDEVICE_

#include <sys/queue.h>
#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>
#include <xaal.h>


/*
 * Section Definition of Data Structures
 */

typedef TAILQ_HEAD(valshead, valentry) vals_t;
typedef struct valentry {
  char *name;
  char *type;
  cbor_item_t *value;
  time_t date;
  TAILQ_ENTRY(valentry) entries;
} val_t;


typedef TAILQ_HEAD(maphead, mapentry) map_t;
typedef struct mapentry {
  char *key, *value;
  TAILQ_ENTRY(mapentry) entries;
} kv_t;


typedef TAILQ_HEAD(deviceshead, deviceentry) devices_t;
typedef struct deviceentry {
  xAAL_devinfo_t devinfo;
  time_t timeout;
  vals_t vals;
  map_t map;
  TAILQ_ENTRY(deviceentry) entries;
} device_t;



/*
 * Section Devices
 */

/* load a DB of devices from a cbor file */
void load_devices(devices_t *devices, char *file);

/* dump DB of devices to a cbor file */
void dump_devices(devices_t *devices, char *file);

/* update a device from loading cbor data */
void update_device(devices_t *devices, cbor_item_t *cdevice);

/* select a device by its address */
device_t *select_device(devices_t *devices, const uuid_t *addr);

/* select or insert a device (its addr) */
device_t *add_device(devices_t *devices, const uuid_t *addr);

/* update the description of a device */
void update_description(device_t *device, cbor_item_t *cdescription);

/* get (cbor) devices (having a given key in its map, or all) */
cbor_item_t *get_cbor_devices(devices_t *devices, const char *key);

/* get (json) devices (having a given key in its map, or all) */
json_object *get_json_devices(devices_t *devices, const char *key);

/* serialize a device to cbor */
cbor_item_t *get_cbor_device(device_t *device);

/* serialize a device to json */
json_object *get_json_device(device_t *device);

/* select a device and serialize it to cbor */
cbor_item_t *get_cbor_device_by_addr(devices_t *devices, const uuid_t *addr);

/* select a device and serialize it to json */
json_object *get_json_device_by_addr(devices_t *devices, const uuid_t *addr);

/* delete a device */
void delete_device(devices_t * devices, device_t *device);



/*
 * Section Values of Attributes of a Device
 */

/* select a value by its name */
val_t *select_value(vals_t *vals, const char *name);

/* insert or update the value of an attribute of a device */
void update_value(vals_t *vals, const char *name, const char *type, cbor_item_t *value, time_t date);

/* serialize values of attributes of a device to cbor */
cbor_item_t *get_cbor_values(device_t *device);

/* serialize values of attributes of a device to json */
json_object *get_json_values(device_t *device);

/* select a device and serialize its attributes values to cbor */
cbor_item_t *get_cbor_values_by_addr(devices_t *devices, const uuid_t *addr);

/* select a device and serialize its attributes values to json */
json_object *get_json_values_by_addr(devices_t *devices, const uuid_t *addr);

/* delete the list of values */
void delete_values(vals_t *vals);



/*
 * Section Map (keys-values metadata) of a Device
 */

/* select a key-value in the map if present */
kv_t *select_kv(map_t *map, const char *key);

/* insert a key-value in the map */
void insert_kv(map_t *map, const char *key, const char *value);

/* serialize KVs-map of a device in cbor */
cbor_item_t *get_cbor_kvs(device_t *device);

/* serialize KVs-map of a device in json */
json_object *get_json_map(device_t *device);

/* get (cbor) KVs-map of a device by addr */
cbor_item_t *get_cbor_kvs_by_addr(devices_t *devices, const uuid_t *addr);

/* get (json) KVs-map of a device by addr */
json_object *get_json_kvs_by_addr(devices_t *devices, const uuid_t *addr);

/* delete a key-value in the map */
void delete_kv(map_t *map, const char *key);

/* delete the map of kv */
void delete_map(map_t *map);

/* update the map (merge) */
void update_map(map_t *map, cbor_item_t *cmap);


#endif
