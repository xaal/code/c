/* xaaws - xAAL web interface
 * Part of the 'xaaws' software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PROTOXAAL_
#define _PROTOXAAL_

#include <libwebsockets.h>
#include <json-c/json.h>


struct per_session_data__xaal_dump {
  json_object *janswer;
};


struct per_session_data__xaal_ctl {
  json_object *janswer;
  struct json_tokener *tok;
};


int callback_xaal_ctl(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

int callback_xaal_dump(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

bool serve_ws_stream(struct lws *wsi, json_object *jobj);


#endif
