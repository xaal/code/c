/* xaaws - xAAL web interface
 * Part of the 'xaaws' software
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PROTOHTTP_
#define _PROTOHTTP_


#include <stdbool.h>
#include <libwebsockets.h>
#include <json-c/json.h>
#include <cbor.h>


#define REST_JSON_PATH	"/json/"
#define REST_CBOR_PATH	"/cbor/"


struct per_session_data__http {
  json_object *janswer;
  cbor_item_t *canswer;
  bool complete;
  char *post_msg;
  char *post_uri;
  size_t post_len;
};


int callback_http(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

bool serve_http_json(struct lws *wsi, json_object *jobj);

bool serve_http_cbor(struct lws *wsi, cbor_item_t *item);

bool hdr_http_stream(struct lws *wsi);

bool serve_http_json_stream(struct lws *wsi, json_object *jobj);

bool serve_http_cbor_stream(struct lws *wsi, cbor_item_t *item);


#endif
