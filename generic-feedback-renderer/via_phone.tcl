#!/usr/bin/expect --
set timeout 45

set PREAMBULE "Bonjour. Ceci est un message automatique de votre maison."

set stdin [open "/dev/stdin" "r" ]
set MSGTXT [read $stdin]
close $stdin

# Configuration:
# - Asterisk Manager
#   Check IP, port, username, password according to
#   /etc/asterisk/manager.conf and /etc/asterisk/manager.d/*.conf
#
# - Sip accounts for caller and callee, see
#   /etc/asterisk/users.conf and /etc/asterisk/sip.conf


spawn telnet localhost 5038

expect {
  "Asterisk Call Manager" {
    sleep .1
    send "Action: Login\n"
    send "Username: xaal\n"
    send "Secret: mysecret\n"
    send "\n"
    expect {
      "Authentication failed" {
        exit
      }
      "Fully Booted" {
        send "Action: Originate\n"
        send "Channel: SIP/6001\n"
        send "CallerID: 6002\n"
        send "Account: 6002\n"
        send "Application: espeak\n"
        send "Data: \"$PREAMBULE $MSGTXT\"\n"
        send "\n"
        expect {
          "Response" {
            send "Action: Logoff\n"
            send "\n"
          }
        }
      }
    }
  }
  "Goodbye" {
    exit
  }
  "Connection closed" {
    exit
  }
  eof {
    exit
  }
}
