/* xAAL feedback-renderer
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>




#define ALIVE_PERIOD    60

/* setup device info */
xAAL_devinfo_t device = { .dev_type	= "feedback_renderer.basic",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "Team IHSEV",
			.product_id	= "Generic user feedback renderer device",
			.hw_id		= NULL,
			.version	= "0.4",
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.schema		= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/feedback_renderer.basic",
			.info		= NULL,
			.unsupported_attributes = NULL,
			.unsupported_methods = NULL,
			.unsupported_notifications = NULL
		      };

xAAL_businfo_t bus;



void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &device) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


bool reply_get_attributes(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *device,
			 const uuid_t *target,
			 cbor_item_t *cattribute) {
  return xAAL_write_busl(bus, device, xAAL_REPLY, "get_attributes", cbor_incref(cattribute), target, NULL);
}



void try_inform(const xAAL_businfo_t *bus,
		const xAAL_devinfo_t *device,
		const uuid_t *target,
		cbor_item_t *cbody,
		char *inform_app,
		cbor_item_t **cattribute) {
  time_t date;
  char *stxt;
  FILE *p_app;
  int status;
  size_t len;

  stxt = xAAL_cbor_string_dup(xAAL_cbor_map_get(cbody, "msg"), &len);
  if ( !stxt )
    return;

  time(&date);
  cbor_decref(cattribute);
  *cattribute = cbor_new_definite_map(2);
  (void)!cbor_map_add(*cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("msg")), cbor_move(cbor_build_string(stxt)) });
  (void)!cbor_map_add(*cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("date")), cbor_move(xAAL_cbor_build_int(date)) });

  p_app = popen(inform_app, "w");
  if (p_app == NULL) {
    fprintf(xAAL_error_log, "Could not call %s: %s\n", inform_app, strerror(errno));
    return;
  }
  fprintf(p_app, "%s", stxt);

  status = pclose(p_app);
  if (WIFEXITED(status)) {
    if (WEXITSTATUS(status) != 0)
      fprintf(xAAL_error_log, "%s ended with code %d\n", inform_app, WEXITSTATUS(status));
  } else if (WIFSIGNALED(status)) {
    fprintf(xAAL_error_log, "%s ended with signal %d\n", inform_app, WTERMSIG(status));
  }
}



/* Manage received xAAL message */
void manage_msg(const xAAL_businfo_t *bus, xAAL_devinfo_t *device, char *inform_app, cbor_item_t **cattribute) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  /* Recive a message */
  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if ( !xAAL_targets_match(ctargets, &device->addr) ) {
    /* This is not for me */
    xAAL_free_msg(ctargets, source, dev_type, action, cbody);
    return;
  }

  if (msg_type == xAAL_REQUEST) {
    if ( (strcmp(action, "is_alive") == 0)
	&& xAAL_is_aliveDevType_match(cbody, device->dev_type) ) {
      if ( !xAAL_notify_alive(bus, device) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, device, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      if ( !reply_get_attributes(bus, device, source, *cattribute) )
	fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

    } else if ( strcmp(action, "inform") == 0 ) {
      try_inform(bus, device, source, cbody, inform_app, cattribute);

    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}




/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  char *inform_app = NULL;
  bool arg_error = false;
  struct sigaction act_alarm;
  cbor_item_t *cattribute = cbor_new_indefinite_map();

  xAAL_error_log = stderr;

  uuid_clear(device.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:i:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, device.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(device.addr);
	}
	break;
      case 'i':
	inform_app = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase || !inform_app ) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] -i <inform app>\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(device.addr) ) {
    char uuid[37];
    uuid_generate(device.addr);
    uuid_unparse(device.addr, uuid);
    printf("Device: %s\n", uuid);
  }


  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &device) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");


  /* Main loop */
  for (;;)
    manage_msg(&bus, &device, inform_app, &cattribute);
}
