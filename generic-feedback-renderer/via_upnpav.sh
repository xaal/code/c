#!/bin/bash

set -e

# Format message
MSG="$(par 30j|sed -e 's,^\(.*\)$,<tspan sodipodi:role="line">\0<\\/tspan>,g'| tr -d '\012')"

# Build jpeg
TMPSVG=`tempfile -s .svg`
TMPPNG=`tempfile -s .png`
TMPJPG=`tempfile -s .jpg`
trap "rm -f $TMPSVG $TMPPNG $TMPJPG" EXIT

sed -e "s/@MSGTEXT@/$MSG/g" template.svg > "$TMPSVG"
inkscape -f "$TMPSVG" -e "$TMPPNG"
convert "$TMPPNG" "$TMPJPG"

# Push jpeg
uav-play -H "$TMPJPG"
# uav-play -i lo -H "$TMPJPG"
