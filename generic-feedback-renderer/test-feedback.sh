#!/bin/bash

ADDR="224.0.29.200"
PORT="1234"

FEEDBACK_GATEWAY=e208f14b-21fc-42cd-92e3-232047f323b4
function send_inform() {
  # $1: device's address
  cat << EOF | socat - UDP-DATAGRAM:$ADDR:$PORT,bind=:$PORT,ip-add-membership=$ADDR:0,reuseaddr &>/dev/null
{
  "header": {
    "version": "0.4",
    "targets": [ "$1" ],
    "source": "$FEEDBACK_GATEWAY",
    "devType": "feedback_gateway.basic",
    "msgType": "request",
    "action": "inform",
    "cipher": "none",
    "signature": ""
  },
  "body": {
    "msg": "La température est trop élevée pour votre confort et votre bien-être."
  }
}
`sleep 5`
EOF
}

send_inform $1
