# Generic Feedback Renderer

This xAAL device is a so-called user feedback renderer. It waits for an 'inform' request on the xAAL bus, then calls a third-party appplication able to inform the user in a mean or in another.

## API of third-party inform-app
Third-party informing appplications are called and the content of the message is provided in the standard input.

### Via UPnP Renderers
As an example one proposes 'via_upnpav.sh'
This shell-script arranges the text of the message into an svg picture (see template.svg), convert it to jpeg, then push the jpeg on UPnP-AV rederers.

It depends on: par, sed, inkscape, convert (ImageMagic), and uav-play <http://uav-play.sourceforge.net/>


### Via a phone call
The second example is 'via_phone.tcl'
This expect-script connects to an asterisk manager port, initiate a call, and feed the asterisk_espeak plugin with the text of message.

It depends on: asterisk, espeak (+ wanted voices), and the asterisk-espeak module <http://zaf.github.io/Asterisk-eSpeak/>


### Notes on configuring asterisk

#### Install
Debian packages for asterisk and espeak are fine.
Unfortunately, asterisk-espeak is not yet packaged; compile it.
  apt-get install asterisk asterisk-dev espeak libespeak-dev mbrola-fr1 libsndfile1-dev libsndfile1 libsamplerate0-dev libsamplerate0
  cd /usr/local/src
  git clone https://github.com/zaf/Asterisk-eSpeak.git
  cd Asterisk-eSpeak
  sudo make install


#### Users accounts
One need an account which playing the role of the caller. The callee can be registered elsewhere.
Arrange something like this at the end of /etc/asterisk/users.conf :
  [6001]
  context=default
  secret = 1234
  host=dynamic

  [6002]
  context=default
  secret = 1234
  host=dynamic


#### Account for the manager
Create a file like /etc/asterisk/manager.d/xaal.conf :
  [xaal]
  secret = mysecret
  deny=0.0.0.0/0.0.0.0
  permit=127.0.0.1/255.255.255.0
  read = system,call,log,verbose,command,agent,user,originate
  write = system,call,log,verbose,command,agent,user,originate


#### Configure the asterisk-espeak module
Edit /etc/asterisk/espeak.conf :
  [general]
  usecache=no
  samplerate=16000

  [voice]
  voice=mb-fr1
  speed=80
  volume=100
  wordgap=0
  pitch=40
  capind=15


#### Restart asterisk
  sudo /etc/init.d/asterisk restart
or
  asterisk -vvvcr
  > core reload
  > exit



### Notes on configuring the via_phone.tcl script
- Asterisk Manager
  Check IP, port, username, password according to /etc/asterisk/manager.conf and /etc/asterisk/manager.d/*.conf

- Sip accounts for caller and callee, see /etc/asterisk/users.conf and  /etc/asterisk/sip.conf
