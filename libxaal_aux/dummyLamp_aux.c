/* xAAL dummy auxiliary basic lamp
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>
#include <xaal_aux.h>



#define ALIVE_PERIOD    60

/* setup lamp device info */
xAAL_devinfo_t lamp = { .dev_type	= "lamp.basic",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "IHSEV",
			.product_id	= "Dummy auxiliary basic lamp",
			.hw_id		= NULL,
			.version	= "0.4",
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.info		= NULL,
			.unsupported_attributes = NULL,
			.unsupported_methods = (char *[]){ "get_attributes", NULL },
			.unsupported_notifications = (char *[]){ "attributes_change", NULL }
		      };

xAAL_businfo_t bus;


void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &lamp) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  bool attribute_light = false;
  struct sigaction act_alarm;
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  xAAL_error_log = stderr;

  uuid_clear(lamp.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:u:s:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 'u':
	if ( uuid_parse(optarg, lamp.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(lamp.addr);
	}
	break;
      case 's':
	passphrase = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }


  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);


  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }


  /* Generate device address if needed */
  if ( uuid_is_null(lamp.addr) ) {
    char str[37];
    uuid_generate(lamp.addr);
    uuid_unparse(lamp.addr, str);
    printf("Device: %s\n", str);
  }
  xAAL_add_wanted_target(&lamp.addr, &bus);


  /* Put an hadrware Id */
  lamp.hw_id = xAAL_aux_eth_addr(),
  printf("hw_id: ");
  cbor_describe(lamp.hw_id, stdout);
  printf("\n");


  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &lamp) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");


  /* Main loop */
  for (;;) {

    /* Show lamp */
    if (attribute_light)
      printf(" (O) On \r");
    else
      printf("  .  Off\r");
    fflush(stdout);

    /* Recive a message */
    if (!xAAL_read_bus(&bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
      continue;

    if (msg_type == xAAL_REQUEST) {
      if ( (strcmp(action, "is_alive") == 0)
	  && xAAL_is_aliveDevType_match(cbody, lamp.dev_type) ) {
	if ( !xAAL_notify_alive(&bus, &lamp) )
	  fprintf(xAAL_error_log, "Could not reply to is_alive\n");

      } else if ( strcmp(action, "get_description") == 0 ) {
	if ( !xAAL_reply_get_description(&bus, &lamp, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_description\n");

      } else if ( strcmp(action, "get_attributes") == 0 ) {
	/* I ignore this */

      } else if ( strcmp(action, "on") == 0 ) {
	  attribute_light = true;

      } else if ( strcmp(action, "off") == 0 ) {
	    attribute_light = false;
      }
    }
    xAAL_free_msg(ctargets, source, dev_type, action, cbody);
  }
}
