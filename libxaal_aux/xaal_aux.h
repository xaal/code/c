/* libxaal_aux
 * Auxiliary helpers for developing xAAL devices
 *
 * (c) 2017 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LIBXAAL_AUX
#define LIBXAAL_AUX

#define xAAL_AUX_VERSION	"0.2"

#include <cbor.h>


/*
 * Get the Ethernet address of the interfaces of the host
 * in a way suitable for filling hwInfo (get_description)
 */

cbor_item_t *xAAL_aux_eth_addr();

#endif
