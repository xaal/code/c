/* libxaal_aux
 * Auxiliary helpers for developing xAAL devices
 *
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <arpa/inet.h>
#include <cbor.h>
#include <string.h>
#include <libmnl/libmnl.h>
#include <linux/if.h>
#include <linux/if_link.h>
#include <linux/rtnetlink.h>
#include <linux/if_arp.h>

#include <xaal_aux.h>


/*
 * Get the Ethernet address of the interfaces of the host
 * in a way suitable for filling hwInfo (get_description)
 *
 * Based on libmnl/examples/rtnl/rtnl-link-dump.c
 * Alternative: ioctl(.., SIOCGIFHWADDR, ..)
 */


static int data_attr_cb(const struct nlattr *attr, void *data) {
  const struct nlattr **tb = data;
  int type = mnl_attr_get_type(attr);

  /* skip unsupported attribute in user-space */
  if (mnl_attr_type_valid(attr, IFLA_MAX) < 0)
    return MNL_CB_OK;

  switch(type) {
  case IFLA_ADDRESS:
    if (mnl_attr_validate(attr, MNL_TYPE_BINARY) < 0) {
      perror("mnl_attr_validate");
      return MNL_CB_ERROR;
    }
    break;
  case IFLA_MTU:
    if (mnl_attr_validate(attr, MNL_TYPE_U32) < 0) {
      perror("mnl_attr_validate");
      return MNL_CB_ERROR;
    }
    break;
  case IFLA_IFNAME:
    if (mnl_attr_validate(attr, MNL_TYPE_STRING) < 0) {
      perror("mnl_attr_validate2");
      return MNL_CB_ERROR;
    }
    break;
  }
  tb[type] = attr;
  return MNL_CB_OK;
}


char *print_mac(const unsigned char *mac, unsigned len) {
  unsigned txt_sz = len * 3;

  if (!txt_sz)
    return strdup("");
    
  char *txt = malloc(txt_sz);

  for (unsigned i = 0; i < len; i++)
    sprintf(txt+3*i, "%02x:", mac[i]);
  txt[txt_sz-1] = '\0';

  return txt;
}


static int data_cb(const struct nlmsghdr *nlh, void *data) {
  cbor_item_t *links = data;
  struct nlattr *tb[IFLA_MAX+1] = {};
  struct ifinfomsg *ifm = mnl_nlmsg_get_payload(nlh);

  if ( ifm->ifi_type != ARPHRD_LOOPBACK ) {
    cbor_item_t *link = cbor_new_indefinite_map();

    mnl_attr_parse(nlh, sizeof(*ifm), data_attr_cb, tb);

    if (tb[IFLA_IFNAME])
      (void)!cbor_map_add(link, (struct cbor_pair){ cbor_move(cbor_build_string("name")),
			  cbor_move(cbor_build_string(mnl_attr_get_str(tb[IFLA_IFNAME]))) });

    (void)!cbor_map_add(link, (struct cbor_pair){ cbor_move(cbor_build_string("protocol")),
			  cbor_move(cbor_build_string("Ethernet")) });

    if (tb[IFLA_ADDRESS]) {
      char *txt = print_mac(mnl_attr_get_payload(tb[IFLA_ADDRESS]), mnl_attr_get_payload_len(tb[IFLA_ADDRESS]));
      (void)!cbor_map_add(link, (struct cbor_pair){ cbor_move(cbor_build_string("addr")),
			  cbor_move(cbor_build_string(txt)) });
      free(txt);
    }

    (void)!cbor_array_push(links, link);
  }

  return MNL_CB_OK;
}


cbor_item_t *xAAL_aux_eth_addr() {
  cbor_item_t *links = cbor_new_indefinite_array();
  struct mnl_socket *nl;
  char buf[MNL_SOCKET_BUFFER_SIZE];
  struct nlmsghdr *nlh;
  struct rtgenmsg *rt;
  int ret;
  unsigned int seq, portid;

  nlh = mnl_nlmsg_put_header(buf);
  nlh->nlmsg_type  = RTM_GETLINK;
  nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
  nlh->nlmsg_seq   = seq = time(NULL);
  rt = mnl_nlmsg_put_extra_header(nlh, sizeof(struct rtgenmsg));
  rt->rtgen_family = AF_PACKET;

  nl = mnl_socket_open(NETLINK_ROUTE);
  if (nl == NULL) {
    perror("mnl_socket_open");
    return links;
  }

  if (mnl_socket_bind(nl, 0, MNL_SOCKET_AUTOPID) < 0) {
    perror("mnl_socket_bind");
    return links;
  }
  portid = mnl_socket_get_portid(nl);

  if (mnl_socket_sendto(nl, nlh, nlh->nlmsg_len) < 0) {
    perror("mnl_socket_send");
    return links;
  }

  ret = mnl_socket_recvfrom(nl, buf, sizeof(buf));
  while (ret > 0) {
    ret = mnl_cb_run(buf, ret, seq, portid, data_cb, links);
    if (ret <= MNL_CB_STOP)
      break;
    ret = mnl_socket_recvfrom(nl, buf, sizeof(buf));
  }
  if (ret == -1) {
    perror("mnl_socket_recvfrom");
    return links;
  }

  mnl_socket_close(nl);

  return links;
}
