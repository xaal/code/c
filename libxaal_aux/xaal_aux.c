/* libxaal_aux
 * Auxiliary helpers for developing xAAL devices
 *
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <unistd.h>
#include <fcntl.h>
#include <cbor.h>

#include <xaal_aux.h>


typedef enum { none, eth } action_t;


void call_xAAL_aux_eth_addr(char *fname) {
  cbor_item_t *chw_id;
  chw_id = xAAL_aux_eth_addr();
  
  size_t sz, len;
  cbor_mutable_data buf;
  len = cbor_serialize_alloc(chw_id, &buf, &sz);
  cbor_decref(&chw_id);

  int fd;
  fd = creat(fname, (mode_t)0666);
  if (fd == -1) {
    perror(fname);
    exit(EXIT_FAILURE);
  }

  if (write(fd, buf, len) == -1) {
    perror(fname);
    close(fd);
    exit(EXIT_FAILURE);
  } else
    close(fd);
}


int main(int argc, char *argv[]) {
  char *fname = NULL;
  int opt;
  bool arg_error = false;
  action_t act = none;

  while ((opt = getopt(argc, argv, "e:")) != -1) {
    switch (opt) {
      case 'e':
        act = eth;
        fname = optarg;
        break;
      default:
        arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  
  switch (act) {
    case (eth):
      if (fname)
        call_xAAL_aux_eth_addr(fname);
      else
        arg_error = true;
      break;
    default:
      arg_error = true;
  }

  if ( arg_error ) {
    fprintf(stderr, "Usage: %s [-e <file>]\n"
      "Command line tool using libxaal_aux\n"
      "Options:\n"
      "  -e <file>  Call xAAL_aux_eth_addr() and write result in 'file'\n"
      "  ...        nothing else for now\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  exit(EXIT_SUCCESS);
}
