TOPTARGETS := all clean proper

# SUBDIRS := $(wildcard */.)
SUBDIRS := libxaal demo baseservices libxaal_jc xaaws netprobe babbler \
	xaal-stt generic-sender generic-feedback-renderer sound-measure \
	libxaal_aux tts

$(TOPTARGETS): $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: $(TOPTARGETS) $(SUBDIRS)

proper:
	-rm -f *~
