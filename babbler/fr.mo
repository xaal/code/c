��            )         �     �     �     �  	   �     �     	     &  &   C  #   j  &   �     �     �     �     �          "     :     Y     v     �     �  P   �                    #     (     .     5  	   :  �  D     �     �     �            !   "  #   D  (   h  '   �  -   �     �     �          #     >     W     k     �     �     �     �  P   �     =	     M	     R	     X	     \	     b	     i	  	   n	                                                                
                                                              	           an attribute "%s" that is   and  "%.*s'.'%s" has  "%s" has  It is %g degrees Celsius.
 New event from a device %s:
 New event from the lamp %s:
 New event from the roller shutter %s:
 New event from the thermometer %s:
 The dimmer of the lamp is %g percent.
 The lamp has  The lamp is off.
 The lamp is on.
 The roller shutter goes down.
 The roller shutter goes up.
 The roller shutter has  The roller shutter is closed.
 The roller shutter is open.
 The roller shutter stops.
 The thermometer has  Unknown argument %s
 Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] [-l <locale>]
 binary data false none null of %g of %ld true undefined Project-Id-Version: babbler
Report-Msgid-Bugs-To: christophe.lohr@imt-atlantique.fr
POT-Creation-Date: 2017-06-01 15:40+0200
PO-Revision-Date: 2017-06-01 15:40+0200
Last-Translator: Christophe Lohr <christophe.lohr@imt-atlantique.fr>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
  un attribut "%s" qui est   et  "%.*s'.'%s" a  "%s" a  Il fait %g degrés.
 Nouvel événement du device %s:
 Nouvel événement de la lampe %s:
 Nouvel événement du volet roulant %s:
 Nouvel événement du thermomètre %s:
 Le variateur de la lampe est à %g pourcent.
 La lampe a  La lampe est éteinte.
 La lampe est allumée.
 Le volet roulant descend.
 Le volet roulant monte.
 Le volet roulant a  Le volet roulant est fermé.
 Le volet roulant est ouvert.
 Le volet roulant s'arrête.
 Le thermomètre a  Argument inconnu %s
 Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] [-l <locale>]
 donnée binaire faux aucun nul de %g de %ld vrai indéfini 