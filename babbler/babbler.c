/* Babbler: passing xAAL notifications to the espeak TTS, with security
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <locale.h>
#include <libintl.h>
#include <sys/queue.h>
#include <cbor.h>
#include <espeak/speak_lib.h>

#include <xaal.h>


#define ALIVE_PERIOD    60

#define TTS_KEY		"tts"

#define SPELL_PERIOD 	2

/* i18n */
#define PACKAGE		"babbler"
#define LOCALEDIR	"."



/*
 * Chapter TTS
 */

void tts_init_en() {
  espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
  espeak_SetVoiceByName("mb-en1");
  espeak_SetParameter(espeakRATE,   120, 0);
  espeak_SetParameter(espeakVOLUME, 100, 0);
  espeak_SetParameter(espeakPITCH,   40, 0);
  espeak_SetParameter(espeakWORDGAP,  0, 0);
}

void tts_init_fr() {
  espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0);
  espeak_SetVoiceByName("mb-fr1");
  espeak_SetParameter(espeakRATE,   120, 0);
  espeak_SetParameter(espeakVOLUME, 100, 0);
  espeak_SetParameter(espeakPITCH,   40, 0);
  espeak_SetParameter(espeakWORDGAP,  0, 0);
}


void tts_speak(const char *str) {
  printf("%s\n", str);
  espeak_Synth(str, strlen(str)+1, 0, POS_CHARACTER, 0, espeakCHARS_AUTO, NULL, NULL);
  // espeak_Synchronize();
}



/*
 * cbor to sentences
 */

void tell_cbor_item(FILE *fstr, cbor_item_t *cval) {
  switch ( cbor_typeof(cval) ) {
    case CBOR_TYPE_UINT:
    case CBOR_TYPE_NEGINT:
      fprintf(fstr, gettext("of %ld"), cbor_get_int(cval));
      break;

    case CBOR_TYPE_BYTESTRING:
      fprintf(fstr, gettext("binary data"));
      break;

    case CBOR_TYPE_STRING: {
      size_t len;
      char *str = xAAL_cbor_string_dup(cval, &len);
      fprintf(fstr, "\"%s\"", str);
      free(str);
      break; }

    case CBOR_TYPE_ARRAY: {
      size_t i, sz = cbor_array_size(cval);
      for (i=0; i < sz; i++) {
	if (i)
	  fprintf(fstr, gettext(" and "));
	tell_cbor_item(fstr, cbor_move(cbor_array_get(cval, i)));
      }
      break; }

    case CBOR_TYPE_MAP: {
      struct cbor_pair *map = cbor_map_handle(cval);
      size_t len, i, sz = cbor_map_size(cval);
      char *key;
      for (i=0; i < sz; i++) {
	key = xAAL_cbor_string_dup(map[i].key, &len);
	if (key) {
	  if (i)
	    fprintf(fstr, gettext(" and "));
	  fprintf(fstr, gettext(" an attribute \"%s\" that is "), key);
	  free(key);
	  tell_cbor_item(fstr, map[i].value);
	}
      }
      break; }

    case CBOR_TYPE_FLOAT_CTRL: {
      uint8_t ctrl = cbor_ctrl_value(cval);
      switch (ctrl) {
      case CBOR_CTRL_NONE:
	fprintf(fstr, gettext("none"));
	break;
      case CBOR_CTRL_FALSE:
	fprintf(fstr, gettext("false"));
	break;
      case CBOR_CTRL_TRUE:
	fprintf(fstr, gettext("true"));
	break;
      case CBOR_CTRL_NULL:
	fprintf(fstr, gettext("null"));
	break;
      case CBOR_CTRL_UNDEF:
	fprintf(fstr, gettext("undefined"));
	break;
      default:
	if ( cbor_float_get_width(cval) != CBOR_FLOAT_0 )
	  fprintf(fstr, gettext("of %g"), cbor_float_get_float(cval));
	else
	  fprintf(fstr, gettext("null"));
      }
      break; }
    default:
      fprintf(fstr, gettext("undefined"));
  }
}



/*
 * Chapter Devices to sentences
 */


void spell_lamp(const char *source_tag, const char *dev_type,
		      cbor_item_t *cbody) {
  cbor_item_t *cbody2;
  struct cbor_pair *map;
  FILE *fstr;
  char *str, *key;
  size_t str_sz, sz, i, len;

  fstr = open_memstream(&str, &str_sz);
  fflush(fstr);

  if (source_tag)
    fprintf(fstr, gettext("New event from the lamp %s:\n"), source_tag);

  cbody2 = cbor_new_indefinite_map();
  sz = cbor_map_size(cbody);
  map = cbor_map_handle(cbody);
  for (i=0; i < sz; i++) {
    key = xAAL_cbor_string_dup(map[i].key, &len);

    if ( key ) {
      if ( (strcmp((const char*)key, "light")==0) && cbor_is_bool(map[i].value) ) {
	if ( xAAL_cbor_get_bool(map[i].value) )
	  fprintf(fstr, gettext("The lamp is on.\n"));
	else
	  fprintf(fstr, gettext("The lamp is off.\n"));

      } else if ( (strcmp((const char*)key, "dimmer")==0) && cbor_is_float(map[i].value) )
	fprintf(fstr, gettext("The dimmer of the lamp is %g percent.\n"),
		cbor_float_get_float(map[i].value) );
      else
	(void)!cbor_map_add(cbody2, map[i]);
      free(key);
    }
  }

  if ( cbor_map_size(cbody2) ) {
    fprintf(fstr, gettext("The lamp has "));
    tell_cbor_item(fstr, cbody2);
    fprintf(fstr, ".\n");
  }
  cbor_decref(&cbody2);

  tts_speak(str);
  fclose(fstr);
  free(str);
}


void spell_thermometer(const char *source_tag, const char *dev_type,
		       cbor_item_t *cbody) {
  cbor_item_t *cbody2;
  struct cbor_pair *map;
  FILE *fstr;
  char *str, *key;
  size_t str_sz, sz, i, len;

  fstr = open_memstream(&str, &str_sz);
  fflush(fstr);

  if (source_tag)
    fprintf(fstr, gettext("New event from the thermometer %s:\n"), source_tag);

  cbody2 = cbor_new_indefinite_map();
  sz = cbor_map_size(cbody);
  map = cbor_map_handle(cbody);
  for (i=0; i < sz; i++) {
    key = xAAL_cbor_string_dup(map[i].key, &len);

    if ( key ) {
      if ( (strcmp((const char*)key, "temperature")==0) && cbor_is_float(map[i].value) )
	fprintf(fstr, gettext("It is %g degrees Celsius.\n"),
		cbor_float_get_float(map[i].value) );
      else
	(void)!cbor_map_add(cbody2, map[i]);
      free(key);
    }
  }

  if ( cbor_map_size(cbody2) ) {
    fprintf(fstr, gettext("The thermometer has "));
    tell_cbor_item(fstr, cbody2);
    fprintf(fstr, ".\n");
  }
  cbor_decref(&cbody2);

  tts_speak(str);
  fclose(fstr);
  free(str);
}


void spell_shutter(const char *source_tag, const char *dev_type,
		   cbor_item_t *cbody) {
  cbor_item_t *cbody2;
  struct cbor_pair *map;
  FILE *fstr;
  char *str, *key, *value;
  size_t str_sz, sz, i, len;
  double position;

  fstr = open_memstream(&str, &str_sz);
  fflush(fstr);

  if (source_tag)
    fprintf(fstr, gettext("New event from the roller shutter %s:\n"), source_tag);

  cbody2 = cbor_new_indefinite_map();
  sz = cbor_map_size(cbody);
  map = cbor_map_handle(cbody);
  for (i=0; i < sz; i++) {
    key = xAAL_cbor_string_dup(map[i].key, &len);

    if ( key ) {
      if ( (strcmp((const char*)key, "action")==0) && cbor_isa_string(map[i].value) ) {
	value = xAAL_cbor_string_dup(map[i].value, &len);
	if (strcmp((const char*)value, "up")==0)
	  fprintf(fstr, gettext("The roller shutter goes up.\n"));
	else if (strcmp((const char*)value, "down")==0)
	  fprintf(fstr, gettext("The roller shutter goes down.\n"));
	else if (strcmp((const char*)value, "stop")==0)
	  fprintf(fstr, gettext("The roller shutter stops.\n"));
	else
	  (void)!cbor_map_add(cbody2, map[i]);
	free(value);
      } else if ( (strcmp((const char*)key, "position")==0) && cbor_is_float(map[i].value) ) {
	position = cbor_float_get_float(map[i].value);
	if (position < 1.0)
	  fprintf(fstr, gettext("The roller shutter is closed.\n"));
	else if (position > 99.0)
	  fprintf(fstr, gettext("The roller shutter is open.\n"));
	// else 	/* boring to hear that */
	//  fprintf(fstr, gettext("The roller shutter is open at %g percent.\n"), position);
      } else
	(void)!cbor_map_add(cbody2, map[i]);
      free(key);
    }
  }

  if ( cbor_map_size(cbody2) ) {
    fprintf(fstr, gettext("The roller shutter has "));
    tell_cbor_item(fstr, cbody2);
    fprintf(fstr, ".\n");
  }
  cbor_decref(&cbody2);

  if (strlen(str))
    tts_speak(str);
  fclose(fstr);
  free(str);
}


void spell_misc(const char *source_tag, const char *dev_type,
		      cbor_item_t *cbody) {
  FILE *fstr;
  char *str, *dot;
  size_t str_sz;

  fstr = open_memstream(&str, &str_sz);
  fflush(fstr);

  if (source_tag)
    fprintf(fstr, gettext("New event from a device %s:\n"), source_tag);

  dot = index(dev_type, '.');
  if (dot)
    fprintf(fstr, gettext("\"%.*s'.'%s\" has "), (int)(dot-dev_type), dev_type, dot+1);
  else
    fprintf(fstr, gettext("\"%s\" has "), dev_type);

  tell_cbor_item(fstr, cbody);

  fprintf(fstr, ".\n");

  tts_speak(str);
  fclose(fstr);
  free(str);
}


void spell_device(const char *source_tag, const char *dev_type,
		 cbor_item_t *cbody) {

  if (strncmp(dev_type, "lamp.", strlen("lamp.")) == 0)
    spell_lamp(source_tag, dev_type, cbody);

  else if (strncmp(dev_type, "thermometer.", strlen("thermometer.")) == 0)
    spell_thermometer(source_tag, dev_type, cbody);

  else if (strncmp(dev_type, "shutter.", strlen("shutter.")) == 0)
    spell_shutter(source_tag, dev_type, cbody);

  else
    spell_misc(source_tag, dev_type, cbody);
}




/*
 * xAAL
 */
xAAL_businfo_t bus;
xAAL_devinfo_t me = { .dev_type     = "basic.basic",
		      .alivemax       = 2 * ALIVE_PERIOD,
		      .vendor_id       = "IHSEV",
		      .product_id      = "Babbler",
		      .hw_id           = NULL,
		      .version        = "0.1",
		      .group_id        = NULL,
		      .url            = "http://recherche.imt-atlantique.fr/xaal/documentation/",
		      .schema         = "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/basic.basic",
		      .info           = NULL,
		      .unsupported_attributes = NULL,
		      .unsupported_methods = NULL,
		      .unsupported_notifications = NULL
		    };


/* List of detected metadata-databases */
typedef LIST_HEAD(mdbhead, mdbentry) mdbs_t;
typedef struct mdbentry {
  uuid_t addr;
  char *type;
  time_t timeout;
  LIST_ENTRY(mdbentry) entries;
} mdb_t;



/* List of fetched devices */
typedef LIST_HEAD(deviceshead, deviceentry) devices_t;
typedef struct deviceentry {
  uuid_t uuid;		/* device addr */
  char *tts_key;	/* word to be pronounced by the tts for this device, provided by a metadataDB */
  bool is_locale;	/* is that word a generic one (i.e, associated to the "tts" key), or given in the LOCALE by the metadataDB (e.g., the "tts:fr" key) */
  LIST_ENTRY(deviceentry) entries;
} device_t;


void update_device_tts_key(devices_t *devices, const uuid_t *uuid, char *tts_key, bool is_locale) {
  device_t *np;
  LIST_FOREACH(np, devices, entries)
    if (uuid_compare(np->uuid, *uuid) == 0) {
      if ( !np->is_locale || is_locale ) {
	free(np->tts_key);
	np->tts_key = tts_key;
	np->is_locale = is_locale;
      }
      return;
    }
  np = (device_t *)malloc(sizeof(device_t));
  uuid_copy(np->uuid, *uuid);
  np->tts_key = tts_key;
  np->is_locale = is_locale;
  LIST_INSERT_HEAD(devices, np, entries);
}

void delete_device(devices_t *devices, const uuid_t *uuid, bool is_locale) {
  device_t *np;
  LIST_FOREACH(np, devices, entries)
    if (uuid_compare(np->uuid, *uuid) == 0) {
      if ( !np->is_locale || is_locale ) {
	free(np->tts_key);
	LIST_REMOVE(np, entries);
	free(np);
      }
      return;
    }
}


char *get_device_tts_key(devices_t *devices, const uuid_t *uuid) {
  device_t *np;
  LIST_FOREACH(np, devices, entries)
    if (uuid_compare(np->uuid, *uuid) == 0)
      return np->tts_key;
  return NULL;
}


/* Send a get_keys_values request */
bool request_get_keys_values(const xAAL_businfo_t *bus,
			   const xAAL_devinfo_t *me,
			   mdbs_t *mdbs,
			   const uuid_t *addr,
			   const char *tts_locale_key) {
  cbor_item_t *cbody, *ctargets, *ckeys;
  mdb_t *np;

  ckeys =  cbor_new_definite_array(2);
  (void)!cbor_array_push(ckeys, cbor_move(cbor_build_string(TTS_KEY)));
  (void)!cbor_array_push(ckeys, cbor_move(cbor_build_string(tts_locale_key)));

  cbody = cbor_new_definite_map(2);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(cbor_build_bytestring(*addr, 16)) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("keys")), cbor_move(ckeys) });

  /* Build a list of targets to query */
  ctargets = cbor_new_indefinite_array();
  LIST_FOREACH(np, mdbs, entries)
    if ( np->timeout && (time(NULL) > np->timeout) ) {
      // Too old, clean it
      LIST_REMOVE(np, entries);
      free(np->type);
      free(np);
    } else {
      (void)!cbor_array_push(ctargets, cbor_move(cbor_build_bytestring(np->addr, 16)));
    }

  if ( cbor_array_size(ctargets) )
    return xAAL_write_bus(bus, me, xAAL_REQUEST, "get_keys_values", cbody, ctargets);
  else {
    cbor_decref(&cbody);
    cbor_decref(&ctargets);
    return true;
  }
}


/* Manage messages received from metadatadb:
   get_value, get_keys_values, keys_values_changed */
void manage_metadatadb_msg(const uuid_t *source, cbor_item_t *cqueryBody, devices_t *devices, const char *tts_locale_key) {
  cbor_item_t *ckey, *cvalue, *cmap;
  char *key, *value;
  size_t len, i, sz;
  struct cbor_pair *pairs;
  bool is_locale;

  ckey = xAAL_cbor_map_get(cqueryBody, "key");
  key = xAAL_cbor_string_dup(ckey, &len);
  if (key) {
    if ( (is_locale=strcmp(key, tts_locale_key)==0) || (strcmp(key, TTS_KEY)==0) ) {
      cvalue =  xAAL_cbor_map_get(cqueryBody, "value");
      value = xAAL_cbor_string_dup(cvalue, &len);
      update_device_tts_key(devices, source, value, is_locale);
      free(value);
    }
    free(key);
  }

  cmap = xAAL_cbor_map_get(cqueryBody, "map");
  if (cmap) {
    if (cbor_isa_map(cmap)) {

      pairs = cbor_map_handle(cmap);
      sz = cbor_map_size(cmap);
      for (i=0; i < sz; i++) {
	key = xAAL_cbor_string_dup(pairs[i].key, &len);
	if ( !key )
	  continue;

	if ( (is_locale=strcmp(key, tts_locale_key)==0) || (strcmp(key, TTS_KEY)==0) ) {
	  if (cbor_is_null(pairs[i].value)) {
	    delete_device(devices, source, is_locale);
	  } else {
	    value = xAAL_cbor_string_dup(pairs[i].value, &len);
	    if (value) {
	      update_device_tts_key(devices, source, value, is_locale);
	      free(value);
	    }
	  }
	}
	free(key);
      }

    } else if (cbor_is_null(cmap))
      delete_device(devices, source, true);
  }
}




void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


bool reply_get_attributes(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *dev,
			 const uuid_t *target) {
  return xAAL_write_busl(bus, dev, xAAL_REPLY, "get_attributes", NULL, target, NULL);
}


void manage_msg(xAAL_businfo_t *bus, xAAL_devinfo_t *me, mdbs_t *mdbs, devices_t *devices, const char *tts_locale_key) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  mdb_t *mdb;
  static time_t last;
  time_t now;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (xAAL_targets_match(ctargets, &me->addr)) {
    if (msg_type == xAAL_REQUEST) {
      if ( (strcmp(action, "is_alive") == 0)
	  && xAAL_is_aliveDevType_match(cbody, me->dev_type) ) {
	if ( !xAAL_notify_alive(bus, me) )
	  fprintf(xAAL_error_log, "Could not reply to is_alive\n");

      } else if ( strcmp(action, "get_description") == 0 ) {
	if ( !xAAL_reply_get_description(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_description\n");

      } else if ( strcmp(action, "get_attributes") == 0 ) {
	if ( !reply_get_attributes(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_attributes\n");
      }
    }
  }

  /* A metadata database is talking */
  if (strncmp(dev_type, "metadatadb.", strlen("metadatadb.")) == 0) {
    manage_metadatadb_msg(source, cbody, devices, tts_locale_key);

    /* update list of existing DBs */
    bool exists = false;
    LIST_FOREACH(mdb, mdbs, entries)
      if ( uuid_compare(mdb->addr, *source) == 0 ) {
	if ( (msg_type == xAAL_NOTIFY) && (strcmp(action, "alive") == 0) )
	  mdb->timeout = xAAL_read_aliveTimeout(cbody);
	else
	  mdb->timeout = 0;
	exists = true;
	break;
      }
    if (!exists) {
      mdb = malloc(sizeof(mdb_t));
      uuid_copy(mdb->addr, *source);
      mdb->type = strdup(dev_type);
      if ( (msg_type == xAAL_NOTIFY) && (strcmp(action, "alive") == 0) )
	mdb->timeout = xAAL_read_aliveTimeout(cbody);
      else
	mdb->timeout = 0;
      LIST_INSERT_HEAD(mdbs, mdb, entries);
    }
  }


  /* Finally, do the job: spell events */
  now = time(NULL);
  if ( (msg_type == xAAL_NOTIFY)
       && (strcmp(action, "attributes_change") == 0) ) {
    if ( now-last < SPELL_PERIOD )
      fprintf(xAAL_error_log, "I won't talk now, too early! Wait %ld second(s).\n\n", SPELL_PERIOD-(now-last));
    else {
      char *source_tag = get_device_tts_key(devices, source);
      last = now;
      spell_device(source_tag, dev_type, cbody);
      if (!source_tag)  /* Query for tags if the device is unknown, for a next time... */
	request_get_keys_values(bus, me, mdbs, source, tts_locale_key);
    }
  }

  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/*
 * main
 */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  struct sigaction act_alarm;
  char *locale = "";
  char *tts_locale_key;
  devices_t devices;
  mdbs_t mdbs;

  xAAL_error_log = stderr;
  uuid_clear(me.addr);
  LIST_INIT(&devices);
  LIST_INIT(&mdbs);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:l:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, me.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(me.addr);
	}
	break;
      case 'l':
	locale = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, gettext("Unknown argument %s\n"), argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, gettext("Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] [-l <locale>]\n"),
	    argv[0]);
    exit(EXIT_FAILURE);
  }


  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(me.addr) ) {
    char uuid[37];
    uuid_generate(me.addr);
    uuid_unparse(me.addr, uuid);
    printf("Device: %s\n", uuid);
  }
  //  xAAL_add_wanted_target(&me.addr, &bus); /* I want to listen to everybody */

  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");


  /* i18n */
  locale = setlocale(LC_ALL, locale);
  printf("Using locale:\"%s\" path:\"%s\" domain:\"%s\"\n", locale, bindtextdomain(PACKAGE, LOCALEDIR), textdomain(PACKAGE));
  tts_locale_key = malloc( strlen(TTS_KEY) + strlen(locale) + 2);
  sprintf(tts_locale_key, "%s:%s", TTS_KEY, locale);

  /* Initilize espeak */
  if (locale && strncmp(locale, "fr", 2)==0)
    tts_init_fr();
  else
    tts_init_en();

  /* Main loop */
  for (;;)
    manage_msg(&bus, &me, &mdbs, &devices, tts_locale_key);
}
