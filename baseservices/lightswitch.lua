--
--  Lightswitch - Basic automaton to peer a switch and a lamp
--

--[[ -- Just for debuging
print("-- lightswitch.lua --")
print("xAAL_Lua_baseaddr: "..xAAL_Lua_uuid_unparse(xAAL_Lua_baseaddr))
print("xAAL_Lua_group_id: "..xAAL_Lua_uuid_unparse(xAAL_Lua_group_id))
print("xAAL_Lua_parameter: "..xAAL_Lua_parameter)
--]]


device = { info="Peering a switch and lamp", dev_type="scenario.basic",
	   vendor_id="IHSEV Team", product_id="Peering automaton", version="0.3",
	   url="http://recherche.imt-atlantique.fr/xaal/documentation/",
	   schema="https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/scenario.basic",
	   unsupported_attributes={"properties"}, unsupported_methods={"run","abort"},
	   unsupported_notifications={} }
device["addr"] = xAAL_Lua_declare_device(device)

is_enabled = true


-- Assume xAAL_Lua_parameter contains a table named peering
-- with a switch address and a lamp address
assert(loadstring(xAAL_Lua_parameter))()
peering["switch"] = xAAL_Lua_uuid_parse(peering["switch"])
peering["lamp"] = xAAL_Lua_uuid_parse(peering["lamp"])


-- Accept messages from the switch
xAAL_Lua_filter_broadcast_by_source(device["addr"], { peering["switch"] })


-- Main of the job
function xAAL_Lua_receive_callback(dev, source, dev_type, msg_type, action, body)
--[[ -- Just for debuging
  local serpent = dofile("serpent.lua")
  print(xAAL_Lua_uuid_unparse(dev));
  print(xAAL_Lua_uuid_unparse(source));
  print(dev_type);
  print(msg_type);
  print(action);
  print(serpent.block(body))
  print()
--]]
  if ( dev == device.addr ) then
    if ( msg_type == 1 ) then
      if ( action == "enable" ) then
        is_enabled = true
      elseif ( action == "disable" ) then
        is_enabled = false
      elseif ( action == "get_attributes" ) then
        xAAL_Lua_write_bus(dev, 2, "get_attributes", { enabled=is_enabled }, { source } )
      end
    end
  end
  if ( is_enabled and (source == peering["switch"]) and (msg_type == 0) and (action == "attributes_change") ) then
    if ( body["position"] ) then
      xAAL_Lua_write_bus(dev, 1, "turn_on", {}, { peering["lamp"] } )
    else
      xAAL_Lua_write_bus(dev, 1, "turn_off", {}, { peering["lamp"] } )
    end
  end
end
