/* Metadata DB Tester - Do basic tests on metadata-databases
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <termios.h>

#include <sys/types.h>
#include <regex.h>
#include <sys/queue.h>
#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>
#include <sqlite3.h>

#include <xaal.h>

#define ALIVE_PERIOD    120



/* List of detected metadata-databases */
typedef LIST_HEAD(mdbhead, mdbentry) mdbs_t;
typedef struct mdbentry {
  uuid_t addr;
  char *type;
  time_t timeout;
  LIST_ENTRY(mdbentry) entries;
} mdb_t;



/* List of fetched devices */
typedef LIST_HEAD(deviceshead, deviceentry) devices_t;
typedef struct deviceentry {
  uuid_t uuid;
  LIST_ENTRY(deviceentry) entries;
} device_t;

void insert_device(devices_t *devices, const uuid_t *uuid) {
  device_t *np;
  LIST_FOREACH(np, devices, entries)
    if (uuid_compare(np->uuid, *uuid) == 0)
      return;
  np = (device_t *)malloc(sizeof(device_t));
  uuid_copy(np->uuid, *uuid);
  LIST_INSERT_HEAD(devices, np, entries);
}



/* Global variable for the handler */
xAAL_businfo_t bus;
xAAL_devinfo_t me;

void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* Send a request to a set of metadata databases */
/* Return true if success */
bool bulk_request(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
		  mdbs_t *mdbs, const char *action,
		  cbor_item_t *cbody) {
  mdb_t *np;
  cbor_item_t *ctargets;

  /* Build a list of targets to query */
  ctargets = cbor_new_indefinite_array();
  LIST_FOREACH(np, mdbs, entries)
    if ( np->timeout && (time(NULL) > np->timeout) ) {
      // Too old, clean it
      LIST_REMOVE(np, entries);
      free(np->type);
      free(np);
    } else {
      (void)!cbor_array_push(ctargets, cbor_move(xAAL_new_tagged_uuid(&(np->addr))));
    }

  return xAAL_write_bus(bus, me, xAAL_REQUEST, action, cbody, ctargets);
}


/* Send a get_devices request */
bool request_get_devices(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
			mdbs_t *mdbs, const char *key, const char *value) {
  cbor_item_t *cbody;
  bool r;

  if (value)
    printf(" get_devices(%s,%s): ", key, value);
  else
    printf(" get_devices(%s): ", key);

  cbody = cbor_new_indefinite_map();
  if (key)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("key")), cbor_move(cbor_build_string(key)) });
  if (value)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("value")), cbor_move(cbor_build_string(value)) });

  r = bulk_request(bus, me, mdbs, "get_devices", cbody);
  printf("%s\n", r?"ok":"ko");
  return r;
}


/* Send a get_keys_values request */
bool request_get_keys_values(const xAAL_businfo_t *bus,
			   const xAAL_devinfo_t *me,
			   mdbs_t *mdbs,
			   const uuid_t *addr) {
  cbor_item_t *cbody;
  char uuid[37];
  bool r;

  uuid_unparse(*addr, uuid);
  printf(" get_keys_values(%s): ", uuid);

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(xAAL_new_tagged_uuid(addr)) });

  r = bulk_request(bus, me, mdbs, "get_keys_values", cbody);
  printf("%s\n", r?"ok":"ko");
  return r;
}


/* Send a get_value request */
bool request_get_value(const xAAL_businfo_t *bus,
		      const xAAL_devinfo_t *me,
		      mdbs_t *mdbs,
		      const uuid_t *addr, const char *key) {
  cbor_item_t *cbody;
  char uuid[37];
  bool r;

  uuid_unparse(*addr, uuid);
  printf(" get_value(%s, %s): ", uuid, key);

  cbody = cbor_new_definite_map(2);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(xAAL_new_tagged_uuid(addr)) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("key")), cbor_move(cbor_build_string(key)) });

  r = bulk_request(bus, me, mdbs, "get_value", cbody);
  printf("%s\n", r?"ok":"ko");
  return r;
}


/* Send an add_keys_values or an update_keys_values request */
/* addr followed by key(s)-value(s) of type (const char*) */
bool request_addupdate_keys_values(const xAAL_businfo_t *bus,
				 const xAAL_devinfo_t *me,
				 mdbs_t *mdbs,
				 bool update,
				 const char *addr, ...) {
  cbor_item_t *cbody, *cmap;
  va_list ap;
  const char *key, *value;
  uuid_t device;
  bool r;

  if (uuid_parse(addr, device) == -1)
    return false;

  printf(" %s(%s", update?"update_keys_values":"add_keys_values", addr);

  cmap = cbor_new_indefinite_map();
  va_start(ap, addr);
  while ( (key = va_arg(ap, const char*)) ) {
    value = va_arg(ap, const char*);
    printf(" %s %s", key, value);
    (void)!cbor_map_add(cmap, (struct cbor_pair){ cbor_move(cbor_build_string(key)), cbor_move( value? cbor_build_string(value) : cbor_new_null() ) });
  }
  va_end(ap);

  cbody = cbor_new_definite_map(2);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(xAAL_new_tagged_uuid(&device)) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("map")), cbor_move( cbor_map_size(cmap)? cmap : cbor_new_null() ) });

  r = bulk_request(bus, me, mdbs, update?"update_keys_values":"add_keys_values", cbody);
  printf("): %s\n", r?"ok":"ko");
  return r;
}


/* Reply to get_attributes (empty answer, for now) */
bool reply_get_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
			 const uuid_t *target) {
  return xAAL_write_busl(bus, me, xAAL_REPLY, "get_attributes", NULL, target, NULL);
}


/* Send is_alive request */
/* Return true if success */
bool request_is_alive(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me) {
  cbor_item_t *cbody, *cdev_types;

  cdev_types = cbor_new_indefinite_array();
  (void)!cbor_array_push(cdev_types, cbor_move(cbor_build_string("metadatadb.any")));

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("dev_types")), cbor_move(cdev_types) });

  return xAAL_write_busl(bus, me, xAAL_REQUEST, "is_alive", cbody, &xAAL_is_alive_target, NULL);
}


/* Fetch keys-values of devices */
bool fetch_devices(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
		      mdbs_t *mdbs, devices_t *devices) {
  device_t *np;
  bool r = true;

  printf("Fetch metatdata DB (get_keys_values)\n");
  LIST_FOREACH(np, devices, entries)
    r &= request_get_keys_values(bus, me, mdbs, &np->uuid);
  return r;
}


/*  Dump reply on stdout */
void dump_reply(const uuid_t *source, const char *dev_type, const char *action,
		cbor_item_t *cqueryBody, devices_t *devices) {
  static unsigned long n = 0;
  time_t now = time(NULL);
  cbor_item_t *cdev, *cdevs, *ckey, *cvalue, *cmap;
  uuid_t device;
  char device_s[37], *key, *value;
  size_t len, i, sz;
  struct cbor_pair *pairs;

  uuid_unparse(*source, device_s);
  printf("\n  %ld: From %s (%s) at %s  Reply: %s\n", n++, device_s, dev_type, ctime(&now), action);

  cdev = xAAL_cbor_map_get(cqueryBody, "device");
  if (xAAL_cbor_is_uuid(cdev, &device) ) {
    uuid_unparse(device, device_s);
    printf("  device: %s\n", device_s);
    insert_device(devices, &device);
  }

  cdevs = xAAL_cbor_map_get(cqueryBody, "devices");
  if ( cdevs && cbor_isa_array(cdevs) ) {
    printf("  devices:\n");
    sz = cbor_array_size(cdevs);
    for (i = 0; i < sz; i++) {
      cdev = cbor_array_get(cdevs, i);
      if (xAAL_cbor_is_uuid(cdev, &device) ) {
	uuid_unparse(device, device_s);
	printf("   [%lu] %s\n", i, device_s);
	insert_device(devices, &device);
      }
      cbor_decref(&cdev);
    }
  }

  ckey = xAAL_cbor_map_get(cqueryBody, "key");
  key = xAAL_cbor_string_dup(ckey, &len);
  if (key) {
    printf("  key: \"%s\"\n", key);
    free(key);
  }

  cvalue =  xAAL_cbor_map_get(cqueryBody, "value");
  if (cvalue) {
    value = xAAL_cbor_string_dup(cvalue, &len);
    if (value) {
      printf("  value: \"%s\"\n", value);
      free(value);
    } else if (cbor_is_null(cvalue))
      printf("  value: null\n");
    else
      printf("  value: <unexpected type>\n");
  }

  cmap = xAAL_cbor_map_get(cqueryBody, "map");
  if (cmap) {
    if (cbor_isa_map(cmap)) {

      pairs = cbor_map_handle(cmap);
      sz = cbor_map_size(cmap);
      printf("  map[%lu]: ", sz);
      for (i=0; i < sz; i++) {
	key = xAAL_cbor_string_dup(pairs[i].key, &len);
	if ( !key )
	  continue;

	if (cbor_is_null(pairs[i].value))
	  printf(" \"%s\":null", key);
	else {
	  value = xAAL_cbor_string_dup(pairs[i].value, &len);
	  if (value) {
	    printf(" \"%s\":\"%s\"", key, value);
	    free(value);
	  } else
	    printf(" \"%s\":<unexpected type>", key);
	}
	free(key);
      }

    } else if (cbor_is_null(cmap))
      printf("  map: null\n");
    else
      printf("  map: <unexpected type>\n");
  }
  printf("\n");
}


/* Manage received message */
void manage_msg(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
		mdbs_t *mdbs, devices_t *devices) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action, uuid[37];
  xAAL_msg_type_t msg_type;
  uuid_t *source;
  mdb_t *mdb;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (xAAL_targets_match(ctargets, &me->addr)) {

    if (msg_type == xAAL_REQUEST) {

      if ( (strcmp(action, "is_alive") == 0)
	   && xAAL_is_aliveDevType_match(cbody, me->dev_type) ) {
	if ( !xAAL_notify_alive(bus, me) )
	  fprintf(xAAL_error_log, "Could not reply to is_alive\n");

      } else if ( strcmp(action, "get_description") == 0 ) {
	if ( !xAAL_reply_get_description(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_description\n");

      } else if ( strcmp(action, "get_attributes") == 0 ) {
	if ( !reply_get_attributes(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

      }

    }
  }

  /* A metadata database is talking */
  if (strncmp(dev_type, "metadatadb.", strlen("metadatadb.")) == 0) {
    dump_reply(source, dev_type, action, cbody, devices);

    /* update list of existing DBs */
    bool exists = false;
    LIST_FOREACH(mdb, mdbs, entries)
      if ( uuid_compare(mdb->addr, *source) == 0 ) {
	if ( (msg_type == xAAL_NOTIFY) && (strcmp(action, "alive") == 0) )
	  mdb->timeout = xAAL_read_aliveTimeout(cbody);
	else
	  mdb->timeout = 0;
	exists = true;
	break;
      }
    if (!exists) {
      mdb = malloc(sizeof(mdb_t));
      uuid_copy(mdb->addr, *source);
      mdb->type = strdup(dev_type);
      if ( (msg_type == xAAL_NOTIFY) && (strcmp(action, "alive") == 0) )
	mdb->timeout = xAAL_read_aliveTimeout(cbody);
      else
	mdb->timeout = 0;
      LIST_INSERT_HEAD(mdbs, mdb, entries);
      uuid_unparse(*source, uuid);
      printf("New metadata db: %s\n", uuid);
    }

  }

  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/*************************/
/* Chapter "User Option" */
/*************************/

/* Options from cmdline or conffile */
typedef struct {
  char *addr;
  char *port;
  int hops;
  char *passphrase;
  char *conffile;
  bool immutable;
  char *logfile;
} options_t;


/* Parse cmdline */
void parse_cmdline(int argc, char **argv, options_t *opts) {
  int opt;
  bool arg_error = false;

  while ((opt = getopt(argc, argv, "a:p:h:s:u:c:il:")) != -1) {
    switch (opt) {
      case 'a':
	opts->addr = optarg;
	break;
      case 'p':
	opts->port = optarg;
	break;
      case 'h':
	opts->hops = atoi(optarg);
	break;
      case 's':
	opts->passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, me.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(me.addr);
	}
	break;
      case 'c':
	opts->conffile = optarg;
	break;
      case 'i':
	opts->immutable = true;
	break;
      case 'l':
	opts->logfile = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (arg_error) {
    fprintf(stderr, "Usage: %s [-a <addr>] [-p <port>] [-h <hops>] [-s <secret>] [-u <uuid>]\n"
		"		[-c <conffile>] [-l <logfile>]\n"
		"-a <addr>	multicast IPv4 or IPv6 address of the xAAL bus\n"
		"-p <port>	UDP port of the xAAL bus\n"
		"-h <hops>	Hops limit for multicast packets\n"
		"-s <secret>	Secret passphrase\n"
		"-u <uuid>	xAAL address of the device; random by default\n"
		"-c <conffile>	Filename of the configuration file (cson format)\n"
		"		Use 'me.conf' by default\n"
		"-i		Immutable config file (do not re-write it)\n"
		"-l <logfile>	Filename to write errors; stderr by default\n"
		, argv[0]);
    exit(EXIT_FAILURE);
  }

}


/* Read a config file (json format) */
void read_config(options_t *opts) {
  struct json_object *jconf, *jaddr, *jport, *jhops, *jpassphrase, *juuid,
		     *jconffile, *jimmutable, *jlogfile;

  /* read file */
  jconf = json_object_from_file(opts->conffile);
  if (json_object_is_type(jconf, json_type_null)) {
    fprintf(stderr, "Could not parse config file %s\n", opts->conffile);
    return;
  }

  /* parse bus addr */
  if (json_object_object_get_ex(jconf, "addr", &jaddr)
      && json_object_is_type(jaddr, json_type_string))
    opts->addr = strdup(json_object_get_string(jaddr));

  /* parse bus port */
  if (json_object_object_get_ex(jconf, "port", &jport)
      && json_object_is_type(jport, json_type_string))
    opts->port = strdup(json_object_get_string(jport));

  /* parse bus hops */
  if (json_object_object_get_ex(jconf, "hops", &jhops)
      && json_object_is_type(jhops, json_type_int))
    opts->hops = json_object_get_int(jhops);

  /* parse passphrase */
  if (json_object_object_get_ex(jconf, "passphrase", &jpassphrase)
      && json_object_is_type(jpassphrase, json_type_string))
    opts->passphrase = strdup(json_object_get_string(jpassphrase));

  /* parse me xAAL address (uuid) */
  if (json_object_object_get_ex(jconf, "uuid", &juuid)
      && json_object_is_type(juuid, json_type_string))
    if ( uuid_parse(json_object_get_string(juuid), me.addr) == -1 )
      uuid_clear(me.addr);

  /* parse config file name  */
  if (json_object_object_get_ex(jconf, "conffile", &jconffile)
      && json_object_is_type(jconffile, json_type_string))
    opts->conffile = strdup(json_object_get_string(jconffile));

  /* parse immutable flag  */
  if (json_object_object_get_ex(jconf, "immutable", &jimmutable)
      && json_object_is_type(jimmutable, json_type_boolean))
    opts->immutable = json_object_get_boolean(jimmutable);

  /* parse log file name  */
  if (json_object_object_get_ex(jconf, "logfile", &jlogfile)
      && json_object_is_type(jlogfile, json_type_string))
    opts->logfile = strdup(json_object_get_string(jlogfile));

  json_object_put(jconf);
}


/* Re-write config file (json format) */
void write_config(options_t *opts) {
  struct json_object *jconf;
  char uuid[37];

  jconf = json_object_new_object();
  jconf = json_object_new_object();
  json_object_object_add(jconf, "addr",      json_object_new_string(opts->addr));
  json_object_object_add(jconf, "port",      json_object_new_string(opts->port));
  json_object_object_add(jconf, "hops",      json_object_new_int(opts->hops));
  json_object_object_add(jconf, "passphrase",json_object_new_string(opts->passphrase));
  uuid_unparse(me.addr,  uuid);
  json_object_object_add(jconf, "uuid",      json_object_new_string(uuid));
  json_object_object_add(jconf, "conffile",  json_object_new_string(opts->conffile));
  json_object_object_add(jconf, "immutable", json_object_new_boolean(opts->immutable));
  if (opts->logfile)
    json_object_object_add(jconf, "logfile", json_object_new_string(opts->logfile));

  if (json_object_to_file_ext(opts->conffile, jconf, JSON_C_TO_STRING_PRETTY
			      | JSON_C_TO_STRING_SPACED) == -1)
    fprintf(xAAL_error_log, "Writing config file: %s\n", strerror(errno));

  json_object_put(jconf);
}




/************************/
/* Chapter "Test cases" */
/************************/

/* Manage silent stdin */
struct termios initial_term;

void setsilent() {
  struct termios term;
  if ( tcgetattr(STDIN_FILENO, &initial_term) == -1) {
    perror("tcgetattr");
    return;
  }
  term = initial_term;
  term.c_lflag &= ~(ICANON | ECHO);
  term.c_cc[VMIN] = 1;
  term.c_cc[VTIME] = 0;
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &term) == -1)
    perror("tcsetattr");
}

void restore_term() {
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &initial_term) == -1)
    perror("tcsetattr");
}


void run_tests(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
	       mdbs_t *mdbs, devices_t *devices) {
  static unsigned step = 0;
  char c;
  uuid_t uuid;
  char *addrs[] = { "24401337-8b15-4206-9b23-a75dd991f0c0",
		    "bf7a22aa-e51c-4978-8178-61e1bc197f56",
		    "17d73ce6-d153-4eff-8869-9635db58f81e",
		    "16e5bf03-3d28-4a08-bfb3-23053740c113",
		    "24ef3b61-817e-4ca8-a78d-27d79fbf1e36",
		    "377e67f0-64c1-4200-a986-e30db4ec9cc6",
		    "e20eb371-a67a-49d3-9ad0-1abd44b95a5d" };
//  char *words[] = { "cuisine", "salon", "chambre", "cave",
//		    "plafond", "fenetre", "porte",
//		    "rez_de_chausse", "etage1", "etage2",
//		    "Roger", "Bob", "Alice", "Sindy" };

  switch ( read(STDIN_FILENO, &c, 1) ) {
  case -1:
    fprintf(xAAL_error_log, "read/stdin: %s\n", strerror(errno));
    break;
  case 0:
    exit(EXIT_SUCCESS);
    break;
  }

  printf("\nExecute step #%d:\n", step);

  switch (step++) {
  case 0:
    request_get_devices(bus, me, mdbs, NULL, NULL);
    break;
  case 1:
    request_get_devices(bus, me, mdbs, "location", NULL);
    break;
  case 2:
    request_addupdate_keys_values(bus, me, mdbs, false, addrs[0], "location", "cuisine", NULL);
    request_addupdate_keys_values(bus, me, mdbs, false, addrs[1], "location", "salon", "floor", "rez-de-chaussé", NULL);
    request_addupdate_keys_values(bus, me, mdbs, false, addrs[2], "location", "chambre", "user", "Bob", NULL);
    request_addupdate_keys_values(bus, me, mdbs, false, addrs[3], "location", "cave", "floor", "sous-sol", NULL);
    request_addupdate_keys_values(bus, me, mdbs, false, addrs[4], "location", "plafond", "floor", "1er étage", NULL);
    request_addupdate_keys_values(bus, me, mdbs, false, addrs[5], "location", "fenetre", "user", "Alice", NULL);
    break;
  case 3:
    fetch_devices(bus, me, mdbs, devices);
    break;
  case 4:
    uuid_parse(addrs[1], uuid);
    request_get_value(bus, me, mdbs, &uuid, "floor");
    break;
  case 5:
    request_get_devices(bus, me, mdbs, "location", NULL);
    break;
  case 6:
    request_addupdate_keys_values(bus, me, mdbs, true, addrs[4], "location", "porte", "floor", NULL, "user", "Sindy", NULL);
    break;
  case 7:
    request_addupdate_keys_values(bus, me, mdbs, true, addrs[5], NULL);
    break;
  case 8:
    fetch_devices(bus, me, mdbs, devices);
    break;
  case 9:
    printf("Done.\n");
    exit(EXIT_SUCCESS);
    break;
  default:
    printf("Nothing.\n");
  }
}


/*************************************/
/* Chapter "Main of the application" */
/*************************************/

/* Global variable for the handler */
options_t *opts;

/* Called at exit */
void terminate() {
  if (!opts->immutable)
    write_config(opts);
}

/* Handler for Ctrl-C &co. */
void cancel(int s) {
  terminate();
  exit(EXIT_SUCCESS);
}


/* Main */
int main(int argc, char **argv) {
  options_t options = { .addr=NULL, .port=NULL, .hops=-1, .passphrase=NULL,
			.conffile="metadatadbTester.conf",
			.immutable=false, .logfile=NULL };
  devices_t devices;
  mdbs_t mdbs;
  fd_set rfds, rfds_;

  uuid_clear(me.addr);
  LIST_INIT(&devices);
  LIST_INIT(&mdbs);

  /* Parse cmdline arguments */
  parse_cmdline(argc, argv, &options);

  /* Load config */
  read_config(&options);

  /* Manage logfile */
  if (options.logfile) {
    xAAL_error_log = fopen(options.logfile, "a");
    if (xAAL_error_log == NULL) {
      perror("Opening logfile");
      xAAL_error_log = stderr;
    }
  } else
    xAAL_error_log = stderr;

  /* Join the xAAL bus */
  if ( !options.addr || !options.port || !options.passphrase) {
    fprintf(xAAL_error_log, "Please provide the address, the port and the passphrase of the xAAL bus.\n");
    exit(EXIT_FAILURE);
  } else if ( !xAAL_join_bus(options.addr, options.port, options.hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(options.passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(me.addr) ) {
    char str[37];
    uuid_generate(me.addr);
    uuid_unparse(me.addr, str);
    printf("Device: %s\n", str);
  }
  // xAAL_add_wanted_target(&options.addr, &bus); // Listen to everybody

  /* Setup device info */
  me.dev_type    = "basic.basic";
  me.alivemax   = 2 * ALIVE_PERIOD;
  me.vendor_id   = "IHSEV";
  me.product_id  = "Metatdata DB Tester";
  me.hw_id	= NULL;
  me.version    = "0.4";
  me.group_id    = NULL;
  me.url        = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  me.schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/basic.basic";
  me.info	= NULL;
  me.unsupported_attributes = NULL;
  me.unsupported_methods = (char *[]){ "get_attributes", NULL };
  me.unsupported_notifications = (char *[]){ "attributes_change", NULL };

  /* Manage alive notifications */
  {
    struct sigaction act_alarm;

    act_alarm.sa_handler = alive_sender;
    act_alarm.sa_flags = ~SA_RESETHAND;
    sigemptyset(&act_alarm.sa_mask);
    sigaction(SIGALRM, &act_alarm, NULL);
    alarm(ALIVE_PERIOD);
    if ( !xAAL_notify_alive(&bus, &me) )
      fprintf(xAAL_error_log, "Could not send initial alive notification.\n");
  }

  if ( !request_is_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send is_alive request.\n");

  /* Manage Ctrl-C &co. */
  opts = &options;
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);
  atexit(terminate);

  /* Manage user interaction */
  setsilent();
  atexit(restore_term);
  printf("Press a key at each step.\n");

  FD_ZERO(&rfds);
  FD_SET(STDIN_FILENO, &rfds);
  FD_SET(bus.sfd, &rfds);

  /* main loop */
  for (;;) {

    rfds_ = rfds;
    if ( (select(bus.sfd+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(STDIN_FILENO, &rfds_))
      run_tests(&bus, &me, &mdbs, &devices);

    else if (FD_ISSET(bus.sfd, &rfds_))
      manage_msg(&bus, &me, &mdbs, &devices);
  }
}
