/* Cache - A simple cache service
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <termios.h>

#include <sys/types.h>
#include <regex.h>
#include <sys/queue.h>
#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>
#include <sqlite3.h>

#include <xaal.h>

#define ALIVE_PERIOD    120


/******************************/
/* Chapter: internal database */
/******************************/

/* List of attributes of a device */
typedef LIST_HEAD(attrshead, attrentry) attrs_t;
typedef struct attrentry {
  char *name;
  cbor_item_t *val;
  time_t date;
  LIST_ENTRY(attrentry) entries;
} attr_t;


/* List of devices */
typedef LIST_HEAD(deviceshead, deviceentry) devices_t;
typedef struct deviceentry {
  uuid_t uuid;
  char *dev_type;
  time_t timeout;
  attrs_t attributes;
  LIST_ENTRY(deviceentry) entries;
} device_t;


/* Retrive or insert a device in the list */
device_t *insert_device(devices_t *devices, const uuid_t *uuid, const char *dev_type) {
  device_t *np;
  LIST_FOREACH(np, devices, entries)
    if (uuid_compare(*uuid, np->uuid) == 0)
      return np;
  np = (device_t *)malloc(sizeof(device_t));
  uuid_copy(np->uuid, *uuid);
  np->dev_type = strdup(dev_type);
  np->timeout = 0;
  LIST_INIT(&(np->attributes));
  LIST_INSERT_HEAD(devices, np, entries);
  return np;
}


/* Update or add an attribute of a device */
void insert_attribute(device_t *device, const char *name, cbor_item_t *val) {
  attr_t *np = NULL;
  LIST_FOREACH(np, &(device->attributes), entries)
    if (strcmp(name, np->name) == 0) {
      cbor_decref(&np->val);
      break;
  }
  if (np == NULL) {
    np = (attr_t *)malloc(sizeof(attr_t));
    np->name = strdup(name);
    LIST_INSERT_HEAD(&(device->attributes), np, entries);
  }
  np->val = cbor_incref(val);
  np->date = time(NULL);
}



/*****************/
/* Chapter: xAAL */
/*****************/

/* Global variable for the handler */
xAAL_businfo_t bus;
xAAL_devinfo_t me;

void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* Pick up info about attributes in msg and update internal database */
void manage_attributes_info(device_t *device, cbor_item_t *cbody) {
  struct cbor_pair *map;
  size_t i, map_sz;
  char *attribute;

  if (cbody) {
    map = cbor_map_handle(cbody);
    map_sz = cbor_map_size(cbody);

    for (i=0; i<map_sz; i++) {
      if (cbor_isa_string(map[i].key) && cbor_string_is_definite(map[i].key) ) {
	attribute = strndup((const char*)cbor_string_handle(map[i].key), cbor_string_length(map[i].key));
	insert_attribute(device, attribute, map[i].value);
	free(attribute);
      }
    }
  }
}


/* Serialize an attribute into cbor */
cbor_item_t *get_cbor_attribute(attr_t *attribute) {
  cbor_item_t *cattribute = cbor_new_definite_map(3);
  (void)!cbor_map_add(cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("name")),  cbor_move(cbor_build_string(attribute->name)) });
  (void)!cbor_map_add(cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("value")), attribute->val });
  (void)!cbor_map_add(cattribute, (struct cbor_pair){ cbor_move(cbor_build_string("date")),  cbor_move(xAAL_cbor_build_int(attribute->date)) });
  return cattribute;
}


/* Reply to get_device_attributes */
bool reply_get_device_attributes(const xAAL_businfo_t *bus,
			       const xAAL_devinfo_t *me,
			       const uuid_t *target,
			       cbor_item_t *cqueryBody,
			       devices_t *devices) {
  cbor_item_t *cqueryDevice, *cbody, *cattributes, *cache;
  uuid_t queryDevice;
  device_t *device = NULL;
  attr_t *attribute = NULL;
  size_t i, sz, len;
  char *name;

  cqueryDevice = xAAL_cbor_map_get(cqueryBody, "device");
  if ( !xAAL_cbor_is_uuid(cqueryDevice, &queryDevice) )
    return false;

  LIST_FOREACH(device, devices, entries)
    if ( uuid_compare(device->uuid, queryDevice) == 0 )
      break;
  if (device == NULL)
    return false;

  cache = cbor_new_indefinite_array();
  
  cattributes = xAAL_cbor_map_get(cqueryBody, "attributes");
  if (!cattributes || !cbor_isa_array(cattributes) || ((sz=cbor_array_size(cattributes)) == 0) ) {
    LIST_FOREACH(attribute, &(device->attributes), entries) {
      (void)!cbor_array_push(cache, cbor_move(get_cbor_attribute(attribute)));
    }
  } else {
    for (i=0; i<sz; i++) {
      name = xAAL_cbor_string_dup(cbor_move(cbor_array_get(cattributes, i)), &len);
      LIST_FOREACH(attribute, &(device->attributes), entries) {
        if ( !name || (strcmp(attribute->name, name) == 0) ) {
          (void)!cbor_array_push(cache, cbor_move(get_cbor_attribute(attribute)));
          break;
        }
      }
      free(name);
    }
  }

  cbody = cbor_new_definite_map(2);
  
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(xAAL_new_tagged_uuid(&queryDevice)) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("attributes")), cbor_move(cache) });

  return  xAAL_write_busl(bus, me, xAAL_REPLY, "get_device_attributes", cbody, target, NULL);
}


/* Reply to get_attributes (empty answer, for now) */
bool reply_get_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
			 const uuid_t *target) {
  return xAAL_write_busl(bus, me, xAAL_REPLY, "get_attributes", NULL, target, NULL);
}


/* Send is_alive request */
/* Return true if success */
bool request_is_alive(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me) {
  cbor_item_t *cbody, *cdev_types;

  cdev_types = cbor_new_definite_array(1);
  (void)!cbor_array_push(cdev_types, cbor_move(cbor_build_string("any.any")));

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("dev_types")), cbor_move(cdev_types) });

  return xAAL_write_busl(bus, me, xAAL_REQUEST, "is_alive", cbody, &xAAL_is_alive_target, NULL);
}


/* Manage received message */
void manage_msg(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
		devices_t *devices) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (msg_type == xAAL_REQUEST) {

    if ( (strcmp(action, "is_alive") == 0)
	 && xAAL_is_aliveDevType_match(cbody, me->dev_type) ) {
      if ( !xAAL_notify_alive(bus, me) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, me, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "get_attributes") == 0 ) {
      if ( !reply_get_attributes(bus, me, source) )
	fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

    } else if ( strcmp(action, "get_device_attributes") == 0 ) {
      if ( !reply_get_device_attributes(bus, me, source, cbody, devices) )
	fprintf(xAAL_error_log, "Could not reply to get_device_attributes\n");

    }

  } else if (msg_type == xAAL_NOTIFY) {
    if ( strcmp(action, "alive") == 0 ) {
      device_t *device = insert_device(devices, source, dev_type);
      device->timeout = xAAL_read_aliveTimeout(cbody);
      if ( !xAAL_write_busl(bus, me, xAAL_REQUEST, "get_attributes", NULL, source, NULL) )
	fprintf(xAAL_error_log, "Could not send get_attributes\n");

    } else if ( strcmp(action, "attributes_change") == 0 ) {
      device_t *device = insert_device(devices, source, dev_type);
      manage_attributes_info(device, cbody);
    }

  } else if (msg_type == xAAL_REPLY) {
    if ( strcmp(action, "get_attributes") == 0 ) {
      device_t *device = insert_device(devices, source, dev_type);
      manage_attributes_info(device, cbody);
    }

  }

  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/*************************/
/* Chapter "User Option" */
/*************************/

/* Options from cmdline or conffile */
typedef struct {
  char *addr;
  char *port;
  int hops;
  char *passphrase;
  char *conffile;
  bool immutable;
  char *logfile;
} options_t;


/* Parse cmdline */
void parse_cmdline(int argc, char **argv, options_t *opts) {
  int opt;
  bool arg_error = false;

  while ((opt = getopt(argc, argv, "a:p:h:s:u:c:il:")) != -1) {
    switch (opt) {
      case 'a':
	opts->addr = optarg;
	break;
      case 'p':
	opts->port = optarg;
	break;
      case 'h':
	opts->hops = atoi(optarg);
	break;
      case 's':
	opts->passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, me.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(me.addr);
	}
	break;
      case 'c':
	opts->conffile = optarg;
	break;
      case 'i':
	opts->immutable = true;
	break;
      case 'l':
	opts->logfile = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (arg_error) {
    fprintf(stderr, "Usage: %s [-a <addr>] [-p <port>] [-h <hops>] -s <secret> [-u <uuid>]\n"
		"		[-c <conffile>] [-l <logfile>]\n"
		"-a <addr>	multicast IPv4 or IPv6 address of the xAAL bus\n"
		"-p <port>	UDP port of the xAAL bus\n"
		"-h <hops>	Hops limit for multicast packets\n"
		"-s <secret>	Secret passphrase\n"
		"-u <uuid>	xAAL address of the device; random by default\n"
		"-c <conffile>	Filename of the configuration file (cson format)\n"
		"		Use 'cache.conf' by default\n"
		"-i		Immutable config file (do not re-write it)\n"
		"-l <logfile>	Filename to write errors; stderr by default\n"
		, argv[0]);
    exit(EXIT_FAILURE);
  }

}




/* Read a config file (json format) */
void read_config(options_t *opts) {
  struct json_object *jconf, *jaddr, *jport, *jhops, *jpassphrase, *juuid,
		     *jconffile, *jimmutable, *jlogfile;

  /* read file */
  jconf = json_object_from_file(opts->conffile);
  if (json_object_is_type(jconf, json_type_null)) {
    fprintf(stderr, "Could not parse config file %s\n", opts->conffile);
    return;
  }

  /* parse bus addr */
  if (json_object_object_get_ex(jconf, "addr", &jaddr)
      && json_object_is_type(jaddr, json_type_string))
    opts->addr = strdup(json_object_get_string(jaddr));

  /* parse bus port */
  if (json_object_object_get_ex(jconf, "port", &jport)
      && json_object_is_type(jport, json_type_string))
    opts->port = strdup(json_object_get_string(jport));

  /* parse bus hops */
  if (json_object_object_get_ex(jconf, "hops", &jhops)
      && json_object_is_type(jhops, json_type_int))
    opts->hops = json_object_get_int(jhops);

  /* parse passphrase */
  if (json_object_object_get_ex(jconf, "passphrase", &jpassphrase)
      && json_object_is_type(jpassphrase, json_type_string))
    opts->passphrase = strdup(json_object_get_string(jpassphrase));

  /* parse me xAAL address (uuid) */
  if (json_object_object_get_ex(jconf, "uuid", &juuid)
      && json_object_is_type(juuid, json_type_string))
    if ( uuid_parse(json_object_get_string(juuid), me.addr) == -1 )
      uuid_clear(me.addr);

  /* parse config file name  */
  if (json_object_object_get_ex(jconf, "conffile", &jconffile)
      && json_object_is_type(jconffile, json_type_string))
    opts->conffile = strdup(json_object_get_string(jconffile));

  /* parse immutable flag  */
  if (json_object_object_get_ex(jconf, "immutable", &jimmutable)
      && json_object_is_type(jimmutable, json_type_boolean))
    opts->immutable = json_object_get_boolean(jimmutable);

  /* parse log file name  */
  if (json_object_object_get_ex(jconf, "logfile", &jlogfile)
      && json_object_is_type(jlogfile, json_type_string))
    opts->logfile = strdup(json_object_get_string(jlogfile));

  json_object_put(jconf);
}


/* Re-write config file (json format) */
void write_config(options_t *opts) {
  struct json_object *jconf;
  char uuid[37];

  jconf = json_object_new_object();
  jconf = json_object_new_object();
  json_object_object_add(jconf, "addr",      json_object_new_string(opts->addr));
  json_object_object_add(jconf, "port",      json_object_new_string(opts->port));
  json_object_object_add(jconf, "hops",      json_object_new_int(opts->hops));
  json_object_object_add(jconf, "passphrase",json_object_new_string(opts->passphrase));
  uuid_unparse(me.addr, uuid);
  json_object_object_add(jconf, "uuid",      json_object_new_string(uuid));
  json_object_object_add(jconf, "conffile",  json_object_new_string(opts->conffile));
  json_object_object_add(jconf, "immutable", json_object_new_boolean(opts->immutable));
  if (opts->logfile)
    json_object_object_add(jconf, "logfile", json_object_new_string(opts->logfile));

  if (json_object_to_file_ext(opts->conffile, jconf, JSON_C_TO_STRING_PRETTY
			      | JSON_C_TO_STRING_SPACED) == -1)
    fprintf(xAAL_error_log, "Writing config file: %s\n", strerror(errno));

  json_object_put(jconf);
}



/***********************/
/* Chapter: dump cache */
/***********************/

/* Manage silent stdin */
struct termios initial_term;

void setsilent() {
  struct termios term;
  if ( tcgetattr(STDIN_FILENO, &initial_term) == -1) {
    perror("tcgetattr");
    return;
  }
  term = initial_term;
  term.c_lflag &= ~(ICANON | ECHO);
  term.c_cc[VMIN] = 1;
  term.c_cc[VTIME] = 0;
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &term) == -1)
    perror("tcsetattr");
}

void restore_term() {
  if ( tcsetattr(STDIN_FILENO, TCSANOW, &initial_term) == -1)
    perror("tcsetattr");
}


void dump_cache(devices_t *devices) {
  char c;
  device_t *device;
  attr_t *attribute;
  char uuid[37];

  switch ( read(STDIN_FILENO, &c, 1) ) {
  case -1:
    fprintf(xAAL_error_log, "read/stdin: %s\n", strerror(errno));
    break;
  case 0:
    exit(EXIT_SUCCESS);
    break;
  }

  LIST_FOREACH(device, devices, entries) {
    uuid_unparse(device->uuid, uuid);
    printf("\nDevice %s %s timeout: %s", uuid, device->dev_type, ctime(&(device->timeout)));
    LIST_FOREACH(attribute, &(device->attributes), entries) {
      printf("  %s: ", attribute->name);
      cbor_describe(attribute->val, stdout);
      printf("\n");
    }
  }
  printf("\n--\n");
}


/************************************/
/* Chapter: Main of the application */
/************************************/

/* Global variable for the handler */
options_t *opts;

/* Called at exit */
void terminate() {
  if (!opts->immutable)
    write_config(opts);
}

/* Handler for Ctrl-C &co. */
void cancel(int s) {
  terminate();
  exit(EXIT_SUCCESS);
}


/* Main */
int main(int argc, char **argv) {
  options_t options = { .addr=NULL, .port=NULL, .hops=-1, .passphrase=NULL,
			.conffile="cache.conf", .immutable=false,
			.logfile=NULL };
  devices_t devices;
  fd_set rfds, rfds_;

  LIST_INIT(&devices);
  uuid_clear(me.addr);

  /* Parse cmdline arguments */
  parse_cmdline(argc, argv, &options);

  /* Load config */
  read_config(&options);

  /* Manage logfile */
  if (options.logfile) {
    xAAL_error_log = fopen(options.logfile, "a");
    if (xAAL_error_log == NULL) {
      perror("Opening logfile");
      xAAL_error_log = stderr;
    }
  } else
    xAAL_error_log = stderr;

  /* Join the xAAL bus */
  if ( !options.addr || !options.port || !options.passphrase) {
    fprintf(xAAL_error_log, "Please provide the address, the port and the secret of the xAAL bus.\n");
    exit(EXIT_FAILURE);
  } else if ( !xAAL_join_bus(options.addr, options.port, options.hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(options.passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(me.addr) ) {
    char uuid[37];
    uuid_generate(me.addr);
    uuid_unparse(me.addr, uuid);
    printf("Device: %s\n", uuid);
  }
  xAAL_add_wanted_target(&me.addr, &bus);

  /* Setup device info */
  me.dev_type    = "cache.basic";
  me.alivemax   = 2 * ALIVE_PERIOD;
  me.vendor_id   = "IHSEV";
  me.product_id  = "Cache";
  me.hw_id	= NULL;
  me.version    = "0.3";
  me.group_id    = NULL;
  me.url        = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  me.schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/cache.basic";
  me.info	= NULL;
  me.unsupported_attributes = NULL;
  me.unsupported_methods = NULL;
  me.unsupported_notifications = NULL;

  /* Manage alive notifications */
  {
    struct sigaction act_alarm;

    act_alarm.sa_handler = alive_sender;
    act_alarm.sa_flags = ~SA_RESETHAND;
    sigemptyset(&act_alarm.sa_mask);
    sigaction(SIGALRM, &act_alarm, NULL);
    alarm(ALIVE_PERIOD);
    if ( !xAAL_notify_alive(&bus, &me) )
      fprintf(xAAL_error_log, "Could not send initial alive notification.\n");
  }

  if ( !request_is_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send is_alive request.\n");

  /* Manage Ctrl-C &co. */
  opts = &options;
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);
  atexit(terminate);

  /* Manage user interaction */
  setsilent();
  atexit(restore_term);
  printf("Press a key to display cache data.\n");

  FD_ZERO(&rfds);
  FD_SET(STDIN_FILENO, &rfds);
  FD_SET(bus.sfd, &rfds);

  /* main loop */
  for (;;) {

    rfds_ = rfds;
    if ( (select(bus.sfd+1, &rfds_, NULL, NULL, NULL) == -1) && (errno != EINTR) )
      fprintf(xAAL_error_log, "select(): %s\n", strerror(errno));

    if (FD_ISSET(STDIN_FILENO, &rfds_))
      dump_cache(&devices);

    else if (FD_ISSET(bus.sfd, &rfds_))
      manage_msg(&bus, &me, &devices);
  }
}
