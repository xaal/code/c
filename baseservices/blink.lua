--
-- Blink - Basic automaton to make a lamp blinking
--

--[[ -- Just for debuging
print("-- blink.lua --")
print("xAAL_Lua_baseaddr: "..xAAL_Lua_uuid_unparse(xAAL_Lua_baseaddr))
print("xAAL_Lua_group_id: "..xAAL_Lua_uuid_unparse(xAAL_Lua_group_id))
print("xAAL_Lua_parameter: "..xAAL_Lua_parameter)
--]]


device = { info="Blinker", dev_type="scenario.basic",
	   vendor_id="IHSEV Team", product_id="blinking automaton", version="0.3",
	   url="http://recherche.imt-atlantique.fr/xaal/documentation/",
	   schema="https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/scenario.basic",
	   unsupported_attributes={"properties"}, unsupported_methods={"run","abort"},
	   unsupported_notifications={} }
device["addr"] = xAAL_Lua_declare_device(device)

is_enabled = true


-- Assume xAAL_Lua_parameter contains a table named conf
-- with a period in seconds and a lamp address
assert(loadstring(xAAL_Lua_parameter))()
conf["lamp"] = xAAL_Lua_uuid_parse(conf["lamp"])


-- A filter accepting no one
xAAL_Lua_filter_broadcast_by_source(device["addr"], { xAAL_Lua_uuid_parse("00000000-0000-0000-0000-000000000000") })


-- Declare it, but empty (I do not expect messages)
function xAAL_Lua_receive_callback(dev, source, dev_type, msg_type, action, body)
  if ( dev == device["addr"] ) then
    if ( msg_type == 1 ) then
      if ( action == "enable" ) then
        is_enabled = true
        xAAL_Lua_set_alarm(conf["period"], device["addr"], "on")
      elseif ( action == "disable" ) then
        is_enabled = false
      elseif ( action == "get_attributes" ) then
        xAAL_Lua_write_bus(dev, 2, "get_attributes", { enabled=is_enabled }, { source } )
      end
    end
  end
end


-- Main of the job
function xAAL_Lua_alarm_callback(dev, parameter)
  if ( is_enabled ) then
    if ( parameter == "on" ) then
      xAAL_Lua_write_bus(dev, 1, "turn_on", {}, { conf["lamp"] } )
      xAAL_Lua_set_alarm(conf["period"], dev, "off")
    else
      xAAL_Lua_write_bus(dev, 1, "turn_off", {}, { conf["lamp"] } )
      xAAL_Lua_set_alarm(conf["period"], dev, "on")
    end
  end
end


-- Bootstrap
xAAL_Lua_set_alarm(conf["period"], device["addr"], "on")
