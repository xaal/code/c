/* Metadata DB - Basic database to manage tags associated with devices
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#include <sys/types.h>
#include <regex.h>
#include <sys/queue.h>
#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>
#include <sqlite3.h>

#include <xaal.h>

#define ALIVE_PERIOD    120


/*********************/
/* Chapter "sqlite3" */
/*********************/


bool create_tables(sqlite3 *db) {
  char *sql;
  char *zErrMsg = NULL;
  int rc;

  sql = "CREATE TABLE devices (\n"
	"  id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
	"  address TEXT UNIQUE );\n"
	"CREATE TABLE keys (\n"
	"  id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
	"  key TEXT UNIQUE );\n"
	"CREATE TABLE vals (\n"
	"  id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
	"  value TEXT UNIQUE );\n"
	"CREATE TABLE device_key_value (\n"
	"  id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
	"  device_id INT NOT NULL CONSTRAINT device_id_exists REFERENCES devices(id) ON DELETE CASCADE,\n"
	"  key_id INT NOT NULL CONSTRAINT key_id_exists REFERENCES keys(id) ON DELETE CASCADE,\n"
	"  value_id INT NOT NULL CONSTRAINT value_id_exists REFERENCES vals(id) ON DELETE CASCADE,\n"
	"  UNIQUE(device_id, key_id, value_id) );";
  rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error A %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool drop_tables(sqlite3 *db) {
  char *sql;
  char *zErrMsg = NULL;
  int rc;

  sql = "DROP TABLE devices;\n"
	"DROP TABLE keys;\n"
	"DROP TABLE vals;\n"
	"DROP TABLE device_key_value;";
  rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error B %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}



bool insert_item(sqlite3 *db, const char *tbl, const char *cel, const char *item) {
  char *query = "INSERT INTO %s ( %s ) VALUES ( '%s' );";
  char *sql;
  char *zErrMsg = NULL;
  int rc;

  sql = malloc(strlen(query)+strlen(tbl)+strlen(cel)+strlen(item)+1);
  sprintf(sql, query, tbl, cel, item);
  rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK && rc != SQLITE_CONSTRAINT) {
    fprintf(xAAL_error_log, "SQL error C %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool delete_item(sqlite3 *db, const char *tbl, const char *cel, const char *item) {
  char *query = "DELETE FROM %s WHERE %s = '%s' ;";
  char *sql;
  char *zErrMsg = NULL;
  int rc;

  sql = malloc(strlen(query)+strlen(tbl)+strlen(cel)+strlen(item)+1);
  sprintf(sql, query, tbl, cel, item);
  rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK && rc != SQLITE_CONSTRAINT) {
    fprintf(xAAL_error_log, "SQL error D %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool insert_device(sqlite3 *db, const uuid_t *addr) {
  char str[37];
  uuid_unparse(*addr, str);
  return insert_item(db, "devices", "address", str);
}


bool delete_device(sqlite3 *db, const uuid_t *addr) {
  char str[37];
  uuid_unparse(*addr, str);
  return delete_item(db, "devices", "address", str);
}


bool validate_str(const char *str) {
  static regex_t *preg = NULL;
  int r;

  if (!preg) {
    preg = (regex_t *)malloc(sizeof(regex_t));
    r = regcomp(preg, "^[^'\\\"]*$", REG_NOSUB); // safe but too restrictive, to be improved
    if ( r != 0 ) {
      preg = NULL;
      fprintf(xAAL_error_log, "Error regcomp: %d\n", r);
      return false;
    }
  }
  return regexec(preg, str, 0, NULL, 0) != REG_NOMATCH;
}


bool insert_key(sqlite3 *db, const char *key) {
  if ( !validate_str(key) )
    return false;
  else
    return insert_item(db, "keys", "key", key);
}


bool delete_key(sqlite3 *db, const char *key) {
  if ( !validate_str(key) )
     return false;
  else
    return delete_item(db, "keys", "key", key);
}


bool insert_value(sqlite3 *db, const char *value) {
  if ( !validate_str(value) )
     return false;
  else
    return insert_item(db, "vals", "value", value);
}


bool delete_value(sqlite3 *db, const char *value) {
  if ( !validate_str(value) )
     return false;
  else
    return delete_item(db, "vals", "value", value);
}


bool insert_device_kv(sqlite3 *db, const uuid_t *addr, const char *key, const char *value) {
  char *query = "INSERT INTO device_key_value ( device_id, key_id, value_id ) "
		"SELECT devices.id, keys.id, vals.id FROM devices JOIN keys JOIN vals "
		"WHERE devices.address = '%s' AND keys.key = '%s' AND vals.value = '%s';";
  char *sql;
  char *zErrMsg = NULL;
  int rc;
  char addr_s[37];

  if ( !validate_str(key) || !validate_str(value) )
    return false;
  uuid_unparse(*addr, addr_s);
  sql = malloc(strlen(query)+strlen(addr_s)+strlen(key)+strlen(value)+1);
  sprintf(sql, query, addr_s, key, value);
  rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK && rc != SQLITE_CONSTRAINT) {
    fprintf(xAAL_error_log, "SQL error E %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool delete_device_kv(sqlite3 *db, const uuid_t *addr, const char *key, const char *value) {
  char *query = "DELETE FROM device_key_value WHERE id IN "
		"( SELECT device_key_value.id FROM device_key_value JOIN devices JOIN keys JOIN vals "
		"  ON     device_key_value.device_id = devices.id "
		"     AND device_key_value.key_id = keys.id "
		"     AND device_key_value.value_id = vals.id "
		"  WHERE devices.address = '%s' AND keys.key = '%s' AND vals.value = '%s' ) ;";
  char *sql;
  char *zErrMsg = NULL;
  int rc;
  char addr_s[37];

  if ( !validate_str(key) || !validate_str(value) )
    return false;
  uuid_unparse(*addr, addr_s);
  sql = malloc(strlen(query)+strlen(addr_s)+strlen(key)+strlen(value)+1);
  sprintf(sql, query, addr_s, key, value);
  rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK && rc != SQLITE_CONSTRAINT) {
    fprintf(xAAL_error_log, "SQL error F %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}




static int callback_devices(void *data, int argc, char **argv, char **azColName){
  cbor_item_t **carray = data;
  uuid_t uuid;

  if ( argv[0] && (uuid_parse(argv[0], uuid) == 0) )
    (void)!cbor_array_push(*carray, cbor_move(xAAL_new_tagged_uuid(&uuid)));
  return 0;
}


static int callback_map(void *data, int argc, char **argv, char **azColName){
  cbor_item_t **cmap = data;

  if (argv[0] && argv[1])
    (void)!cbor_map_add(*cmap, (struct cbor_pair){ cbor_move(cbor_build_string(argv[0])), cbor_move(cbor_build_string(argv[1])) });
  return 0;
}


static int callback_value(void *data, int argc, char **argv, char **azColName){
  char **value = data;
  if (argv[0]) {
    free(*value);
    *value = strdup(argv[0]);
  }
  return 0;
}


bool select_keys_values(sqlite3 *db, const uuid_t *addr,
			     cbor_item_t **cmap) {
  char *query = "SELECT key,value FROM device_key_value JOIN devices JOIN keys JOIN vals "
		"ON     device_key_value.device_id = devices.id "
		"   AND device_key_value.key_id = keys.id "
		"   AND device_key_value.value_id = vals.id "
		"WHERE devices.address = '%s' ;";
  char *sql;
  char *zErrMsg = NULL;
  int rc;
  char addr_s[37];

  uuid_unparse(*addr, addr_s);
  sql = malloc(strlen(query)+strlen(addr_s)+1);
  sprintf(sql, query, addr_s);
  *cmap = cbor_new_indefinite_map();
  rc = sqlite3_exec(db, sql, callback_map, cmap, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error G %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool select_value(sqlite3 *db, const uuid_t *addr, const char *key, char **value) {
  char *query = "SELECT value FROM device_key_value JOIN devices JOIN keys JOIN vals "
		"ON     device_key_value.device_id = devices.id "
		"   AND device_key_value.key_id = keys.id "
		"   AND device_key_value.value_id = vals.id "
		"WHERE devices.address = '%s' AND keys.key = '%s';";
  char *sql;
  char *zErrMsg = NULL;
  int rc;
  char addr_s[37];

  if ( !validate_str(key) )
    return false;
  uuid_unparse(*addr, addr_s);
  sql = malloc(strlen(query)+strlen(addr_s)+strlen(key)+1);
  sprintf(sql, query, addr_s, key);
  *value = NULL;
  rc = sqlite3_exec(db, sql, callback_value, value, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error H %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool select_devices_with_kv(sqlite3 *db, const char *key, const char *value, cbor_item_t **cdevices) {
  char *query = "SELECT address FROM device_key_value JOIN devices JOIN keys JOIN vals "
		"ON     device_key_value.device_id = devices.id "
		"   AND device_key_value.key_id = keys.id "
		"   AND device_key_value.value_id = vals.id "
		"WHERE keys.key = '%s' AND vals.value = '%s';";
  char *sql;
  char *zErrMsg = NULL;
  int rc;

  if ( !validate_str(key) || !validate_str(value) )
    return false;
  sql = malloc(strlen(query)+strlen(key)+strlen(value)+1);
  sprintf(sql, query, key, value);
  *cdevices = cbor_new_indefinite_array();
  rc = sqlite3_exec(db, sql, callback_devices, cdevices, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error I %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}



bool select_devices_with_key(sqlite3 *db, const char *key, cbor_item_t **cdevices) {
  char *query = "SELECT address FROM device_key_value JOIN devices JOIN keys "
		"ON     device_key_value.device_id = devices.id "
		"   AND device_key_value.key_id = keys.id "
		"WHERE keys.key = '%s';";
  char *sql;
  char *zErrMsg = NULL;
  int rc;

  if ( !validate_str(key) )
    return false;
  sql = malloc(strlen(query)+strlen(key)+1);
  sprintf(sql, query, key);
  *cdevices = cbor_new_indefinite_array();
  rc = sqlite3_exec(db, sql, callback_devices, cdevices, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error J %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool select_devices_with_value(sqlite3 *db, const char *value, cbor_item_t **cdevices) {
  char *query = "SELECT address FROM device_key_value JOIN devices JOIN vals "
		"ON     device_key_value.device_id = devices.id "
		"   AND device_key_value.value_id = vals.id "
		"WHERE vals.value = '%s';";
  char *sql;
  char *zErrMsg = NULL;
  int rc;

  if ( !validate_str(value) )
    return false;
  sql = malloc(strlen(query)+strlen(value)+1);
  sprintf(sql, query, value);
  *cdevices = cbor_new_indefinite_array();
  rc = sqlite3_exec(db, sql, callback_devices, cdevices, &zErrMsg);
  free(sql);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error K %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool select_devices(sqlite3 *db, cbor_item_t **cdevices) {
  char *sql = "SELECT address FROM devices ;";
  char *zErrMsg = NULL;
  int rc;

  *cdevices = cbor_new_indefinite_array();
  rc = sqlite3_exec(db, sql, callback_devices, cdevices, &zErrMsg);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error L %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}


bool delete_unused(sqlite3 *db) {
  char *sql = "DELETE FROM devices WHERE id NOT IN (SELECT device_id FROM device_key_value);"
	      "DELETE FROM keys WHERE id NOT IN (SELECT device_id FROM device_key_value);"
	      "DELETE FROM valse WHERE id NOT IN (SELECT device_id FROM device_key_value);";
  char *zErrMsg = NULL;
  int rc;

  rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);
  if (rc != SQLITE_OK) {
    fprintf(xAAL_error_log, "SQL error L %d: %s\n", rc, zErrMsg);
    sqlite3_free(zErrMsg);
    return false;
  }
  return true;
}



/******************/
/* Chapter "xAAL" */
/******************/

/* Global variable for the handler */
xAAL_businfo_t bus;
xAAL_devinfo_t me;

void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &me) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* Reply to get_attributes (empty answer, for now) */
bool reply_get_attributes(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
			 const uuid_t *target) {
  return xAAL_write_busl(bus, me, xAAL_REPLY, "get_attributes", NULL, target, NULL);
}


/* Reply to get_devices */
bool reply_get_devices(const xAAL_businfo_t *bus,
		      const xAAL_devinfo_t *me,
		      const uuid_t *target,
		      cbor_item_t *cqueryBody,
		      sqlite3 *db) {
  cbor_item_t *cdevs, *ckey, *cvalue, *cbody;
  char *key, *value;
  size_t len;
  bool r;

  ckey = xAAL_cbor_map_get(cqueryBody, "key");
  key = xAAL_cbor_string_dup(ckey, &len);

  cvalue = xAAL_cbor_map_get(cqueryBody, "value");
  value = xAAL_cbor_string_dup(cvalue, &len);

  if (key && value)
    r = select_devices_with_kv(db, key, value, &cdevs);
  else if (key && !value)
    r = select_devices_with_key(db, key, &cdevs);
  else if (!key && value)
    r = select_devices_with_value(db, value, &cdevs);
  else
    r = select_devices(db, &cdevs);
  free(key);
  free(value);

  if (!r) {
    cbor_decref(&cdevs);
    return false;
  }

  len = 1;
  if (ckey) len++;
  if (cvalue) len++;
  cbody = cbor_new_definite_map(len);

  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("devices")), cbor_move(cdevs) });
  if (ckey) (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("key")), ckey });
  if (cvalue) (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("value")), cvalue });
  return xAAL_write_busl(bus, me, xAAL_REPLY, "get_devices", cbody, target, NULL);
}


/* Reply to get_keys_values */
bool reply_get_keys_values(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *me,
			 const uuid_t *target,
			 cbor_item_t *cqueryBody,
			 sqlite3 *db) {
  cbor_item_t *cdev, *cmap, *cbody, *ckeys;
  uuid_t device;

  cdev = xAAL_cbor_map_get(cqueryBody, "device");
  if (!xAAL_cbor_is_uuid(cdev, &device) )
    return false;

  ckeys = xAAL_cbor_map_get(cqueryBody, "keys");
  if ( ckeys && cbor_isa_array(ckeys) ) {
    char *key, *value;
    size_t len, i, sz = cbor_array_size(ckeys);
    cmap = cbor_new_indefinite_map();
    for (i=0; i < sz; i++) {
      key = xAAL_cbor_string_dup(cbor_move(cbor_array_get(ckeys, i)), &len);
      if ( key ) {
       if ( select_value(db, &device, key, &value) && value ) {
	 (void)!cbor_map_add(cmap, (struct cbor_pair){ cbor_move(cbor_build_string(key)), cbor_move(cbor_build_string(value)) });
	 free(value);
       }
       free(key);
      }
    }

  } else if (!select_keys_values(db, &device, &cmap)) {
    cbor_decref(&cmap);
    return false;
  }

  if (cmap && cbor_map_size(cmap)) {
    cbody = cbor_new_definite_map(2);
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(xAAL_new_tagged_uuid(&device)) });
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("map")), cbor_move(cmap) });
    return xAAL_write_busl(bus, me, xAAL_REPLY, "get_keys_values", cbody, target, NULL);
  } else {
    cbor_decref(&cmap);
    return true;
  }
}


/* Reply to get_value */
bool reply_get_value(const xAAL_businfo_t *bus,
		    const xAAL_devinfo_t *me,
		    const uuid_t *target,
		    cbor_item_t *cqueryBody,
		    sqlite3 *db) {
  cbor_item_t *cdev, *ckey, *cbody;
  char *key, *value;
  uuid_t device;
  size_t len;

  cdev = xAAL_cbor_map_get(cqueryBody, "device");
  if ( !xAAL_cbor_is_uuid(cdev, &device) )
    return false;

  ckey = xAAL_cbor_map_get(cqueryBody, "key");
  if ( !ckey )
    return false;
  key = xAAL_cbor_string_dup(ckey, &len);

  if (!select_value(db, &device, key, &value)) {
    free(key);
    return false;
  }
  free(key);

  cbody = cbor_new_definite_map(3);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(xAAL_new_tagged_uuid(&device)) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("key")), ckey });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("value")), cbor_move(cbor_build_string(value)) });
  return xAAL_write_busl(bus, me, xAAL_REPLY, "get_value", cbody, target, NULL);
}


/* Send keys_values_changed notification */
bool send_keys_values_changed(const xAAL_businfo_t *bus,
			    const xAAL_devinfo_t *me,
			    cbor_item_t *cdev,
			    cbor_item_t *cmap) {
  cbor_item_t *cbody = cbor_new_definite_map(2);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(cdev) });
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("map")), cbor_move(cmap) });
  return xAAL_write_bus(bus, me, xAAL_NOTIFY, "keys_values_changed", cbody, NULL);
}



// DEPRECATED - merged with update_keys_values()
// /* Manage add_keys_values */
// bool do_add_keys_values(const xAAL_businfo_t *bus,
// 		      const xAAL_devinfo_t *me,
// 		      cbor_item_t *cqueryBody,
// 		      sqlite3 *db) {
//   cbor_item_t *cdev, *cmap, *cchangedKV;
//   char *key, *value, *existing_value;
//   uuid_t device;
//   size_t len, sz, i;
//   struct cbor_pair *pairs;
//   bool ok;
// 
//   cdev = xAAL_cbor_map_get(cqueryBody, "device");
//   if ( !xAAL_cbor_is_uuid(cdev, &device) )
//     return false;
// 
//   cmap = xAAL_cbor_map_get(cqueryBody, "map");
//   if ( !cmap || !cbor_isa_map(cmap) )
//     return false;
// 
//   insert_device(db, &device);
// 
//   cchangedKV = cbor_new_indefinite_map();
// 
//   pairs = cbor_map_handle(cmap);
//   sz = cbor_map_size(cmap);
//   for (i=0; i < sz; i++) {
// 
//     key = xAAL_cbor_string_dup(pairs[i].key, &len);
//     if ( !key )
//       continue;
// 
//     if (!select_value(db, &device, key, &existing_value)) {
//       free(key);
//       continue;
//     }
// 
//     if (existing_value) {
//       free(key);
//       free(existing_value);
//       continue;
//     }
// 
//     value = xAAL_cbor_string_dup(pairs[i].value, &len);
//     if ( !value ) {
//       free(key);
//       continue;
//     }
// 
//     ok = insert_key(db, key) && insert_value(db, value) && insert_device_kv(db, &device, key, value);
//     free(key);
//     free(value);
// 
//     if (ok)
//       cbor_map_add(cchangedKV, pairs[i]);
//   }
// 
//   if ( cbor_map_size(cchangedKV) )
//     send_keys_values_changed(bus, me, cbor_incref(cdev), cchangedKV);
//   else
//     cbor_decref(&cchangedKV);
// 
//   return true;
// }


/* Manage update_keys_values */
bool do_update_keys_values(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *me,
			 cbor_item_t *cqueryBody,
			 sqlite3 *db) {
  cbor_item_t *cdev, *cmap, *cchangedKV;
  char *key, *value, *existing_value;
  uuid_t device;
  size_t sz, len, i;
  struct cbor_pair *pairs;
  bool ok;

  cdev = xAAL_cbor_map_get(cqueryBody, "device");
  if ( !xAAL_cbor_is_uuid(cdev, &device) )
    return false;

  cmap = xAAL_cbor_map_get(cqueryBody, "map");
  if ( !cmap )
    return false;

  insert_device(db, &device);

  if ( cbor_is_null(cmap) ) {
    if (select_keys_values(db, &device, &cchangedKV)) {
      pairs = cbor_map_handle(cchangedKV);
      sz = cbor_map_size(cchangedKV);

      for (i=0; i < sz; i++) {
	cbor_decref(&pairs[i].value);
	pairs[i].value = cbor_new_null();
      }
      if (sz)
	send_keys_values_changed(bus, me, cbor_incref(cdev), cchangedKV);
      else
	cbor_decref(&cchangedKV);
    }
    return delete_device(db, &device);
  }

  if ( cbor_isa_map(cmap) ) {
    cchangedKV = cbor_new_indefinite_map();
    pairs = cbor_map_handle(cmap);
    sz = cbor_map_size(cmap);

    for (i=0; i < sz; i++) {
      key = xAAL_cbor_string_dup(pairs[i].key, &len);

      if ( !select_value(db, &device, key, &existing_value) ) {
	free(key);
	continue;
      }

      value = xAAL_cbor_string_dup(pairs[i].value, &len);

      if ( !value && !existing_value ) {
	free(key);
	continue;
      }

      if (!value && existing_value) {
	ok = delete_device_kv(db, &device, key, existing_value);
	free(key);
	free(existing_value);
	if (ok)
	  (void)!cbor_map_add(cchangedKV, (struct cbor_pair){ pairs[i].key, cbor_move(cbor_new_null()) });
	continue;
      }

      if (value && !existing_value) {
	ok = insert_key(db, key) && insert_value(db, value) && insert_device_kv(db, &device, key, value);
	free(key);
	free(value);
	if (ok)
	  (void)!cbor_map_add(cchangedKV, pairs[i]);
	continue;
      }

      if (value && existing_value) {
	if ( strcmp(value, existing_value) == 0 ) {
	  free(key);
	  free(value);
	  free(existing_value);
	  continue;

	} else {
	  ok = delete_device_kv(db, &device, key, existing_value);
	  free(existing_value);
	  if (!ok) {
	    free(key);
	    free(value);
	    continue;
	  }
	  ok = insert_key(db, key) && insert_value(db, value) && insert_device_kv(db, &device, key, value);
	  free(key);
	  free(value);
	  if (ok)
	    (void)!cbor_map_add(cchangedKV, pairs[i]);
	  continue;
	}
      }
    }

    if ( cbor_map_size(cchangedKV) )
      send_keys_values_changed(bus, me, cbor_move(xAAL_new_tagged_uuid(&device)), cchangedKV);
    else
      cbor_decref(&cchangedKV);
  }
  return true;
}





/* Manage received message */
void manage_msg(const xAAL_businfo_t *bus, const xAAL_devinfo_t *me,
		sqlite3 *db) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (xAAL_targets_match(ctargets, &me->addr)) {

    if (msg_type == xAAL_REQUEST) {

      if ( (strcmp(action, "is_alive") == 0)
	   && xAAL_is_aliveDevType_match(cbody, me->dev_type) ) {
	if ( !xAAL_notify_alive(bus, me) )
	  fprintf(xAAL_error_log, "Could not reply to is_alive\n");

      } else if ( strcmp(action, "get_description") == 0 ) {
	if ( !xAAL_reply_get_description(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_description\n");

      } else if ( strcmp(action, "get_attributes") == 0 ) {
	if ( !reply_get_attributes(bus, me, source) )
	  fprintf(xAAL_error_log, "Could not reply to get_attributes\n");

      } else if ( strcmp(action, "get_devices") == 0 ) {
	if ( !reply_get_devices(bus, me, source, cbody, db) )
	  fprintf(xAAL_error_log, "Could not reply to get_devices\n");

      } else if ( strcmp(action, "get_keys_values") == 0 ) {
	if ( !reply_get_keys_values(bus, me, source, cbody, db) )
	  fprintf(xAAL_error_log, "Could not reply to get_keys_values\n");

      } else if ( strcmp(action, "get_value") == 0 ) {
	if ( !reply_get_value(bus, me, source, cbody, db) )
	  fprintf(xAAL_error_log, "Could not reply to get_value\n");

//    } else if ( strcmp(action, "add_keys_values") == 0 ) {
//	if ( !do_add_keys_values(bus, me, cbody, db) )
//	  fprintf(xAAL_error_log, "Could not do add_keys_values\n");

      } else if ( strcmp(action, "update_keys_values") == 0 ) {
	if ( !do_update_keys_values(bus, me, cbody, db) )
	  fprintf(xAAL_error_log, "Could not do update_keys_values\n");

      }

    }
  }

  /* Spy bus about is_alive and insert dev_type as key-value */
  if ( (msg_type == xAAL_NOTIFY) && (strcmp(action, "alive") == 0) ) {
    insert_device(db, source);
    insert_key(db, "dev_type");
    insert_value(db, dev_type);
    insert_device_kv(db, source, "dev_type", dev_type);
  }

  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/*************************/
/* Chapter "User Option" */
/*************************/

/* Options from cmdline or conffile */
typedef struct {
  char *addr;
  char *port;
  int hops;
  char *passphrase;
  char *conffile;
  bool immutable;
  bool daemon;
  char *logfile;
  char *pidfile;
  char *dbfile;
} options_t;


/* Parse cmdline */
void parse_cmdline(int argc, char **argv, options_t *opts) {
  int opt;
  bool arg_error = false;

  while ((opt = getopt(argc, argv, "a:p:h:s:u:c:idl:P:f:")) != -1) {
    switch (opt) {
      case 'a':
	opts->addr = optarg;
	break;
      case 'p':
	opts->port = optarg;
	break;
      case 'h':
	opts->hops = atoi(optarg);
	break;
      case 's':
	opts->passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, me.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(me.addr);
	}
	break;
      case 'c':
	opts->conffile = optarg;
	break;
      case 'i':
	opts->immutable = true;
	break;
      case 'd':
	opts->daemon = true;
	break;
      case 'l':
	opts->logfile = optarg;
	break;
      case 'P':
	opts->pidfile = optarg;
	break;
      case 'f':
	opts->dbfile = optarg;
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (arg_error) {
    fprintf(stderr, "Usage: %s [-a <addr>] [-p <port>] [-h <hops>] [-s <secret>] [-u <uuid>]\n"
		"		[-c <conffile>] [-d] [-l <logfile>] [-P <pidfile>]\n"
		"		[-f <dbfile>]\n"
		"-a <addr>	multicast IPv4 or IPv6 address of the xAAL bus\n"
		"-p <port>	UDP port of the xAAL bus\n"
		"-h <hops>	Hops limit for multicast packets\n"
		"-s <secret>	Secret passphrase\n"
		"-u <uuid>	xAAL address of the device; random by default\n"
		"-c <conffile>	Filename of the configuration file (cson format)\n"
		"		Use 'metadatadb.conf' by default\n"
		"-i		Immutable config file (do not re-write it)\n"
		"-d		Start as a daemon\n"
		"-l <logfile>	Filename to write errors; stderr by default\n"
		"-P <pidfile>	Filename to write pid; none by default\n"
		"-f <dbfile>	Filename of the sqlite3 database\n"
		, argv[0]);
    exit(EXIT_FAILURE);
  }

}


/* Read a config file (json format) */
void read_config(options_t *opts) {
  struct json_object *jconf, *jaddr, *jport, *jhops, *jpassphrase, *juuid,
		     *jconffile, *jimmutable, *jdaemon, *jlogfile, *jpidfile,
		     *jdb;

  /* read file */
  jconf = json_object_from_file(opts->conffile);
  if (json_object_is_type(jconf, json_type_null)) {
    fprintf(stderr, "Could not parse config file %s\n", opts->conffile);
    return;
  }

  /* parse bus addr */
  if (json_object_object_get_ex(jconf, "addr", &jaddr)
      && json_object_is_type(jaddr, json_type_string))
    opts->addr = strdup(json_object_get_string(jaddr));

  /* parse bus port */
  if (json_object_object_get_ex(jconf, "port", &jport)
      && json_object_is_type(jport, json_type_string))
    opts->port = strdup(json_object_get_string(jport));

  /* parse bus hops */
  if (json_object_object_get_ex(jconf, "hops", &jhops)
      && json_object_is_type(jhops, json_type_int))
    opts->hops = json_object_get_int(jhops);

  /* parse passphrase */
  if (json_object_object_get_ex(jconf, "passphrase", &jpassphrase)
      && json_object_is_type(jpassphrase, json_type_string))
    opts->passphrase = strdup(json_object_get_string(jpassphrase));

  /* parse my xAAL address (uuid) */
  if (json_object_object_get_ex(jconf, "uuid", &juuid)
      && json_object_is_type(juuid, json_type_string))
    if ( uuid_parse(json_object_get_string(juuid), me.addr) == -1 )
      uuid_clear(me.addr);

  /* parse config file name  */
  if (json_object_object_get_ex(jconf, "conffile", &jconffile)
      && json_object_is_type(jconffile, json_type_string))
    opts->conffile = strdup(json_object_get_string(jconffile));

  /* parse immutable flag  */
  if (json_object_object_get_ex(jconf, "immutable", &jimmutable)
      && json_object_is_type(jimmutable, json_type_boolean))
    opts->immutable = json_object_get_boolean(jimmutable);

  /* parse daemon flag  */
  if (json_object_object_get_ex(jconf, "daemon", &jdaemon)
      && json_object_is_type(jdaemon, json_type_boolean))
    opts->daemon = json_object_get_boolean(jdaemon);

  /* parse pid file name  */
  if (json_object_object_get_ex(jconf, "pidfile", &jpidfile)
      && json_object_is_type(jpidfile, json_type_string))
    opts->pidfile = strdup(json_object_get_string(jpidfile));

  /* parse log file name  */
  if (json_object_object_get_ex(jconf, "logfile", &jlogfile)
      && json_object_is_type(jlogfile, json_type_string))
    opts->logfile = strdup(json_object_get_string(jlogfile));

  /* parse db file name */
  if (json_object_object_get_ex(jconf, "db", &jdb)
      && json_object_is_type(jdb, json_type_string))
    opts->dbfile = strdup(json_object_get_string(jdb));

  json_object_put(jconf);
}


/* Re-write config file (json format) */
void write_config(options_t *opts) {
  struct json_object *jconf;
  char uuid[37];

  jconf = json_object_new_object();
  jconf = json_object_new_object();
  json_object_object_add(jconf, "addr",      json_object_new_string(opts->addr));
  json_object_object_add(jconf, "port",      json_object_new_string(opts->port));
  json_object_object_add(jconf, "hops",      json_object_new_int(opts->hops));
  json_object_object_add(jconf, "passphrase",json_object_new_string(opts->passphrase));
  uuid_unparse(me.addr,  uuid);
  json_object_object_add(jconf, "uuid",      json_object_new_string(uuid));
  json_object_object_add(jconf, "conffile",  json_object_new_string(opts->conffile));
  json_object_object_add(jconf, "immutable", json_object_new_boolean(opts->immutable));
  json_object_object_add(jconf, "daemon",    json_object_new_boolean(opts->daemon));
  if (opts->logfile)
    json_object_object_add(jconf, "logfile", json_object_new_string(opts->logfile));
  if (opts->pidfile)
    json_object_object_add(jconf, "pidfile", json_object_new_string(opts->pidfile));
  json_object_object_add(jconf, "db",        json_object_new_string(opts->dbfile));

  if (json_object_to_file_ext(opts->conffile, jconf, JSON_C_TO_STRING_PRETTY
			      | JSON_C_TO_STRING_SPACED) == -1)
    fprintf(xAAL_error_log, "Writing config file: %s\n", strerror(errno));

  json_object_put(jconf);
}



/*************************************/
/* Chapter "Main of the application" */
/*************************************/

/* Global variable for the handler */
options_t *opts;
sqlite3 *db;

/* Called at exit */
void terminate() {
  if (!opts->immutable)
    write_config(opts);
  if (opts->pidfile)
    unlink(opts->pidfile);
  sqlite3_close(db);
}

/* Handler for Ctrl-C &co. */
void cancel(int s) {
  terminate();
  exit(EXIT_SUCCESS);
}


/* Main */
int main(int argc, char **argv) {
  options_t options = { .addr=NULL, .port=NULL, .hops=-1, .passphrase=NULL,
			.conffile="metadatadb.conf",
			.immutable=false, .daemon=false, .logfile=NULL,
			.pidfile=NULL, .dbfile="metadata.db" };

  /* Parse cmdline arguments */
  uuid_clear(me.addr);
  parse_cmdline(argc, argv, &options);

  /* Load config */
  read_config(&options);

  /* Open database */
  {
    int rc;
    bool do_create_tables = false;
    if ( access(options.dbfile, F_OK|R_OK|W_OK) == -1 ) {
      perror("Database file");
      do_create_tables = true;
    }
    rc = sqlite3_open(options.dbfile, &db);
    if (rc) {
      fprintf(stderr, "Could not open database: %s\n", sqlite3_errmsg(db));
      exit(EXIT_FAILURE);
    }
    if (do_create_tables) {
      xAAL_error_log = stderr;
      if (!create_tables(db))
	exit(EXIT_FAILURE);
    }
  }

  /* Manage logfile */
  if (options.logfile) {
    xAAL_error_log = fopen(options.logfile, "a");
    if (xAAL_error_log == NULL) {
      perror("Opening logfile");
      xAAL_error_log = stderr;
    }
  } else
    xAAL_error_log = stderr;

  /* Join the xAAL bus */
  if ( !options.addr || !options.port || !options.passphrase) {
    fprintf(xAAL_error_log, "Please provide the address, the port and the secret of the xAAL bus.\n");
    exit(EXIT_FAILURE);
  } else if ( !xAAL_join_bus(options.addr, options.port, options.hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(options.passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(me.addr) ) {
    char str[37];
    uuid_generate(me.addr);
    uuid_unparse(me.addr, str);
    printf("Device: %s\n", str);
  }
  // xAAL_add_wanted_target(&options.addr, &bus); // Listen to everybody

  /* Setup device info */
  me.dev_type    = "metadatadb.basic";
  me.alivemax   = 2 * ALIVE_PERIOD;
  me.vendor_id   = "IHSEV";
  me.product_id  = "Metatdata DB";
  me.hw_id	= NULL;
  me.version    = "0.4";
  me.group_id    = NULL;
  me.url        = "http://recherche.imt-atlantique.fr/xaal/documentation/";
  me.schema	= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/metadatadb.basic";
  me.info	= NULL;
  me.unsupported_attributes = NULL;
  me.unsupported_methods = NULL;
  me.unsupported_notifications = NULL;

  /* Start as a daemon */
  if (options.daemon && (daemon(1,1) == -1) )
    fprintf(xAAL_error_log, "daemon: %s\n", strerror(errno));

  /* Write pidfile */
  {
    FILE *pfile;

    if (options.pidfile) {
      pfile = fopen(options.pidfile, "w");
      if (pfile == NULL)
	fprintf(xAAL_error_log, "Opening pidfile: %s\n", strerror(errno));
      else {
	fprintf(pfile, "%d\n", getpid());
	fclose(pfile);
      }
    }
  }

  /* Manage alive notifications */
  {
    struct sigaction act_alarm;

    act_alarm.sa_handler = alive_sender;
    act_alarm.sa_flags = ~SA_RESETHAND;
    sigemptyset(&act_alarm.sa_mask);
    sigaction(SIGALRM, &act_alarm, NULL);
    alarm(ALIVE_PERIOD);
    if ( !xAAL_notify_alive(&bus, &me) )
      fprintf(xAAL_error_log, "Could not send initial alive notification.\n");
  }

  /* Manage Ctrl-C &co. */
  opts = &options;
  signal(SIGHUP,  cancel);
  signal(SIGINT,  cancel);
  signal(SIGQUIT, cancel);
  signal(SIGTERM, cancel);
  atexit(terminate);

  /* main loop */
  for (;;)
    manage_msg(&bus, &me, db);
}
