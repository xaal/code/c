/* xAAL dummy app to update a key-value on metadata DB
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <json-c/json.h>
#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>


#define ALIVE_PERIOD    60



bool send_attribute(const xAAL_businfo_t *bus,
			 const xAAL_devinfo_t *device,
			 cbor_item_t *attribute) {
  cbor_item_t *cbody;

  cbody = cbor_incref(attribute);
  return xAAL_write_bus(bus, device, xAAL_NOTIFY, "attributes_change", cbody, NULL);
}




/* main */
int main(int argc, char **argv) {
  xAAL_devinfo_t me = { .dev_type	= "basic.basic",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "IHSEV",
			.product_id	= "Dummy device to update a key-value on DB",
			.version	= "0.2",
			.hw_id		= NULL,
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.schema		= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/basic.basic",
			.info		= NULL,
			.unsupported_attributes = NULL,
			.unsupported_methods = NULL,
			.unsupported_notifications = NULL
		      };

  xAAL_businfo_t bus;
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  char *key = NULL, *value = NULL, *device = NULL;
  cbor_item_t *cbody, *cmap;
  uuid_t uuid;

  xAAL_error_log = stderr;

  uuid_clear(me.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, me.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(me.addr);
	}
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>] [-P <pipe name>] [-D <dev_type>]\n"
		    "It reads its standard input. Each line should be on the form '<device> <key> <value>'.\n"
		    "Then it sends broadcast update_keys_values() requests.\n"
		    "Please use this only for debbuging purpose.\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(me.addr) ) {
    char str[37];
    uuid_generate(me.addr);
    uuid_unparse(me.addr, str);
    printf("Device: %s\n", str);
  }
  xAAL_add_wanted_target(&me.addr, &bus);


  /* Loop on stdin */
  while (scanf("%ms %ms %ms", &device, &key, &value) == 3) {
    cbody = cbor_new_definite_map(2);
    if (uuid_parse(device, uuid) == -1)
      continue;
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("device")), cbor_move(xAAL_new_tagged_uuid(&uuid)) });

    cmap = cbor_new_definite_map(2);
    (void)!cbor_map_add(cmap, (struct cbor_pair){ cbor_move(cbor_build_string("key")), cbor_move(cbor_build_string(key)) });
    (void)!cbor_map_add(cmap, (struct cbor_pair){ cbor_move(cbor_build_string("value")), cbor_move(cbor_build_string(value)) });

    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("map")), cbor_move(cmap) });

    printf("%s: \"%s\" \"%s\":\"%s\"\n",
	   xAAL_write_bus(&bus, &me, xAAL_REQUEST, "update_keys_values", cbody, NULL) ? "OK " : "NOK",
	   device, key, value);
    free(key); free(value); free(device);
  }
  if (ferror(stdin)) {
    perror("Input");
    exit(EXIT_FAILURE);
  } else
    exit(EXIT_SUCCESS);
}
