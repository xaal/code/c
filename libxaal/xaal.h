/* libxaal
 * A minimal xAAL library, with security
 *
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LIBXAAL
#define LIBXAAL

#include <stdio.h>
#include <stdbool.h>
#include <netdb.h>
#include <time.h>
#include <uuid/uuid.h>
#include <cbor.h>

#define xAAL_LIB_VERSION	"0.9"

#define xAAL_BUF_SIZE   65536	/* Adjust it to your needs and capabilities */
#define xAAL_VERSION	7	/* Read this as 0.7 to follow former numbering scheme */

typedef enum {
 xAAL_NOTIFY	= 0,	/* Magic values coding message type */
 xAAL_REQUEST	= 1,	/**/
 xAAL_REPLY	= 2	/**/
} xAAL_msg_type_t;
#define xAAL_MSGTYPE_LAST	xAAL_REPLY


extern FILE *xAAL_error_log;


/*
 * Data structures
 */

typedef struct {
  int sfd;
  struct sockaddr_storage addr;
  socklen_t addrlen;
  int hops;
  time_t maxAge;
  unsigned char *key;
  uuid_t *wanted_targets;
  unsigned wanted_targets_nb;
} xAAL_businfo_t;


typedef struct {
  uuid_t addr;
  char *dev_type;
  unsigned alivemax;
  char *vendor_id;
  char *product_id;
  cbor_item_t *hw_id;
  uuid_t *group_id;
  char *version;
  char *url;
  char *schema;
  char *info;
  char **unsupported_attributes;
  char **unsupported_methods;
  char **unsupported_notifications;
} xAAL_devinfo_t;



/*
 * Misc Helpers for data structures
 */

/* Retrieve an item within a null-terminated list of strings */
/* May be used on unsupported{Attributes,Methods,Notifications} */
char **xAAL_strings_get(char **list, const char *item);

/* Pack a null terminated list of strings as a cbor array of strings */
cbor_item_t *xAAL_strings_pack(char **list);

/* Unpack a cbor array of strings as a null terminated list of strings */
char **xAAL_strings_unpack(cbor_item_t *carray);


/* Retrieve (position or -1) of an uuid within a vector of uuids */
/* May be used with wanted targets */
int xAAL_uuids_get_item(const uuid_t *uuids, unsigned nb, const uuid_t *uuid);


/* Helper to retreive a value within a cbor map, with a definite string key */
cbor_item_t *xAAL_cbor_map_get(const cbor_item_t *map, const char *key);

/* Helper to update (change or add) a value within a cbor map, with a definite string key */
void xAAL_cbor_map_update(cbor_item_t *map, const char *key, cbor_item_t *newval);

/* Helper to delete a value within a cbor map, with a definite string key */
void xAAL_cbor_map_del(cbor_item_t *map, const char *key);


/* Test if a cbor bitestream could be an uuid, and extract it */
bool xAAL_cbor_is_uuid(const cbor_item_t *item, uuid_t *dst);

/* Build a tagged cbor UUID */
cbor_item_t *xAAL_new_tagged_uuid(const uuid_t *uuid);

/* Build a cbor array from a vector of uuids */
cbor_item_t *xAAL_pack_uuids(const uuid_t *uuids, unsigned nb);

/* Build a cbor array from a variadic list of uuids */
cbor_item_t *xAAL_pack_luuids(const uuid_t *uuid0, ...);


/* Helper to build a cbor item according to given number value */
cbor_item_t *xAAL_cbor_build_int(long long v);

/* Helper to build a cbor item according to given float value */
cbor_item_t *xAAL_cbor_build_float(double v);

/* Helper to read a cbor boolean */
bool xAAL_cbor_get_bool(cbor_item_t *item);

/* Return a null-terminated allocated string copy of a (in)definite cbor string */
char *xAAL_cbor_string_dup(cbor_item_t *item, size_t *len);

/* Return a new allocated bytestring copy of a (in)definite cbor bytestring */
unsigned char *xAAL_cbor_bytestring_dup(cbor_item_t *item, size_t *len);

/* Jump cbor tag, if any */
cbor_item_t *xAAL_cbor_untag(cbor_item_t *item);


/*
 * Helpers for accessing the bus
 */


/* Join the xAAL bus
   Return 'true' if everything was ok, and 'false' otherwise. */
bool xAAL_join_bus(const char *addr,
		   const char *port,
		   int hops,
		   int mcast_loop,
		   xAAL_businfo_t *bus);


/* Add an uuid address to the list of wanted_targets */
bool xAAL_add_wanted_target(const uuid_t *uuid, xAAL_businfo_t *bus);

/* Del an uuid address to the list of wanted_targets */
bool xAAL_del_wanted_target(const uuid_t *uuid, xAAL_businfo_t *bus);


/* Compute a key from a pass-phrase
 * using crypto_pwhash_scryptsalsa208sha256
 * salt: buffer of zeros
 * opslimit: crypto_pwhash_scryptsalsa208sha256_OPSLIMIT_INTERACTIVE
 * memlimit: crypto_pwhash_scryptsalsa208sha256_MEMLIMIT_INTERACTIVE
 */
unsigned char *xAAL_pass2key(const char *passphrase);


/* Manage received message */
/* Read a message from the bus, parse it thanks to the cbor library,
   get parameters of the header, and the body if any.
   Return 'true' if everything was ok, and 'false' otherwise.
   Note: In case of success you should call xAAL_free_msg() after use. */
#ifdef XAAL_MORE_INFO
bool xAAL_read_bus(const xAAL_businfo_t *bus,
		   cbor_item_t **ctargets,
		   uuid_t **source,
		   char **dev_type,
		   xAAL_msg_type_t *msg_type,
		   char **action,
		   cbor_item_t **cbody,
		   ssize_t *msglen, char **ip);
#else
bool xAAL_read_bus(const xAAL_businfo_t *bus,
		   cbor_item_t **ctargets,
		   uuid_t **source,
		   char **dev_type,
		   xAAL_msg_type_t *msg_type,
		   char **action,
		   cbor_item_t **cbody);
#endif


/* Free data allocated by xAAL_read_bus() */
void xAAL_free_msg(cbor_item_t *ctargets,
		   uuid_t *source,
		   char *dev_type,
		   char *action,
		   cbor_item_t *cbody);


/* Send a message on the bus */
/* Helps devices to write a message on the bus.
   It build the header, add the provided cbor body if any, an send it.
   Targets is a cbor array of uuids (addresses).
   Return true if success
   Note: call cbor_decref(cbody) cbor_decref(ctargets)
   i.e. free corresponding data if no more in use. */
bool xAAL_write_bus(const xAAL_businfo_t *bus,
		    const xAAL_devinfo_t *device,
		    xAAL_msg_type_t msg_type,
		    const char *action,
		    cbor_item_t *cbody,
		    cbor_item_t *ctargets);


/* Send a message on the bus */
/* Helps devices to write a message on the bus.
   It build the header, add the provided cbor body if any, an send it.
   Targets is a fixed size vector of uuids (addresses).
   (see xAAL_write_busl() for passing targets as a list)
   Return true if success */
#define xAAL_write_busv(bus, device, msg_type, action, cbody, targets, nb)	xAAL_write_bus(bus, device, msg_type, action, cbody, xAAL_pack_uuids(targets, nb))


/* Send a message on the bus */
/* Helps devices to write a message on the bus.
   It build the header, add the provided cbor body if any, an send it.
   Targets is a list of uuids (addresses) terminated by NULL.
   (see xAAL_write_busv() for passing targets as a vector)
   Return true if success */
#define xAAL_write_busl(bus, device, msg_type, action, cbody, ...)	xAAL_write_bus(bus, device, msg_type, action, cbody, xAAL_pack_luuids(__VA_ARGS__))



/*
 * Helpers for handling usual messages
 */

/* The special (UUID)(0) broadcasting adress specific to is_alive requests, according to xAAL 0.7rev2 */
extern const uuid_t xAAL_is_alive_target;


/* Send alive messages */
/* Return true if success */
bool xAAL_notify_alive(const xAAL_businfo_t *bus,
		       const xAAL_devinfo_t *device);


/* xAAL_reply_get_description */
/* Return true if success */
bool xAAL_reply_get_description(const xAAL_businfo_t *bus,
			       const xAAL_devinfo_t *device,
			       const uuid_t *target);


/* Test if a given address belong to the cbor targets list (or boradcast) */
bool xAAL_targets_match(const cbor_item_t *ctargets,
			const uuid_t *addr);


/* Get timeout value within an alive notify */
/* Return 0 if there is no timeout indication or timeout+now otherwise */
time_t xAAL_read_aliveTimeout(const cbor_item_t *cbody);


/* Test if requested dev_types of an is_alive match a given one */
/* i.e., <prefix>.<suffix>  or  <prefix>.any  or  any.any  */
bool xAAL_is_aliveDevType_match(const cbor_item_t *cbody,
			       const char *dev_type);



#endif
