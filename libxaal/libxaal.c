/* libxaal
 * A minimal xAAL library, with security
 *
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <stdarg.h>
#include <endian.h>
#include <stdint.h>
#include <sys/time.h>
#include <sodium.h>

#include <xaal.h>


FILE *xAAL_error_log = NULL;
const uuid_t xAAL_is_alive_target = {};


/*
 * Helpers for accessing the bus
 */

bool is_multicast(struct addrinfo *ai) {
  switch (ai->ai_family) {
    case AF_INET:
      return IN_MULTICAST(ntohl( ((struct sockaddr_in*)(ai->ai_addr))->sin_addr.s_addr ));
    case AF_INET6:
      return IN6_IS_ADDR_MULTICAST( ((struct sockaddr_in6 *)(ai->ai_addr))->sin6_addr.s6_addr); 
    default:
      return false;
  }
}


/* Join the xAAL bus
   Return 'true' if everything was ok, and 'false' otherwise. */
bool xAAL_join_bus(const char *addr,
		   const char *port,
		   int hops,
		   int mcast_loop,
		   xAAL_businfo_t *bus) {
  int s;
  int one = 1;
  struct ip_mreqn mreqn;
  struct ipv6_mreq mreq6;
  struct addrinfo hints;
  struct addrinfo *result, *rp;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;		/* Allow IPv4 or IPv6 */
  hints.ai_socktype = SOCK_DGRAM;	/* Datagram socket */
  hints.ai_protocol = 0;		/* Any protocol */
  hints.ai_canonname = NULL;
  hints.ai_addr = NULL;
  hints.ai_next = NULL;

  s = getaddrinfo(addr, port, &hints, &result);
  if (s != 0) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "getaddrinfo: %s\n", gai_strerror(s));
    return false;
  }

  for (rp = result; rp != NULL; rp = rp->ai_next) {
    bus->sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
    if (bus->sfd == -1)
      continue;
    if (is_multicast(rp)) {
      // Debate between the new (and non-standard) SO_REUSEPORT vs. the plain old SO_REUSEADDR ... still not clear...
      if (setsockopt(bus->sfd, SOL_SOCKET, SO_REUSEPORT, &one, sizeof(one)) == -1) {
        if (xAAL_error_log) fprintf(xAAL_error_log, "SO_REUSEPORT: %s\n", strerror(errno));
        close(bus->sfd);
        continue;
      }
      if (bind(bus->sfd, rp->ai_addr, rp->ai_addrlen) == 0)
        break;				/* Success */
    } else {
      if (connect(bus->sfd, rp->ai_addr, rp->ai_addrlen) == 0)
        break;				/* Success */
    }
    if (xAAL_error_log) fprintf(xAAL_error_log, "Trying %s %s: %s\n", addr, port, strerror(errno));
    close(bus->sfd);
  }
  if (rp == NULL) {			/* No address succeeded */
    if (xAAL_error_log) fprintf(xAAL_error_log, "Could not open socket\n");
    return false;
  }

  if (is_multicast(rp))  /* Now, join the group */
    switch (rp->ai_family) {
      case AF_INET:
        memcpy(&mreqn.imr_multiaddr.s_addr, &(((struct sockaddr_in*)(rp->ai_addr))->sin_addr), sizeof(struct in_addr));
        mreqn.imr_address.s_addr = INADDR_ANY;
        mreqn.imr_ifindex = 0;
        if ( setsockopt(bus->sfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreqn, sizeof(mreqn)) == -1) {
          if (xAAL_error_log) fprintf(xAAL_error_log, "Could not join the multicast group: %s\n", strerror(errno));
          return false;
        }
        if ( setsockopt(bus->sfd, IPPROTO_IP, IP_MULTICAST_TTL, &hops, sizeof(hops)) == -1) {
          if (xAAL_error_log) fprintf(xAAL_error_log, "Could not set TTL to %d: %s\n", hops, strerror(errno));
          return false;
        }
        if ( setsockopt(bus->sfd, IPPROTO_IP, IP_MULTICAST_LOOP, &mcast_loop, sizeof(mcast_loop)) == -1) {
          if (xAAL_error_log) fprintf(xAAL_error_log, "Could not %s multicast loop: %s\n", mcast_loop?"enable":"disable", strerror(errno));
          return false;
        }
        break;
      case AF_INET6:
        memcpy(&mreq6.ipv6mr_multiaddr, &(((struct sockaddr_in6*)(rp->ai_addr))->sin6_addr), sizeof(struct in6_addr));
        mreq6.ipv6mr_interface = 0;
        if ( setsockopt(bus->sfd, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &mreq6, sizeof(mreq6)) == -1) {
          if (xAAL_error_log) fprintf(xAAL_error_log, "Could not join the multicast group: %s\n", strerror(errno));
          return false;
        }
        if ( setsockopt(bus->sfd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &hops, sizeof(hops)) == -1) {
          if (xAAL_error_log) fprintf(xAAL_error_log, "Could not set Hops to %d: %s\n", hops, strerror(errno));
          return false;
        }
        if ( setsockopt(bus->sfd, IPPROTO_IPV6, IPV6_MULTICAST_LOOP, &mcast_loop, sizeof(mcast_loop)) == -1) {
          if (xAAL_error_log) fprintf(xAAL_error_log, "Could not %s multicast loop: %s\n", mcast_loop?"enable":"disable", strerror(errno));
          return false;
        }
        break;
      default:
        if (xAAL_error_log) fprintf(xAAL_error_log, "Unknown protocol %d\n", rp->ai_family);
        return false;
    }

  memcpy(&bus->addr, rp->ai_addr, rp->ai_addrlen);
  bus->addrlen = rp->ai_addrlen;

  freeaddrinfo(result);

  if (sodium_init() == -1)
    return false;

  bus->wanted_targets = malloc(sizeof(uuid_t));
  bus->wanted_targets_nb = 1;
  uuid_clear(bus->wanted_targets[0]);	/* Add the (UUID)(0) within the list of wanted target, according to xAAL 0.7rev2 */
  return true;
}


/* Retrieve (position or -1) of an uuid within a vector of uuids */
/* May be used with wanted targets */
int xAAL_uuids_get_item(const uuid_t *uuids, unsigned nb, const uuid_t *uuid) {
  unsigned i;
  for (i=0; i < nb; i++)
    if (uuid_compare(uuids[i], *uuid) == 0)
      return i;
  return -1;
}


/* Add an uuid address to the list of wanted_targets */
bool xAAL_add_wanted_target(const uuid_t *uuid, xAAL_businfo_t *bus) {
  if ( xAAL_uuids_get_item(bus->wanted_targets, bus->wanted_targets_nb, uuid) >= 0 )
    return true;

  bus->wanted_targets = realloc(bus->wanted_targets,
				++bus->wanted_targets_nb * sizeof(uuid_t) );
  uuid_copy(bus->wanted_targets[bus->wanted_targets_nb-1], *uuid);
  return true;
}


/* Del an uuid address to the list of wanted_targets */
bool xAAL_del_wanted_target(const uuid_t *uuid, xAAL_businfo_t *bus) {
  int pos = xAAL_uuids_get_item(bus->wanted_targets, bus->wanted_targets_nb, uuid);

  if (pos < 0)
    return true;

  if (pos < bus->wanted_targets_nb-1)
    memmove(bus->wanted_targets+pos, bus->wanted_targets+pos+1,
	    (bus->wanted_targets_nb - 1 - pos) * sizeof(uuid_t) );
  bus->wanted_targets = realloc(bus->wanted_targets,
				--bus->wanted_targets_nb * sizeof(uuid_t) );
  return true;
}


static bool wanted_match(const cbor_item_t *ctargets, const uuid_t *uuids, unsigned nb) {
  cbor_item_t *ctarget;
  uuid_t target;
  int i, len;

  if ( !ctargets || !cbor_isa_array(ctargets) )
    return true;

  len = cbor_array_size(ctargets);
  if (len == 0 || nb == 0)
    return true;

  for (i=0; i < len; i++) {
    ctarget = cbor_move(cbor_array_get(ctargets, i));
    if ( xAAL_cbor_is_uuid(ctarget, &target) && (xAAL_uuids_get_item(uuids, nb, &target) >= 0) )
	return true;
  }
  return false;
}



/* Compute a key from a pass-phrase
 * using crypto_pwhash_scryptsalsa208sha256
 * salt: buffer of zeros
 * opslimit: crypto_pwhash_scryptsalsa208sha256_OPSLIMIT_INTERACTIVE
 * memlimit: crypto_pwhash_scryptsalsa208sha256_MEMLIMIT_INTERACTIVE
 */
unsigned char *xAAL_pass2key(const char *passphrase) {
  unsigned char *k;
  unsigned char salt[crypto_pwhash_scryptsalsa208sha256_SALTBYTES];

  sodium_memzero(salt, sizeof(salt));

  k = sodium_malloc(crypto_aead_chacha20poly1305_IETF_KEYBYTES);

  if (crypto_pwhash_scryptsalsa208sha256(
	    k, crypto_aead_chacha20poly1305_IETF_KEYBYTES,
	    passphrase, strlen(passphrase), salt,
	    crypto_pwhash_scryptsalsa208sha256_OPSLIMIT_INTERACTIVE,
	    crypto_pwhash_scryptsalsa208sha256_MEMLIMIT_INTERACTIVE) != 0) {
    /* out of memory */
    sodium_free(k);
    return NULL;
  }
  return k;
}


typedef union {
  const unsigned char buf[12];
  struct {
    uint64_t sec;
    uint32_t usec; };
} nonce_t;



/*
void cbor2diag(unsigned char *buf, size_t len) {
  FILE *p = popen("cbor2diag.rb", "w");
  if (p) {
    fwrite(buf, len, 1, p);
    pclose(p);
  }
}*/


/* Manage received message */
/* Read a message from the bus, parse it thanks to the cbor library,
   get parameters of the header, and the body if any.
   Return 'true' if everything was ok, and 'false' otherwise.
   Note: In case of success you should call xAAL_free_msg() after use. */
#ifdef XAAL_MORE_INFO
bool xAAL_read_bus(const xAAL_businfo_t *bus,
		   cbor_item_t **ctargets,
		   uuid_t **source,
		   char **dev_type,
		   xAAL_msg_type_t *msg_type,
		   char **action,
		   cbor_item_t **cbody,
		   ssize_t *msglen, char **ip) {
#else
bool xAAL_read_bus(const xAAL_businfo_t *bus,
		   cbor_item_t **ctargets,
		   uuid_t **source,
		   char **dev_type,
		   xAAL_msg_type_t *msg_type,
		   char **action,
		   cbor_item_t **cbody) {
#endif
  unsigned char buf[xAAL_BUF_SIZE];
  ssize_t nread;
  struct cbor_load_result cresult;
  cbor_item_t *cmsg, *ctargets_bs, *csec, *cusec,
		     *cpayload_bs, *cpayload,
		     *cversion, *csource, *cmsg_type, *cdev_type, *caction;
  const unsigned char *pl, *ad;
  unsigned char *m;
  unsigned long long adlen, pllen, mlen;
  struct timeval timestamp;
  nonce_t npub;
  time_t now;

#ifdef XAAL_MORE_INFO
  struct sockaddr_storage addr;
  socklen_t addr_len=sizeof(addr);
  static char buffer[INET6_ADDRSTRLEN];
  int s;

  /* Recive a message */
  addr_len=sizeof(addr);
  nread = recvfrom(bus->sfd, buf, xAAL_BUF_SIZE, 0, (struct sockaddr*)&addr, &addr_len);
  if (nread == -1)
    return false;
  *msglen = nread;

  /* Get IP of the sender */
  s = getnameinfo( (struct sockaddr*)&addr, addr_len, buffer, INET6_ADDRSTRLEN, 0,0,NI_NUMERICHOST);
  if (s == 0) {
    *ip = buffer;
  } else {
    if (xAAL_error_log)
      fprintf(xAAL_error_log, "getnameinfo: %s\n", gai_strerror(s));
    *ip = NULL;
  }
#else
  /* Recive a message */
  nread = recvfrom(bus->sfd, buf, xAAL_BUF_SIZE, 0, NULL, NULL);
  if (nread == -1)
    return false;
#endif

  /* Use the cbor library */
  cmsg = cbor_load(buf, nread, &cresult);
  if (cmsg == NULL) {
    if (xAAL_error_log) fprintf(xAAL_error_log,  "CBOR error in message; code %d, position %lu\n", cresult.error.code, cresult.error.position);
    return false;
  } else if ( !cbor_isa_array(cmsg) || (cbor_array_size(cmsg)!=5) ) {
    if (xAAL_error_log) fprintf(xAAL_error_log,  "Received message is not a cbor definite array[5]\n");
    cbor_decref(&cmsg);
    return false;
  }
  // printf("\nSecurity Layer\n");cbor2diag(buf,nread);//cbor_describe(cmsg, stdout);


  /*
   * Parse the security layer
   * [ version timestamp-sec timestamp-usec targets payload ]
   */
  /* version */
  cversion = cbor_move(cbor_array_get(cmsg, 0));
  if ( !cbor_isa_uint(cversion) ) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'version' field in xAAL message\n");
    cbor_decref(&cmsg);
    return false;
  }
  if ( cbor_get_int(cversion) != xAAL_VERSION ) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'version' value in xAAL message\n");
    cbor_decref(&cmsg);
    return false;
  }

  /* timestamp */
  csec  = cbor_move(cbor_array_get(cmsg, 1));
  cusec = cbor_move(cbor_array_get(cmsg, 2));

  if ( !cbor_isa_uint(csec)  || !cbor_isa_uint(cusec) ) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'timestamp' values in xAAL message\n");
    cbor_decref(&cmsg);
    return false;
  }

  timestamp.tv_sec = cbor_get_int(csec); 
  timestamp.tv_usec = cbor_get_int(cusec);
  npub.sec = htobe64(timestamp.tv_sec);   // build nonce for chacha20
  npub.usec = htobe32(timestamp.tv_usec);

  now = time(NULL);
  if (   timestamp.tv_sec < now - bus->maxAge     // too old
      || timestamp.tv_sec > now + bus->maxAge ) { // too young
    cbor_decref(&cmsg);
    return false;
  }


  /* targets */
  ctargets_bs = cbor_move(cbor_array_get(cmsg, 3));
  if (!cbor_isa_bytestring(ctargets_bs) || cbor_bytestring_is_indefinite(ctargets_bs)) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'targets' field in xAAL message\n");
    cbor_decref(&cmsg);
    return false;
  }
  ad = cbor_bytestring_handle(ctargets_bs); // used as additional data for chacha20
  adlen = cbor_bytestring_length(ctargets_bs);

  if (adlen) {
    *ctargets = cbor_load(ad, adlen, &cresult);
    if (*ctargets == NULL) {
      if (xAAL_error_log) fprintf(xAAL_error_log,  "CBOR error in targets; code %d, position %lu\n", cresult.error.code, cresult.error.position);
      return false;
    } else if (!cbor_isa_array(*ctargets)) {
      if (xAAL_error_log) fprintf(xAAL_error_log,  "Received 'targets' field is not a cbor array\n");
      cbor_decref(ctargets);
      cbor_decref(&cmsg);
      return false;
    }
  } else
    *ctargets = cbor_new_definite_array(0);

  if (!wanted_match(*ctargets, bus->wanted_targets, bus->wanted_targets_nb)) {
    cbor_decref(ctargets);
    cbor_decref(&cmsg);
    return false;
  }


  /* ciphered payload */
  cpayload_bs = cbor_move(cbor_array_get(cmsg, 4));
  if (!cbor_isa_bytestring(cpayload_bs) || cbor_bytestring_is_indefinite(cpayload_bs)) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'payload' field in xAAL message\n");
    cbor_decref(ctargets);
    cbor_decref(&cmsg);
    return false;
  }
  pl = cbor_bytestring_handle(cpayload_bs);
  pllen = cbor_bytestring_length(cpayload_bs);

  mlen = pllen - crypto_aead_chacha20poly1305_IETF_ABYTES;
  if (mlen <= 0) {
    cbor_decref(ctargets);
    cbor_decref(&cmsg);
    return false;
  }
  m = malloc(mlen);
  if (crypto_aead_chacha20poly1305_ietf_decrypt((unsigned char *)m, &mlen, NULL,
       (const unsigned char *)pl, pllen, (const unsigned char *)ad, adlen,
       npub.buf, bus->key) == -1) {
    cbor_decref(ctargets);
    cbor_decref(&cmsg);
    free(m);
    return false;
  }

  cpayload = cbor_load(m, mlen, &cresult);
  if (cpayload == NULL) {
    if (xAAL_error_log) fprintf(xAAL_error_log,  "CBOR error in payload; code %d, position %lu\n", cresult.error.code, cresult.error.position);
    return false;
  } else if (!cbor_isa_array(cpayload) || (cbor_array_size(cpayload)<4) || (cbor_array_size(cpayload)>5) ) {
    if (xAAL_error_log) fprintf(xAAL_error_log,  "Decoded 'payload' field is not a cbor defined array[4|5]\n");
    cbor_decref(&cpayload);
    cbor_decref(ctargets);
    cbor_decref(&cmsg);
    return false;
  }
  //printf("Application Layer\n");cbor_diag(m,mlen);//cbor_describe(cpayload, stdout);
  free(m);

  /*
   * Parse the application layer
   * [ source, dev_type, msg_type, action, <body> ]
   */

  /* Parse header */
  /* source */
  csource = cbor_move(cbor_array_get(cpayload, 0));
  *source = (uuid_t *)malloc(sizeof(uuid_t));
  if ( !xAAL_cbor_is_uuid(csource, *source) ) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'source' field in xAAL header\n");
    cbor_decref(&cmsg);
    cbor_decref(ctargets);
    cbor_decref(&cpayload);
    return false;
  }

  /* dev_type */
  cdev_type = cbor_move(cbor_array_get(cpayload, 1));
  if ( !cdev_type || !cbor_isa_string(cdev_type) || cbor_string_is_indefinite(cdev_type)) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'dev_type' field in xAAL header\n");
    cbor_decref(&cmsg);
    cbor_decref(ctargets);
    cbor_decref(&cpayload);
    free(*source);
    return false;
  }
  *dev_type = strndup((const char *)cbor_string_handle(cdev_type), cbor_string_length(cdev_type));

  /* msg_type */
  cmsg_type = cbor_move(cbor_array_get(cpayload, 2));
  if ( !cbor_isa_uint(cmsg_type) || cbor_get_int(cmsg_type) > xAAL_MSGTYPE_LAST ) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'msg_type' field in xAAL header\n");
    cbor_decref(&cmsg);
    cbor_decref(ctargets);
    cbor_decref(&cpayload);
    free(*source);
    free(*dev_type);
    return false;
  }
  *msg_type = cbor_get_int(cmsg_type);

  caction = cbor_move(cbor_array_get(cpayload, 3));
  if (!cbor_isa_string(caction) || cbor_string_is_indefinite(caction)) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid 'action' field in xAAL header\n");
    cbor_decref(&cmsg);
    cbor_decref(ctargets);
    cbor_decref(&cpayload);
    free(*source);
    free(*dev_type);
    return false;
  }
  *action = strndup((const char *)cbor_string_handle(caction), cbor_string_length(caction));

  /* Get body if any */
  if (cbor_array_size(cpayload) > 4)
    *cbody = cbor_array_get(cpayload, 4);
  else
    *cbody = NULL;
  if (*cbody && !cbor_isa_map(*cbody) ) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Invalid xAAL body\n");
    cbor_decref(&cmsg);
    cbor_decref(ctargets);
    cbor_decref(cbody);
    cbor_decref(&cpayload);
    free(*source);
    free(*dev_type);
    free(*action);
    return false;
  }

  cbor_decref(&cmsg);
  cbor_decref(&cpayload);
  return true;
}


/* Free data allocated by xAAL_read_bus() */
void xAAL_free_msg(cbor_item_t *ctargets,
		   uuid_t *source,
		   char *dev_type,
		   char *action,
		   cbor_item_t *cbody) {
  if (ctargets)
    cbor_decref(&ctargets);
  if (source)
    free(source);
  if (dev_type)
    free(dev_type);
  if (action)
    free (action);
  if (cbody)
    cbor_decref(&cbody);
}


/* Send a message on the bus */
/* Helps devices to write a message on the bus.
   It build the header, add the provided cbor body if any, an send it.
   Targets is a cbor array of uuids (addresses).
   Return true if success
   Note: call cbor_decref(cbody) cbor_decref(ctargets)
   i.e. free corresponding data if no more in use. */
bool xAAL_write_bus(const xAAL_businfo_t *bus,
		    const xAAL_devinfo_t *device,
		    const xAAL_msg_type_t msg_type,
		    const char *action,
		    cbor_item_t *cbody,
		    cbor_item_t *ctargets) {
  cbor_item_t *cmsg, *cpayload, *ctargets_bs, *cpayload_bs;
  unsigned char *ad, *m, *c, *msg;
  size_t sz, adlen, mlen, msglen;
  unsigned long long clen;
  struct timeval timestamp;
  nonce_t npub;
  bool r;

  /* Build payload */
  cpayload = cbor_new_definite_array( cbody? 5 : 4);
  r = cbor_array_set(cpayload, 0, cbor_move(cbor_build_bytestring(device->addr, 16)) );	/* source */
  r = r && cbor_array_set(cpayload, 1, cbor_move(cbor_build_string(device->dev_type)) );	/* dev_type */
  r = r && (msg_type <= xAAL_MSGTYPE_LAST);
  r = r && cbor_array_set(cpayload, 2, cbor_move(cbor_build_uint8(msg_type)) );		/* msg_type */
  r = r && cbor_array_set(cpayload, 3,  cbor_move(cbor_build_string(action)) );		/* action */
  if ( cbody ) {
    r = r && cbor_array_set(cpayload, 4, cbody);	/* body if any */
    cbor_decref(&cbody);
  }
  if ( !r ) {
    cbor_decref(&cpayload);
    if (ctargets) cbor_decref(&ctargets);
    return false;
  }

  /* Public signed additionnal data */
  if (!ctargets)
    ctargets = cbor_new_definite_array(0);
  adlen = cbor_serialize_alloc(ctargets, &ad, &sz);

  /* Public nonce (timestamp) */
  gettimeofday(&timestamp, NULL);
  npub.sec = htobe64(timestamp.tv_sec);
  npub.usec = htobe32(timestamp.tv_usec);

  /* Ciffering */
  mlen = cbor_serialize_alloc(cpayload, &m, &sz);
  clen = mlen + crypto_aead_chacha20poly1305_IETF_ABYTES;
  c = malloc(clen);
  r = ( crypto_aead_chacha20poly1305_ietf_encrypt(c, &clen,
	      (const unsigned char *)m, mlen, ad, adlen, NULL, npub.buf, bus->key) != -1 );
  free(m);
  if ( !r ) {
    cbor_decref(&cpayload);
    free(c);
    if (ctargets) cbor_decref(&ctargets);
    return false;
  }

  /* Build message */
  cmsg = cbor_new_definite_array(5);
  r = cbor_array_set(cmsg, 0, cbor_move(cbor_build_uint8(xAAL_VERSION)) );		/* version */
  r = r && cbor_array_set(cmsg, 1, cbor_move(cbor_build_uint64(timestamp.tv_sec)) );	/* timestamp sec */
  r = r && cbor_array_set(cmsg, 2, cbor_move(cbor_build_uint32(timestamp.tv_usec)) );	/* timestamp usec */

  ctargets_bs = cbor_new_definite_bytestring();
  cbor_bytestring_set_handle(ctargets_bs, ad, adlen);
  r = r && cbor_array_set(cmsg, 3, cbor_move(ctargets_bs) );				/* targets */

  cpayload_bs = cbor_new_definite_bytestring();
  cbor_bytestring_set_handle(cpayload_bs, c, clen);
  r = r && cbor_array_set(cmsg, 4, cbor_move(cpayload_bs) );				/* payload */
  if ( !r ) {
    cbor_decref(&cpayload);
    cbor_decref(&cmsg); /* note: buffer c is included and should be deallocated automaticaly */
    if (ctargets) cbor_decref(&ctargets);
    return false;
  }

  /* Send message */
  msglen = cbor_serialize_alloc(cmsg, &msg, &sz);

  if ( sendto(bus->sfd, msg, msglen, 0, (struct sockaddr *)&(bus->addr), bus->addrlen) == -1) {
    if (xAAL_error_log) fprintf(xAAL_error_log, "Error while sending message: %s\n", strerror(errno));
    r = false;
  }

  cbor_decref(&cpayload);
  cbor_decref(&cmsg);
  if (ctargets) cbor_decref(&ctargets);
  free(msg);
  return r;
}



/*
 * Helpers for handling usual messages
 */


/* Send alive messages */
/* Return true if success */
bool xAAL_notify_alive(const xAAL_businfo_t *bus,
		       const xAAL_devinfo_t *device) {
  cbor_item_t *cbody;

  cbody = cbor_new_definite_map(1);
  (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("timeout")),
					  cbor_move(xAAL_cbor_build_int(2 * device->alivemax)) });

  return xAAL_write_bus(bus, device, xAAL_NOTIFY, "alive", cbody, NULL);
}


/* xAAL_reply_get_description */
/* Return true if success */
bool xAAL_reply_get_description(const xAAL_businfo_t *bus,
			       const xAAL_devinfo_t *device,
			       const uuid_t *target) {
  cbor_item_t *cbody;

  cbody = cbor_new_indefinite_map();
  if (device->vendor_id)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("vendor_id")), cbor_move(cbor_build_string(device->vendor_id)) } );

  if (device->product_id)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("product_id")), cbor_move(cbor_build_string(device->product_id)) });

  if (device->hw_id)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("hw_id")), device->hw_id });

  if (device->version)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("version")), cbor_move(cbor_build_string(device->version)) });

  if (device->group_id)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("group_id")), cbor_move(xAAL_new_tagged_uuid(device->group_id)) });

  if (device->url) {
    cbor_item_t *url = cbor_new_tag(32);
    cbor_tag_set_item(url, cbor_move(cbor_build_string(device->url)));
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("url")), cbor_move(url) });
  }

  if (device->schema) {
    cbor_item_t *url = cbor_new_tag(32);
    cbor_tag_set_item(url, cbor_move(cbor_build_string(device->url)));
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("schema")), cbor_move(url) });
  }

  if (device->info)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("info")), cbor_move(cbor_build_string(device->info)) });

  if (device->unsupported_attributes)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("unsupported_attributes")), cbor_move(xAAL_strings_pack(device->unsupported_attributes)) });

  if (device->unsupported_methods)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("unsupported_methods")), cbor_move(xAAL_strings_pack(device->unsupported_methods)) });

  if (device->unsupported_notifications)
    (void)!cbor_map_add(cbody, (struct cbor_pair){ cbor_move(cbor_build_string("unsupported_notifications")), cbor_move(xAAL_strings_pack(device->unsupported_notifications)) });

  return xAAL_write_busl(bus, device, xAAL_REPLY, "get_description", cbody, target, NULL);
}



/* Test if a given address belong to the ctarget list (or boradcast) */
bool xAAL_targets_match(const cbor_item_t *ctargets, const uuid_t *addr) {
  return wanted_match(ctargets, addr, 1);
}


/* Get timeout value within an alive notify */
/* Return 0 if there is no timeout indication or timeout+now otherwise */
time_t xAAL_read_aliveTimeout(const cbor_item_t *cbody) {
  cbor_item_t *ctimeout;
  long timeout;

  if (cbody)
    ctimeout = xAAL_cbor_map_get(cbody, "timeout");
  else
    return 0;

  if ( ctimeout && cbor_isa_uint(ctimeout) ) {
    timeout = cbor_get_int(ctimeout);
    if ( timeout==0 )
      return 0;
    else
      return time(NULL) + timeout;

  } else
    return 0;
}


/* Test if requested dev_types of an is_alive match a given one */
/* i.e., <prefix>.<suffix>  or  <prefix>.any  or  any.any */
bool xAAL_is_aliveDevType_match(const cbor_item_t *cbody, const char *dev_type) {
  cbor_item_t *cdev_types, *cdev_type;
  const char *sdev_type;
  char *anyType, *p;
  int i, len;
  size_t sz;

  if (cbody)
    cdev_types = xAAL_cbor_map_get(cbody, "dev_types");
  else
    return true;

  if ( !cdev_types || !cbor_isa_array(cdev_types) )
    return true;

  anyType = malloc(strlen(dev_type)+strlen(".any")+1);
  strcpy(anyType, dev_type);
  p = strchr(anyType, '.');
  if (p)
    strcpy(p, ".any");
  else
    strcat(anyType, ".any");

  len = cbor_array_size(cdev_types);
  for (i = 0; i < len; i++) {
    cdev_type = cbor_move(cbor_array_get(cdev_types, i));
    if ( cbor_isa_string(cdev_type) && cbor_string_is_definite(cdev_type) ) {
      sdev_type = (const char *)cbor_string_handle(cdev_type);
      sz = cbor_string_length(cdev_type);
      if ( (strncmp(sdev_type, dev_type, sz) == 0)
	  || (strncmp(sdev_type, anyType, sz) == 0)
	  || (strncmp(sdev_type, "any.any", sz) == 0) ) {
	free(anyType);
	return true;
      }
    }
  }
  free(anyType);
  return false;
}



/*
 * Helpers for data structures
 */


/* Retrieve an item within a null-terminated list of strings */
/* May be used on unsupported{Attributes,Methods,Notifications} */
char **xAAL_strings_get(char **list, const char *item) {
  char **idx;
  if (list && item)
    for (idx = list; *idx != NULL; idx++)
      if (strcmp(*idx, item) == 0)
	return idx;
  return NULL;
}


/* Pack a null terminated list of strings as a cbor array of strings */
cbor_item_t *xAAL_strings_pack(char **list) {
  cbor_item_t *carray = cbor_new_indefinite_array();
  char **idx;
  if (list)
    for (idx = list; *idx != NULL; idx++)
      (void)!cbor_array_push(carray, cbor_move(cbor_build_string(*idx)));
  return carray;
}


/* Unpack a cbor array of strings as a null terminated list of strings */
char **xAAL_strings_unpack(cbor_item_t *carray) {
  char **list;
  char *item;
  size_t sz, len, i, j;

  if ( !carray || !cbor_isa_array(carray) )
    return NULL;

  sz = cbor_array_size(carray);
  list = (char **)malloc((sz+1)*sizeof(char *));

  for (j = i = 0; i < sz; i++) {
    item = xAAL_cbor_string_dup(cbor_move(cbor_array_get(carray, i)), &len);
    if ( item )
      list[j++] = item;
  }
  list[j] = NULL;
  return list;
}


/* Helper to retreive a value within a cbor map, with a definite string key */
cbor_item_t *xAAL_cbor_map_get(const cbor_item_t *map, const char *key) {
  struct cbor_pair *pairs;
  size_t i, sz;

  if ( !map || !cbor_isa_map(map) || !key )
    return NULL;

  pairs = cbor_map_handle(map);
  sz = cbor_map_size(map);
  for (i = 0; i < sz; i++) {
    if ( cbor_isa_string(pairs[i].key) && cbor_string_is_definite(pairs[i].key)
	&& (strncmp((const char *)cbor_string_handle(pairs[i].key), key, cbor_string_length(pairs[i].key)) == 0) )
      return pairs[i].value;
  }
  return NULL;
}


/* Helper to update (change or add) a value within a cbor map, with a definite string key */
void xAAL_cbor_map_update(cbor_item_t *map, const char *key, cbor_item_t *newval) {
  struct cbor_pair *pairs;
  size_t i, sz;

  if ( !map || !cbor_isa_map(map) || !key )
    return;

  pairs = cbor_map_handle(map);
  sz = cbor_map_size(map);
  for (i = 0; i < sz; i++) {
    if ( cbor_isa_string(pairs[i].key) && cbor_string_is_definite(pairs[i].key)
	&& (strncmp((const char *)cbor_string_handle(pairs[i].key), key, cbor_string_length(pairs[i].key)) == 0) ) {
      cbor_decref(&(pairs[i].value));
      pairs[i].value = cbor_incref(newval);
      return;
    }
  }
  (void)!cbor_map_add(map, (struct cbor_pair){ cbor_move(cbor_build_string(key)), newval });
}


/* Helper to delete a value within a cbor map, with a definite string key */
void xAAL_cbor_map_del(cbor_item_t *map, const char *key) {
 struct cbor_pair *pairs;
  size_t i, sz;

  if ( !map || !cbor_isa_map(map) || !key )
    return;

  pairs = cbor_map_handle(map);
  sz = cbor_map_size(map);
  for (i = 0; i < sz; i++)
    if ( cbor_isa_string(pairs[i].key) && cbor_string_is_definite(pairs[i].key)
	&& (strncmp((const char *)cbor_string_handle(pairs[i].key), key, cbor_string_length(pairs[i].key)) == 0) ) {
      // I found it
      cbor_decref(&(pairs[i].key));
      cbor_decref(&(pairs[i].value));
      break;
    }

  // Now, I slide the following items from one cell
  for (; i < sz-1; i++) {
    pairs[i].key = pairs[i+1].key;
    pairs[i].value = pairs[i+1].value;
  }

  // Ajust the size
  map->metadata.map_metadata.end_ptr--;
}


/* Test if a cbor bitestream could be an uuid, and extract it */
bool xAAL_cbor_is_uuid(const cbor_item_t *item, uuid_t *dst) {
  if (!item) return false;
  
  const cbor_item_t *aux;
  if ( cbor_isa_tag(item) && (cbor_tag_value(item) == 37) )
    aux = cbor_move(cbor_tag_item(item));
  else
    aux = item;  
  
  if (cbor_isa_bytestring(aux) && cbor_bytestring_is_definite(aux) && (cbor_bytestring_length(aux) == 16)) {
    uuid_copy(*dst, cbor_bytestring_handle(aux));
    return true;
  }
  return false;
}


/* Build a tagged cbor UUID */
cbor_item_t *xAAL_new_tagged_uuid(const uuid_t *uuid) {
  cbor_item_t *cuuid = cbor_new_tag(37);
  cbor_tag_set_item(cuuid, cbor_move(cbor_build_bytestring(*uuid, 16)));
  return cuuid;
}


/* Build a cbor array from a vector of uuids */
cbor_item_t *xAAL_pack_uuids(const uuid_t *uuids, unsigned nb) {
  cbor_item_t *carray = cbor_new_definite_array(nb);
  unsigned i;

  for (i=0; i < nb; i++)
    (void)!cbor_array_push(carray, cbor_move(cbor_build_bytestring(uuids[i], 16)));
  return carray;
}


/* Build a cbor array from a variadic list of uuids */
cbor_item_t *xAAL_pack_luuids(const uuid_t *uuid0, ...) {
  cbor_item_t *carray = cbor_new_indefinite_array();
  va_list ap;
  uuid_t *uuid;

  if (uuid0) {
    (void)!cbor_array_push(carray, cbor_move(cbor_build_bytestring(*uuid0, 16)));

    va_start(ap, uuid0);
    while ( (uuid = va_arg(ap, uuid_t*)) )
      (void)!cbor_array_push(carray, cbor_move(cbor_build_bytestring(*uuid, 16)));
    va_end(ap);
  }
  return carray;
}


/* Helper to build a cbor item according to given number value */
cbor_item_t *xAAL_cbor_build_int(long long v) {
  bool neg;

  if (v < 0) {
    neg = true;
    v = -v - 1;
  } else
    neg = false;

  if (v <= UINT8_MAX)
    return neg? cbor_build_negint8(v) : cbor_build_uint8(v);
  else if (v <= UINT16_MAX)
    return neg? cbor_build_negint16(v) : cbor_build_uint16(v);
  else if (v <= UINT32_MAX)
    return neg? cbor_build_negint32(v) : cbor_build_uint32(v);
  else
    return neg? cbor_build_negint64(v) : cbor_build_uint64(v);
}



#include <math.h>

/* Helper to build a cbor item according to given (double) value */
cbor_item_t *xAAL_cbor_build_float(double v) {

  /* Trick to use binary operators on double */
  typedef union { double val; uint64_t data; } dd_t;

  /* ieee754 floats.  half- single- double- precision
	 half: sign (1bit), exponent  (5bits), mantissa (10bits)
       single: sign (1bit), exponent  (8bits), mantissa (23bits)
       double: sign (1bit), exponent (11bits), mantissa (52bits)
     Following are constants with matissa bits set to 1,
     and min-max for the exponent */
  const dd_t hm = {.val = 3.998046875		}; /* mantissa mask for a half */
  const dd_t fm = {.val = 3.99999976158142089844}; /* mantissa mask for a single */
  const int he_min =  -14;	/* exponent min for a half */
  const int he_max =   15;	/* exponent max for a half */
  const int se_min = -126;	/* exponent min for a single */
  const int se_max =  127;	/* exponent max for a single */

  dd_t m;
  int e;

  switch (fpclassify(v)) {
  case FP_NAN:
  case FP_INFINITE:
  case FP_ZERO:
    return cbor_build_float2(v);
  default: ;
  }

  m.val = fabs(frexp(v, &e));

  if ( ((m.data | hm.data) == hm.data) && (e >= he_min) && (e <= he_max) )
    return cbor_build_float2(v);
  else if ( ((m.data | fm.data) == fm.data) && (e >= se_min) && (e <= se_max) )
    return cbor_build_float4(v);
  else
    return cbor_build_float8(v);
}


/* Helper to read a cbor boolean */
bool xAAL_cbor_get_bool(cbor_item_t *item) {
  return cbor_ctrl_value(item) == CBOR_CTRL_TRUE;
}


/* Return a null-terminated allocated string copy of a (in)definite cbor string */
char *xAAL_cbor_string_dup(cbor_item_t *item, size_t *len) {
  if ( !item || !cbor_isa_string(item) ) {
    *len = 0;
    return NULL;
  }

  if ( cbor_string_is_definite(item) ) {
    *len = cbor_string_length(item);
    return strndup((const char *)cbor_string_handle(item), *len);

  } else {
    size_t i, chunk_len, chunks_nb = cbor_string_chunk_count(item);
    cbor_item_t **chunks = cbor_string_chunks_handle(item);
    char *str, *r;

    for (*len = 0, i = 0; i < chunks_nb; i++)
      *len += cbor_string_length(chunks[i]);

    r = str = malloc(*len+1);
    for (i = 0; i < chunks_nb; i++) {
      chunk_len = cbor_string_length(chunks[i]);
      memcpy(str, cbor_string_handle(chunks[i]), chunk_len);
      str += chunk_len;
    }
    str[*len] = '\0';
    return r;
  }
}


/* Return a new allocated bytestring copy of a (in)definite cbor bytestring */
unsigned char *xAAL_cbor_bytestring_dup(cbor_item_t *item, size_t *len) {
  unsigned char *r;

  if ( !item || !cbor_isa_bytestring(item) ) {
    *len = 0;
    return NULL;
  }

  if ( cbor_bytestring_is_definite(item) ) {
    *len = cbor_bytestring_length(item);
    r = (unsigned char *)malloc(*len);
    return memcpy(r, cbor_bytestring_handle(item), *len);

  } else {
    size_t i, chunk_len, chunks_nb = cbor_string_chunk_count(item);
    cbor_item_t **chunks = cbor_string_chunks_handle(item);
    unsigned char *bstr;

    for (*len = 0, i = 0; i < chunks_nb; i++)
      *len += cbor_string_length(chunks[i]);

    r = bstr = (unsigned char *)malloc(*len);
    for (i = 0; i < chunks_nb; i++) {
      chunk_len = cbor_string_length(chunks[i]);
      memcpy(bstr, cbor_bytestring_handle(chunks[i]), chunk_len);
      bstr += chunk_len;
    }
    return r;
  }
}

/* Jump cbor tag, if any */
cbor_item_t *xAAL_cbor_untag(cbor_item_t *item) {
  if  ( item && cbor_isa_tag(item) ) {
    cbor_item_t *stuff = cbor_tag_item(item);
    cbor_decref(&item);
    return stuff;
  } else
    return item;
}
