/* xAAL tts
 * (c) 2019 Christophe Lohr <christophe.lohr@imt-atlantique.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/wait.h>

#include <cbor.h>
#include <uuid/uuid.h>

#include <xaal.h>


#define ALIVE_PERIOD    60

/* setup device info */
xAAL_devinfo_t device = { .dev_type	= "tts.basic",
			.alivemax	= 2 * ALIVE_PERIOD,
			.vendor_id	= "IHSEV",
			.product_id	= "Text To Speech",
			.version	= "0.2",
			.hw_id		= NULL,
			.group_id	= NULL,
			.url		= "http://recherche.imt-atlantique.fr/xaal/documentation/",
			.schema		= "https://redmine.telecom-bretagne.eu/svn/xaal/schemas/branches/schemas-0.7/tts.basic",
			.info		= NULL,
			.unsupported_attributes = NULL,
			.unsupported_methods = NULL,
			.unsupported_notifications = NULL
		      };

xAAL_businfo_t bus;


/* Text-To-Speech
   Input: text, lang
   Description: call pico2wave and aplay commands
 */
void tts(const char *txt, const char *lang) {
  char wavefile[] = "/tmp/tts_XXXXXX.wav";
  int s;

  s = mkstemps(wavefile, 4);
  if ( s == -1 ) {
    fprintf(xAAL_error_log, "Creating temp wav file: %s", strerror(errno));
    return;
  }
  if ( close(s) == -1 ) {
    fprintf(xAAL_error_log, "Closing temp wav file: %s", strerror(errno));
    unlink(wavefile);
    return;
  }

  switch ( fork() ) {
  case -1:
    perror("fork");
    unlink(wavefile);
    return;
  case 0:
    execlp("pico2wave", "pico2wave", "-l", lang, "-w", wavefile, txt, NULL);
    fprintf(xAAL_error_log, "exec pico2wave: %s", strerror(errno));
    exit(EXIT_FAILURE);
  default:
    wait(&s);
    if (s != 0) {
      unlink(wavefile);
      return;
    }
  }

  switch ( fork() ) {
  case -1:
    perror("fork");
    unlink(wavefile);
    return;
  case 0:
    execlp("aplay", "aplay", "-q", wavefile, NULL);
    fprintf(xAAL_error_log, "exec aplay: %s", strerror(errno));
    exit(EXIT_FAILURE);
  default:
    wait(&s);
    if (s != 0) {
      unlink(wavefile);
      return;
    }
  }

  if ( unlink(wavefile) == -1 )
    fprintf(xAAL_error_log, "Deleting temp wav file: %s", strerror(errno));
}


void try_speech(cbor_item_t *cbody) {
  char *txt, *lang;
  size_t txt_len, lang_len;

  txt = xAAL_cbor_string_dup(xAAL_cbor_map_get(cbody, "txt"), &txt_len);
  if ( !txt )
    return;

  lang = xAAL_cbor_string_dup(xAAL_cbor_map_get(cbody, "lang"), &lang_len);
  if ( lang ) {
    tts(txt, lang);
    free(lang);
  }
  else
    tts(txt, "en-EN");
  free(txt);
}


void alive_sender(int sig) {
  if ( !xAAL_notify_alive(&bus, &device) )
    fprintf(xAAL_error_log, "Could not send spontaneous alive notification.\n");
  alarm(ALIVE_PERIOD);
}


/* Manage received xAAL message */
void manage_msg(const xAAL_businfo_t *bus, xAAL_devinfo_t *device) {
  cbor_item_t *cbody, *ctargets;
  char *dev_type, *action;
  xAAL_msg_type_t msg_type;
  uuid_t *source;

  /* Recive a message */
  if (!xAAL_read_bus(bus, &ctargets, &source, &dev_type, &msg_type, &action, &cbody))
    return;

  if (msg_type == xAAL_REQUEST) {
    if ( (strcmp(action, "is_alive") == 0)
	&& xAAL_is_aliveDevType_match(cbody, device->dev_type) ) {
      if ( !xAAL_notify_alive(bus, device) )
	fprintf(xAAL_error_log, "Could not reply to is_alive\n");

    } else if ( strcmp(action, "get_description") == 0 ) {
      if ( !xAAL_reply_get_description(bus, device, source) )
	fprintf(xAAL_error_log, "Could not reply to get_description\n");

    } else if ( strcmp(action, "speech") == 0 ) {
      try_speech(cbody);

    }
  }
  xAAL_free_msg(ctargets, source, dev_type, action, cbody);
}



/* main */
int main(int argc, char **argv) {
  int opt;
  char *addr=NULL, *port=NULL;
  int hops = -1;
  char *passphrase = NULL;
  bool arg_error = false;
  struct sigaction act_alarm;

  xAAL_error_log = stderr;

  uuid_clear(device.addr);

  /* Parse cmdline arguments */
  while ((opt = getopt(argc, argv, "a:p:h:s:u:")) != -1) {
    switch (opt) {
      case 'a':
	addr = optarg;
	break;
      case 'p':
	port = optarg;
	break;
      case 'h':
	hops = atoi(optarg);
	break;
      case 's':
	passphrase = optarg;
	break;
      case 'u':
	if ( uuid_parse(optarg, device.addr) == -1 ) {
	  fprintf(stderr, "Warning: invalid uuid '%s'\n", optarg);
	  uuid_clear(device.addr);
	} 
	break;
      default: /* '?' */
	arg_error = true;
    }
  }
  if (optind < argc) {
    fprintf(stderr, "Unknown argument %s\n", argv[optind]);
    arg_error = true;
  }
  if (!addr || !port || arg_error || !passphrase) {
    fprintf(stderr, "Usage: %s -a <addr> -p <port> [-h <hops>] -s <secret> [-u <uuid>]\n",
	    argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Join the xAAL bus */
  if ( !xAAL_join_bus(addr, port, hops, 1, &bus) )
    exit(EXIT_FAILURE);

  /* Setup security of the bus */
  bus.maxAge = 2*60; /*seconds*/;
  bus.key = xAAL_pass2key(passphrase);
  if (bus.key == NULL) {
    fprintf(stderr, "Could not compute key from passphrase\n");
    exit(EXIT_FAILURE);
  }

  /* Generate device address if needed */
  if ( uuid_is_null(device.addr) ) {
    char uuid[37];
    uuid_generate(device.addr);
    uuid_unparse(device.addr, uuid);
    printf("Device: %s\n", uuid);
  }
  xAAL_add_wanted_target(&device.addr, &bus);


  /* Manage alive notifications */
  act_alarm.sa_handler = alive_sender;
  act_alarm.sa_flags = ~SA_RESETHAND;
  sigemptyset(&act_alarm.sa_mask);
  sigaction(SIGALRM, &act_alarm, NULL);
  alarm(ALIVE_PERIOD);

  if ( !xAAL_notify_alive(&bus, &device) )
    fprintf(xAAL_error_log, "Could not send initial alive notification.\n");


  /* Main loop */
  for (;;)
    manage_msg(&bus, &device);
}
